﻿function getAppInfo(callback) {
    var obj = {};

    obj['appCodeName'] = navigator.appCodeName;
    obj['appName'] = navigator.appName;
    obj['appVersion'] = navigator.appVersion;
    obj['platform'] = navigator.platform;
    obj['userAgent'] = navigator.userAgent;
    obj['version'] = window['appVersion'] || 'версия неопределена';
    obj['theme'] = window['appTheme'];
    obj['location'] = location.href;

    if (typeof callback == 'function')
        callback(obj);
}

var gOldOnError = window.onerror;
// Переопределить прошлый обработчик события.
window.onerror = function myErrorHandler(errorMsg, url, lineNumber, point, typeError) {
    if (gOldOnError)
        // Вызвать прошлый обработчик события.
        return gOldOnError(errorMsg, url, lineNumber);

    writeErrorItem('Msg:' + errorMsg + ' Url:' + url + ' lineNumber:' + lineNumber + ' point:' + point + ' typeError:' + ((typeError && typeError.stack) ? typeError.stack : ''));

    // Просто запустить обработчик события по умолчанию.
    return false;
}
/*
 * запись ошибки в хранилище
 * @param text {string}
 */
function writeErrorItem(text) {
    var items = getReportErrors();
    var item = { msg: text, date: new Date() };
    items.push(item);
    sendErrorToServer(item);
    localStorage.setItem('errorreports', JSON.stringify(items));
}

/**
 * возвращается список ошибок
 */
function getReportErrors() {
    var items = localStorage.getItem('errorreports');
    if (!items) {
        localStorage.setItem('errorreports', JSON.stringify([]));
        items = [];
    } else {
        items = JSON.parse(items);
    }
    return items;
}
/**
 * передача сообщения на сервер
 * @param error {any} 
 */
function sendErrorToServer(error) {
    getAppInfo(function (obj) {

        for (var i in error)
            obj[i] = error[i];

        if (isSending() == true) {

            if (window['errorUrl']) {
                var xhr = new XMLHttpRequest();

                xhr.open("POST", errorUrl, true)
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

                xhr.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        if (this.status != 200) {
                            console.log('Ошибка отправки отчета. ' + JSON.stringify(error));
                        }
                    }
                };

                xhr.send(JSON.stringify(obj));
            } else {
                alert('Не указан адрес для отправки ошибок');
            }
        }
    });
}
/*
 * разрешить отправку отчета
 */
function isSending() {
    return navigator.onLine == true &&
            (location.href.indexOf('localhost') < 0 || window['localhostsending'] == true);
}