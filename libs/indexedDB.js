﻿var indexedDBObject = function (options) {
    this.key = options.key || 'id';
    this.dbName = options.dbName || 'localhost';
    this.tableName = options.tableName;
    this.autoIncrement = options.autoIncrement || false;

    this.db = null;

    // хранить данные в памяти или нет
    this.localCache = options.localCache || false;
    // выполение операции чтения и записи
    this.readWrite = options.readWrite || false;

    function getIndexedDB() {
        try{
            return indexedDB || webkitIndexedDB;
        } catch (exc) {

        }
    }

    /**
     * возвращается кэш
     */
    this.getTableCache = function () {
        var database = this['database'];

        if (!database)
            database = this['database'] = {};

        if (!database[this.dbName])
            database[this.dbName] = {};

        var databaseObject = database[this.dbName];
        if (!databaseObject[this.tableName])
            database[this.dbName][this.tableName] = [];
        return databaseObject[this.tableName];
    }

    /**
     * Получить объект для хранения колекций
     * @param {String} mode r-для чтения, w-для записи данных
     * @param {function} callback функция обратного вызова. Возвращает объект для хранения колекций
     */
    this.getStorageObject = function (mode, callback) {
        var me = this;
        obj = {
            r: 'readonly',
            w: 'readwrite'
        };
        mode = obj[mode];

        this.getDB(function (db) {
            me.checkStorageObject(db, function (db) {
                var tx = db.transaction([me.tableName], mode),
                    store = tx.objectStore(me.tableName);

                if (callback) {
                    callback(store, tx);
                }
            });
        });
    }


    /**
     * Проверить наличие таблицы, если таблицы нет, то создать ее
     * @param db {any} - база данных
     * @param callback {(any)=>any} - функция обратного вызова, возвращает базу данных
     */
    this.checkStorageObject = function (db, callback) {
        if (!db.objectStoreNames.contains(this.tableName)) {
            db.close();
            var me = this;
            this.openDB(function (db) {
                me.db = db;
                callback(db);
            }, parseInt(db.version) + 1);
        } else {
            this.db = db;
            callback(db);
        }
    }

    /*
     * Операция открытия базы
     * @param callback {(any)=>any} - функция обратного вызова, возвращает базу данных
     * @param version {number} - версия базы данных
     */
    this.openDB = function (callback, version) {
        var me = this,
            request;

        // можно продолжать работу

        if (version)
            request = getIndexedDB().open(me.dbName, version)
        else
            request = getIndexedDB().open(me.dbName);

        request.onerror = this.onError;
        request.onsuccess = function (event) {
            var db = event.target.result;
            if (db.objectStoreNames.contains(me.tableName)) {
                callback(db);
            } else {
                db.close();
                me.openDB(callback, db.version + 1);
            }
        };
        request.onupgradeneeded = function (e) {
            var db = (e.currentTarget).result,
                store;
            if (!db.objectStoreNames.contains(me.tableName)) {
                try{
                    store = db.createObjectStore(me.tableName, { keyPath: null, autoIncrement: me.autoIncrement });
                } catch (exc) {

                }
            }
        };
        request.onblocked = function (e) {

        }
    }

    /**
     * Получить открытую базу для дальнейших действий
     * @param {function} callback функция обратного вызова. Возвращает открытую базу
     */
    this.getDB = function (callback) {
        if (this.db) {
            callback(this.db);
        } else {
            var me = this;
            this.openDB(function (db) {
                me.db = db;
                callback(db);
            });
        }
    }

    /**
    Записать данные в кэш
    @param {items} данные для записи в кэш
    */
    this.writeToCache = function (items) {
        this.getTableCache();
        this['database'][this.dbName][this.tableName] = items;
    }

    /**
    Считать данные из кэша
    */
    this.readFromCache = function () {
        return this.getTableCache();
    }

    this.insertToCache = function (item, keyField) {
        var items = this.readFromCache();
        for (var i = 0; i <= items.length - 1; i++) {
            if (items[i][keyField] == item[keyField]) {

                for (var j in item) {
                    items[i][j] = item[j];
                }

                this.removeMapping(item, items);
                this.setMapping(item, items);

                return;
            }
        }
        var cd = this.getTableCache();
        cd.push(item);
        this.setMapping(item, items);
    }

    this.updateToCache = function (item, keyField) {
        this.insertToCache(item, keyField);
    }

    this.removeToCache = function (id, keyField) {
        var items = this.getTableCache();
        var tmp = [];

        for (var i = 0; i <= items.length - 1; i++) {
            if (id == items[i][keyField]) {
                continue;
            } else {
                tmp.push(items[i]);
                this.setMapping(items[i], tmp);
            }
        }
        this['database'][this.dbName][this.tableName] = tmp;
    }

    /*
     * Вернуть все записи из таблицы
     * @param store {Object} таблица
     * @param callback {(any)=>records} возвращает записи из таблицы
     */
    this.getAllStorageRecords = function (store, tx, callback) {
        var items = [];
        var tmpArray = this.readFromCache();
        if (this.localCache == true && tmpArray.length > 0) {
            callback(tmpArray);
        } else {
            var me = this;
            tx.oncomplete = function (evt) {
                if (me.localCache == true) {
                    var array = items;
                    for (var j = 0; j < array.length; j++)
                        me.setMapping(array[j], array);

                    me.writeToCache(array);
                }

                callback(items);
            };

            var cursorRequest = store.openCursor();

            cursorRequest.onerror = this.onError;

            cursorRequest.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    items.push(cursor.value);
                    cursor['continue']();
                }
            };
        }
    }

    /**
     * @private
     * Получить запись по заданному ключу
     * @param {String} id Ключ
     * @param {function} callback функция обратного вызова. Возвращает запись из базы
     */
    this.getItem = function (id, callback) {
        var tmpArray = this.readFromCache();
        if (this.localCache == true && tmpArray.length > 0) {
            for (var i = 0; i < tmpArray.length; i++) {
                var item = tmpArray[i];
                if (item[this.key] == id) {
                    callback(item);
                    break;
                }
            }
        } else {
            var me = this;
            this.getStorageObject('r', function (store) {
                var r = store.get(id);
                r.onerror = me.onError;
                r.onsuccess = function (event) {
                    var data = event.target.result ? event.target.result : null;
                    callback(data);
                }
            });
        }
    }

    /**
     * @private
     * Получить все записи
     * @param {function} callback функция обратного вызова. Возвращает массив записей
     */
    this.getAllRecords = function (callback) {
        var tmpArray = this.readFromCache();
        if (this.localCache == true && tmpArray.length > 0) {
            callback(tmpArray);
        } else {
            var me = this;
            this.getStorageObject('r', function (store, tx) {
                if (store.getAll) {
                    var r = store.getAll();
                    r.onerror = me.onError;
                    r.onsuccess = function (event) {
                        if (me.localCache == true) {
                            var array = event.target.result;
                            for (var j = 0; j < array.length; j++)
                                me.setMapping(array[j], array);

                            me.writeToCache(array);
                        }
                        callback(event.target.result);
                    }
                } else {
                    this.getAllStorageRecords(store, tx, callback);
                }
            });
        }
    }

    /**
     * @private
     * Добавить в коллекцию данные под заданным ключем
     * @param {String} key Ключ
     * @param {Object} value Значение
     * @param {function} callback функция обратного вызова
     */
    this.setItem = function (key, value, callback) {
        var me = this;
        this.getStorageObject('w', function (store) {
            var r = store.put(value, key);
            r.onerror = me.onError;
            r.onsuccess = function (event) {
                if (me.localCache == true &&
                    me.readWrite == true) {
                    me.insertToCache(value, me.key);
                }
                callback();
            }
        });
    }

    /**
     * Выводит описание ошибки в консоль
     * @param {Object} err Ошибка
     */
    this.onError = function (err) {
        console.log(err);
    }

    /**
     * Получить запись по ключу.
     * @param {String} id Ключ
     * @param {function} callback функция обратного вызова. Возвращает запись
     */
    this.readSingleRecord = function (id, callback) {
        var success = true;
        this.getItem(id, function (data) {
            if (data === null)
                success = false;

            callback(success, data)
        });
    }

    this.readSuccess = function (callback, records) {
        if (typeof callback == 'function') {
            callback({
                records: records,
                total: records.length,
                loaded: true
            });
        }
    }

    this.setMapping = function (record, records) {
        var data = record;
        if (!records['maps']) {
            records.isMaps = true;
            records['maps'] = {};
            records.getRecordByKey = function (link) {
                return records.maps[link];
            };

            // вовзращаются все записи по ключу
            records.getRecordsByFK = function (fieldName, link) {
                if (records.maps[fieldName])
                    return records.maps[fieldName][link] || [];
                else
                    return [];
            };

            // возвращается одна запись по ключу
            records.getRecordByFK = function (fieldName, link) {
                if (records.maps[fieldName]) {
                    var results = records.maps[fieldName][link] || [];
                    if (results.length > 0) {
                        return results[0];
                    }
                    return null;
                } else
                    return null;
            }
        }

        records['maps'][record[this.key]] = record;

        for (var i in data) {

            if (this.isObject(data[i]) == true) {
                var link = record[i][this.key];
                if (link) {
                    if (!records['maps'][i])
                        records['maps'][i] = {};

                    if (!records['maps'][i][link])
                        records['maps'][i][link] = [];
                    records['maps'][i][link].push(record);
                }
            }
        }
    }

    this.removeMapping = function (record, records) {
        if (records['maps']) {
            delete records['maps'][record[this.key]];
            for (var i in record) {

                if (this.isObject(record[i]) == true) {
                    var link = record[i][this.key];
                    if (link) {
                        if (records['maps'][i]) {
                            if (records['maps'][i][link]) {
                                var array = records['maps'][i][link];
                                if (array.length > 0) {
                                    var tmp = [];
                                    for (var k = 0; k < array.length > 0; k++) {
                                        var item = array[k];
                                        if (item[this.key] != record[this.key])
                                            tmp.push(item);
                                    }
                                    records['maps'][i][link] = tmp;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    this.isObject = function (item) {
        return (typeof item === "object" && !Array.isArray(item) && item !== null);
    }

    /**
     * Обновить записи
     * @param {Ext.data.Model[]} records Записи  
     * @param {Object[]} [dbids] Идентификаторы уже существующие.
     * @param {function} callback функция обратного вызова. Возвращает идентификаторы добавленных записей
     * @param {Ыекштп[]} [ids] Идентификаторы добавленных записей
     */
    this.updateRecords = function (records, callback, ids) {
        var me = this,
            record, id;

        if (!ids)
            ids = [];
        if (records.length == 0)
            callback(ids);
        else {
            record = records[0];
            records.shift();
            id = record[me.key];
            this.setRecord(record, function () {
                if (me.localCache == true &&
                    me.readWrite == true) {
                    me.updateToCache(record, me.key);
                }

                if (ids.filter(function (i) { return i == id; }.length == 0)) {
                    ids.push(id);
                }

                me.updateRecords(records, callback, ids);
            });
        }
    }

    /**
     * Удалить запись
     * @param {String} key Идентификатор записи
     * @param {function} callback функция обратного вызова. Возвращает идентификатор удаленной записи
     */
    this.removeItem = function (key, callback) {
        var me = this;

        this.getStorageObject('w', function (store) {
            var r = store['delete'](key);
            r.onerror = function () {
                callback(null);
            };
            r.onsuccess = function (event) {
                callback(key);
            }
        });
    }

    /**
     * Получить количество записей
     */
    this.getCount = function (callback) {
        this.getStorageObject('r', function (store) {
            var r = store.count();
            r.onerror = onError;
            r.onsuccess = function (event) {
                callback(event.target.result);
            }
        });
    }

    this.closeDB = function (callback) {
        var me = this;
        this.getDB(function (db) {
            db.close();
            me.db = undefined;
            callback();
        });
    }

    this.getSize = function () {
        return JSON.stringify(this['database'][this.dbName]).length;
    }

    /**
     * Доступна ли таблицы
     * @param callback {function} функция обратного вызова. Возвращает результат
     */
    this.exists = function (callback) {
        var me = this;
        this.getDB(function (db) {
            var result = false;
            for (var i = 0; i < db.objectStoreNames.length; i++) {
                var name = db.objectStoreNames[i];
                if (name == me.tableName && callback) {
                    result = true;
                    break;
                }
            }
            if (callback) {
                callback(result);
            }
        });
    }

    /**
     * Добавляет переданную запись в коллекцию.
     * @param {Ext.data.Model} record Запись
     * @param {function} callback функция обратного вызова
     */
    this.setRecord = function (record, callback) {
        var me = this,
            key = record[me.key];

        this.setItem(key, record, function () {
            if (typeof callback == 'function')
                callback();
        });
    }

    /**
     * Добавляет переданные записи в коллекцию.
     * @param {Ext.data.Model[]} records Запись
     * @param {function} callback функция обратного вызова. Возвращает массив добавленных идентификаторов
     * @param {String[]} [ids] массив уже добавленных идентификаторов
     */
    this.setRecords = function (records, callback, ids) {
        var me = this,
            record = records[0],
            id;
        if (!ids)
            ids = [];

        id = record[me.key];
        this.setRecord(record, function () {
            ids.push(id);
            records.shift();
            if (records.length > 0)
                me.setRecords(records, callback, ids);
            else {
                if (typeof callback == 'function')
                    callback(ids);
            }
        });
    }

    /**
     * @inheritdoc
     */
    this.read = function (id, callback) {
        var me = this;

        //read a single record
        if (id) {
            this.readSingleRecord(id, function (success, record) {
                if (success == true) {
                    me.readSuccess(callback, [record]);
                } else {
                    callback('Unable to load records');
                }
            });
        } else {
            this.getAllRecords(function (recs) {
                /*for (var i = 0; i < recs.length; i++) {
                    records.push(recs[i]);
                    me.setMapping(recs[i], records);
                }*/

                me.closeDB(function () {
                    me.readSuccess(callback, recs);
                });
            });
        }
    }

    /**
     * @inheritdoc
     */
    this.update = function (records, callback) {
        var me = this;

        this.updateRecords(records, function (ids) {
            me.closeDB(function () {
                if (typeof callback == 'function')
                    callback();
            });
        });

    }

    /**
     * Удалить записи
     * @param {Ext.data.Model[]} records Записи  
     * @param {function} callback функция обратного вызова. Возвращает идентификаторы добавленных записей
     * @param {Ыекштп[]} [ids] Идентификаторы удаленных записей
     */
    this.removeRecords = function (records, callback, ids) {
        var me = this,
            record, key;

        if (!ids)
            ids = [];

        if (records.length == 0) {
            callback(ids);
        } else {
            record = records[0];
            records.shift();
            key = record[me.key];
            this.removeItem(key, function (id) {
                if (id)
                    ids.push(id);

                if (me.localCache == true &&
                    me.readWrite == true) {
                    me.removeToCache(record[me.key], me.key);
                }

                me.removeRecords(records, callback, ids);
            });
        }
    }


    /**
     * Отчистить коллекцию
     *
     */
    this.clear = function (callback) {
        var me = this;
        this.writeToCache([]);
        this.getStorageObject('w', function (store) {
            var r = store.clear();
            r.onerror = onError;
            r.onsuccess = function (event) {
                callback();
            }
        });
    }

    return this;
}