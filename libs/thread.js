﻿var It;
(function (It) {
    var Core;
    (function (Core) {
        var Thread;
        (function (Thread) {
            /*
             * Приоритетность задачи
             */
            (function (Priority) {
                /*
                 * Низкий
                 */
                Priority[Priority["LOW"] = 0] = "LOW";
                /*
                 * Выше низкого
                 */
                Priority[Priority["HIGHLOW"] = 1] = "HIGHLOW";
                /*
                 * Средний
                 */
                Priority[Priority["MEDIUM"] = 2] = "MEDIUM";
                /*
                 * Выше среднего
                 */
                Priority[Priority["HIGHMEDIUM"] = 3] = "HIGHMEDIUM";
                /*
                 * Высокий
                 */
                Priority[Priority["HIGH"] = 4] = "HIGH";
                /*
                 * Самый высокий
                 */
                Priority[Priority["VERYHIGH"] = 5] = "VERYHIGH";
            })(Thread.Priority || (Thread.Priority = {}));
            var Priority = Thread.Priority;
        })(Thread = Core.Thread || (Core.Thread = {}));
    })(Core = It.Core || (It.Core = {}));
})(It || (It = {}));
var It;
(function (It) {
    var Core;
    (function (Core) {
        var Thread;
        (function (Thread) {
            /*
             * Класс для обработки коллекций задач
             */
            var Tasks = (function () {
                /*
                 * Конструктор
                 * @param obj {any} текущий объект
                 */
                function Tasks(obj) {
                    this.currentObject = obj;
                    this.tasks = [];
                    this.taskIteration = 0;
                }
                /*
                 * Добавить задачу
                 * @param func {(currentObject: any,callback: () => void) => void} функция для обработки
                 * @param priority { TaskPriority } приоритетность задачи. По умолчанию MEDIUM
                 * @param options {any} дополнительный параметр
                 */
                Tasks.prototype.Add = function (func, priority, options) {
                    if (priority == null || priority == undefined)
                        priority = Thread.Priority.MEDIUM;
                    this.tasks.push({ func: func, priority: priority, options: options });
                };
                /*
                 * Очистка задач
                 */
                Tasks.prototype.Clear = function () {
                    this.tasks = [];
                    this.taskCount = 0;
                    this.taskIteration = 0;
                };
                /*
                 * проверка на завершение задач
                 * @param callback {(boolean) => void} функция обратного вызова
                 */
                Tasks.prototype.IsTaskCollectionFinish = function (callback) {
                    if (callback) {
                        this.taskIteration++;
                        if (this.taskCount == this.taskIteration) {
                            callback(true);
                            this.onFinish();
                        }
                        else {
                            callback(false);
                        }
                    }
                };
                Tasks.prototype.taskCallBack = function (tasks, callback) {
                    var _this = this;
                    var success = [];
                    if (tasks.length == 0)
                        callback();
                    for (var i = 0; i < tasks.length; i++) {
                        var task = tasks[i];
                        task.func(this.currentObject, function () {
                            success.push(true);
                            if (success.length == tasks.length)
                                callback();
                            _this.IsTaskCollectionFinish(function (result) {
                            });
                        }, task.options);
                    }
                };
                /*
                 * Запуск задач
                 * @param callback {() => void} функция обратного вызова при завершении
                    @param syncly {bool} выполнение задач синхронно
                 */
                Tasks.prototype.Run = function (callback, syncly) {
                    var _this = this;
                    this.taskCount = this.tasks.length;
                    if (this.taskCount == 0) {
                        if (callback)
                            callback();
                        return;
                    }
                    this.onFinish = function () {
                        if (callback)
                            callback();
                    };
                    if (syncly == true) {
                        this._runTaskAfterTask(this.tasks);
                        return;
                    }
                    var LOW = this.tasks.filter(function (fn) { return fn.priority == 0; });
                    var HIGHLOW = this.tasks.filter(function (fn) { return fn.priority == 1; });
                    var MEDIUM = this.tasks.filter(function (fn) { return fn.priority == 2; });
                    var HIGHMEDIUM = this.tasks.filter(function (fn) { return fn.priority == 3; });
                    var HIGH = this.tasks.filter(function (fn) { return fn.priority == 4; });
                    var VERYHIGH = this.tasks.filter(function (fn) { return fn.priority == 5; });
                    this.taskCallBack(VERYHIGH, function () {
                        _this.taskCallBack(HIGH, function () {
                            _this.taskCallBack(HIGHMEDIUM, function () {
                                _this.taskCallBack(MEDIUM, function () {
                                    _this.taskCallBack(HIGHLOW, function () {
                                        _this.taskCallBack(LOW, function () {
                                        });
                                    });
                                });
                            });
                        });
                    });
                };
                Tasks.prototype._runTaskAfterTask = function (tasks) {
                    var _this = this;
                    var task = tasks.shift();
                    this.taskCallBack([task], function () {
                        if (tasks.length != 0)
                            _this._runTaskAfterTask(tasks);
                    });
                };
                Tasks.prototype._run = function (tasks, callback) {
                    var _this = this;
                    for (var i = 0; i < tasks.length; i++) {
                        var task = tasks[i];
                        task.func(this.currentObject, function () {
                            _this.IsTaskCollectionFinish(function (result) {
                                if (result === true) {
                                    if (callback) {
                                        callback();
                                    }
                                }
                            });
                        }, task.options);
                    }
                };
                return Tasks;
            }());
            Thread.Tasks = Tasks;
        })(Thread = Core.Thread || (Core.Thread = {}));
    })(Core = It.Core || (It.Core = {}));
})(It || (It = {}));