﻿var yandexCoder = function () {

    indexedDBTable = null;

    function getIndexedDbTable() {
        if (!indexedDBTable) {
            indexedDBTable = new indexedDBObject({
                tableName: 'geocode',
                localCache: true,
                readWrite: true
            });
        }

        return indexedDBTable;
    }

    /**
    * добавляем данные в локальное хранилище
    * @param address {string} адрес 
    * @param latitude {number} долгота
    * @param longitude {number} широта
    * @param callback {()=>void} функция обратного вызова
    */
    function addInGeocoder(address, latitude, longitude, callback) {
        var hash = md5(address);

        var table = getIndexedDbTable();
        table.setRecord({
            id: hash,
            address: address,
            latitude: latitude,
            longitude: longitude,
            coordsHash: {
                id: md5(JSON.stringify([latitude, longitude]))
            }
        }, callback);
    }

    /*
    * возвращается объект с координатами
    * @param address {string} адрес
    * @param callback {(any)=>void} функция обратного вызова
    */
    function getGeocoder(address, callback) {
        var hash = md5(address);
        var table = getIndexedDbTable();
        table.read(hash, callback);
    }

    /*
    * возвращение
    * @param data {any} данные
    * @param callback {(data)=>void} функция обратного вызова
    */
    function returnCallback(data, callback) {
        if (callback && typeof callback === 'function') {
            callback(data);
            return true;
        }

        return false;
    }

    /*
    * возвраащется пустой объект для возврата данных
    */
    function getEmptyResult() {
        var result = {
            success: false,
            msg: '',
            data: {}
        };

        return result;
    }

    /*
    * возвращается объект для ошибки
    * @param msg {string} текст ошибки
    */
    function getErrorResult(msg) {
        var result = getEmptyResult();

        result.success = false;
        result.data = null;
        result.msg = msg;

        return result;
    }

    /*
    * возвращается объект для рабочих данных
    * @param data {any} данные
    */
    function getWorkResult(data) {
        var result = getEmptyResult();
        result.success = true;
        result.data = data;
        return result;
    }

    /*
    * на основе адреса определяем координаты точки
    * @param address {string} адрес
    * @param callback {(result)=>void} результат обработки
    * msg - текст сообщения
    * success - статус выполнения
    * data - данные
    */
    this.getCoord = function (address, callback) {

        if (!address)
            return returnCallback(getErrorResult('Требуется указать адрес для поиска геоданных'), callback);

        getGeocoder(address, function (item) {
            if (item && item.total == 1) {
                var coordinates = [item.records[0].longitude, item.records[0].latitude];
                returnCallback(
                    coordinates[0] != 0 && coordinates[1] != 0
                    ? getWorkResult({
                        N_Latitude: coordinates[1],
                        N_Longitude: coordinates[0]
                    })
                    : getErrorResult('Вы передали неверный адрес: "' + address + '"')
                    , callback);
            } else {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', 'https://geocode-maps.yandex.ru/1.x/?geocode=' + address + '&format=json', true);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            var rec = JSON.parse(xhr.responseText);
                            var res = rec.response;

                            if (res.GeoObjectCollection.featureMember.length > 0) {
                                var pos = res.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
                                var coordinates = pos.split(' ');
                                addInGeocoder(address, coordinates[1], coordinates[0], function () {
                                    returnCallback(getWorkResult({
                                        N_Latitude: coordinates[1],
                                        N_Longitude: coordinates[0],
                                        fromYandex: true
                                    }), callback);
                                });
                            }
                            else {
                                addInGeocoder(address, 0, 0, function () {
                                    returnCallback(getErrorResult('Вы передали неверный адрес: "' + address + '"'), callback);
                                });
                            }
                        }
                    }
                };
                xhr.send(null);
            }
        });
    }

    /*
    * возвращается близлежащий адрес точки с координатами coords
    * @param coords {number[]} координаты точки
    * @param callback {(address)=>void} функция обратного вызова
    */
    this.getAddress = function (coords, callback) {
        if (!coords || (coords && !Array.isArray(coords)))
            return this.returnCallback(this.getErrorResult('Требуется указать координаты для поиска геоданных'), callback);

        var table = this.getIndexedDbTable();
        table.read(null, function (data) {
            if (data.total > 0) {
                var records = data.records;
                var id = md5(JSON.stringify(coords));
                var record = records.getRecordByFK('coordsHash', id);
                if (record) {
                    return returnCallback(getWorkResult({
                        name: record.address
                    }), callback);
                }
            }

            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'https://geocode-maps.yandex.ru/1.x/?geocode=' + coords[0] + ',' + coords[1] + '&format=json', true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        var rec = JSON.parse(xhr.responseText);
                        var res = rec.response;
                        try {
                            var featureMembers = res.GeoObjectCollection.featureMember;
                            if (featureMembers.length > 0) {
                                var name = featureMembers[0].GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.AddressLine;

                                addInGeocoder(name, coords[0], coords[1], function () {
                                    returnCallback(getWorkResult({
                                        name: name,
                                        fromYandex: true
                                    }), callback);
                                });
                            }
                            if (callback) {
                                return returnCallback(this.getErrorResult('Не удалось определить адрес точки.'));
                            }
                        } catch (exc) {
                            return returnCallback(this.getErrorResult('Поиск адреса по координатам. Ошибка при обработке данных от yandex.'));
                        }
                    }
                }
            };
            xhr.send(null);
        });
    }

    return this;
}