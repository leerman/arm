﻿Ext.define('Ext.overrides.grid.filters.filter.Boolean', {
    extend: 'Ext.grid.filters.filter.Boolean',

    yesText: 'Да',
    noText: 'Нет'
});