﻿Ext.define('Ext.overrides.form.field.ComboBox', {
    override: 'Ext.form.field.ComboBox',

    setValue: function (value) {
        var _value = value;
        if (value && typeof value == 'object') {
            value = value[this.valueField];
        }
        this.callParent(arguments);

        if (_value && typeof _value == 'object') {
            this.setRawValue(_value[this.displayField]);
        }
    }
});