﻿Ext.define('Ext.overrides.form.field.Text', {
    override: 'Ext.form.field.Text',

    tip: null,

    onRender: function () {
        this.callParent(arguments);
        if (this.tooltip) {
            this.tip = Ext.create('Ext.tip.ToolTip', {
                target: this,
                html: this.tooltip
            });
        }
    },

    destroy: function () {
        if (this.tip)
            this.tip.destroy();
        this.callParent(arguments);
    }
});