﻿Ext.define('Ext.overrides.form.field.Date', {
    override: 'Ext.form.field.Date',
    formatText: 'Дата должна быть в формате: {0}'
});