﻿Ext.define('Ext.overrides.Component', {
    override: 'Ext.Component',

    mask: function (msg, msgCls) {
        if (this.rendered == false)
            return;
        this.callParent(arguments);
    },

    unmask: function () {
        if (this.rendered == false)
            return;
        this.callParent(arguments);
    }
});