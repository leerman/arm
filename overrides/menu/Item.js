﻿Ext.define('Ext.overrides.menu.Item', {
    override: 'Ext.menu.Item',

    actionId: null,

    constructor: function (cfg) {
        this.hackByMainMenu(cfg);
        this.callParent(arguments);
    },

    setAction: function (val) {
        this.actionId = val;
    },

    getAction: function () {
        return this.actionId;
    },

    cls: '',

    setCls: function (value) {
        this.cls = value;
    },

    getCls: function () {
        return this.cls;
    },

    privates: {
        /*
         * дополнителная возможность для главного меню.
         * Позволяет вывести слово настройка для определенного пункта
         */
        hackByMainMenu: function (cfg) {
            if (cfg && cfg.actionId == "settings") {
                if (!cfg.text)
                    cfg.text = 'Настройки';
            }
        }
    }
});
