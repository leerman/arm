# kazan-local/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    kazan-local/sass/etc
    kazan-local/sass/src
    kazan-local/sass/var
