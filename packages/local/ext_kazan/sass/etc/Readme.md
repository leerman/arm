# kazan-local/sass/etc

This folder contains miscellaneous SASS files. Unlike `"kazan-local/sass/etc"`, these files
need to be used explicitly.
