﻿/*
 * акт инструментальной проверки
 */
Ext.define('ARM.overrides.view.task.card.DT_CONTROL_CHECK', {
    override: 'ARM.view.task.card.DT_CONTROL_CHECK',
    defaultListenerScope: true,
    
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            ui: 'top',
            items: ['->', {
                ui: 'white-blue',
                iconCls: 'fa fa-print',
                handler: 'onReportShow'
            }]
        }
    ],
    tpl: [
        '<div class="document-field">',
        '<div class="item"><b>Дата формирования:</b><span>{[this.getDate(values, "D_Date")]}</span></div>',
        '<div class="item"><b>Номер акта:</b><span>{C_Doc_Number}</span></div>',
        '<div class="item"><b>Лицевой счет:</b><span>{F_Doc_Details___C_Subscr}</span></div>',
        '<div class="item"><b>Потребитель:</b><span>{F_Doc_Details___C_Owner}</span></div>',
        '<div class="item"><b>Адрес объекта:</b><span>{F_Doc_Details___C_Address}</span></div>',
        '<div class="item"><b>Квартира:</b><span>{F_Doc_Details___N_Premise_Number}</span></div>',

        '<div class="item"><b>Номер телефона:</b><span>{C_Phone}</span></div>',

        '<div class="item"><b>Тип ПУ:</b><span>{F_Doc_Details___C_Device_Types}</span></div>',
        '<div class="item"><b>Номер ПУ:</b><span>{F_Doc_Details___C_Number}</span></div>',
        '<div class="item"><b>Расчетный коэффициент ПУ:</b><span>{F_Doc_Details___N_Rate}</span></div>',

        '<tpl for="seals">',
            '<div class="item"><b>Тип пломбы №{[xindex]}:</b><span>{[this.withEmpty(values, "S_Stamp_Types_Prev.C_Name")]} | {[this.withEmpty(values, "F_Stamp_Types.C_Name")]}</span></div>',
            '<div class="item"><b>Номер пломбы №{[xindex]}:</b><span>{[this.withEmpty(values, "S_Number_Prev")]} | {[this.withEmpty(values, "C_Number")]}</span></div>',
            '<div class="item"><b>Место установки пломбы №{[xindex]}:</b><span>{[this.withEmpty(values, "S_Places_Prev.C_Name")]} | {[this.withEmpty(values, "F_Places.C_Name")]}</span></div>',
        '</tpl>',

        '<div class="item"><b>Замечание:</b><span>{F_Doc_Details___F_Violations___C_Name}</span></div>',
        '<div class="item"><b>Срок устранения:</b><span>{[this.getDate(values, "F_Doc_Details___D_Date_Elimination")]}</span></div>',
        '<div class="item"><b>Описание замечания:</b><span>{F_Doc_Details___C_Violation}</span></div>',

        '<div class="item"><b>Погрешность:</b><span>{N_Error}</span></div>',

        '<div class="item"><b>Ia, А:</b><span>{N_Ia}</span></div>',
        '<div class="item"><b>Ib, А:</b><span>{N_Ib}</span></div>',
        '<div class="item"><b>Ic, А:</b><span>{N_Ic}</span></div>',

        '<div class="item"><b>Ua, В:</b><span>{N_Ua}</span></div>',
        '<div class="item"><b>Ub, В:</b><span>{N_Ub}</span></div>',
        '<div class="item"><b>Uc, В:</b><span>{N_Uc}</span></div>',

        '<div class="item"><b>А, имп./кВт*ч:</b><span>{N_Gear_Ratio}</span></div>',
        '<div class="item"><b>n, шт:</b><span>{N_Turn}</span></div>',
        '<div class="item"><b>t, c:</b><span>{N_Turn_Time}</span></div>',

        '<div class="item"><b>Соответствует / Не соответствует:</b><span>{[this.yesNo(values, "B_Validated")]}</span></div>',

        '<div class="item"><b>Примечание:</b><span>{C_Note}</span></div>',
        '<tpl if="C_Signature != \'\'"><div class="item signature"><b>Подпись:</b><span><img src="{C_Signature}" style="width:80%" /></span></div></tpl>',

        '</div>',
        {
            getDate: function (values, field) {
                return Ext.Date.format(values[field], 'd.m.Y H:i:s');
            },
            withEmpty: function (values, field) {
                var array = field.split('.');
                var result = values;
                if (array.length > 0) {
                    for (var j = 0; j < array.length; j++) {
                        var name = array[j];
                        if (result[name])
                            result = result[name];
                        else {
                            result = null;
                            break;
                        }
                    }
                }
                return result || 'не указан';
            },
            yesNo: function (values, field) {
                if (values[field]) return 'Да';
                else return 'Нет';
            }
        }
    ],
    privates: {
        /*
         * Показывает печатный акт
         */
        onReportShow: function () {
            var me = this;
            var window = {
                title: 'Печатный акт',
                height: '80%',
                width: '80%',
                closeToolText: 'Закрыть',
                layout: 'fit',
                scrollable: true,
                html: me.getData().C_Act_Html,
                tools: [{
                    glyph: 'f02f@FontAwesome',
                    tooltip: 'Распечатать акт',
                    handler: me.onActPrint.bind(me)
                },
                {
                    glyph: 'f0c7@FontAwesome',
                    tooltip: 'Сохранить акт',
                    callback: me.onActSave.bind(me)
                }]
            };
            Ext.create('Ext.window.Window', window).show();
        },
        /*
         * Сохраняет печатный акт
         */
        onActSave: function (panel, tool, event) {
            var me = this;
            var head = '<head><meta charset="UTF-8"></head>';
            var blob = new Blob([head + me.getData().C_Act_Html], { type: "text/html" });
            var blob_url = URL.createObjectURL(blob);
            var a = document.createElement('a');
            a.href = blob_url;
            a.download = me.getData().C_Doc_Number;
            a.href = blob_url;
            a.click();
            URL.revokeObjectURL(blob_url);
        },
        /*
         * Печатает печатный акт
         */
        onActPrint: function (panel, tool, event) {
            var blob = new Blob([this.getData().C_Act_Html], { type: "text/html" });
            var blob_url = URL.createObjectURL(blob);
            var iframe = document.createElement('iframe');
            iframe.src = blob_url;
            iframe.style = 'display:none;'
            document.body.appendChild(iframe);
            iframe.contentWindow.print();
            URL.revokeObjectURL(blob_url);
        },
    }
});