﻿/*
 * компонент для отображения главного блока фильтраций
 */
Ext.define('ARM.overrides.view.main.MainFilter', {
    override: 'ARM.view.main.MainFilter',
    //emptyText у Division
    DIVISION_LABEL: 'Филиал',
    //emptyText у SubDivision
    SUBDIVISION_LABEL: 'РЭС',

    //label для диспечера
    MANAGER_LABEL:'Старший контроллер'
});