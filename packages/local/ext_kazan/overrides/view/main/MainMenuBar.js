﻿/*
 * компонент для отображения главного блока фильтраций
 */
Ext.define('ARM.overrides.view.main.MainMenuBar', {
    override: 'ARM.view.main.MainMenuBar',
    
    rawMenuItems: [
        {
            "text": "Главная",
            "actionId": "home",
            "Roles": "Chief,Manager,User",
            "handler": "onAction"
        },
        {
            "text": "Назначение заданий",
            "actionId": "page/appoint",
            "Roles": "Manager",
            "handler": "onAction",
            "menu": {
                "cls": 'menu-without-icons',
                "items": [
                    {
                        "text": "Маршруты",
                        "actionId": "documents",
                        "Roles": "Manager",
                        "handler": "onAction"
                    },
                    {
                        "text": "Создание маршрутов",
                        "actionId": "page/devices",
                        "Roles": "Manager",
                        "handler": "onAction"
                    },
                    {
                        "text": "Потребители",
                        "actionId": "page/subscrs",
                        "Roles": "Manager",
                        "handler": "onAction"
                    }
                ]
            }
        },
        {
            "text": "История маршрутов",
            "actionId": "history",
            "Roles": "Chief,Manager,User",
            "handler": "onAction"
        },
        {
            "text": "Задачи",
            "actionId": "page/task",
            "Roles": "Chief,Manager,User",
            "handler": "onAction"
        },
        {
            "text": "Отчеты",
            "actionId": "report",
            "Roles": "Chief,Manager",
            "handler": "onAction",
            "children": [
                {
                    "text": "Обходной лист",
                    "actionId": "http://10.3.245.226/Reports_MSSQLSERVER226/Pages/Report.aspx?ItemPath=%2fMobileService%2fОбходной+лист"
                }
            ]
        },
        {
            "text": "Администрирование",
            "actionId": "admin",
            "Roles": "Admin",
            "handler": "onAction"
        },
        {
            "text": "Статистика",
            "actionId": "stat",
            "Roles": "Chief,Manager",
            "handler": "onAction"
        },
        {
            "text": "Настройки",
            "actionId": "settings",
            "cls": "setting",
            "iconCls": "x-fa fa-cog",
            "handler": "onAction",
            "Roles": "Admin"
        },
        {
            "text": null,
            "actionId": null,
            "xtype": "menuseparator",
            "flex": "1"
        },
        {
            "text": null,
            "actionId": null,
            "cls": "user-profile",
            "bind": {
                "text": "{userName}"
            },
            "iconCls": "x-fa fa-user",
            "menu": {
                "cls": "main-context-menu",
                "minWidth": 200,
                "layout": "vbox",
                "items": [
                    {
                        "text": "Информация о метках",
                        "actionId": "page/legend",
                        "handler": "onAction"
                    },
                    {
                        "text": "История изменений",
                        "actionId": "view/version",
                        "handler": "onAction"
                    },
                    {
                        "text": "Написать сообщение",
                        "handler": "onChat"
                    },
                    {
                        "xtype": "menuseparator"
                    },
                    {
                        "text": "Выход",
                        "iconCls": "x-fa fa-sign-out",
                        "handler": "onExit"
                    }
                ]
            }
        }
    ]
});