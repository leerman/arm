﻿Ext.define('ARM.overrides.view.stat.MainChart', {
    override: 'ARM.view.stat.MainChart',

    DT_CONTROL_METER_READINGS_TEXT: 'Снято показаний',
    DT_CONTROL_CHECK_TEXT: 'Инструментальные проверки',
    DT_FAILURE: 'Отказ от выполнения',

    privates: {
        /*
         * возвращаются выходные документы
         */
        getUserDocuments: function () {
            return [{
                type: 'line',
                xField: 'Day',
                yField: 'Total_Fact_DT_CONTROL_METER_READINGS',
                style: {
                    lineWidth: 1
                },
                marker: {
                    radius: 3,
                    lineWidth: 1
                },
                label: {
                    field: 'Total_Fact_DT_CONTROL_METER_READINGS',
                    display: 'over',
                    color: 'transparent'
                },
                highlight: {
                    fillStyle: '#f25858',
                    radius: 3,
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tooltip: {
                    trackMouse: true,
                    showDelay: 0,
                    dismissDelay: 0,
                    hideDelay: 0,
                    renderer: 'onSeriesTooltipRender'
                },
                title: this.DT_CONTROL_METER_READINGS_TEXT
            },
           {
               type: 'line',
               xField: 'Day',
               yField: 'Total_Fact_DT_CONTROL_CHECK',
               style: {
                   lineWidth: 1
               },
               marker: {
                   radius: 3,
                   lineWidth: 1
               },
               label: {
                   field: 'Total_Fact_DT_CONTROL_CHECK',
                   display: 'over',
                   color: 'transparent'
               },
               highlight: {
                   fillStyle: '#2cb316',
                   radius: 3,
                   lineWidth: 1,
                   strokeStyle: '#fff'
               },
               tooltip: {
                   trackMouse: true,
                   showDelay: 0,
                   dismissDelay: 0,
                   hideDelay: 0,
                   renderer: 'onSeriesTooltipRender'
               },
               title: this.DT_CONTROL_CHECK_TEXT
           }, {
               type: 'line',
               xField: 'Day',
               yField: 'Total_Fact_DT_FAILURE',
               style: {
                   lineWidth: 1
               },
               marker: {
                   radius: 3,
                   lineWidth: 1
               },
               label: {
                   field: 'Total_Fact_DT_FAILURE',
                   display: 'over',
                   color: 'transparent'
               },
               highlight: {
                   fillStyle: '#596ad7',
                   radius: 3,
                   lineWidth: 1,
                   strokeStyle: '#fff'
               },
               tooltip: {
                   trackMouse: true,
                   showDelay: 0,
                   dismissDelay: 0,
                   hideDelay: 0,
                   renderer: 'onSeriesTooltipRender'
               },
               title: this.DT_FAILURE
           }];
        }
    }
});