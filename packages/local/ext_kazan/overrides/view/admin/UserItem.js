﻿/*
 * Администрирование (карточка пользователя)
 */
Ext.define('ARM.overrides.view.admin.UserItem', {
    override: 'ARM.view.admin.UserItem',
    //label у Division
    DIVISION_LABEL: 'Филиал',
    //label у SubDivision
    SUBDIVISION_LABEL: 'РЭС',

    //label для диспечера
    MANAGER_LABEL:'Старший контроллер',
    
    MANAGER_TOOLTIP:'ФИО старшего контроллера, к которому будет привязан обходчик',
    
    //всплывающая подсказка для цвета обходчика
    COLOR_TOOLTIP: 'Цвет метки обходчика на карте. Для администратора и старшего контроллера не обязателен',
});