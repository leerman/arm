﻿/*
 * компонент для отображения главного блока фильтраций
 */
Ext.define('ARM.overrides.view.main.MainMenuBar', {
    override: 'ARM.view.main.MainMenuBar',
    
    rawMenuItems: [
        {
            "text": "Главная",
            "actionId": "home",
            "Roles": "Chief,Manager,User",
            "handler": "onAction"
        },
        {
            "text": "Назначение заданий",
            "actionId": "page/appoint",
            "Roles": "Manager",
            "handler": "onAction",
            "menu": {
                "cls": 'menu-without-icons',
                "items": [
                    {
                        "text": "Маршруты",
                        "actionId": "documents",
                        "Roles": "Manager",
                        "handler": "onAction"
                    }
                ]
            }
        },
        {
            "text": "История маршрутов",
            "actionId": "history",
            "Roles": "Chief,Manager,User",
            "handler": "onAction"
        },
        {
            "text": "Задачи",
            "actionId": "page/task",
            "Roles": "Chief,Manager,User",
            "handler": "onAction"
        },
        {
            "text": "Отчеты",
            "actionId": "report",
            "Roles": "Chief,Manager",
            "handler": "onAction",
            "children": [
                {
                    "text": "Мониторинг выполнения работ",
                    "actionId": "http://192.168.90.13/Reports/Pages/Report.aspx?ItemPath=%2fMobileService%2f%d0%9c%d0%be%d0%bd%d0%b8%d1%82%d0%be%d1%80%d0%b8%d0%bd%d0%b3+%d0%b2%d1%8b%d0%bf%d0%be%d0%bb%d0%bd%d0%b5%d0%bd%d0%b8%d1%8f+%d1%80%d0%b0%d0%b1%d0%be%d1%82"
                },
                {
                    "text": "Сводные данные по объемам выполенных работ",
                    "actionId": "http://192.168.90.13/Reports/Pages/Report.aspx?ItemPath=%2fMobileService%2f%d0%a1%d0%b2%d0%be%d0%b4%d0%bd%d1%8b%d0%b5+%d0%b4%d0%b0%d0%bd%d0%bd%d1%8b%d0%b5+%d0%bf%d0%be+%d0%be%d0%b1%d1%8a%d0%b5%d0%bc%d0%b0%d0%bc+%d0%b2%d1%8b%d0%bf%d0%be%d0%bb%d0%b5%d0%bd%d0%bd%d1%8b%d1%85+%d1%80%d0%b0%d0%b1%d0%be%d1%82"
                }
            ]
        },
        {
            "text": "Администрирование",
            "actionId": "admin",
            "Roles": "Admin",
            "handler": "onAction"
        },
        {
            "text": "Статистика",
            "actionId": "stat",
            "Roles": "Chief,Manager",
            "handler": "onAction"
        },
        {
            "Caption": "Служебное",
            "ActionId": "service",
            "Map": {
                "Roles": "Manager,User",
                "handler": "onAction"
            }
        },
        {
            "text": "Настройки",
            "actionId": "settings",
            "cls": "setting",
            "iconCls": "x-fa fa-cog",
            "handler": "onAction",
            "Roles": "Admin"
        },
        {
            "xtype": "menuseparator",
            "flex": 1
        },
        {
            "cls": "user-profile",
            "bind": {
                "text": "{userName}"
            },
            "iconCls": "x-fa fa-user",
            "menu": {
                "cls": "main-context-menu",
                "minWidth": 200,
                "layout": "vbox",
                "items": [
                    {
                        "text": "Информация о метках",
                        "actionId": "page/legend",
                        "handler": "onAction"
                    },
                    {
                        "text": "История изменений",
                        "actionId": "view/version",
                        "handler": "onAction"
                    },
                    {
                        "text": "Написать сообщение",
                        "handler": "onChat"
                    },
                    {
                        "xtype": "menuseparator"
                    },
                    {
                        "text": "Выход",
                        "iconCls": "x-fa fa-sign-out",
                        "handler": "onExit"
                    }
                ]
            }
        }
    ]
});