﻿/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'ARM.Application',

    name: 'ARM',

    requires: [
        // This will automatically load all classes in the ARM namespace
        // so that application classes do not need to require each other.
        'ARM.*',
        'Core.Meta',
        'Core.AuthProvider',
        'Core.InterceptStatusCode',
        'Core.data.proxy.Direct',
        'Core.DynamicMask',
        'Core.DoubleClickByMask',
        'Core.DataWorker',
        'Core.Utilits',
        'ARM.Socket',
        'Ext.data.identifier.*',
        'ARM.view.legend.Legend',
        'Ext.form.field.File'
    ]
});

Ext.getCurrentApp = function () {
    if (ARM.app)
        return ARM.app;

    Ext.Error.raise('Приложение не найдено');
}

Ext.getConf = function (value) {
    return Ext.getCurrentApp().getConfiguration().get(value);
}

Ext.getGlobalParams = function (value) {
    return Ext.getCurrentApp().getGlobalParams().getData()[value];
}

Ext.conf = function () {
    return Ext.getCurrentApp().getConfiguration();
}