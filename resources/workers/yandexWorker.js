﻿importScripts('../../libs/indexedDB.js', '../../libs/yandexCoder.js', '../../libs/md5.js', '../../libs/nimble.min.js', '../../libs/thread.js');

/*
 * 
        var dataWorker = Ext.create('Lib.core.DataWorker', { path: 'resources/workers/yandexWorker.js' });


        dataWorker.onTask({ type: 'geocoder', data: geoData }, function (results, options) {
            if (results == null) {
                // значит это только итерация
                return;
            } else {
                console.info('finish');
            }
        });

        где geoData - это объект
 */

onmessage = function (e) {
    switch (e.data.type) {
        case 'geocoder':
            onGeocoderAction(e.data.data, e.data.tid);
            break;
    }
}

function onGeocoderAction(data, tid) {
    var coder = new yandexCoder();
    var tasks = new It.Core.Thread.Tasks(this);
    var idx = 0;
    for (var i in data) {
        tasks.Add(function (sender, callback, options) {
            coder.getCoord(data[options], function (result) {
                idx++;
                if (result.success == true) {
                    data[options] = [result.data.N_Latitude, result.data.N_Longitude];
                } else {
                    delete data[options];
                }
                if (result.data && result.data.fromYandex == true)
                    postMessage([null, tid, idx]);
                callback();
            });
        }, null, i);
    }
    tasks.Run(function () {
        postMessage([data, tid]);
    });
}