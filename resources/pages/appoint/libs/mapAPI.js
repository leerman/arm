var mapAPI = function (options, ready) {
    var elementId = options.id;
    var iconGenerator = options.iconGenerator;
    var onUserClick = options.onUserClick;

    var map = null;
    var taskObjectManagers = {};
    var routeCluster = {};
    var regions = {};

    // коллекция пользователей
    var userCluster = null;

    /*
     * коллекция маршрутов пользователей
     */
    var routes = {};

    var defaultControls = ['zoomControl', 'searchControl', 'typeSelector', 'fullscreenControl'];
    var pointConterter = options.pointConterter;

    var me = this;
    //надо для рисования выделения задач
    var _rectangle = null;
    var _downCoords = null;
    var hasUserColor = 'blue';

    /*
     * область является одной точкой
     * @param array {[][]} точки
     */
    function isBoundsPoint(array) {
        if (array[0][0] == array[1][0] && array[0][1] == array[1][1])
            return true;

        return false;
    }

    /**
     * проверка доступности пользователя
     * @param {any} data - данные пользователя 
     */
    function existsUser(data) {
        var objects = userCluster.getGeoObjects();
        for (var j = 0; j < objects.length; j++) {
            var point = objects[j];
            if (point.properties.get('data').uuid == data.uuid) {
                return true;
            }
        }
        return false;
    }

    /**
     * инициализация карты
     */
    function init() {
        if (!ymaps.template.filtersStorage.get('additional')) {
            var addressFilter = function (data, props, filterValue) {
                var innerHtml = '<div>' + props.get('DocName') + '</div>' +
                    '<div><b>Статус: </b>' + props.get('StatusName') + '</div>' +
                    '<div><b>Адрес: </b>' + props.get('Address') + '</div>';

                var user = props.get('UserFIO');
                if (user && user.length > 0) {
                    innerHtml += '<div><b>Назначен: </b>' + user + '</div>';
                }

                return '<div style="border-top:1px solid #d3d3d3;padding-top:8px;padding-bottom:8px;">' +
                    innerHtml +
                '</div>';
            };

            ymaps.template.filtersStorage.add('additional', addressFilter);
        }

        var center = options.mapOptions ? options.mapOptions.center || [55.76, 37.64] : [55.76, 37.64];
        var zoom = options.mapOptions ? options.mapOptions.zoom || 7 : 7;

        map = new ymaps.Map(elementId, {
            center: center,
            zoom: zoom,
            controls: options.defaultControls || defaultControls
        }, {
            autoFitToViewport: 'always'
        });

        map.behaviors.disable(['rightMouseButtonMagnifier']);
        map.events.add('mousedown', createSelectorRectangle);
        map.events.add('mousemove', onMouseMove);
        map.events.add('mouseup', selectorRectangleCompleate);

        createUserCluster();

        document.getElementById('msg').style.display = 'none';

        // инициализируем элементы
        if (window['initElements'])
            initElements();

        if (typeof ready == 'function')
            ready(this);
    }

    /**
     * Создать прямоугольник для выделения области
     */
    function createSelectorRectangle(e) {
        if (e.get('domEvent').originalEvent.button == 2) {
            if (_rectangle) {
                map.geoObjects.remove(_rectangle);
            }

            _downCoords = e.get('coords');
            _rectangle = new ymaps.Rectangle([_downCoords, _downCoords], {
            }, {
                interactivityModel: 'default#transparent',
                fill: true,
                coordRendering: "boundsPath",
                strokeWidth: 1
            });

            map.geoObjects.add(_rectangle);

            for (var i in regions) {
                regions[i].hint.close();
                regions[i].options.set('openHintOnHover', false);
            }
        }
    }

    /**
     * Перерисовка прямоугольника для выделоения области
     */
    function onMouseMove(e) {
        if (_rectangle) {
            if (e.get('domEvent').originalEvent.button == 2) {
                var coord = e.get('coords');
                var newBound = [
                    [
                        _downCoords[0] <= coord[0] ? _downCoords[0] : coord[0],
                        _downCoords[1] <= coord[1] ? _downCoords[1] : coord[1]
                    ],
                    [
                        _downCoords[0] >= coord[0] ? _downCoords[0] : coord[0],
                        _downCoords[1] >= coord[1] ? _downCoords[1] : coord[1]
                    ]
                ];

                _rectangle.geometry.setCoordinates(newBound);
            }
            else {
                selectorRectangleCompleate(e, true);
            }
        }
    }

    /**
     * Окончание выделения области
     * @param {any} e событие
     * @param {boolean} forseFire вызвать событие принудительно
     */
    function selectorRectangleCompleate(e, forseFire) {
        if ((e.get('domEvent').originalEvent.button == 2 || forseFire === true) && _rectangle) {

            //сначала обрабатываем события, только потом удаляем выделенную область
            selectTasksFromBound(_rectangle);
            map.geoObjects.remove(_rectangle);
            _rectangle = null;

            for (var i in regions) {
                regions[i].options.set('openHintOnHover', true);
            }
        }
    }

    /**
     * Выбрать все задания из области
     * @param {any} rect - область выделения
     */
    function selectTasksFromBound(rect) {
        if (me.appointCluster) {
            var placemarks = me.appointCluster.getGeoObjects();
            var result = ymaps.geoQuery(placemarks).searchInside(rect);
            var task = []; result._objects;

            for (var i = 0; i < result._objects.length; i++) {
                task.push(result._objects[i].properties.get('addData'));
            }

            selectPlacemark.call(me, result._objects);

            options.onBoundsSelect(task);
        }
    }

    /*
     * создание объекта для хранения заданий для пользователя с идентификатором uuid
     * @param uuid {string} идентификатор устройства
     */
    function createTaskObjectManager(uuid) {
        var taskObjectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32
        });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        taskObjectManager.clusters.options.set('clusterIconLayout', 'default#pieChart');
        // Радиус диаграммы в пикселях.
        taskObjectManager.clusters.options.set('clusterIconPieChartRadius', 25);
        // Радиус центральной части макета.
        taskObjectManager.clusters.options.set('clusterIconPieChartCoreRadius', 10);
        // Ширина линий-разделителей секторов и внешней обводки диаграммы.
        taskObjectManager.clusters.options.set('clusterIconPieChartStrokeWidth', 3);

        map.geoObjects.add(taskObjectManager);

        taskObjectManagers[uuid] = taskObjectManager;
        return taskObjectManager;
    }

    /*
     * создание объекта для хранения гео-данных для пользователя с идентификатором uuid
     * @param uuid {string} идентификатор устройства
     */
    function createRouteCluster(uuid) {

        var clusterer = new ymaps.Clusterer({
            // Зададим массив, описывающий иконки кластеров разного размера.
            clusterIcons: [{
                href: 'images/spacer.png',
                size: [40, 40],
                offset: [-20, -20]
            }],
            clusterIconContentLayout: null
        });

        map.geoObjects.add(clusterer);

        routeCluster[uuid] = clusterer;
        return clusterer;
    }

    /*
     * создание объекта для хранения пользователей
     */
    function createUserCluster() {
        var iconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="position:relative">' +
                '<div style="color: #FFFFFF; font-weight: bold">' +
                    '<img src="images/placemark_user.png" style="position:absolute;top:3px;left:11px;" />' +
                    '<div style="padding-top:10px;font-size:11px">$[properties.geoObjects.length]</div>' +
                '</div>' +
            '</div>');

        userCluster = new ymaps.Clusterer({
            // Зададим массив, описывающий иконки кластеров разного размера.
            clusterIcons: [{
                href: 'images/placemark.png',
                size: [32, 32],
                offset: [-20, -20]
            }],
            clusterIconContentLayout: iconContentLayout
        });

        map.geoObjects.add(userCluster);
    }

    /**
     * Вернуть описание региона
     * @param {string} displayName - имя региона
     * @param {string} description - описание региона
     * @param {any[]} users - пользователи привязанные к региону
     */
    function getRegionHint(displayName, description, users) {
        var hintContent = typeof displayName == 'string' && displayName.length > 0 ? '<b>Наименование области</b>: ' + displayName + '<br/>' : '';
        if (typeof description == 'string' && description.length > 0)
            hintContent += '<b>Описание области</b>: ' + description;

        hintContent += '<br />' + getRegionUsersHint(users);

        return hintContent;
    }

    /**
     * Вернуть описание пользователей привязанных к региону
     * @param {any[]} users - пользователи привязанные к региону
     */
    function getRegionUsersHint(users) {
        if (users && !users.length)
            users = [users];

        var USER_LIST_TEXT = '<b>Привязанные исполнители</b>: ';
        var text = '';
        if (users && users.length > 0) {
            users.forEach(function (user) {
                text += user.C_Fio + ', ';
            });

            text = text.substring(0, text.length - 2);
        } else {
            text = 'отсутствуют';
        }

        return USER_LIST_TEXT + text;
    }

    /**
     * Вывести контекстное меню
     * @param {number[]} coords - координаты вывода контекстного меню
     * @param {()=>void} onEdit - нажатие на кнопку редактировать
     * @param {()=>void} onRemove - нажатие на кнопку удалить
     * @param {()=>void} onAppointToUser - нажатие на кнопку назначить
     */
    function createContextMenu(coords, onEdit, onRemove, onAppointToUser) {
        if ($('.context-menu').css('display') == 'block') {
            $('.context-menu').remove();
        } else {
            var menuContent =
                '<div class="context-menu popup-container" id="menu1">\
                    <div id="edit" align="center"><input type="submit" value="Редактировать" /></div>\
                    <div id="remove" align="center"><input type="submit" value="Удалить" /></div>\
                    <div id="appointToUser" align="center"><input type="submit" value="Назначить обходчику" /></div>\
                </div> ';

            $('body').append(menuContent);

            $('.context-menu').css({
                left: coords[0],
                top: coords[1]
            });

            $('.context-menu #edit').click(function () {
                $('.context-menu').remove();
                if (typeof onEdit == 'function') {
                    onEdit();
                }
            });

            $('.context-menu #remove').click(function () {
                $('.context-menu').remove();
                if (typeof onRemove == 'function') {
                    onRemove();
                }
            });

            $('.context-menu #appointToUser').click(function () {
                $('.context-menu').remove();
                if (typeof onAppointToUser == 'function') {
                    onAppointToUser();
                }
            });
        }
    }

    /**
     * добавление обходчика на карту (или обновление)
     * @param item {any} данные пользователя
     */
    this.addUser = function (item) {
        if (existsUser(item) == true) {

        } else {
            var placemark = pointConterter.toUser(item.uuid, item, iconGenerator.replace('{1}', 'placemark').replace('{0}', item.color));
            placemark.events.add('click', function (e) {
                var userData = e.originalEvent.target.properties.get('data');
                if (onUserClick) {
                    onUserClick(userData);
                }
            });
            userCluster.add(placemark);
        }
    }

    /**
     * Вернуть регионы
     */
    this.getRegions = function () {
        return regions;
    }

    /**
     * Добавить регион на карту
     * @param link {string} идентификатор региона
     * @param coords {Array<Array<number>>} координаты региона
     * @param options {any} дополнительные параметры
     *      сolor
     *      displayName
     *      description
     *      users
     *      onEdit - функция вызывается после редактирования региона
     *      force - насильно обновить регион
     */
    this.addRegion = function (link, coords, options) {

        if (regions[link]) {
            if (options.force === true) {
                map.geoObjects.remove(regions[link]);
                delete regions[link];
            }
            else {
                return regions[link];
            }
        }


        var color = typeof options.color !== 'string' || options.color.length == 0 ? '#ff0000' : options.color;

        var poligon = new ymaps.Polygon([coords], {
            hintContent: getRegionHint(options.displayName, options.description, options.users),
            user: options.users
        }, {
            // Цвет заливки.
            fillColor: color + '00',
            // Цвет обводки.
            strokeColor: color,
            // Ширина обводки.
            strokeWidth: 3,
            //объект получает все DOM-события, а затем прокидывает их на карту.
            interactivityModel: 'default#transparent'
            //interactivityModel: 'default#geoObject'
        });

        poligon.events.add('contextmenu', function (e) {
            createContextMenu(e.get('pagePixels'),
                function () {
                    options.onEdit(link);
                },
                function () {
                    options.onRemove(link);
                },
                function () {
                    options.onAppointToUser(options);
                }
            );
        });

        if (options.phantom != true) {
            poligon.link = link;
            regions[link] = poligon;
        }

        map.geoObjects.add(poligon);

        return poligon;
    }

    /**
     * Вывести форму создания региона
     * @param {(region)=>void} onCreate - создать новый регион
     * @param {(title, msg)=>void} onError - вывод ошибки
     */
    this.createNewRegion = function (onCreate, onError) {
        var me = this;
        var poligon = null;

        //вызывается при создании региона
        var onCreatePress = function (regionData) {
            if (poligon != null) {
                var coords = poligon.geometry.getCoordinates();

                poligon.editor.stopEditing();
                poligon.editor.stopDrawing();

                if (typeof onCreate == 'function')
                    onCreate({
                        displayName: regionData.displayName,
                        description: regionData.description,
                        color: regionData.color,
                        coords: coords[0]
                    },
                    function () {
                        map.geoObjects.remove(poligon);
                    });
            }
        };

        //отмена создания региона
        var onCancel = function () {
            if (poligon) {
                poligon.editor.stopEditing();
                poligon.editor.stopDrawing();

                map.geoObjects.remove(poligon);
            }
            else {
                map.events.remove('click', onMapClick);
            }
        }

        //изменение цвета, чтобы поменять
        var onColorChange = function (color) {
            if (poligon) {
                poligon.options.set('strokeColor', color);
            }
        }

        //создаем форму
        var control = getAddRegionControl('Создание области контроля', {}, onCreatePress, onCancel, onError, onColorChange);

        //выводим
        map.controls.add(control, {
            float: 'none',
            position: { right: 20, top: 20 }
        });

        //ждем начала создания региона
        var onMapClick = function (event) {
            var coords = event.get('coords');

            poligon = me.addRegion(-1, [coords], { phantom: true });

            poligon.editor.startEditing();
            poligon.editor.startDrawing();

            map.events.remove('click', onMapClick);
        }

        map.events.add('click', onMapClick);
    }

    /**
     * Удалить область
     * @param {number} link - идентификатор региона
     */
    this.removeRegion = function (link) {
        if (regions[link]) {
            regions[link].events.remove('contextmenu', regions[link].events.types.contextmenu[0]);
            map.geoObjects.remove(regions[link]);
            delete regions[link];
        }
    }

    /**
     * Изменить область
     * @param {number} link - идентификатор региона
     * @param {any} options - параметры изменения 
     * @param {()=>void} onSuccess - применить изменения
     * @param {()=>void} onCancel - отмена изменения
     * @param {(title, msg)=>void} onError - вывод ошибки
     */
    this.editRegion = function (link, options, onSuccess, onCancel, onError) {
        var me = this;
        var poligon = regions[link];

        //вызывается при создании региона
        var onCreatePress = function (regionData) {
            if (poligon != null) {
                var coords = poligon.geometry.getCoordinates();

                poligon.editor.stopEditing();
                poligon.editor.stopDrawing();

                if (typeof onSuccess == 'function')
                    onSuccess({
                        displayName: regionData.displayName,
                        description: regionData.description,
                        color: regionData.color,
                        coords: coords[0]
                    },
                    function () {
                        map.geoObjects.remove(poligon);
                        delete regions[link];
                    });
            }
        };

        //отмена создания региона
        var onReject = function () {
            if (poligon) {
                poligon.editor.stopEditing();
                poligon.editor.stopDrawing();

                if (typeof onCancel == 'function')
                    onCancel();
            }
        }

        //изменение цвета, чтобы поменять
        var onColorChange = function (color) {
            if (poligon) {
                poligon.options.set('strokeColor', color);
            }
        }

        var control = getAddRegionControl('Редактирование области контроля', options, onCreatePress, onReject, onError, onColorChange);

        map.controls.add(control, {
            float: 'none',
            position: { right: 20, top: 20 }
        });

        poligon.editor.startEditing();
        poligon.editor.startDrawing();
    }

    /**
     * Назначить область пользователю
     * @param {number} region - регион
     * @param {any[]} usersList - список обходчиков
     * @param {number} onSuccess - применить изменения
     * @param {number} onCancel - отмена изменения
     * @param {(title, msg)=>void} onError - вывод ошибки
     */
    this.appointRegionToUser = function (region, usersList, onSuccess, onCancel, onError) {
        var options = {
            data: usersList,
            displayField: 'name',
            valueField: 'value'
        };

        if (region.users) {
            options.line1 = {
                key: 'Описание области',
                value: region.description
            }
            options.line2 = {
                key: 'Статаус',
                value: 'Назначено'
            }
            options.line3 = {
                key: 'Исполнитель',
                value: region.users.length ? region.users[0].C_Fio : region.users.C_Fio
            };
            options.buttonText = 'Переназначить';
        }
        else {
            options.line1 = {
                key: 'Наименование области',
                value: region.displayName
            };
            options.line2 = {
                key: 'Описание области',
                value: region.description
            }
            options.line3 = {
                key: 'Статаус',
                value: 'Не назначено'
            }
            options.buttonText = 'Назначить';
        }

        var control = getAppointUserControl(region.users ? region.displayName : 'Назначение участка', options, onSuccess, onCancel, onError);

        map.controls.add(control, {
            float: 'none',
            position: { right: 20, top: 20 }
        });
    }

    /**
     * Назначить задания пользователю
     * @param {any[]} tasks - задачи
     * @param {any[]} usersList - список обходчиков
     * @param {()=>void} onSuccess - применить изменения
     * @param {()=>void} onCancel - отмена изменения
     * @param {(title, msg)=>void} onError - вывод ошибки
     */
    this.appointTasksToUser = function (tasks, usersList, onSuccess, onCancel, onError) {
        var options = {
            data: usersList,
            displayField: 'name',
            valueField: 'value'
        };

        var control = getAppointTasksToUserControl(tasks, options, onSuccess, onCancel, onError);

        map.controls.add(control, {
            float: 'none',
            position: { right: 20, top: 20 }
        });
    }

    /**
    * добавление меток (задание)
    * @param {any} data - список заданий
    * @param {()=>void} onSelectPlacemarks - выделение меток
    * @param {bool} updatePart - обновлять метки
    */
    this.addAppointTask = function (data, onSelectPlacemarks, updatePart) {

        var placemarks = [];
        var placemarksLinks = [];
        for (var i = 0; i < data.length; i++) {
            placemarksLinks.push(data[i].get('LINK'));
            placemarks.push(createNewPlaceMark(
                data[i].getCoord(),
                '',
                data[i].get('UserId') == '' ? 2 : 3,
                data[i]
            ));
        }

        if (!this.appointCluster) {
            var hintLayout = getPlacemarkHintLayout("{% for geoObject in properties.geoObjects %}" +
                                '<div>{{ geoObject.properties.addData|additional|raw }}</div>' +
                            "{% endfor %}");

            this.appointCluster = new ymaps.Clusterer({
                hasBalloon: false,
                clusterDisableClickZoom: true,
                // Макет метки кластера pieChart.
                clusterIconLayout: 'default#pieChart',
                // Радиус диаграммы в пикселях.
                clusterIconPieChartRadius: 25,
                // Радиус центральной части макета.
                clusterIconPieChartCoreRadius: 10,
                // Ширина линий-разделителей секторов и внешней обводки диаграммы.
                clusterIconPieChartStrokeWidth: 3,
                clusterIconInteractivityModel: 'default#opaque',
                gridSize: 128,
                clusterHintLayout: hintLayout
            });

            var me = this;
            this.appointCluster.events
                    // Можно слушать сразу несколько событий, указывая их имена в массиве.
                    .add(['click'], function (e) {
                        var target = e.get('target'),
                            type = e.get('type');

                        if (typeof target.getGeoObjects != 'undefined') {
                            // Событие произошло на кластере.
                            selectPlacemark.call(me, target.getGeoObjects())
                            var c = me.appointCluster;
                        } else {
                            // Событие произошло на геообъекте.
                            selectPlacemark.call(me, [target])
                        }
                    })
                    .add('contextmenu', function (e) {
                        e.stopPropagation();
                        e.preventDefault();

                        var target = e.get('target'),
                            type = e.get('type');

                        var data = null;

                        if (typeof target.getGeoObjects != 'undefined') {
                            data = getSelectedData.call(me, target.getGeoObjects());
                        } else {
                            data = getSelectedData.call(me, [target]);
                        }

                        if (data != null && data.length > 0 && typeof onSelectPlacemarks == 'function') {
                            onSelectPlacemarks(data);
                        }
                    });
        }

        if (updatePart != true) {
            this.appointCluster.removeAll();
        }
        else {
            //перерисовываем только указанные метки
            var mapPlacemarks = this.appointCluster.getGeoObjects();
            for (var i = 0; i < mapPlacemarks.length; i++) {
                var addData = mapPlacemarks[i].properties.get('addData');

                if (addData && placemarksLinks.indexOf(appData.get('LINK')) > -1) {
                    this.appointCluster.remove(mapPlacemarks[i]);
                }
            }
        }

        this.appointCluster.add(placemarks);
        map.geoObjects.add(this.appointCluster);
        if (this.appointCluster.getGeoObjects().length > 0)
            map.setBounds(this.appointCluster.getBounds());
    }

    /**
     * Изменить к кому привязано задание
     * @param {any} user - обходчик
     * @param {string[]} links - список идентификаторов заданий
     */
    this.changeTasksOwner = function (user, links) {
        var placemarks = [];
        var placemarks = this.appointCluster.getGeoObjects().filter(function (item) {
            var addData = item.properties.get('addData');
            if (addData) {
                return links.indexOf(addData.get('LINK')) > -1;
            }
        });

        if (placemarks.length > 0) {
            //снимаем выдение меток
            selectPlacemark.call(this, placemarks);

            for (var i = 0; i < placemarks.length; i++) {
                placemarks[i].properties.get('addData').set('UserId', user.LINK);
                placemarks[i].properties.get('addData').set('UserFIO', user.C_Fio);
                placemarks[i].options.set('iconColor', getPlacemarkColorByType(3));
                placemarks[i].properties.set('placemarkType', 3);
            }
        }
    }

    /**
     * Вернуть цвет по типу метки
     * @param {number} type - тип метки (1-selected; 2-unassigned; 3-assigned)
     */
    var getPlacemarkColorByType = function (type) {
        if (type == 1)
            return '#DF0024'//selected

        if (type == 2)
            return 'grey';
        //return '#0A0909'//unassisned

        if (type == 3)
            return '#404DDD'//assigned
    }

    /**
     * Вернуть разметку хинта
     * @param {string} innerMarking - внутренний текст
     */
    var getPlacemarkHintLayout = function (innerMarking) {
        return ymaps.templateLayoutFactory.createClass("<div class='popup-container' style=''>\
                        <div style='margin-borrom:8px'><b>Точка маршрута</b></div>\
                        <div>\
                        <div class='custom-scroller' style='overflow: auto;width:300px;max-height:250px;'>"+
                            innerMarking +
                        "</div>\
                        </div>\
                        </div>", {
                            // Определяем метод getShape, который
                            // будет возвращать размеры макета хинта.
                            // Это необходимо для того, чтобы хинт автоматически
                            // сдвигал позицию при выходе за пределы карты.
                            getShape: function () {
                                var el = this.getElement(),
                                    result = null;
                                if (el) {
                                    var firstChild = el.firstChild;
                                    result = new ymaps.shape.Rectangle(
                                        new ymaps.geometry.pixel.Rectangle([
                                            [0, 0],
                                            [firstChild.offsetWidth, firstChild.offsetHeight]
                                        ])
                                    );
                                }
                                return result;
                            }
                        }
                    );
    }

    /**
     * Вернуть новую метку с заданием
     * @param {number[]} coords - координаты
     * @param {string} content - запись метки
     * @param {string} color - цвет метки
     * @param {any} addData - описание задания
     */
    var createNewPlaceMark = function (coords, content, placemarkType, addData) {
        var hintLayout = getPlacemarkHintLayout("{{ properties.addData|additional|raw }}");

        var iconLayout = ymaps.templateLayoutFactory.createClass(
            '<nobr style="color:#212121;background-color:rgba(255,255,255,0.9);position:relative;padding:5px 5px 6px 21px;border-radius:15px">' +
            '<img src="images/placemark-$[properties.placemarkType].png" style="position:absolute;z-index:1;top:2px;left:2px" />' +
            '<img src="images/status-4.png" style="position:absolute;z-index:2;top:5px;left:5px" />' +
            '</nobr>'
        );

        return new ymaps.Placemark(coords, {
            placemarkType: placemarkType,
            addData: addData
        }, {
            interactivityModel: 'default#opaque',
            //addData: addData,

            iconLayout: 'default#imageWithContent',
            // Своё изображение иконки метки.
            iconImageHref: 'images/spacer.png',
            // Размеры метки.
            iconImageSize: [22, 22],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-24, -24],
            // Макет содержимого.
            iconContentLayout: iconLayout,
            iconColor: getPlacemarkColorByType(placemarkType),
            hintLayout: hintLayout
        });
    }

    /**
     * Вернуть описание всех выделенных меток
     */
    var getSelectedData = function () {
        var data = [];
        var geoObjects = this.appointCluster.getGeoObjects();

        for (var i = 0; i < geoObjects.length; i++) {
            if (geoObjects[i].properties.get('placemarkType') == 1) {
                data.push(geoObjects[i].properties.get('addData'));
            }
        }

        return data;
    }

    /**
     * Выделить метки
     * @param {any[]} placemarks - метки
     */
    var selectPlacemark = function (placemarks) {
        var containsSelected = false;

        //проверяем есть ли среди меток уже выденные
        for (var i = 0; i < placemarks.length; i++) {
            if (placemarks[i].properties.get('placemarkType') === 1) {
                containsSelected = true;
                break;
            }
        }

        if (containsSelected === true) {
            for (var i = 0; i < placemarks.length; i++) {
                var placemarkType = placemarks[i].properties.get('placemarkType');
                if (placemarkType === 1) {
                    var oldType = placemarks[i].properties.get('addData').get('UserId') === '' ? 2 : 3;
                    placemarks[i].options.set('iconColor', getPlacemarkColorByType(oldType));
                    placemarks[i].properties.set('placemarkType', oldType);
                }
            }
        } else {
            for (var i = 0; i < placemarks.length; i++) {
                placemarks[i].options.set('iconColor', getPlacemarkColorByType(1));
                placemarks[i].properties.set('placemarkType', 1);
            }
        }


        //чтобы кластер применил изменения, надо добавить нвую метку
        //костыль, не нашел метода обновления
        if (placemarks.length > 1) {
            var lastPlacemark = placemarks[placemarks.length - 1];
            this.appointCluster.remove(lastPlacemark);
            this.appointCluster.add(lastPlacemark);
        }
    }

    /**
     * Вернуть задачи внутри регионов, но не привязынные
     * @returns {} 
     */
    this.getUnAssignedTasks = function () {
        var placemarks = this.appointCluster.getGeoObjects();

        //получаем все неназначенные задания
        placemarks = placemarks.filter(function(item) {
            var addData = item.properties.get("addData");
            if (addData && typeof addData.get === "function") {
                var fio = addData.get("UserFIO");
                var userlink = addData.get("UserId");

                if (fio && fio !== "" && userlink && userlink !== "") {
                    return false;
                }
                return true;
            }
            return false;
        });

        var toAssign = [];

        //ищем по регионам входящие точки
        for (var zone in regions) {
            var region = regions[zone];
            var rgionUser = region.properties.get("user");

            //регион привязвн к обходчику, то и точки долны быть назначены ему
            if (rgionUser && rgionUser.LINK) {
                var points = ymaps.geoQuery(placemarks);
                var pointsInside = points.searchInside(region);
                if (pointsInside.getLength() > 0) {

                    var links = [];
                    var tasks = [];
                    for (var i = 0; i < pointsInside._objects.length; i++) {
                        var item = pointsInside._objects[i];
                        var addData = item.properties.get("addData");
                        if (addData && addData.getData) {
                            var task = addData.getData();
                            links.push(task.LINK);
                            tasks.push(task);
                        }
                    }

                    toAssign.push({
                        user: rgionUser,
                        links: links,
                        tasks: tasks,
                        fio: rgionUser.C_Fio
                    });
                }
            }
        }
        return toAssign;
    }

    /**
     * Удалить все с карты
     * @returns {} 
     */
    this.removeAll = function () {
        if (userCluster)
            userCluster.removeAll();

        if (this.appointCluster)
            this.appointCluster.removeAll();
        }

    ymaps.ready(init);
    return this;
}