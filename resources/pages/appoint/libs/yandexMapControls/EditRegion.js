﻿var getYandexMapAddRegionHTML = function (title) {
    return ' <div id="popupcontainer" class="appoint-create-region-container popup-container"> \
            <div class="flex-row">\
                <div class="title font-roboto-14" style="flex:1;"> '+
                    title +
                '</div>\
                <div id="region-cancel" class="close">&#10006;</div>\
            </div>\
            <input id="displayName" class="input font-roboto-14" placeholder="Наименование" />\
            <input id="description" class="input font-roboto-14" placeholder="Описание" />\
            <div class="color-picker">\
                <div class="color-picker-title font-roboto-14">\
                    Выбор цвета\
                </div>\
                <div class="color-picker-wraper flex-row">\
                    <div class="wraper" style="padding-right: 20px;">\
                        <div id="picker">\
                            <div id="picker-indicator" class="picker-indicator"></div>\
                        </div>\
                    </div>\
                    <div class="wraper">\
                        <div id="slide">\
                            <div id="slide-indicator" class="slide-indicator"></div>\
                        </div>\
                    </div>\
                </div>\
                <div class="hex-wraper flex-row">\
                    <div>\
                        <div class="hex-left-block flex-row">\
                            <div id="hex-container" class="hex-container">\
                            </div>\
                            <div class="hex-text font-roboto-14">HEX</div>\
                            <div>\
                                <input id="hex-value" class="hex-value font-roboto-14" value="#000000" />\
                            </div>\
                        </div>\
                    </div>\
                    <div style="padding-left:10px">\
                        <div class="rgb-container flex-row">\
                            <div class="hex-text font-roboto-14">R</div>\
                            <input id="r-value" class="hex-value font-roboto-14" value="0" />\
                        </div>\
                        <div class="rgb-container flex-row">\
                            <div class="hex-text font-roboto-14">G</div>\
                            <input id="g-value" class="hex-value font-roboto-14" value="0" />\
                        </div>\
                        <div class="rgb-container flex-row">\
                            <div class="hex-text font-roboto-14">B</div>\
                            <input id="b-value" class="hex-value font-roboto-14" value="255" />\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div class="separator"></div>\
            <div class="appoint-buttons flex-row">\
                <div id="region-save" type="submit" class="button">\
                    <div class="save-text font-roboto-14">\
                        Сохранить\
                    </div>\
                </div>\
            </div>\
        </div>\
    ';
}

var getAddRegionControl = function (title, options, onSuccess, onCancel, onError, onColorChange) {
    $('#popupcontainer').remove();
    // Создаем собственный класс.
    CustomControlClass = function (options) {
        CustomControlClass.superclass.constructor.call(this, options);
        this._$content = null;
    };
    // И наследуем его от collection.Item.
    ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
        onAddToMap: function (map) {
            CustomControlClass.superclass.onAddToMap.call(this, map);
            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
        },

        onRemoveFromMap: function (oldMap) {
            if (this._$content) {
                this.colorPicker = null;
                this.color = null;
                document.getElementById('hex-value').onchange = null;
                document.getElementById('r-value').onchange = null;
                document.getElementById('g-value').onchange = null;
                document.getElementById('b-value').onchange = null;
                document.getElementById('region-save').onclick = null;

                this._$content.remove();
            }
            CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
        },

        _onGetChildElement: function (parentDomContainer) {
            // Создаем HTML-элемент с текстом.
            this._$content = $(getYandexMapAddRegionHTML(title)).appendTo(parentDomContainer);

            this.colorPicker = ColorPicker(document.getElementById('slide'),
                  document.getElementById('picker'),
                  function (hex, hsv, rgb, mousePicker, mouseSlide) {
                      this.color = hex;

                      ColorPicker.positionIndicators(
                          document.getElementById('slide-indicator'),
                          document.getElementById('picker-indicator'),
                          mouseSlide, mousePicker
                      );
                      document.body.style.backgroundColor = hex;

                      document.getElementById('hex-value').value = hex;
                      document.getElementById('hex-container').style.background = hex;

                      document.getElementById('r-value').value = rgb.r.toFixed();
                      document.getElementById('g-value').value = rgb.g.toFixed();
                      document.getElementById('b-value').value = rgb.b.toFixed();

                      if (typeof onColorChange == 'function') {
                          onColorChange(hex);
                      }
                  }.bind(this));

            document.getElementById('hex-value').onchange = this.onValueChange.bind(this, 'hex');
            document.getElementById('r-value').onchange = this.onValueChange.bind(this);
            document.getElementById('g-value').onchange = this.onValueChange.bind(this);
            document.getElementById('b-value').onchange = this.onValueChange.bind(this);


            this.colorPicker.setHex(options.color || '#AB12FE');

            document.getElementById('description').value = (options.description || '');
            document.getElementById('displayName').value = (options.displayName || '');

            document.getElementById('region-save').onclick = this.onSaveClick.bind(this);
            document.getElementById('region-cancel').onclick = this.onCancelClick.bind(this);
        },

        /**
         * Нажатие сохранить
         */
        onSaveClick: function () {
            if (typeof onSuccess == 'function') {
                var description = document.getElementById('description').value;
                var displayName = document.getElementById('displayName').value;

                if (!displayName || displayName.length == 0) {
                    onError('Заполните поле "Наименование зоны"');
                    return;
                }

                if (!description || description.length == 0) {
                    onError('Заполните поле "Описание"');
                    return;
                }

                onSuccess({
                    description: description,
                    displayName: displayName,
                    color: this.color
                });
                this.onRemoveFromMap(this.getMap());
            }
        },

        onCancelClick: function () {
            if (typeof onCancel == 'function') {
                onCancel();
            }
            this.onRemoveFromMap(this.getMap());
        },

        /**
         * Изменение цвета
         */
        onValueChange: function (type) {
            switch (type) {
                case 'hex':
                    var value = document.getElementById('hex-value').value;
                    if (value) {
                        value = value.replace('#', '');
                        if (value.length == 6)
                            this.colorPicker.setHex('#' + value);
                    }

                    break;
                default:
                    this.colorPicker.setRgb({
                        r: parseInt(document.getElementById('r-value').value || 0),
                        g: parseInt(document.getElementById('g-value').value || 0),
                        b: parseInt(document.getElementById('b-value').value || 0)
                    });
            }
        },
    });

    return new CustomControlClass();
}