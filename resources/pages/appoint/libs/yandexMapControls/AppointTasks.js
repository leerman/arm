﻿
var getYandexMapAppointTasksToUserHTML = function (data, status, address) {
    return '<div id="popupcontainer" class="re-appoint popup-container">\
	    <div class="flex-row">\
		    <div class="title font-roboto-14" style="flex:1">Назначение объекта</div>\
		    <div id="region-cancel" class="close">&#10006;</div>\
	    </div>\
        <div class="appoint-tasks-container custom-scroller" >' + data + '</div>\
	    <div id="magicsuggest"></div>\
	    <div class="appoint-buttons flex-row">\
		    <div id="region-save" type="submit" class="button">\
			    <div class="save-text font-roboto-14">Назначить</div>\
		    </div>\
	    </div>\
    </div>';
}

var getAppointTasksToUserControl = function (data, options, onSuccess, onCancel, onError, onColorChange) {
    $('#popupcontainer').remove();
    // Создаем собственный класс.
    CustomControlClass = function (options) {
        CustomControlClass.superclass.constructor.call(this, options);
        this._$content = null;
    };
    // И наследуем его от collection.Item.
    ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
        onAddToMap: function (map) {
            CustomControlClass.superclass.onAddToMap.call(this, map);
            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
        },

        onRemoveFromMap: function (oldMap) {
            if (this._$content) {

                this._$content.remove();
            }
            CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
        },

        _onGetChildElement: function (parentDomContainer) {
            var me = this;
            var dataHTML = [];

            for (var i = 0; i < data.length; i++) {
                dataHTML.push('<div class="values-container">\
	                <div class="title font-roboto-14 "><span>' + data[i].doc + '</span></div>\
	                <div class="title font-roboto-14 ">Статус: <span>' + data[i].status + '</span></div>\
	                <div class="title font-roboto-14 ">Адрес: <span>' + data[i].address + '</span></div>' +
                    (data[i].user && data[i].user.length > 0 ? '<div class="title">Назначен: <span>' + data[i].user + '</span></div>' : '') +
                '</div>')
            }
            // Создаем HTML-элемент с текстом.
            this._$content = $(getYandexMapAppointTasksToUserHTML(dataHTML.join(''))).appendTo(parentDomContainer);

            var ms = $('#magicsuggest').magicSuggest({

                data: options.data,
                displayField: options.displayField,
                valueField: options.valueField,
                maxSelection: 1,
                placeholder: 'ФИО',
                useZebraStyle: true,
                expandOnFocus: true,
                maxSelectionRenderer: function (v) {
                    return '';
                },
            });

            $(ms).on(
			  'selectionchange', function (e, cb, s) {
			      me._$value = cb.getValue();
			  }
			);

            document.getElementById('region-save').onclick = this.onSaveClick.bind(this);
            document.getElementById('region-cancel').onclick = this.onCancelClick.bind(this);
        },

        /**
         * Нажатие сохранить
         */
        onSaveClick: function () {
            if (typeof onSuccess == 'function') {

                if (!this._$value || this._$value.length == 0) {
                    onError('Укажите ФИО обходчика');
                    return;
                }

                onSuccess(this._$value[0]);
                this.onRemoveFromMap(this.getMap());
            }
        },

        onCancelClick: function () {
            if (typeof onCancel == 'function') {
                onCancel();
            }
            this.onRemoveFromMap(this.getMap());
        },
    });

    return new CustomControlClass();
}