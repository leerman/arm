﻿/*
 * балун для отображения информациии о задании на карте
 */
var taskBalloonContentLayout;

function initElements() {
    // Создание вложенного макета содержимого балуна.
    taskBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
        '<h3 class="popover-title">$[properties.balloonHeader]</h3>' +
        '<div class="popover-content">$[properties.balloonContent]</div>'
    );
}