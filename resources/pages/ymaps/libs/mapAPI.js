var mapAPI = function (options, ready) {
    var elementId = options.id;
    var iconGenerator = options.iconGenerator;
    var onUserClick = options.onUserClick;
    var onTaskClick = options.onTaskClick;

    var map = null;
    var taskObjectManagers = {};
    var routeCluster = {};

    // коллекция пользователей
    var userCluster = null;

    /*
     * коллекция маршрутов пользователей
     */
    var routes = {};

    var defaultControls = ['zoomControl', 'searchControl', 'typeSelector', 'fullscreenControl'];
    var pointConterter = options.pointConterter;

    var me = this;

    $(document).on('click', 'a.goto-doc', function () {
        if (onTaskClick)
            onTaskClick($(this).attr('data-href'));
    });

    /*
     * область является одной точкой
     * @param array {[][]} точки
     */
    function isBoundsPoint(array) {
        if (array
            && Array.isArray(array)
            && array[0][0] == array[1][0]
            && array[0][1] == array[1][1])
            return true;

        return false;
    }

    /**
     * фокусируем карту на кластере
     * @param cluster {any} кластер
     */
    function focusByCluster(cluster) {
        if (cluster) {
            var length = cluster.objects.getLength();
            if (length > 1 && !isBoundsPoint(cluster.getBounds())) {
                if (cluster.getBounds()) {
                    map.setBounds(cluster.getBounds());
                    // Иногда может понадобиться вот такой вот хак
                    map.setZoom(map.getZoom());
                }
            } else if (length > 0) {
                var item = cluster.objects.getAll()[0];
                map.setCenter(item.geometry.coordinates, 16, {
                    checkZoomRange: true
                });
            }
        }
    }

    /**
     * проверка доступности пользователя
     * @param {any} data - данные пользователя 
     */
    function existsUser(data) {
        var user = getUser(data.uuid);
        if (user) {
            return true;
        }
        return false;
    }

    /*
     * возвращается пользователь
     * @param uuid {string} идентификатор устройства
     */
    function getUser(uuid) {
        if (userCluster) {
            var objects = userCluster.getGeoObjects();
            for (var j = 0; j < objects.length; j++) {
                var point = objects[j];
                if (point.properties.get('data').uuid == uuid) {
                    return point;
                }
            }
        }

        return null;
    }

    /**
     * обрабочик изменения зума у карты
     */
    function onBoundsChange(e) {
        var newZoom = e.get('newZoom'),
                  oldZoom = e.get('oldZoom');
        if (newZoom != oldZoom) {
            for (var i in routeCluster) {
                var cluster = routeCluster[i];
                var objects = cluster.getGeoObjects();
                for (var j = 0; j < objects.length; j++) {
                    var point = objects[j];
                    point.options.set('visible', newZoom >= 13);
                }
            }

            for (var i in routes) {
                var route = routes[i];
                var points = route.getWayPoints();
                points.each(function (n, t) {
                    n.options.set('visible', newZoom >= 13);
                });
            }
        }
    }

    /**
     * инициализация карты
     */
    function init() {

        var center = options.mapOptions ? options.mapOptions.center || [55.76, 37.64] : [55.76, 37.64];
        var zoom = options.mapOptions ? options.mapOptions.zoom || 7 : 7;

        map = new ymaps.Map(elementId, {
            center: center,
            zoom: zoom,
            controls: options.defaultControls || defaultControls
        }, {
            autoFitToViewport: 'always'
        });

        map.events.add('boundschange', onBoundsChange);

        document.getElementById('msg').style.display = 'none';

        // инициализируем элементы
        if (window['initElements'])
            initElements();

        if (typeof ready == 'function')
            ready(this);
    }

    /*
     * создание объекта для хранения заданий для пользователя с идентификатором uuid
     * @param uuid {string} идентификатор устройства
     */
    function createTaskObjectManager(uuid) {
        var taskObjectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 100
        });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        taskObjectManager.clusters.options.set('clusterIconLayout', 'default#pieChart');
        // Радиус диаграммы в пикселях.
        taskObjectManager.clusters.options.set('clusterIconPieChartRadius', 25);
        // Радиус центральной части макета.
        taskObjectManager.clusters.options.set('clusterIconPieChartCoreRadius', 10);
        // Ширина линий-разделителей секторов и внешней обводки диаграммы.
        taskObjectManager.clusters.options.set('clusterIconPieChartStrokeWidth', 3);

        map.geoObjects.add(taskObjectManager);

        taskObjectManagers[uuid] = taskObjectManager;
        return taskObjectManager;
    }

    /*
     * создание объекта для хранения гео-данных для пользователя с идентификатором uuid
     * @param uuid {string} идентификатор устройства
     */
    function createRouteCluster(uuid) {

        var clusterer = new ymaps.Clusterer({
            // Зададим массив, описывающий иконки кластеров разного размера.
            clusterIcons: [{
                href: 'images/spacer.png',
                size: [40, 40],
                offset: [-20, -20]
            }],
            clusterIconContentLayout: null
        });

        map.geoObjects.add(clusterer);

        routeCluster[uuid] = clusterer;
        return clusterer;
    }

    /*
     * создание объекта для хранения пользователей
     */
    function createUserCluster() {
        if (!userCluster) {
            var iconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="position:relative">' +
                    '<div style="color: #FFFFFF; font-weight: bold">' +
                        '<img src="images/placemark_user.png" style="position:absolute;top:3px;left:11px;" />' +
                        '<div style="padding-top:10px;font-size:11px">$[properties.geoObjects.length]</div>' +
                    '</div>' +
                '</div>');

            userCluster = new ymaps.Clusterer({
                // Зададим массив, описывающий иконки кластеров разного размера.
                clusterIcons: [{
                    href: 'images/placemark.png',
                    size: [30, 30],
                    offset: [-20, -20]
                }],
                clusterIconContentLayout: iconContentLayout,

                //clusterDisableClickZoom: true,
                //clusterOpenBalloonOnClick: true,
                // Устанавливаем режим открытия балуна. 
                // В данном примере балун никогда не будет открываться в режиме панели.
                clusterBalloonPanelMaxMapArea: 0,
                // Устанавливаем размер макета контента балуна (в пикселях).
                clusterBalloonContentLayoutWidth: 350,
                // Устанавливаем собственный макет.
                clusterBalloonItemContentLayout: customItemContentLayout,
                // Устанавливаем ширину левой колонки, в которой располагается список всех геообъектов кластера.
                clusterBalloonLeftColumnWidth: 120
            });

            map.geoObjects.add(userCluster);
        }
    }

    /**
     * добавление обходчика на карту (или обновление)
     * @param item {any} данные пользователя
     * @param color {any} цвет иконки
     */
    this.addUser = function (item, color) {
        createUserCluster();
        if (existsUser(item) == true) {

        } else {
            var placemark = pointConterter.toUser(item.uuid, item, iconGenerator.replace('{1}', 'placemark').replace('{0}', color || item.C_Icon_Color));
            placemark.events.add('click', function (e) {
                var userData = e.originalEvent.target.properties.get('data');
                if (onUserClick) {
                    onUserClick(userData);
                }
            });
            userCluster.add(placemark);
        }
    }

    /**
     * добавление маршрута 
     * @param uuid {string} идентификатор устройства
     * @param items {any[]} массив меток
     * @param color {string} цвет маршрута. Например (000000)
     * @param fio {string} имя пользователя
     * @param start_finish {boolean} вывод крайних точек (по умолчанию false)
     */
    this.addRoute = function (uuid, items, color, fio, start_finish) {
        if (start_finish == undefined)
            start_finish = false;

        var points = [];
        var viaIndexes = [];
        for (var i = 0; i < items.length; i++) {
            points.push(pointConterter.toGeoPoint(items[i]));
            // начальная точка или последняя точка
            if (i == 0 || i == items.length - 1) {
                continue;
            }

            viaIndexes.push(i);
        }

        var multiRoute = new ymaps.multiRouter.MultiRoute({
            referencePoints: points,
            params: {
                results: 1,
                //Тип маршрутизации
                routingMode: 'auto',
                viaIndexes: viaIndexes
            }
        }, {
            //boundsAutoApply: true,
            wayPointFinishVisible: start_finish,
            wayPointStartVisible: start_finish,
            viaPointVisible: false,
            pinVisible: false,

            //маршрут
            routeActiveStrokeWidth: 2,
            routeActiveStrokeStyle: "solid",
            routeActiveStrokeColor: "#" + color
        });

        /**
            * Ждем, пока будут загружены данные мультимаршрута и созданы отображения путевых точек.
            * @see https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel-docpage/#event-requestsuccess
            */
        multiRoute.model.events.once("requestsuccess", function () {
            // нужно добавить иконки geo позицей
            //if (items.length >= 3) {
                var geoPoints = items; //.slice(1, items.length - 1);
                var cluster = createRouteCluster(uuid);
                var geoObjects = [];

                for (var i = 0 ; i < geoPoints.length; i++) {
                    geoObjects.push(pointConterter.toGeoViaPoint(i, geoPoints[i], iconGenerator.replace('{1}', 'geo').replace('{0}', color), uuid, fio));
                }

                cluster.add(geoObjects);
            //}
        });

        // Добавляем мультимаршрут на карту.
        map.geoObjects.add(multiRoute);
        routes[uuid] = multiRoute;
    },

    /**
     * добавление меток (задание)
     * @param uuid {string} идентификатор устройства
     * @param data {any} проиндексированный объект
     * @param color {string} цвет маршрута. Например (000000)
     */
    this.addTask = function (uuid, data, color) {
        var manager = createTaskObjectManager(uuid);
        var idx = 0;
        var features = [];
        for (var i in data) { // достаем точки для отрисовки на карте
            var point = data[i][0];
            if (point) {
                features.push(pointConterter.toTaskPoint(idx++, point, iconGenerator.replace('{1}', 'task').replace('{0}', color), data, i));
            }
        }

        // Добавление коллекции объектов.
        var collection = {
            type: 'FeatureCollection',
            features: features
        }

        manager.add(collection);
    }

    /**
     * фокусируем карту на обходчиках
     */
    this.focusByUsers = function () {
        if (userCluster) {
            var length = userCluster.getGeoObjects().length;
            if (length > 1 && !isBoundsPoint(userCluster.getBounds())) {
                map.setBounds(userCluster.getBounds());
                // Иногда может понадобиться вот такой вот хак
                map.setZoom(map.getZoom());
            } else if (length > 0) {
                var item = userCluster.getGeoObjects()[0];
                map.setCenter(item.geometry.getCoordinates(), map.getZoom(), {
                    checkZoomRange: true
                });
            }
        }
    }

    /*
     * устанавливаем фокус на указанном объекте
     * @param type {string} тип объекта
     * @param uuid {string} идентификатор устройства
     */
    this.focusObject = function (type, uuid) {
        switch (type) {
            //#region user
            case 'user':
                var placemark = getUser(uuid);
                if (placemark) {
                    map.setCenter(placemark.geometry.getCoordinates(), 16, {
                        checkZoomRange: true
                    });
                }
                return placemark;
                //#endregion

                //#region route
            case 'route':
                var route = routeCluster[uuid];
                if (route) {
                    var bounds = route.getBounds();
                    if (!isBoundsPoint(bounds)) {
                        map.setBounds(bounds);
                        // Иногда может понадобиться вот такой вот хак
                        map.setZoom(map.getZoom(), {
                            duration: 1500
                        });
                    } else {
                        map.setCenter(bounds[0], 16, {
                            checkZoomRange: true
                        }, {
                            duration: 1500
                        });
                    }
                }
                return route;
                //#endregion

                //#region tasks
            case 'tasks':
                var tasks = taskObjectManagers[uuid];
                if (tasks) {
                    focusByCluster(tasks);
                }
                return tasks;
                //#endregion
        }
    }

    /*
     * удаление элемента с карты
     * @param type {string} тип элемента
     * @param uuid {string} идентификатор устройства
     */
    this.removeObject = function (type, uuid) {
        switch (type) {
            //#region user
            case 'user':
                var placemark = getUser(uuid);
                if (placemark) {
                    userCluster.remove(placemark);
                }
                break;
                //#endregion

                //#region route
            case 'route':
                var route = routes[uuid];
                if (route) {
                    map.geoObjects.remove(route);
                    delete routes[uuid];
                }
                var cluster = routeCluster[uuid];
                if (cluster) {
                    map.geoObjects.remove(cluster);
                    delete routeCluster[uuid];
                }
                break;
                //#endregion

                //#region tasks
            case 'tasks':
                var tasks = taskObjectManagers[uuid];
                if (tasks) {
                    map.geoObjects.remove(tasks);
                    delete taskObjectManagers[uuid];
                }
                break;
                //#endregion
        }
    },

    /*
     * очистка данных на карте
     */
    this.removeAll = function () {
        if (map) {
            userCluster = null;
            taskObjectManagers = {};
            routeCluster = {};
            routes = {};
            map.geoObjects.removeAll();
        }
    }

    /*
     * удаление карты
     */
    this.destroy = function () {
        this.removeAll();
        if (map)
            map.destroy();
    }

    ymaps.ready(init);
    return this;
}