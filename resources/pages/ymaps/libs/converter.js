var converter = function() {

    /**
     * возвращается номер статуса выполнения
     * @param done {number} количество выполненных
     * @param total {number} общее кол-во
     */
    function getStatusNumber(done, total){
        if(done  == total)
            return 3;

        if(done > 0)
            return 2;

        return 1;
    }
   
    /**
     * возвращается цвет статуса выполнения
     * @param done {number} количество выполненных
     * @param total {number} общее кол-во
     */
    function getStatusColor(done, total){
        if(done  == total)
            return '#1bad03';

        if(done > 0)
            return '#ffda2b';

        return '#e10c2e';
    }

    /*
     * возвращается отформатированная дата
     * @param date {Date} дата
     */
    function getDate(date) {
        return dateFormat(date, "dd.mm.yyyy HH:MM:ss");
    }

    /*
     * возвращает контент для вывода данных о документе
     * @param doc {any} информация о документе
     * @param userId {string} идентификтаор пользователя
     * @param docLink {string} идентификатор документа
     */
    function getDocumentContent(doc, userId, docLink) {
        return '<b>Адрес</b><span>' + doc.address + '</span>' +
            '<b>Статус</b><span>' + doc.status + '</span>' +
            '<b>Номер документа</b><span>' + doc.number + '</span>' +
            '<b>Тип</b><span>' + doc.type + '</span>' +
            '<a class="goto-doc" href="#" data-href="#page/task/' + userId + '/' + docLink + '">Перейти на документ</a>';
    }

    /**
     * преобразование точки во внутренных элемент
     * @param id {number} идентификатор объекта
     * @param obj {string} объект преобразования
     * @param icon {string} путь к иконке задания
     * @param data {any} объект с данными для обработки
     * @param coordHash {string} хэш сумма
     */
    this.toTaskPoint = function (id, obj, icon, data, coordHash) {
        var doneCount = data.getDoneCount(coordHash);
        var totalCount = data.getTotalCount(coordHash);

        var iconContent = 'Состояние:&nbsp;' + doneCount + '&nbsp;из&nbsp;' + totalCount;
        var iconLayout = ymaps.templateLayoutFactory.createClass(
            '<nobr style="color:#212121;background-color:rgba(255,255,255,0.9);position:relative;padding:5px 5px 6px 30px;border-top-left-radius:15px;border-bottom-left-radius:15px;border-top-right-radius:7px;border-bottom-right-radius:7px">' +
            '<img src="' + icon + '" style="position:absolute;z-index:1;top:2px;left:2px" />' +
            '<img src="images/status-'+getStatusNumber(doneCount, totalCount)+'.png" style="position:absolute;z-index:2;top:5px;left:5px" />' +
            '$[properties.iconContent]' +
            '</nobr>'
        );
        //#region поиск документов
        var docs = {};
        for (var i = 0; i < data[coordHash].length; i++) {
            var item = data[coordHash][i];
            if (docs[item.Number]) {
                continue;
            } else {
                docs[item.Number] = {
                    address: item.Address,
                    status: item.StatusName,
                    type: item.TypeName,
                    number: item.Number,
                    id: item.DocLink
                };
            }
        }
        //#endregion
        
        var balloonContent = '<div class="document">';
        for (var i in docs) {
            balloonContent += getDocumentContent(docs[i], obj.UserId, obj.DocLink);
        }
        balloonContent += '</div>';

        return {
            type: 'Feature',
            id: id,
            geometry: {
                type: 'Point',
                coordinates: [obj.N_Latitude, obj.N_Longitude]
            },
            properties: {
                hintContent: '<b>Адрес:</b> ' + obj.Address +  '<br /><b>Обходчик: </b>' + obj.UserFIO,
                iconContent: iconContent,
                balloonHeader: 'Документ' + (data[coordHash].length > 1 ? '' : 'ы'),
                balloonContent: balloonContent
            },
            options: {
                balloonContentLayout: taskBalloonContentLayout,
                balloonPanelMaxMapArea: 0,
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'images/spacer.png',
                // Размеры метки.
                iconImageSize: [22, 22],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-24, -24],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                //iconContentOffset: [-2, -5],
                // Макет содержимого.
                iconContentLayout: iconLayout,
                iconColor: getStatusColor(doneCount, totalCount) // нужно чтобы для кластера строилась диаграмма
            }
        };
    }

    /**
     * преобразование точки в обходчика
     */
    this.toUser = function (id, obj, icon) {
        var iconLayout = ymaps.templateLayoutFactory.createClass(
            '<nobr style="color:#212121;background-color:rgba(255,255,255,0.9);position:relative;padding:10px 5px 9px 35px;border-top-left-radius:17px;border-bottom-left-radius:17px;border-top-right-radius:7px;border-bottom-right-radius:7px">' +
            '<img src="' + icon + '" style="position:absolute;z-index:1;top:2px;left:2px" />' +
            '<img src="images/placemark_user.png" style="position:absolute;z-index:2;top:9px;left:12px" />' +
            '$[properties.iconContent]' +
            '</nobr>'
        );

        return new ymaps.Placemark([obj.N_Latitude, obj.N_Longitude], {
                hintContent: 'Время: ' + getDate(obj.S_Date),
                data: obj,
                iconContent: obj.C_Fio,
                balloonContentHeader: obj.C_Fio,
                balloonContentBody: 'Время выхода на связь: ' + getDate(obj.S_Date) + '<br />Филиал: ' + obj.C_MainDivision + '<br />Отделение: ' + obj.C_Division + '<br />Участок: ' + obj.C_SubDivision + '<br />IMEI: ' + obj.uuid 
            },
            {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'images/spacer.png',
                // Размеры метки.
                iconImageSize: [30, 30],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-24, -24],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                //iconContentOffset: [-2, -5],
                // Макет содержимого.
                iconContentLayout: iconLayout
            });
    }

    /*
     * Создание точки с геопозиции по пользователю
     * @param id {string} идентификтаор
     * @param obj {any} данные
     * @param icon {string} путь к иконке
     * @param uuid {string} идентифкатор устройства
     * @param fio {string} имя пользователя
     */
    this.toGeoViaPoint = function (id, obj, icon, uuid, fio) {
        return new ymaps.Placemark([obj.N_Latitude, obj.N_Longitude], {
            hintContent: 'Время: ' + getDate(obj.S_Date) + '<br/>ФИО: ' + fio + '<br />IMEI: ' + uuid,
            data: obj
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#imageWithContent',
            // Своё изображение иконки метки.
            iconImageHref: icon,
            // Размеры метки.
            iconImageSize: [16, 16],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [0, 0],
            visible: false
        });
    }

    /**
     * преобразование в координату
     */
    this.toGeoPoint = function (obj) {
        return [obj.N_Latitude, obj.N_Longitude];
    }

    return this;
}