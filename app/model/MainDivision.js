﻿/*
 * филиал
 */
Ext.define('ARM.model.MainDivision', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'int' },
        { name: 'C_Name', type: 'string' },
        { name: 'N_Code', type: 'int' }
    ]
});