﻿/*
 * статистика по обходам
 */
Ext.define('ARM.model.StatCard', {
    extend: 'ARM.model.Base',

    fields: [
        { name: 'name', type: 'string' },
        { name: 'title', type: 'string' }, // наименование статистики (выполнено всего, за сегодня и т.д.)
        { name: 'person', type: 'number' }, // ФЛ
        { name: 'noperson', type: 'number' } // ЮЛ
    ]
});