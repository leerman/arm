﻿/*
 * данные о пользователе
 */
Ext.define('ARM.model.UserInfo', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    fields: [
        { name: 'LINK', type: 'string' },
        { name: 'C_Fio', type: 'string' },
        { name: 'Roles', type: 'string' }
    ]
});