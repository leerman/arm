﻿/*
 * статистика по документам
 */
Ext.define('ARM.model.GetDocStatistics', {
    extend: 'ARM.model.Base',
    idProperty: '',
    identifier: '',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'C_Name', type: 'string' },
        { name: 'B_Done', type: 'boolean' },
        { name: 'C_Status_Const', type: 'string' },
        { name: 'N_Total', type: 'int' },
        { name: 'N_Done', type: 'int' },
        { name: 'N_NotDone', type: 'int' },
        { name: 'N_Done_Today', type: 'int' }
    ]
});