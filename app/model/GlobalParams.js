﻿/*
 * настройки
 */
Ext.define('ARM.model.GlobalParams', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'uuid',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'C_Url', type: 'string' },
        { name: 'N_DefaultLatitude', type: 'number' },
        { name: 'N_DefaultLongitude', type: 'number' },
        { name: 'N_GeoInterval', type: 'int' },
        { name: 'B_ValidatorDigits', type: 'boolean' },
        { name: 'B_Remove_Record', type: 'boolean' },
        { name: 'C_Smtp_Host', type: 'string' },
        { name: 'C_Smtp_User', type: 'string' },
        { name: 'C_Smtp_Pwd', type: 'string' },
        { name: 'C_Smtp_From', type: 'string' },
        { name: 'N_Smtp_Port', type: 'int' },
        { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' }
    ]
});