﻿/*
 * файлы
 */
Ext.define('ARM.model.File', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'uuid',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'C_File_Name', type: 'string' },
        { name: 'D_Date', type: 'date' },
        { name: 'F_Doc_Details', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Number' },
        { name: 'F_Docs_Out', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Doc_Number' },
        { name: 'F_Type', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'N_Latitude', type: 'number' }, { name: 'N_Longitude', type: 'number' },
        { name: 'D_Coords_Date', type: 'date' }, { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' }
    ]
});