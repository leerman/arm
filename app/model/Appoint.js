﻿/*
 * задание отображаемое на карте
 */
Ext.define('ARM.model.Appoint', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [

        { name: 'Doc', type: 'string' }, // номер документа
        { name: 'DocStatus', type: 'string' }, // статус документа
        { name: 'PointAddress', type: 'string' }, // адрес расположения метки
        { name: 'DetailLinks', type: 'string' }, // идентификаторы строк документа, через запятую
        { name: 'UserFIO', type: 'string' }, // фио пользователя, который выполняет задание
        { name: 'DetailCount', type: 'number' },
        { name: 'F_Users', type: 'string' },
        { name: 'S_MainDivision', type: 'number' },
        { name: 'S_Division', type: 'number' },
        { name: 'S_SubDivision', type: 'number' },
    ],

    /**
     * устанавливаются координаты
     * @param coords {number[]} координаты
     */
    setCoord: function (coords) {
        if (coords && Array.isArray(coords)) {
            this.set('N_Latitude', coords[0]);
            this.set('N_Longitude', coords[1]);
        }
    },

    /**
     * Вернуть координаты
     */
    getCoord: function () {
        return [this.get('N_Latitude'), this.get('N_Longitude')]
    },

    /**
     * возвращается hash сумма по координатам
     */
    getCoordHash: function () {
        return md5(Ext.String.format('{0}x{1}', this.get('N_Latitude'), this.get('N_Longitude')));
    },

    /*
     * проверка на равенство координат
     * @param data {any} данные для проверки. Можно передать как массив координат, так и hash сумму
     */
    isCoordEqual: function (data) {
        if (Array.isArray(data)) {
            return this.getCoordHash() == md5(Ext.String.format('{0}x{1}', data[0], data[1]));
        } else if (typeof data == 'string') {
            return this.getCoordHash() == data;
        }

        return false;
    },

    /**
     * проверка на доступность координат
     */
    isCoordExist: function () {
        if (this.get('N_Latitude') > 0 && this.get('N_Longitude'))
            return true;

        return false;
    }
});