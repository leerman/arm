﻿Ext.define('ARM.model.Doc', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'uuid',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'F_Statuses', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'F_Types', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'D_Date', type: 'date' },
        { name: 'D_Date_End', type: 'date' },
        { name: 'C_Number', type: 'string' },
        { name: 'F_Users', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Fio' },
        { name: 'N_Order', type: 'int' },
        { name: 'B_Done', type: 'boolean' },
        //{ name: 'B_Received', type: 'boolean' }, // принят пользователем
        { name: 'S_Person', type: 'boolean' },
        { name: 'S_MainDivision', type: 'int' },
        { name: 'S_Division', type: 'int' },
        { name: 'S_SubDivision', type: 'int' },
        { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' },
        { name: 'DetailsCount', type: 'int' }
    ],

    proxy: {
        type: 'itdirect',
        api: {
            update: 'PN.Domain.DD_Docs.Update'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records'
        },
        writer: {
            //writeAllFields: true,
            dateFormat: "Y-m-d H:i:sO"
        }
    },
});