﻿/*
 * дата на которой присутствуют координаты
 */
Ext.define('ARM.model.GetGeoLocationDays', {
    extend: 'ARM.model.Base',
    idProperty: 'Date',

    fields: [
        { name: 'Date', type: 'date' },
        { name: 'Day', type: 'number' },
        { name: 'DayOfWeek', type: 'number' },
        { name: 'DayOfYear', type: 'number' },
        { name: 'Hour', type: 'number' },
        { name: 'Kind', type: 'number' },
        { name: 'Millisecond', type: 'number' },
        { name: 'Minute', type: 'number' },
        { name: 'Month', type: 'number' },
        { name: 'Second', type: 'number' },
        { name: 'Ticks', type: 'number' },
        { name: 'TimeOfDay', type: 'number' },
        { name: 'Year', type: 'number' }
    ]
});