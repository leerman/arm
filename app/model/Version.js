﻿Ext.define('ARM.model.Version', {
    extend: 'ARM.model.Base',
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'string' },
        { name: 'date', type: 'date' },
        { name: 'message', type: 'string' }
    ]
});