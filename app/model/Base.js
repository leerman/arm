Ext.define('ARM.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'ARM.model'
    }
});
