﻿/*
 * статистика выполненых работ
 */
Ext.define('ARM.model.GetStatistics', {
    extend: 'ARM.model.Base',
    idProperty: '',
    identifier: '',

    fields: [
        { name: 'C_Name', type: 'string' },
        { name: 'C_Const', type: 'string' },
        { name: 'N_Done_Today_EE', type: 'int' },
        { name: 'N_Done_Today_PE', type: 'int' },
        { name: 'N_Done_EE', type: 'int' },
        { name: 'N_Done_PE', type: 'int' },
        { name: 'N_Total_EE', type: 'int' },
        { name: 'N_Total_PE', type: 'int' },
        { name: 'N_NotDone_EE', type: 'int' },
        { name: 'N_NotDone_PE', type: 'int' },
        { name: 'N_Done_Today_All', type: 'int' },
        { name: 'N_Total_Today_All', type: 'int' },
        { name: 'N_Done_All', type: 'int' },
        { name: 'N_Total_All', type: 'int' }
    ]
});