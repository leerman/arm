﻿Ext.define('ARM.model.RES', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'C_Name', type: 'string' },
        { name: 'N_Code', type: 'int' },
        { name: 'F_Division', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'F_SubDivision', type: 'auto', valueField: 'LINK', displayField: 'C_Name' }
    ]
});