﻿/*
 * область обслуживания
 */
Ext.define('ARM.model.Region', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'number' },
        { name: 'C_Area', type: 'string' },
        { name: 'C_Color', type: 'string' },
        { name: 'C_Name', type: 'string' },
        { name: 'C_Note', type: 'string' },
        { name: 'F_Managers', type: 'auto', valueField: 'LINK', displayField: 'C_Fio' }
    ],

    getRegionInfo: function () {
        return {
            link: this.get('LINK'),
            displayName: this.get('C_Name'),
            description: this.get('C_Note'),
            color: this.get('C_Color'),
            coords: this.getRegionCoords(),
            users: this.get('F_Managers')
        }
    },

    getRegionCoords: function () {
        var coordStr = this.get('C_Area');
        if (Ext.isEmpty(coordStr))
            return [];

        var coords = [];
        coordStr.split(',').forEach(function (item) {
            coords.push(item.split(';'));
        });
        return coords;
    }
});