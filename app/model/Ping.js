﻿Ext.define('ARM.model.Ping', {
    extend: 'ARM.model.Base',

    idProperty: 'id',
    fields: [
        { name: 'id', type: 'number' },
        { name: 'Success', type: 'bool' }
    ]
});
