﻿/**
 * модель сообщения
 */
Ext.define('ARM.model.Message', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'string' },
        { name: 'text', type: 'string' },
        { name: 'created', type: 'number' },
        { name: 'modifed', type: 'number' },
        { name: 'check', type: 'bool' }, // просмотрено сообщение или нет
        { name: 'userBegin', type: 'string' },
        { name: 'userEnd', type: 'string' },
        { name: 'userNameBegin', type: 'string' },
        { name: 'userNameEnd', type: 'string' },
        { name: 'in', type: 'bool' }, // входящее сообщение
        { name: 'sended', type: 'bool' }, // сообщение было отправлено
        { name: 'status', type: 'string' }
    ]
});
