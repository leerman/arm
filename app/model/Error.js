﻿/*
 * журнал ошибок
 */
Ext.define('ARM.model.Error', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'negative',

    fields: [
        { name: 'LINK', type: 'int' },
        { name: 'S_Date', type: 'date' },
        { name: 'C_Level', type: 'string' },
        { name: 'C_Message', type: 'string' },
        { name: 'C_Exception', type: 'string' },
        { name: 'C_User', type: 'string' }
    ]
});