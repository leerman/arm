﻿// Показания
Ext.define('ARM.model.Meter', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'uuid',

    fields: [
        {name:'LINK', type:'string', isGuid:true},            
        { name: 'F_Doc_Details', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Number' },
        { name: 'C_Name', type: 'string' },
        { name: 'N_Value', type: 'number' },
        { name: 'N_Value_Prev', type: 'number' },
        { name: 'D_Date', type: 'date' },
        { name: 'D_Date_Prev', type: 'date' },
        { name: 'N_Digits', type: 'number' },
        { name: 'S_Quantity_Prev', type: 'number' },
        { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' }
    ]
});