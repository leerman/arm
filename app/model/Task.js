﻿/*
 * задание отображаемое на карте
 */
Ext.define('ARM.model.Task', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'string' },
        { name: 'Address', type: 'string' },
        { name: 'Appartament', type: 'string' },
        { name: 'PhoneIMEI', type: 'string' },
        { name: 'UserId', type: 'string' },
        { name: 'UserFIO', type: 'string' },
        { name: 'DocDate', type: 'date' }, // дата документа
        { name: 'DocDateEnd', type: 'date' }, // дата завершения документа
        { name: 'DetailDate', type: 'date' }, // дата строки документа
        { name: 'DocName', type: 'string' }, //Полное название документа
        { name: "Number", type: 'string' }, // номер документа
        { name: 'StatusConst', type: 'string' }, // статус выполнения документа
        { name: 'StatusName', type: 'string' },
        { name: 'TypeConst', type: 'string' },// наряд документа на...
        { name: 'TypeName', type: 'string' },
        { name: 'N_Latitude', type: 'number' },
        { name: 'N_Longitude', type: 'number' },
        { name: 'DocLink', type: 'string' },
        { name: 'IsDone', type: 'bool' }, // выполнение
        { name: 'Owner', type: 'string' }, // Потребитель
        { name: 'IsPerson', type: 'bool' }, // Физическое лицо
        { name: 'D_Done_Date', type: 'date' },
        { name: 'C_Subscr', type: 'string' },
        { name: 'C_Device_Number', type: 'string' },
        { name: 'B_Received', type: 'boolean' }, // принят пользователем
        { name: 'B_Overdue', type: 'boolean' }, // документ просрочен
        { name: 'F_Subscrs', type: 'string' }
    ],

    /**
     * устанавливаются координаты
     * @param coords {number[]} координаты
     */
    setCoord: function (coords) {
        if (coords && Array.isArray(coords)) {
            this.set('N_Latitude', coords[0]);
            this.set('N_Longitude', coords[1]);
        }
    },

    /**
     * Вернуть координаты
     */
    getCoord: function () {
        return [this.get('N_Latitude'), this.get('N_Longitude')]
    },

    /**
     * возвращается hash сумма по координатам
     */
    getCoordHash: function () {
        return md5(Ext.String.format('{0}x{1}', this.get('N_Latitude'), this.get('N_Longitude')));
    },

    /*
     * проверка на равенство координат
     * @param data {any} данные для проверки. Можно передать как массив координат, так и hash сумму
     */
    isCoordEqual: function (data) {
        if (Array.isArray(data)) {
            return this.getCoordHash() == md5(Ext.String.format('{0}x{1}', data[0], data[1]));
        } else if (typeof data == 'string') {
            return this.getCoordHash() == data;
        }

        return false;
    },

    /**
     * проверка на доступность координат
     */
    isCoordExist: function () {
        if (this.get('N_Latitude') > 0 && this.get('N_Longitude'))
            return true;

        return false;
    }
});