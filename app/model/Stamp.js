﻿/*
* Пломбы
*/
Ext.define('ARM.model.Stamp', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'F_Stamp_Types___C_Name', type: 'string' },
        { name: 'C_Seal_Number', type: 'string' },
        { name: 'F_Users___C_Fio', type: 'string' },
        { name: 'F_Users___C_Login', type: 'string' },
        { name: 'B_Use', type: 'boolean' }
    ]
});