﻿/*
 * статистика (общая)
 */
Ext.define('ARM.model.GetCommonStatisticsQuery', {
    extend: 'ARM.model.Base',

    fields: [
        { name: 'D_Date', type: 'date' },
        { name: 'F_Types', type: 'number' },
        { name: 'C_Const', type: 'string' },
        { name: 'N_Plan', type: 'number' },
        { name: 'N_Fact', type: 'number' }
    ]
});