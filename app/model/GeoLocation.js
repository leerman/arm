﻿/*
 * данные о гопозиции обходчика
 */
Ext.define('ARM.model.GeoLocation', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'string' },
        { name: 'N_Latitude', type: 'number' },
        { name: 'N_Longitude', type: 'number' },
        { name: 'S_Date', type: 'date' },
        { name: 'uuid', type: 'string' }
    ]
});