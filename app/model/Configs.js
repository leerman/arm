﻿
Ext.define('ARM.model.Configs', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'uuid',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'C_Name', type: 'string' },
        { name: 'C_Const', type: 'string' },
        { name: 'N_Code', type: 'number' },
        { name: 'B_Default', type: 'boolean' },
        { name: 'C_Prev_MS_Status', type: 'string' },
        { name: 'C_MS_Status', type: 'string' },
        { name: 'C_Work_Status', type: 'string' },
        { name: 'C_Done_Status', type: 'string' },
        { name: 'C_Timeout_Status', type: 'string' },
        { name: 'N_Seals_Mode', type: 'number' },
        { name: 'B_MainDivision', type: 'boolean' },
        { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' },
        { name: 'B_System_Edit', type: 'boolean' }
    ]
});