/*
* Model 
*/
Ext.define('ARM.model.Devices',
{
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    displayProperty: 'C_Number',
    identifier: 'uuid',

    fields: [
            /*Идентификатор*/
            {name:'LINK', type:'string', isGuid:true},
            /*F_Subscrs*/
            {name:'F_Subscrs', type:'auto', isGuid:true, valueField:'LINK', displayField:'C_Name'},
            /*Номер ПУ*/
            {name:'C_Number', type:'string'},
            /*Информация по измерительному комплексу*/
            {name:'C_Info', type:'string'},
            /*Дата установки*/
            {name:'D_Setup_Date', type:'date'},
            /*Дата гос.поверки*/
            {name:'D_Control_Date', type:'date'},
            /*Адрес*/
            { name: 'F_Subscrs___C_Address_Short', type: 'string' },
            /*Квартира*/
            {name:'N_Premise_Number', type:'string'},
            /*N_Premise_Number_INT*/
            {name:'N_Premise_Number_INT', type:'int'},
            /*Тип ПУ*/
            {name:'C_Device_Types', type:'string'},
            /*Коэффициент ПУ/Расчетный коэффициент ПУ */
            {name:'N_Rate', type:'number'},
            /*Признак: Трехфазный*/
            {name:'B_Phase3', type:'boolean'},
            /*Широта*/
            {name:'S_Latitude', type:'number'},
            /*Долгота*/
            {name:'S_Longitude', type:'number'},
            /*Дата создания записи*/
            {name:'S_Create_Date', type:'date'},
            /*Дата модификации записи*/
            {name:'S_Modif_Date', type:'date'},
            /*Создатель записи*/
            {name:'S_Creator', type:'string'},
            /*Автор изменений*/
            { name: 'S_Owner', type: 'string' },

            /*Номер лицевого счета*/
            { name: 'F_Subscrs___C_Code', type: 'string' },
            /*ФИО*/
            { name: 'F_Subscrs___C_Fio', type: 'string' },
            /*Линия низкого напряжения*/
            { name: 'C_NN_Line', type: 'string' },
            /*ТП*/
            { name: 'C_TP', type: 'string' },
            /* ПО */
            { name: 'F_Subscrs___S_Division___C_Name', type: 'string' },
            /* Участок */
            { name: 'F_Subscrs___S_SubDivision___C_Name', type: 'string' }

    ],

    
    validators:[
            {type:'presence', field:'B_Phase3'}
    ]
});
