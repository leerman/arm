﻿/*
* Бланки
*/
Ext.define('ARM.model.Blank', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'F_Doc_Types___C_Name', type: 'string' },
        { name: 'C_Blank_Number', type: 'string' },
        { name: 'F_Users___C_Fio', type: 'string' },
        { name: 'F_Users___C_Login', type: 'string' },
        { name: 'B_Use', type: 'boolean' }
    ]
});