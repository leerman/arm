﻿/*
 * пломбы
 */
Ext.define('ARM.model.ConnSeals', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    displayProperty: 'S_Number_Prev',
    identifier: 'uuid',

    fields: [
            { name: 'LINK', type: 'string', isGuid: true }
        , { name: 'F_Doc_Details', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Number' }
        , { name: 'S_Number_Prev', type: 'string' }
        , { name: 'S_Stamp_Types_Prev', type: 'auto', valueField: 'LINK', displayField: 'C_Name' }
        , { name: 'S_Seals_Prev', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Seal_Number' }
        , { name: 'S_Places_Prev', type: 'auto', valueField: 'LINK', displayField: 'C_Name' }
        , { name: 'F_Seals', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Seal_Number' }
        , { name: 'F_Stamp_Types', type: 'auto', valueField: 'LINK', displayField: 'C_Name' }
        , { name: 'F_Places', type: 'auto', valueField: 'LINK', displayField: 'C_Name' }
        , { name: 'S_Create_Date', type: 'date' }
        , { name: 'S_Modif_Date', type: 'date' }
        , { name: 'S_Creator', type: 'string' }
        , { name: 'S_Owner', type: 'string' }
    ],


    validators: [
    ]
});