﻿// ПО
Ext.define('ARM.model.Division', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'int' },
        { name: 'C_Name', type: 'string' },
        { name: 'N_Code', type: 'int' },
        { name: 'F_Division', type: 'auto', valueField: 'LINK', displayField: 'C_Name' }
    ]
});