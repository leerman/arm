﻿/*
* Model Тип документа
*/
Ext.define('ARM.model.ActType',
{
    /*BEGIN_GEN*/
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    displayProperty: 'C_Name',
    identifier: 'negative',

    fields: [
            /*Идентификатор*/
            { name: 'LINK', type: 'int' },
            /*Наименование*/
            { name: 'C_Name', type: 'string' },
            /*Константа*/
            { name: 'C_Const', type: 'string' },
            /*N_Code*/
            { name: 'N_Code', type: 'int' },
            /*Родительский тип документа*/
            { name: 'F_Parents', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
            /*Описание типа*/
            { name: 'C_Note', type: 'string' },
            /*Отключено*/
            { name: 'B_Disable', type: 'boolean' },
            /*Дата создания записи*/
            { name: 'S_Create_Date', type: 'date' },
            /*Дата модификации записи*/
            { name: 'S_Modif_Date', type: 'date' },
            /*Создатель записи*/
            { name: 'S_Creator', type: 'string' },
            /*Автор изменений*/
            { name: 'S_Owner', type: 'string' }
    ],


    validators: [
            { type: 'presence', field: 'B_Disable' }
    ]
    /*END_GEN*/
});
