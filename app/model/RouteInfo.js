﻿/*
 * Информация о маршруте (документе)
 */
Ext.define('ARM.model.RouteInfo', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'id', type: 'string' },
        { name: 'number', type: 'string' }, 
        { name: 'today', type: 'number' }, // выполнено сегодня
        { name: 'done', type: 'number' }, // всего выполнено
        { name: 'old', type: 'number' }, // осталось выполнить
        { name: 'user', type: 'string'} // идентификатор пользователя
    ]
});