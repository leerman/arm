﻿Ext.define('ARM.model.Brigade', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    identifier: 'negative',

    fields: [
        { name: 'LINK', type: 'int' },
        { name: 'C_Name', type: 'string' },
        { name: 'C_Note', type: 'string' },
        { name: 'F_Managers', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Fio' },
        { name: 'C_Area', type: 'string' },
        { name: 'C_Color', type: 'string' }
    ]
});