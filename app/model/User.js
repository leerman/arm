﻿/*
 * пользователи
 */
Ext.define('ARM.model.User', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'C_Family', type: 'string' },
        { name: 'C_Name', type: 'string' },
        { name: 'C_Otchestvo', type: 'string' },
        { name: 'C_Tab_Number', type: 'string' },
        { name: 'C_Telephone', type: 'string' },
        { name: 'C_Email', type: 'string' },
        { name: 'C_Note', type: 'string' },
        { name: 'F_Brigades', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'uuid', type: 'string' },
        { name: 'C_Icon_Color', type: 'string' },
        { name: 'C_Pwd', type: 'string' },
        { name: 'C_Login', type: 'string' },
        { name: 'F_Managers', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Fio' },
        { name: 'B_Admin', type: 'boolean' },
        { name: 'B_Manager', type: 'boolean' },
        { name: 'B_Chief', type: 'boolean' },
        { name: 'C_Fio', type: 'string' },
        { name: 'C_Photo', type: 'string' },

        // дополнительные поля 
        { name: 'N_Latitude', type: 'number' },
        { name: 'N_Longitude', type: 'number' },
        { name: 'S_Date', type: 'date' },

        { name: 'S_MainDivision___C_Name', type: 'string' },
        { name: 'S_Division___C_Name', type: 'string' },
        { name: 'S_SubDivision___C_Name', type: 'string' },
        { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' },

        { name: 'C_Mobile_Version', type: 'string' },
        { name: 'C_Mobile_Info', type: 'string' },
        { name: 'D_Last_Entry', type: 'string' },

        { name: 'N_Count', type: 'number'} // кол-во пройденных точек за указанный период
    ],

    /*
     * пользователь активен
     */
    isWorking: function () {
        return this.get('S_Date') != null;
    },

    /*
     * пользователь активен за сегодня
     */
    isWorkingToday: function () {
        if (this.get('S_Date')) {
            var date = this.get('S_Date');
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            var ds = dataProvider.getToday(new Date());

            var de = dataProvider.getTodayEnd(new Date());
            if (ds <= date && de >= date)
                return true;
        }
        return false;
    }
});