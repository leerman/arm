﻿/*
 * пользователи
 */
Ext.define('ARM.model.Subscr', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',

    displayProperty: 'C_Name',
    identifier: 'uuid',

    fields: [
            /*Идентификатор*/
            { name: 'LINK', type: 'string', isGuid: true },
            /*Фамилия*/
            { name: 'C_Family', type: 'string' },
            /*Имя*/
            { name: 'C_Name', type: 'string' },
            /*Отчество*/
            { name: 'C_Otchestvo', type: 'string' },
            /*Адрес*/
            { name: 'C_Address', type: 'string' },
            /*Короткий адрес*/
            { name: 'C_Address_Short', type: 'string' },
            /*№ помещения*/
            { name: 'C_Appart_Number', type: 'string' },
            /*Телефон*/
            { name: 'C_Telephone', type: 'string' },
            /*Электронная почта*/
            { name: 'C_Email', type: 'string' },
            /*ИНН*/
            { name: 'C_INN', type: 'string' },
            /*Описание*/
            { name: 'C_Note', type: 'string' },
            /*Отключено*/
            { name: 'B_Disable', type: 'boolean' },
            /*ФИО*/
            { name: 'C_Fio', type: 'string' },
            /*Филиал*/
            { name: 'S_MainDivision', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
            /*Отделение*/
            { name: 'S_Division', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
            /*Участок*/
            { name: 'S_SubDivision', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
            /*Дата создания записи*/
            { name: 'S_Create_Date', type: 'date' },
            /*Дата модификации записи*/
            { name: 'S_Modif_Date', type: 'date' },
            /*Создатель записи*/
            { name: 'S_Creator', type: 'string' },
            /*Автор изменений*/
            { name: 'S_Owner', type: 'string' },
            /*Номер ЛС*/
            { name: 'C_Code', type: 'string' },
            { name: 'B_Additional_Agreement', type: 'boolean' },
            { name: 'B_Power_Attorney', type: 'boolean' }
    ],


    validators: [
            { type: 'presence', field: 'B_Disable' },
            { type: 'presence', field: 'C_Fio' }
    ]
});