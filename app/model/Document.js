﻿/*
 * документ, маршрут
 */
Ext.define('ARM.model.Document', {
    extend: 'ARM.model.Base',
    idProperty: 'LINK',
    displayProperty: 'C_Number',
    identifier: 'uuid',

    fields: [
        { name: 'LINK', type: 'string', isGuid: true },
        { name: 'F_Statuses', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'F_Types', type: 'auto', valueField: 'LINK', displayField: 'C_Name' },
        { name: 'D_Date', type: 'date' },
        { name: 'D_Date_End', type: 'date' },
        { name: 'C_Number', type: 'string' },
        { name: 'F_Users', type: 'auto', isGuid: true, valueField: 'LINK', displayField: 'C_Fio' },
        { name: 'N_Order', type: 'int' },
        { name: 'B_Done', type: 'boolean' },
        { name: 'S_Person', type: 'boolean' },
        { name: 'S_MainDivision', type: 'int' },
        { name: 'S_Division', type: 'int' },
        { name: 'S_SubDivision', type: 'int' },
        { name: 'S_Create_Date', type: 'date' },
        { name: 'S_Modif_Date', type: 'date' },
        { name: 'S_Creator', type: 'string' },
        { name: 'S_Owner', type: 'string' }
    ]
});