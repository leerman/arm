﻿Ext.define('Core.ProgressBar', {
    extend: 'Ext.Panel',

    xtype: 'core-progressbar',

    bgcolor: 'black',
    color: 'white',
    textColor: 'blue',

    config: {
        layout: 'fit',
        hidden: true
    },

    viewModel: {
        data: {
            text: '',
            progress: 0
        }
    },

    constructor: function (cfg) {
        Ext.apply(this, cfg);

        this.items = {
            width: '100%',
            xtype: 'container',
            bind: {
                html: '<div style="position:relative;width:100%;height:100%;background-color:' + this.bgcolor + '" class="core-progressbar-container">'
                    + '<div style="position:absolute;left:0;width:{progress}%;background-color:' + this.color + ';z-index:1;height:100%;"></div>'
                    + '<div style="position:absolute;width:100%;height:100%;background-color:transparent;color:' + this.textColor + ';z-index:2">{text}</div>'
                    + '</div>'
            }
        };
        this.callParent(arguments);
    },

    setText: function (value) {
        if (value) {
            if (this.hidden == true)
                this.show();
        } else {
            this.hide();
        }
        this.getViewModel().set('text', value);
    },

    getText: function () {
        return this.getViewModel().get('text');
    },

    setProgress: function (value) {
        if (parseInt(value) != 0) {
            if (this.hidden == true)
                this.show();
        } else {
            this.hide();
        }
        this.getViewModel().set('progress', value);
    },

    getProgress: function () {
        return this.getViewModel().get('progress');
    }
});