﻿/*
 * пользовательский класс обработки ошибок
 */
Ext.define('Core.Exception', {
    alternateClassName: 'Exception',

    constructor: function (err) {
        if (err) {
            console.info('пользовательская ошибка: %s', err.message);
        }
        this.callParent(arguments);
    }
});