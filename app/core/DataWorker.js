﻿/*
 * компонент для работы с Worker
 */
Ext.define('Core.DataWorker', {
    extend: 'Ext.Component',

    requires: [
        'Core.Utilits'
    ],

    _worker: null, // воркер для обработки yandex данных
    callbacker: {}, // объект для обработки обратного вызова 

    /*
     * @param cfg {any} передаваемы параметр в конструктор
     * cfg.path - путь к файлу
     */
    constructor: function (cfg) {     
        if (!cfg.path)
            throw new Error('Для запуска worker требуется указать путь к файлу');

        this._worker = new Worker(cfg.path);
        var me = this;
        this._worker.onmessage = function (e) {
            me.onWorkerMessage(e);
        }
        this.callParent(arguments);
    },

    /*
     * запуск выполнения задачи
     * @param options {any} передаваемые данные
     * options.type - тип задания
     * options.data - данные
     * @param callback {()=>void} функция обратного вызова
     */
    onTask: function (options, callback) {
        var tid = Utilits.GetGUID();
        this._worker.postMessage({
            token: this.getToken(),
            type: options.type,
            data: options.data,
            tid: tid
        });

        this.callbacker[tid] = callback;
    },

    /*
     * 
     */
    destroy: function () {
        if (this._worker) {
            this._worker.terminate();
            this._worker = null;
        }
        this.callbacker = null;
        this.callParent(arguments);
    },

    privates: {
        /*
         * сообщение от воркера
         * @param e {any} данные
         */
        onWorkerMessage: function (e) {
            var tid = e.data[1];
            var data = e.data[0];
            var options = e.data[2];
            // здесь добавлена возможность вызвать обратный вызов при итерации. проверка сделана на возвращаемое значение
            // options - идентификатор итерации
            this.callbacker[tid](data, options);
            if (data)
                delete this.callbacker[tid];
        },

        /*
         * возвращается токен авторизации
         */
        getToken: function () {
            var application = Ext.getCurrentApp();
            if (application) {
                var authProvider = application.getAuthProvider();
                if (authProvider) {
                    var token = authProvider.getToken();
                    if (token) {
                        return token;
                    }
                }
            }

            return null;
        }
    }
});