﻿/*
 * провайдер для работы с авторизацией
 */
Ext.define('Core.AuthProvider', {
    alternateClassName: 'AuthProvider',
    extend: 'Ext.Component',

    requires: [
        'Core.Configuration'
    ],

    REMOTING_ADDRESS: '',

    /**
     * конструктор
     * обязательным параметром является REMOTING_ADDRESS
     */
    constructor: function (cfg) {
        Ext.apply(this, cfg);
        this.callParent(arguments);
    },

    /*
     * авторизован пользователь или нет
     */
    isAuthorize: function () {
        return localStorage.getItem('Authorize') === 'true';
    },

    /*
     * возвращается токен авторизации
     */
    getToken: function () {
        return localStorage.getItem('token');
    },

    /* при ajax-запросе добавляет в header данные об авторизации
    *  @param value - значения
    */
    setAuthrizeHeader: function (value) {
        Ext.Ajax.setDefaultHeaders({
            "Authorization": "Token " + value
        });
    },

    /* вход в систему
    *  @param login - логин
    *  @param password - пароль
    *  @param rememberMe - значение поля "запомнить меня"
    *  @param callback - функция обратного вызова
    */
    singIn: function (login, password, rememberMe, callback) {
        var me = this;
        me.sendPassToServer(login, password, rememberMe, login, callback);
    },

    /*
    * выход из системы
    */
    singOut: function () {
        this.setAuthorize();
        this.setAuthrizeHeader('');
    },

    /* надо ли авторизоваться еще раз
    *      false - надо
    *      true - не надо
    */
    getRememberMe: function () {
        return localStorage.getItem('rememberMe') == 'true';
    },

    /*
     * возвращается текущий авторизированный логин
     */
    getLogin: function(){
        return localStorage.getItem('login');
    },

    privates: {
        /**
        *   авторизация защищенными ключами
        */
        sendPassToServer: function (_login, _password, rememberMe, login, callback) {
            var me = this;
            Ext.Ajax.request({
                url: me.getAuthUrl(),
                method: 'POST',
                params: {
                    UserName: _login,
                    Password: _password
                },
                success: function (response) {
                    var meta = JSON.parse(response.responseText);
                    if (callback)
                        if (meta.token != null) {
                            me.setAuthorize(meta.token, login, rememberMe);
                            me.setAuthrizeHeader(meta.token);
                            callback({ success: true, token: meta.token });
                        }
                        else {
                            me.setAuthorize();
                            me.setAuthrizeHeader('');
                            var msg = 'Ошибка токена';
                            callback({ success: false, msg: msg });
                        }
                },
                failure: function (response) {
                    if (callback) {
                        var msg = 'Ошибка доступа к серверу';
                        if (response.status == 401)
                            msg = 'Неверный логин или пароль';
                        me.setAuthrizeHeader('');
                        callback({ success: false, msg: msg });
                    }
                }
            });
        },

        /* 
        * получить URL для авторизации
        */
        getAuthUrl: function () {
            return Ext.String.format(Ext.getConf('RPC_URL').replace('/rpc', '/auth'), this.REMOTING_ADDRESS);
        },

        /** 
        * аутентификация (добавление данных о пользователе в хранилище)
        *  @param token - токен
        *  @param login - логин пользователя
        *  @param rememberMe - значение поля "запомнить меня"
        */
        setAuthorize: function (token, login, rememberMe) {
            if (token) {
                localStorage.setItem('Authorize', true);
                localStorage.setItem('token', token);
                if (rememberMe != undefined)
                    localStorage.setItem('rememberMe', rememberMe == 'on' ? true : rememberMe);
                else
                    localStorage.setItem('rememberMe', false);
                localStorage.setItem('login', login);
            } else {
                localStorage.setItem('Authorize', false);
                localStorage.setItem('token', '');
                localStorage.setItem('rememberMe', false);
                localStorage.setItem('login', '');
            }
        }
    }
});