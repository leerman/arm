﻿/*
 * класс со служебными функциями
 */
Ext.define('Core.Utilits', {
    alternateClassName: 'Utilits',

    singleton: true,
    /**
     * возвращается уникальный идентификатор
     */
    GetGUID: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
        });
        return uuid;
    },
    /**
     * найти общие элементы в массивах
     */
    intersecArrays: function (arrA, arrB) {
        var id = 0,
            Rez = [];
        for (var i = 0; i < arrB.length; i++)
        {
            id = arrA.indexOf(arrB[i]);
            if (id >= 0) Rez.push(arrB[i]);
        }
        return Rez;
    }
});