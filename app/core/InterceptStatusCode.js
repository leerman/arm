﻿/*
 * Класс для перехвата статус кода в запросе ответа
 */
Ext.define('Core.InterceptStatusCode', {
    /*
     * имя события при статус коде 401
     */
    REQUIRED_AUTH_EVENT_NAME: 'requiredauth',
    /*
     * событие при возвращении статуса
     */
    RESPONSE_STATUS_EVENT_NAME: 'responsestatus',

    /*
     * событие для 500 ошибки
     */
    SERVER_ERROR_EVENT_NAME: 'servererror',

    _debugMode: false,

    /*
     * приложение рботает в режиме debug
     */
    isDebugMode: function () {
        return this._debugMode;
    },

    /*
     * обработка сообщения для режима разработки
     * @param message {string} сообщение
     */
    _debug: function (message) {
        if (this.isDebugMode())
            console.info(message);
    },

    /*
     * обработчик статуса
     * @param status {string} статус запроса
     * @param xhr {any} результат запрос
     */
    handlerStatus: function (status, xhr) {
        var eventName = this.RESPONSE_STATUS_EVENT_NAME;
        switch (status.toString()) {
            case '401': // требуется авторизация
                Ext.getBody().fireEvent(this.REQUIRED_AUTH_EVENT_NAME, status, this.getViewNameFromUrl(xhr.responseURL || ''));
                this._debug('Требуется авторизация');
                break;

            case '500':
            case '0':
            case 'false':
                Ext.getBody().fireEvent(this.SERVER_ERROR_EVENT_NAME, status);
                break;
        }
        Ext.getBody().fireEvent(eventName, status);
    },

    /*
     * метод возвращает имя представлениях из адресной строки
     * @param url {string} адресная строка
     */
    getViewNameFromUrl: function (url) {
        var i = url.indexOf('?');
        if (i >= 0) {
            var j = url.lastIndexOf('/');
            if (j >= 0) {
                var viewName = url.substr(j + 1, i - (j + 1));
                return viewName.replace('.js', '');
            }
        }

        return null;
    },

    /*
     * добавление токена в url-адрес
     * @param url {string} адрес
     * @param token {string} токен авторизации
     */
    addTokenToUrl: function (url, token) {
        if (token && url) {
            if (url.indexOf('?') >= 0)
                url += '&access_token=' + token;
            else
                url += '?access_token=' + token;
        }

        return url;
    },

    /*
     * переопределение метода Ext.Boot.fetch
     * @param access_token {string} токен авторизации
     */
    overrideBootFetch: function (access_token) {
        var me = this;
        this._debug('Переопределение метода Ext.Boot.fetch');
        Ext.Boot.fetch = function (url, complete, scope, async) {
            async = (async === undefined) ? !!complete : async;
            var xhr = new XMLHttpRequest(),
                result, status, content,
                exception = false,
                readyStateChange = function () {
                    if (xhr && xhr.readyState == 4) {
                        status = (xhr.status === 1223) ? 204 : (xhr.status === 0 && ((self.location || {}).protocol === 'file:' || (self.location || {}).protocol === 'ionp:')) ? 200 : xhr.status;
                        me.handlerStatus(status, xhr);

                        content = xhr.responseText;
                        result = {
                            content: content,
                            status: status,
                            exception: exception
                        };
                        if (complete) {
                            complete.call(scope, result);
                        }
                        xhr = null;
                    }
                };
            if (async) {
                xhr.onreadystatechange = readyStateChange;
            }
            try {
                url = me.addTokenToUrl(url, access_token);
                me._debug("fetching " + url + " " + (async ? "async" : "sync"));
                xhr.open('GET', url, async);
                xhr.send(null);
                var _status = xhr.status;
                switch (xhr.status) {
                    case 404:
                        throw new Error('Файл не найден');
                }
            } catch (err) {
                exception = err;
                readyStateChange();
                return result;
            }
            if (!async) {
                readyStateChange();
            }
            return result;
        };
    },

    /*
     * переопределение метода Ext.Boot.Entry.evaluateLoadElement
     * @param access_token {string} токен авторизации
     */
    overrideBootEntry: function (access_token) {
        var me = this;
        this._debug('Переопределение метода Ext.Boot.Entry.evaluateLoadElement');
        Ext.Boot.Entry.prototype.evaluateLoadElement = function () {
            var el = this.getElement();
            el.src = me.addTokenToUrl(el.src, access_token);

            Ext.Boot.getHead().appendChild(this.getElement());
        };
    }
});