﻿/**
 * информация о текущем пользователя
 */
Ext.define('Core.User', {
    extend: 'Ext.Component',

    data: null,

    constructor: function (cfg) {
        Ext.apply(this, cfg);
        this.callParent(arguments);
    },

    /**
     * возвращается имя пользователя
     */
    getUserName: function () {
        return this.data.C_Fio;
    },

    /*
     * возвращается идентификатор
     */
    getId: function () {
        return this.data.LINK
    },

    /*
     * является администратором
     */
    isAdmin: function () {
        return this.data.Roles.indexOf('admin') >= 0;
    },

    /*
     * возвращается список ролей
     */
    getRoles: function () {
        return (this.data.Roles || '').toLowerCase().split(',');
    }
});