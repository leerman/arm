﻿Ext.define('Core.data.proxy.Direct', {
    extend: 'Ext.data.proxy.Direct',
    alias: 'proxy.itdirect',

    EXCEPTION_EVENT_NAME: 'exception',

    isMessagerInit: false, // была произведена инициализация сообщений
    /*
     * Метод для проверки запроса на success:true
     */
    isSuccess: function (response) {
        if (response.meta && response.meta.success === false)
            return false;
        else
            return true;
    },

    extractResponseData: function (response) {
        
        // --
        this.isMessagerInit = false;
        if (!this.isSuccess(response)) {
            this.showError(response.meta);
            this.isMessagerInit = true;
        }
        //!--

        return Ext.isDefined(response.result) ? response.result : response.data;
    },

    /*
     * Вывести текст ошибки
     * @param meta {any} мета данные результат запроса
     */
    showError: function (meta) {
        Ext.Msg.show({
            title: 'Ошибка',
            message: '<div>' + meta.msg + '<br /><div class="fullMsg" >' + meta.fullMsg + '</div></div>',
            buttons: Ext.Msg.OK,
            icon: Ext.Msg.ERROR,
            fn: function (btn) {

            }
        });
    },

    processResponse: function (success, operation, request, response) {
        var me = this,
            exception, reader, resultSet;

        Ext.getCurrentApp().fireEvent('rpcresult', response, operation);
        if (this.isSuccess(response) == true
            && me.isSimpleProperty(response.result) == true) {
            // подмена объекта
            var obj = response.result;
            response.result = {
                records: [
                    obj
                ],
                success: true,
                total: 1
            };
        }

        // определяем статус и передаем его обработчику

        var code = response.xhr ? response.xhr.status : response.status;

        Ext.create('Core.InterceptStatusCode', {}).handlerStatus(code, response.xhr);

        me.fireEvent('beginprocessresponse', me, response, operation);

        if (success === true) {
            reader = me.getReader();
            if (response.status === 204) {
                resultSet = reader.getNullResultSet();
            } else {
                resultSet = reader.read(me.extractResponseData(response), {

                    recordCreator: operation.getRecordCreator()
                });
                //--
                if (!this.isSuccess(response)) {
                    resultSet.success = false;
                }
                //!--
            }
            operation.process(resultSet, request, response);
            exception = !operation.wasSuccessful();
        } else {
            me.setException(operation, response);
            exception = true;
        }
        if (exception) {
            me.fireEvent('exception', me, response, operation);
            Ext.getBody().fireEvent(me.EXCEPTION_EVENT_NAME, me, success, response);
        }
        me.afterRequest(request, success);

        me.fireEvent('endprocessresponse', me, response, operation);
    },

    /**
    метод для проверки возвращенного резальтата
    @param result {any} результат выполнения
    */
    isSimpleProperty: function (result) {
        if (typeof result != 'object')
            return true;

        return false;
    },

    doRequest: function (operation) {
        if (!window['PN']) { // здесь проверка на наличие RPC методов
            Ext.getCurrentApp().fireEvent('norpc');
        }

        this.callParent(arguments);

        if (window['PN']) {
            Ext.getCurrentApp().fireEvent('rpc', operation);
        }
    }
}); 