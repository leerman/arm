﻿/*
 * для установки параметра select в прокси
 */
Ext.define('Core.data.Selectable', {
    
    selectQueryables: null,

    /*
     * возвращается список полей который требуется вернуть
     */
    getSelectFields: function () {
        this.writeSelectQuery();
        return this.getSelectQueryableToString(this.getSelectQueryables());
    },


    privates: {
        /*
         * возвращается строка для добавления в select параметр
         */
        getSelectQueryableToString: function (items) {
            var result = '';
            if (items) {
                Ext.each(items, function (item) {
                    if (item.alias) {
                        result += '(' + item.dataIndex + ') as ' + item.alias + ',';
                    } else {
                        result += item.dataIndex + ','
                    }
                });
            }

            return result.substr(0, result.length - 1);
        },
        /*
         * возвращается список Select - полей
         */
        getSelectQueryables: function () {
            this.selectQueryables = this.selectQueryables ? this.selectQueryables : [];
            return this.selectQueryables;
        },

        writeSelectQuery: function () {
            var me = this;
            Ext.each(me.getModel().getFields(), function (field) {
                me.pushField(field);
            });
        },

        pushField: function (field) {
            var me = this;
            var items = this.getSelectQueryables();
            items.push({
                dataIndex: field.getName(),
                alias: null
            });
        }
    }
});