﻿/*
 * Справочник команд
 */
Ext.define('Core.CommandHelp', {
    extend: 'Ext.window.Window',
    width: '50%',
    height: '50%',
    layout: 'fit',
    closable: true,
    autoShow: true,
    defaultListenerScope: true,

    title: 'Справочник команд',

    items: [
        {
            xtype: 'panel',
            ui: 'white',
            bodyPadding: 10,
            itemId: 'body'
        }
    ],

    listeners: {
        afterrender: 'onAfterRender'
    },

    /*
     * 
     */
    onAfterRender: function (sender) {
        var body = this.down('#body');
        this.getCommands(function (commands) {
            var html = '';
            Ext.each(commands, function (command) {
                html += '<p><b>'+command.group+'</b></p><ul>';
                Ext.each(command.items, function (item) {
                    html += '<li><i>' + item.cmd + '</i> - ' + item.desc + '</li>';
                });
                html += '</ul>';
            });

            body.setHtml(html);
        });
    },

    privates: {
        /*
         * возвращается список команд 
         * @param callback {any} функция обратного вызова
         */
        getCommands: function (callback) {
            Ext.Ajax.request({
                url: Ext.getCurrentApp().toAbsolutePath('resources/commands.json'),

                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    if (typeof callback == 'function') {
                        callback(obj);
                    }
                },

                failure: function (response, opts) {
                    Ext.Msg.show({
                        title: 'Ошибка',
                        message: 'Файл для чтения команд не найден',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR,
                    });
                }
            });

        }
    }
});