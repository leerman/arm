﻿/**
 * вывод маски с динамичными данными
 * 
 * пример использования:
 * Ext.create('Core.DynamicMask', {
        title: 'Ожидание...',
        functions: [
            {
                msg: 'Добавлена информация о пользователе',
                action: function (callback) {
                    me.setUserInfo(callback);
                },
            },
            {
                msg: 'Обновлен список пунктов главного меню',
                action: function (callback) {
                    me.getInitView().lookupReference('app-mainmenu').loadMenuItems(callback);
                }
            }
        ],
        callback: function () {
            this.destroy();
        }
    });
 */
Ext.define('Core.DynamicMask', {
    extend: 'Ext.window.Window',
    xtype: 'dynamic-mask',
    cls: 'dynamic-mask',
    requires: [
        'Ext.ProgressBar'
    ],

    closable: false,
    modal: true,
    minWidth: 400,
    minHeight: 150,
    iconCls: 'x-fa fa-spinner',
    draggable: false,
    resizable: false,
    wait: true, // добавлена задержка выполнения операции
    waitInterval: 1000, // интервал ожидания для вывода информации

    bar: null,
    functions: [], // список задач который надо выполнить
    inc: 0, // храниться текущая пройденная итерация

    layout:{
        type: 'vbox',
        pack: 'start'
    },

    defaults: {
        width: '100%'
    },

    constructor: function (cfg) {
        cfg.items = [
            {
                xtype: 'progressbar'
            },
            {
                cls: 'message-info',
                xtype: 'container',
                html: 'Выполнение операции может занять некоторое время. Дождитесь её завершения.',
                flex: 1
            }
        ];
        Ext.apply(this, cfg);

        cfg.autoShow = !this.wait;

        this.callParent(arguments);
        this.getBar().updateProgress(0, 'Обработка данных...', true);
        var me = this;

        var timeOut = null;
        if (this.wait == true) {
            timeOut = setTimeout(function () {
                me.show();
            }, this.waitInterval);
        }

        var tasks = new It.Core.Thread.Tasks(this);
        Ext.each(this.getFunctions(), function(item){
            tasks.Add(function (sender, callback, options) {
                options.action(function () {
                    me.updateText(options.msg);
                    callback();
                });
            }, null, item);
        });

        tasks.Run(function () {
            if (typeof me.callback == 'function')
                me.callback();

            if (timeOut)
                clearTimeout(timeOut);
            me.destroy();
        },cfg.sync||false);
    },

    privates: {
        /*
         * Обновить текст сообщения
         * @param msg {string} текст сообщения
         */
        updateText: function (msg) {
            this.inc++;

            this.getBar().updateProgress(((this.inc * 100 / this.getFunctions().length) / 100), msg, true);
        },

        /**
         * возвращается список функция для обработки
         */
        getFunctions: function () {
            return this.functions;
        },

        /*
         * возвращается объект по текущему состоянию
         */
        getBar: function () {
            if (!this.bar)
                this.bar = this.down('progressbar');

            return this.bar;
        }
    }
});