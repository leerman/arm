﻿Ext.define('Core.Meta', {
    alternateClassName: 'Meta',

    requires: [
        'Ext.direct.RemotingProvider'
    ],

    singleton: true,

    privates:{
        /*
         * Установка параметров для Ext.Direct
         * @param configuration {any} пользовательские настройки
         * @param callback { () => void } функция обратного вызова
         */
        setDirectProvider: function (configuration, callback) {
            var me = this;

            Ext.Ajax.request({
                url: Ext.String.format(configuration.get('RPC.REMOTING_API'), configuration.get('REMOTING_ADDRESS')),

                success: function (response, opts) {
                    var text = response.responseText;
                    var data = JSON.parse(text);

                    if (!data.meta || data.meta.success === true) {
                        data.url = Ext.String.format(configuration.get('RPC_URL'), configuration.get('REMOTING_ADDRESS'));
                        me.createDirect(data, configuration);
                    }

                    if (callback)
                        callback(response);
                },

                failure: function (response, opts) {
                    new Exception(new Error('Ошибка при чтение мета данных. ' + response.statusText));
                    if (typeof callback == 'function')
                        callback(response);
                }
            });
        },

        /*
         * Создание Direct'а
         * @param data {any} данные считанные из настроек
         */
        createDirect: function (data, configuration) {
            Ext.ns(configuration.get('REMOTE_NAMESPACE'));
            Ext.Direct.addProvider(data);
        }
    },
    /*
     * Загрузка метаданных
     * @param configuration {any} данные считанные из настроек
     */
    loadMetaData: function (configuration, callback) {
        var me = this;
        Ext.Loader.setPath(configuration.get('REMOTE_NAMESPACE'), Ext.String.format(configuration.get('REMOTE_DATA_URL'), configuration.get('REMOTING_ADDRESS')));
        this.setDirectProvider(configuration, function (response) {
            if (callback)
                callback(response.status);
        });
    }
}); 