﻿/*
 * плагин для установки значения фильтра по умолчанию
 */
Ext.define('Core.DefaultFilters', {
    extend: "Ext.AbstractPlugin",
    alias: "plugin.defaultfilters",

    init: function (component) {
        var store = component.getStore();
        store.on('beforeLoad', this.onBeforeLoad, this);
    },

    destroy: function () {
        var component = this.getCmp();
        var store = component.getStore();
        store.un('beforeLoad', this.onBeforeLoad, this);
        this.callParent(arguments);
    },

    privates: {
        /*
         * обработчик для обработки до загрузки данных
         */
        onBeforeLoad: function (store, operation, eOpts) {
            var filters = operation.getFilters();

            var grid = this.getCmp();
            if (grid && grid.getDefaultParams) {
                var defaultParams = grid.getDefaultParams();

                var _filter = [];
                if (filters) {
                    Ext.each(filters, function (i) {
                        var f = {
                            property: i.getProperty(),
                            value: i.getValue(),
                            operator: i.getOperator()
                        };
                        _filter.push(f);
                    });
                }

                if (defaultParams && defaultParams.filter) {
                    Ext.each(defaultParams.filter, function (i) {
                        _filter.push(i);
                    });
                }

                operation.setParams({
                    filter: _filter
                });
            }
        }
    }
});