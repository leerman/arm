﻿/*
 * миксин класс для установки безопасности
 */
Ext.define('Core.security.Security', {

    /*
     * есть доступ к разделу или нет
     */
    isAccess: function () {
        var app = Ext.getCurrentApp();
        return app.getSecurity().isAccess();
    }

});