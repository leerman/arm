﻿/*
 * чтение файла безопасности
 */
Ext.define('Core.security.Reader', {
    extend: 'Ext.Component',

    url: 'resources/security.json',

    /*
     * 
     */
    constructor: function (cfg) {
        Ext.apply(this, cfg);
        
        this.callParent(arguments);
    },

    
    /*
     * возвращается список безопасности
     */
    getSecurity: function () {
        return this.security;
    },

    /*
     * есть доступ к разделу или нет
     */
    isAccess: function () {
        var app = Ext.getCurrentApp();
        var security = this.getSecurity();
        var user = app.getUser();

        if (!user) {
            return true;
        }

        var hash = location.hash;
        var items = security.filter(function (i) { return i.part == hash; });
        if (items.length > 0) {
            var username = user.getUserName();
            var roles = user.getRoles();
            var access = false;

            Ext.each(items, function (i) {
                var security_roles = i.roles;
                var security_users = i.users;

                switch (i.access) {
                    case 'allow': // разрешить
                        if (security_roles.length > 0) {
                            for (var j = 0; j < security_roles.length; j++) {
                                for (var x = 0; x < roles.length; x++) {
                                    if (roles[x].indexOf(security_roles[j]) >= 0) {
                                        access = true;
                                    }
                                }
                            }
                        }
                        if (security_users.length > 0) {
                            for (var j = 0; j < security_users.length; j++) {
                                if (username.indexOf(security_users[j]) >= 0) {
                                    access = true;
                                }
                            }
                        }
                        break;

                    case 'deny': // запретить
                        if (security_roles.length > 0) {
                            for (var j = 0; j < security_roles.length; j++) {
                                for (var x = 0; x < roles.length; x++) {
                                    if (roles[x].indexOf(security_roles[j]) >= 0) {
                                        access = false;
                                    }
                                }
                            }
                        }
                        if (security_users.length > 0) {
                            for (var j = 0; j < security_users.length; j++) {
                                if (username.indexOf(security_users[j]) >= 0) {
                                    access = false;
                                }
                            }
                        }
                        break;
                }
            });

            return access;
        }

        return true;
    },

    privates: {

        security: [],
        /*
         * устанавливается список безопасности
         */
        setSecurity: function (items) {
            this.security = items;
        },

        /*
         * возвращается адрес чтения безопасности
         */
        getUrl: function () {
            return this.url;
        }
    }
});