﻿/**
 * объект для перехвата команд и их выполнения
 */
Ext.define('Core.Chiter', {
    extend: 'Ext.Component',

    requires: [
        'Core.RequestQueryTesting',
        'Core.CommandHelp'
    ],

    // команда
    command: null,

    constructor: function () {
        this.callParent(arguments);
        var body = Ext.getBody();
        if (body) {
            body.on('keydown', this.onKeyDown, this);
        }
        console.debug('Механизм перехвата кодов включен');
    },

    /*
     * выполнения команды
     */
    runCommand: function (command) {
        console.debug('Команда: ' + command);
        switch (command.replace('~', '')) {
            case 'rpctest':
                this.openRPCTest();
                this.clearCommand();
                this.onRunnedCommand();
                break;

            // создает тестовую ошибку
            case 'testerror':
                throw new Error('Тестовая ошибка в диспетчере');
                this.onRunnedCommand();
                break;

            // разрешает отправку локальных ошибок
            case 'localhostsending':
                window['localhostsending'] = true;
                this.onRunnedCommand();
                break;

            // панель для ввода команд 
            case 'terminal':
                Ext.getCurrentApp().getInitView().down('app-command-console').show();
                this.onRunnedCommand();
                break;

                // диагностика
            case 'diagnostic':
                Ext.getCurrentApp().getInitView().down('app-diagnostic').start();
                this.onRunnedCommand();
                break;

            case 'authreset':
                localStorage.setItem('token', '1122');
                Ext.getCurrentApp().getAuthProvider().setAuthrizeHeader('tfdsjand');
                this.onRunnedCommand();
                break;

            case 'terminal start':
                localStorage.setItem('terminal', true);
                this.onRunnedCommand();
                break;

            case 'terminal stop':
                localStorage.removeItem('terminal');
                this.onRunnedCommand();
                break;

            case 'appinfo':
                Ext.create('Core.AppInfo').show();
                this.onRunnedCommand();
                break;

            case 'mobileversion':
                Ext.create('MobileService.view.stat.MobileService').show();
                this.onRunnedCommand();
                break;

            case 'help':
                Ext.create('Core.CommandHelp').show();
                this.onRunnedCommand();
                break;
        }
    },

    destroy: function () {
        Ext.getBody().un('keydown', this.onKeyDown, this);
        this.callParent(arguments);
    },

    privates: {
        /*
         * обработчик выполнения команды
         */
        onRunnedCommand: function () {
            this.command = null;
        },
        /**
         * очистка команды
         */
        clearCommand: function () {
            this.command = '';
        },
        /*
         * обработчик нажатия клавиши
         */
        onKeyDown: function (e, t, eOpts) {
            var key = e.getKeyName();
            if (e.keyCode == 192) { // это символ ~
                this.command = '';
            }
            else {
                try {
                    if (key.length == 1 && this.command != null)
                        this.command += key.toLowerCase();
                } catch (ex) {

                }
            }
            if (this.command) {
                this.runCommand(this.command);
            }
        },

        /**
         * открыть тестирование rpc запросов
         */
        openRPCTest: function () {
            Ext.create('Ext.window.Window', {
                bodyPadding: 10,
                width: '100%',
                height: '100%',
                layout: 'fit',
                closable: true,
                autoShow: true,
                title: 'Тестирование RPC-запросов',
                items: [
                    {
                        xtype: 'diagnostic-requestquery'
                    }
                ]
            });
        }
    }
});