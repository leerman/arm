﻿/*
 * окно для вывода информации о приложении
 */
Ext.define('Core.AppInfo', {
    extend: 'Ext.window.Window',

    modal: true,
    resizable: false,
    maximizable: false,
    minimizable: false,
    closeAction: 'method-destroy',
    width: 600,
    height: 400,
    defaultListenerScope: true,
    layout: 'fit',
    title: 'Настройки системы',
    viewModel: {
        data: {
            txt: ''
        }
    },

    constructor: function (cfg) {
        this.callParent(arguments);

        var app = Ext.getCurrentApp();
        var config = app.getConfiguration();
        var data = config.getData();
        var comments = config.getComments();
        var html = '<div style="padding:5px">';
        for (var i in data) {
            html += '<div><b>' + i + ':</b> [<i style="text-decoration:underline">' + data[i] + '</i>] - ' + (comments[i] || 'комментарий не указан') + ' </div>';
        }
        html += '</div>';
        this.getViewModel().set('txt', html);
    },

    items: [
        {
            scrollable: true,
            xtype: 'panel',
            ui: 'white',
            bind: {
                html: '{txt}'
            }
        }
    ]
});