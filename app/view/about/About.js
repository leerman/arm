﻿Ext.define('ARM.view.about.About', {
    extend: 'Ext.Container',
    xtype: 'app-about',

    requires: [
        'ARM.view.about.AboutController',
        'ARM.view.about.AboutModel'
    ],

    controller: 'about',
    viewModel: 'about',

    html: 'About',

    destroy: function () {
        console.log('destroy');
        this.callParent(arguments);
    }
});