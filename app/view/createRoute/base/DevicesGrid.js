﻿/*
 * Грид для вывода списка ПУ
 */
Ext.define('ARM.view.createRoute.base.DevicesGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-devices-base-grid',
    cls: 'app-task-items white-scroll',
    loadMask: true,
    title: 'Устройства',

    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeStatFilterHeight: getResponsiveHeight(),
        smallStatFilterHeight: '!largeStatFilterHeight',
        largeStatFilterWidth: getResponsiveWidth(),
        smallStatFilterWidth: '!largeStatFilterWidth'
    },

    responsiveConfig: {
        'largeStatFilterHeight && largeStatFilterWidth': {
            bodyStyle: {
                padding: '10px'
            }
        },
        'smallStatFilterHeight || smallStatFilterWidth': {
            bodyStyle: {
                padding: '0 10px 10px 10px'
            }
        }
    },

    storeName: 'ARM.store.Devices',
    store: null,
    emptyText: 'Нет приборов учета',
    selModel: {
        selType: 'checkboxmodel',
        mode: 'MULTI',
        showHeaderCheckbox: true,
        style: 'margin-left:14px;'
    },

    plugins: [{
        ptype: 'gridfilters'
    }],

    columns: {
        defaults: {
            hideable: false,
            sortable: true,
            draggable: false,
            align: 'center',
            filter: true
        },
        items: [
            { xtype: 'datecolumn', dataIndex: 'S_Create_Date', text: 'Дата создания записи', format: 'd.m.Y', hidden: true },
            { xtype: 'datecolumn', dataIndex: 'S_Modif_Date', text: 'Дата модификации записи', format: 'd.m.Y', hidden: true },
            { dataIndex: 'S_Creator', text: 'Создатель записи', hidden: true },
            { dataIndex: 'S_Owner', text: 'Автор изменений', hidden: true },
            { dataIndex: 'F_Subscrs___C_Code', text: 'Номер ЛС' },
            { dataIndex: 'F_Subscrs___C_Fio', text: 'Владелец', filter: true, flex: 1, minWidth: 200 },
            { dataIndex: 'F_Subscrs', text: 'F_Subscrs', filter: true, hidden: true },
            { dataIndex: 'C_Number', text: 'Номер ПУ', filter: true, hidden: false },
            { xtype: 'datecolumn', dataIndex: 'D_Setup_Date', text: 'Дата установки', format: 'd.m.Y', filter: true, hidden: true },
            { xtype: 'datecolumn', dataIndex: 'D_Control_Date', text: 'Дата контрольной проверки', format: 'd.m.Y', filter: true, hidden: false },
            { dataIndex: 'C_NN_Line', text: 'Линия НН', filter: false },
            { dataIndex: 'C_TP', text: 'ТП', filter: false },
            { dataIndex: 'F_Subscrs___C_Address_Short', text: 'Адрес', filter: false, minWidth: 150 },
            { dataIndex: 'F_Subscrs___S_Division___C_Name', text: 'Филиал', filter: false, minWidth: 150 },
            { dataIndex: 'F_Subscrs___S_SubDivision___C_Name', text: 'РЭС', filter: false,  flex: 1, minWidth: 250 },
            { dataIndex: 'C_Device_Types', text: 'Тип ПУ', filter: true },
            { xtype: 'numbercolumn', dataIndex: 'N_Rate', text: 'Коэффициент ПУ/Расчетный коэффициент ПУ ', filter: true, hidden: true },
            { xtype: 'booleancolumn', dataIndex: 'B_Phase3', text: 'Признак: Трехфазный', filter: true, hidden: true, trueText: 'Да', falseText: 'Нет' },
            { xtype: 'numbercolumn', dataIndex: 'S_Latitude', text: 'Широта', filter: true, hidden: true },
            { xtype: 'numbercolumn', dataIndex: 'S_Longitude', text: 'Долгота', filter: true, hidden: true }
        ]
    },

    constructor: function (cfg) {
        Ext.apply(this, cfg);
        this.store = Ext.create(this.storeName, { pageSize: 1000 });
        this.callParent(arguments);
    },

    listeners: {
        render: function (sender) {
            this.loadGrid();
        },
    },

    /**
     * Загрузить данные в грид
     */
    loadGrid: function (filter) {
        if (!filter)
            filter = [];

        var store = this.getStore();

        if (store) {
            store.load({
                params: {
                    filter: filter,
                    select: store.getSelectFields()
                }
            });
        }
    },

    /**
     * Убрать выделения со всех элементов грида
     */
    unselect: function () {
        this.setSelection(null);
    }
});