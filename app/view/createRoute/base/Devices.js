﻿/*
 * Администрирование
 */
Ext.define('ARM.view.createRoute.base.Devices', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-devices-base',
    defaultListenerScope: true,

    requires: [
        'ARM.view.createRoute.base.DevicesGrid'
    ],

    ui: 'white',
    layout: 'vbox',
    defaults: {
        width: '100%'

    },

    listeners: {
        render: 'onViewRender'
    },

    viewModel: {
        data: {
            selectedRecords: '0'
        }
    },

    constructor: function () {
        this.items = [
            this.getFilterMarking(),
            {
                xtype: 'container',
                flex: 1,
                layout: 'fit',
                items: {
                    xtype: 'app-devices-base-grid',
                    scrollable: true,
                    header: {
                        xtype: 'header',
                        title: 'Приборы учета',
                        items: [
                            {
                                xtype: 'label',
                                style: 'color: #c1b394;',
                                bind: {
                                    text: 'Выбрано {selectedRecords} элементов',
                                }
                            }
                        ]
                    },
                    listeners: {
                        selectionchange: 'onSelectionChange',
                    }
                }
            }
        ]
        this.callParent(arguments);
    },

    onViewRender: function () {
        this.setIsReady(true);
    },

    /**
     * Вернуть разметку фильтра
     */
    getFilterMarking: function () {
        Ext.raise('Необходимо переопределить метод getFilterMarking');
    },

    /**
     * Вывести форму создания маршрута
     */
    createRoute: function (values) {
        var me = this;
        var grid = this.getGrid();
        if (grid) {
            var selected = grid.getSelection();
            if (selected.length == 0) {
                var items = grid.getStore().getData().items;
                if (items.length > 0) {
                    Ext.Msg.show({
                        title: 'Сообщение',
                        msg: "Не выбраны ПУ для маршрута. Использовать весь cписок для нового маршрута?",
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.INFO,
                        fn: function (btn) {
                            if (btn == 'yes')
                                me.showCreateForm(items);
                        }
                    });
                }
                else {
                    Ext.Msg.show({
                        title: 'Сообщение',
                        msg: "Нет ПУ для создания маршрута",
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO
                    });
                }
            }
            else {
                me.showCreateForm(selected);
            }
        }
    },

    /**
     * Вывести форму создания маршрута
     */
    showCreateForm: function (items, mainDivision, division, subDivision) {
        var me = this;
        var filter = this.up('app-main').down('app-main-filter');
        if (filter) {
            var values = filter.getLastValues();

            if (values && typeof values.Division == 'number') {
                if (items.length > 0) {
                    var IDs = [];
                    for (i = 0; i < items.length; i++) {
                        IDs.push(items[i].get('LINK'));
                    }

                    var sub = values.SubDivision;
                    if (typeof sub != 'number')
                        sub = null;
                    var main = values.MainDivision;
                    if (typeof main != 'number')
                        main = null;

                    var form = Ext.create('Ext.Window', {
                        title: 'Создание маршрута',
                        modal: true,
                        items: {
                            xtype: 'devices-create-route',
                            devices: IDs,
                            mainDivision: main,
                            division: values.Division,
                            subDivision: sub,
                            listeners: {
                                close: function () {
                                    var grid = me.getGrid();
                                    if (grid)
                                        grid.unselect();
                                    form.close();
                                }
                            }
                        }
                    }).show();
                }
            }
            else {
                Ext.Msg.show({
                    title: 'Сообщение',
                    msg: "Не выбрано отделение для маршрута",
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.INFO
                });
            }
        }
    },

    /**
     * Вернуть грид
     */
    getGrid: function () {
        return this.down('app-devices-base-grid');
    },

    reloadGrid: function () {
        var filters = (this.leftFilters || []).concat(this.topFilters || []);
        var grid = this.getGrid();
        grid.unselect();
        grid.loadGrid(filters);
    },

    onSelectionChange: function (model) {
        var vm = this.getViewModel();
        vm.set('selectedRecords', model.selected.length);
    },
    /*
     * Дополнительные фильтры
     */
    //верхняя панель фильтров
    topFilters: [],
    //левая панель
    leftFilters: [],

    /**
     * Обработка события изменения фильтров на левой панели
     */
    setUserStore: function () {
        var filter = this.up('app-main').down('app-main-filter');
        if (filter) {
            var values = filter.getLastValues();
            var leftGilters = [];
            if (values) {
                if (typeof values.MainDivision == 'number')
                    leftGilters.push({
                        property: 'F_Subscrs_Ref.S_MainDivision_Ref.LINK',
                        value: values.MainDivision
                    });
                if (typeof values.Division == 'number')
                    leftGilters.push({
                        property: 'F_Subscrs_Ref.S_Division_Ref.LINK',
                        value: values.Division
                    });
                if (typeof values.SubDivision == 'number')
                    leftGilters.push({
                        property: 'F_Subscrs_Ref.S_SubDivision_Ref.LINK',
                        value: values.SubDivision
                    });
            }
            this.leftFilters = leftGilters;
        }
        this.reloadGrid();
    },

    /**
     * очистка левого меню 
     */
    onMainFilterClean: function () {
        if (this.leftFilters.length > 0) {
            this.leftFilters = [];
            this.reloadGrid();
        }
    }
});