﻿/*
 * панель фильтрации
 */
Ext.define('ARM.view.createRoute.base.Filter', {
    extend: 'Ext.Container',
    xtype: 'app-devices-base-filter',

    mixins: ['Ext.mixin.Responsive'],

    ROUTE_CREATE_CLICK_EVENT_NAME: 'routecreateclick',
    FILTER_CHANGE_EVENT_NAME: 'filterchange',

    responsiveFormulas: {
        largeStatFilterHeight: getResponsiveHeight(),
        smallStatFilterHeight: '!largeStatFilterHeight',
        largeStatFilterWidth: getResponsiveWidth(),
        smallStatFilterWidth: '!largeStatFilterWidth'
    },

    responsiveConfig: {
        'largeStatFilterHeight && largeStatFilterWidth': {
            bodyStyle: {
                padding: '20px'
            }
        },
        'smallStatFilterHeight || smallStatFilterWidth': {
            bodyStyle: {
                padding: '10px 10px 10px 10px'
            }
        }
    },
    ui: 'white',

    defaultListenerScope: true,

    constructor: function (cfg) {
        this.items = [{
            xtype: 'form',
            ui: 'white',
            padding: 10,

            items: this.getFormMarking()
        },
        {
            xtype: 'toolbar',
            ui: 'white',
            padding: 5,
            items: [
                {
                    xtype: 'button',
                    text: 'Поиск',
                    margin: '0 0 0 10px',
                    handler: 'onClickSearch'
                },
                {
                    xtype: 'button',
                    text: 'Очистить',
                    margin: '0 0 0 10px',
                    handler: 'onClickClear'
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Создать маршрут',
                    margin: '0 0 0 10px',
                    handler: 'onRouteCreateClick'
                }
            ]
        }];
        this.callParent(arguments);
    },

    /*
     * значения с формы
     */
    getValues: function () {
        return this.getForm().getValues();
    },

    /**
     * Вернуть разметку филдов для формы
     */
    getFormMarking: function () {
        Ext.raise('Необходимо переопределить метод getFormMarking');
    },

    privates: {
        /**
         * Нажатие на поиск
         */
        onClickSearch: function (sender) {
            var form = this.getForm();
            if (form.isValid() == true) {
                this.onCallChangeEvent();
            }
        },

        /**
         * Нажатие на очистку фильра
         */
        onClickClear: function (sender) {
            var form = this.getForm();
            form.reset();
            this.onCallChangeEvent();
        },

        /**
         * Нажатие на кнопку создать маршрут
         */
        onRouteCreateClick: function () {
            this.fireEvent(this.ROUTE_CREATE_CLICK_EVENT_NAME, this, this.getValues());
        },

        /*
         * возвращается форма 
         */
        getForm: function () {
            return this.down('form');
        },

        /*
         * обработчик изменения фильтра
         */
        onFilterChange: function (sender, newValue) {
            var form = this.getForm();
            var clear = sender.getTrigger('clear');
            if (clear)
                clear.show();
            if (form.isValid() == true) {
                this.onCallChangeEvent();
            }
        },

        /*
         * вызов события изменения
         */
        onCallChangeEvent: function () {
            var values = this.getValues();
            this.fireEvent(this.FILTER_CHANGE_EVENT_NAME, this, values);
        }
    }
});