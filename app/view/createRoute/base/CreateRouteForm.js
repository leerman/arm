﻿
Ext.define('ARM.view.createRoute.base.CreateRouteForm', {
    extend: 'Ext.panel.Panel',
    xtype: 'devices-create-route',
    defaultListenerScope: true,
    minWidth: 300,
    minHeight: 300,
    layout: 'fit',
    CLOSE_EVENT_NAME: 'close',
    viewModel: {
    },

    items: [{
        xtype: 'form',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        ui: 'white',
        border: false,
        bodyPadding: 10,
        fieldDefaults: {
            labelAlign: 'top',
            labelWidth: 100,
            labelStyle: 'font-weight:bold'
        },
        items: [{
            xtype: 'datefield',
            fieldLabel: 'Дата с',
            allowBlank: false,
            bind: {
                value: '{Begin}'
            }
        }, {
            xtype: 'datefield',
            fieldLabel: 'Дата по',
            allowBlank: false,
            bind: {
                value: '{End}'
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Номер маршрута',
            allowBlank: false,
            bind: {
                value: '{Number}'
            }
        },
        {
            fieldLabel: 'Тип акта',
            xtype: 'combobox',
            queryMode: 'remote',
            displayField: 'C_Name',
            valueField: 'LINK',
            hidden: true,
            store: Ext.create('ARM.store.ActType', {
                pageSize: 100,
                filters: [{
                    property: 'F_Parents',
                    operator: 'isnull'
                }]
            }),
            autoLoadOnValue: true,
            bind: {
                value: '{Type}',
                hidden: '{!IsShowType}'
            }
        },
        {
            fieldLabel: 'Исполнитель',
            xtype: 'combobox',
            queryMode: 'remote',
            displayField: 'C_Fio',
            valueField: 'LINK',
            store: Ext.create('ARM.store.GetWalkersQuery', { pageSize: 10000 }),
            itemId: 'walkersStore',
            bind: {
                value: '{walker}'
            }
        }],

        buttons: [{
            text: 'Создать',
            handler: 'onFormSubmit'
        }, {
            text: 'Отмена',
            handler: 'onFormClose'
        }]
    }],

    listeners: {
        render: 'onPanelRender'
    },

    onPanelRender: function () {
        var me = this;

        PN.Domain.ED_Devices.GetDefaultsForCreateRoute(me.mainDivision, me.division, me.subDivision, function (data) {
            var vm = me.getViewModel();
            if (vm && typeof data == 'object') {
                data.Begin = new Date(data.Begin);
                data.End = new Date(data.End);
                vm.setData(data);
            }
        });
    },

    /**
     * Нажатие на кнопку Отмена
     */
    onFormClose: function () {
        this.fireEvent(this.CLOSE_EVENT_NAME, this);
    },

    /**
     * Нажатие на кнопку Создать
     */
    onFormSubmit: function () {
        var form = this.down('form');
        var me = this;

        if (form.isValid() == true) {
            this.createRoute();
        }
    },

    privates: {
        /**
         * Создание маршрута на обход
         */
        createRoute: function () {
            var me = this;
            var vm = me.getViewModel();
            if (vm) {
                var data = vm.getData();
                PN.Domain.ED_Devices.CreateRoute(me.mainDivision, me.division, me.subDivision, data.Type, data.Begin, data.End, data.Number, data.walker, me.devices, function (result) {
                    var title = 'Сообщение';
                    var icon = Ext.Msg.INFO;
                    var msg = result.message;

                    if (result.success) {
                        if (data.walker) {
                            me.notifyUser(result.doc, data.walker);
                        }
                        if (!result.message) {
                            msg = 'Маршрут успешно создан';
                        }
                    }
                    else {
                        title = 'Ошибка';
                        icon = Ext.Msg.ERROR;
                    }

                    Ext.Msg.show({
                        title: title,
                        msg: msg,
                        buttons: Ext.Msg.OK,
                        icon: icon
                    });
                    
                    me.onFormClose();
                });
            }
        },

        /**
         * Уведомить обходчика что для него есть задача
         */
        notifyUser: function (doc, walkerId) {
            var me = this;
            var walkersCmp = me.down('#walkersStore');
            if (walkersCmp) {
                var walkersStore = walkersCmp.getStore();
                if (walkersStore) {
                    var users = walkersStore.getData();
                    var user = users.get(walkerId);
                    if (user) {
                        var uuid = user.get('uuid');
                        var number = doc.C_Number;
                        var userName = user.get('C_Fio');

                        var app = Ext.getCurrentApp();
                        var socket = app.getSocket();
                        if (socket)
                            socket.notifUser(uuid, 'add_documents', 'Вам назначен новый документ' + (number ? ' под номером ' + number : ''), (doc ? JSON.stringify(doc) : "{}"), function () {
                                var txt = 'Документ ' + (number ? ' под номером ' + number : '') + ' доставлен ' + userName;
                                app.showTooltip('Уведомление', txt);
                            });
                    }
                }
            }
        }
    }
});