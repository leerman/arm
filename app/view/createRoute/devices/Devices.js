﻿/*
 * Администрирование
 */
Ext.define('ARM.view.createRoute.devices.Devices', {
    extend: 'ARM.view.createRoute.base.Devices',
    xtype: 'app-devices',

    requires: [
        'ARM.view.createRoute.devices.Filter'
    ],

    /**
     * Вернуть разметку фильтра
     */
    getFilterMarking: function () {
        return {
            xtype: 'app-devices-filter',
            listeners: {
                filterchange: 'onFilterChange',
                routecreateclick: 'onRouteCreateClick'
            }
        };
    },

    /**
     * Изменение значений фильтра
     */
    onFilterChange: function (sender, value) {
        var grid = this.getGrid();
        var filters = [];

        if (value) {
            if (value.controlDate) {
                filters.push({
                    value: value.controlDate,
                    property: 'D_Control_Date',
                    operator: '>='
                });
                var next = new Date(value.controlDate);
                next.setDate(next.getDate() + 1);
                filters.push({
                    value: next,
                    property: 'D_Control_Date',
                    operator: '<='
                });
            }

            if (value.startDate)
                filters.push({
                    value: value.startDate,
                    property: 'D_Control_Date',
                    operator: '>='
                });

            if (value.endDate)
                filters.push({
                    value: value.endDate,
                    property: 'D_Control_Date',
                    operator: '<='
                });

            if (value.address) {
                filters.push({
                    value: value.address,
                    property: 'F_Subscrs_Ref.C_Address_Short',
                    operator: 'like'
                })
            }

            if (value.C_TP && value.C_TP.length > 0)
                filters.push({
                    value: value.C_TP,
                    property: 'C_TP',
                    operator: 'like'
                });

            if (value.C_NN_Line && value.C_NN_Line.length > 0) {
                filters.push({
                    value: value.C_NN_Line,
                    property: 'C_NN_Line',
                    operator: 'like'
                })
            }
        }
        this.topFilters = filters;
        this.reloadGrid();
    },

    /**
     * Создание маршрута
     */
    onRouteCreateClick: function (sender, values) {
        this.createRoute(values);
    }
});