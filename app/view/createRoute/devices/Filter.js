﻿/*
 * панель фильтрации
 */
Ext.define('ARM.view.createRoute.devices.Filter', {
    extend: 'ARM.view.createRoute.base.Filter',
    xtype: 'app-devices-filter',

    /**
     * Вернуть разметку фильтра
     */
    getFormMarking: function () {
        return [{
            xtype: 'container',
            cls: 'create-route-filter-container',
            width: '100%',
            defaults: {
                enableKeyEvents: true,
                labelWidth: 200,
                labelStyle: 'color:#808080;padding-top:9px',
                emptyText: '',
                ui: 'white',
                width: 400,
                padding: '0 10 10 0',
                listeners: {
                    keypress: 'onEnterPress'
                }
            },
            items: [{
                xtype: 'textfield',
                name: 'address',
                fieldLabel: 'Адрес'
            }, {
                xtype: 'textfield',
                name: 'C_TP',
                fieldLabel: 'Трансформаторная подстанция'
            }, {
                xtype: 'textfield',
                name: 'C_NN_Line',
                fieldLabel: 'Линия НН'
            }, {
                xtype: 'datefield',
                format: 'd.m.Y',
                fieldLabel: 'Дата контрольного обхода',
                name: 'controlDate',
                maxValue: new Date()
            }, {
                xtype: 'datefield',
                format: 'd.m.Y',
                name: 'startDate',
                fieldLabel: 'Начало периода',
                maxValue: new Date()
            }, {
                xtype: 'datefield',
                format: 'd.m.Y',
                name: 'endDate',
                fieldLabel: 'Конец периода',
                maxValue: new Date()
            }]
        }];
    },

    /*
     * значения с формы
     */
    getValues: function () {
        var values = this.getForm().getValues();
        if (values.controlDate) {
            console.log(values.controlDate)
            values.controlDate = Ext.Date.parse(values.controlDate, 'd.m.Y');
            console.log(values.controlDate)
            }
        if (values.startDate)
            values.startDate = Ext.Date.parse(values.startDate, 'd.m.Y');
        if (values.endDate) {
            values.endDate = Ext.Date.parse(values.endDate + ' 23:59:59', 'd.m.Y H:i:s');
        }
        return values;
    },

    /**
     * Проверка на нажатие кнопки enter
     */
    onEnterPress: function (sender, e) {
        if (e.keyCode == e.ENTER)
            this.onClickSearch();
    }
});