﻿/*
 * Раздел Задачи
 */
Ext.define('ARM.view.task.Task', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-task',
    requires: [
        'ARM.view.task.TaskController',
        'ARM.view.task.TaskModel',

        'ARM.view.task.filter.WorkPanel',
        'ARM.view.task.body.FilterStatus',
        'ARM.view.task.body.TaskItems',
        'ARM.view.task.body.TaskInfo',
        'Ext.form.field.Date'
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeTaskHeight: getResponsiveHeight(),
        smallTaskHeight: '!largeTaskHeight',
        largeTaskWidth: getResponsiveWidth(),
        smallTaskWidth: '!smallTaskWidth'
    },

    controller: 'task',
    viewModel: 'task',

    width: '100%',
    height: '100%',

    layout: {
        type: 'hbox',
        pack: 'start'
    },
    defaults:{
        height: '100%'
    },
    items: [
        {
            width: '100%',
            itemId: 'selectUserId',
            html: '<center style="margin-top:20px">Для вывода информации выберите обходчика</center>'
        },
        {
            xtype: 'app-task-workpanel',
            width: 340,
            ui: 'white',
        },
        {
            flex: 1,
            itemId: 'selectRouteId',
            html: '<center style="margin-top:20px">Для вывода информации выберите маршрут</center>'
        },
        {
            flex: 1,
            xtype: 'panel',
            ui: 'white',
            itemId: 'body',
            hidden: true,
            layout: {
                type: 'vbox',
                pack: 'start'
            },
            defaults:{
                width: '100%'
            },

            items: [
                {
                    xtype: 'app-task-filter-status',
                    ui: 'white',

                    plugins: 'responsive',
                    responsiveConfig: {
                        'largeTaskHeight && largeTaskWidth': {
                            style: {
                                padding: '25px 20px 15px 20px'
                            }
                        },
                        'smallTaskHeight || smallTaskWidth': {
                            style: {
                                padding: '10px 10px 5px 10px'
                            }
                        }
                    }
                },
                {
                    xtype: 'app-task-items',

                    responsiveConfig: {
                        'largeTaskHeight && largeTaskWidth': {
                            style: {
                                padding: '0 20px 15px 20px'
                            },
                            height: 400
                        },
                        'smallTaskHeight || smallTaskWidth': {
                            style: {
                                padding: '0 10px 5px 10px'
                            },
                            height: 300
                        }
                    },

                    listeners: {
                        rowclick: 'onRowClick'
                    }
                },
                {
                    xtype: 'app-task-info',
                    ui: 'white',
                    flex: 1
                }
            ],

            setDefaultState: function () {
                this.down('app-task-filter-status').setDefaultState();
                this.down('app-task-items').setDefaultState();
                var info = this.down('app-task-info');
                if (info) {
                    if (info.isHidden() == false)
                        info.hide();

                    info.setDefaultState();
                }
            }
        }
    ],

    listeners: {
        afterrender: 'onAfterRender',
        setuserstore: 'onSetUserStore'
    },

    /*
     * вывести надпись с информацие о том что нужно выбрать пользователя
     * @param show {boolean} true - вывести надпись
     */
    showSelectUserInfo: function (show) {
        var cmp = this.down('#selectUserId');
        if (cmp && cmp.rendered == true) {
            if (show == true && cmp.isHidden() == true) {
                cmp.show();
            }

            if (!show && cmp.isHidden() == false)
                cmp.hide();
        }
    },

    /*
     * вывести надпись с информацие о том что нужно выбрать маршрут
     * @param show {boolean} true - вывести надпись
     */
    showSelectRouteInfo: function (show) {
        var cmp = this.down('#selectRouteId');
        if (cmp && cmp.rendered == true) {
            if (show == true && cmp.isHidden() == true) {
                cmp.show();
            }

            if (!show && cmp.isHidden() == false)
                cmp.hide();
        }
    },

    /*
     * обработчик отмены фильтрации
     */
    onMainFilterClean: function () {
        var list = this.getUserListComponent();
        if (list) {
            list.fireEvent('unselect', list, null);
        }
    },

    /*
     * передача дополнительных параметров через роутинг 
     * Вызывается в тот момент когда передаются дополнительные параметры
     * @param options {any} дополнительные параметры
     */
    onRouteOptions: function (options) {
        this.callParent(arguments);
        if (options && options.type != 'home') {
            var userId = options.userId;
            this.routeOptions = options;

            var initView = Ext.getCurrentApp().getInitView();
            if (initView) {
                var filter = initView.down('app-main-filter');
                if (filter.getFiltered() != true) {

                    filter.getField('userId').setValue(userId);
                    initView.applyFilter(filter, {
                        uuid: userId
                    });

                    filter.placeHolderReady();
                } else {
                    var list = initView.down('app-user-list');
                    if (list) {

                        // нужно еще спозиционировать карточку
                        var card = list.map[userId];
                        if (card) {
                            card.setMode('maximize');

                            var scroll = list.getScrollable();
                            scroll.scrollIntoView(card.el.dom);
                        }
                        list.fireEvent('select', list, list.map[options.userId]);
                    }
                }
            }
        }
    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);
            list.un('select', controller.onUserSelect, this);
            list.un('unselect', controller.onUserUnSelect, this);
        }

        this.callParent(arguments);
    }
});