﻿/*
 * компонент для вывода статуса документа (маршрута)
 */
Ext.define('ARM.view.task.body.FilterStatus', {
    extend: 'Ext.Panel',
    xtype: 'app-task-filter-status',
    cls: 'app-task-filter-status',

    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeFilter: getResponsiveWidth(),
        smallFilter: '!largeFilter'
    },

    viewModel:{
        data: {
            number: '', // номер документа
            type: '', // тип документа
            status: '', // статус документа
            periodBegin: '', // период с
            periodEnd: '', // период по
            statusColor: '', // цвет статуса
            person: '' // ЮЛ или ФЛ
        }
    },

    layout: {
        type: 'hbox',
        pack: 'start'
    },

    defaults:{
        xtype: 'container'
    },

    items: [
        {
            xtype: 'container',
            defaults: {
                xtype: 'container'
            },

            plugins: 'responsive',
            responsiveConfig: {
                'largeFilter': {
                    layout: {
                        type: 'box',
                        vertical: false,
                        pack: 'start'
                    },
                    flex: 1
                },
                'smallFilter': {
                    layout: {
                        type: 'box',
                        vertical: true,
                        pack: 'start'
                    },
                    flex: 0
                }
            },

            items: [
                {
                    xtype: 'container',
                    defaults: {
                        xtype: 'container'
                    },

                    layout: {
                        type: 'hbox',
                        pack: 'start'
                    },

                    items: [
                        {
                            cls: 'status-field-spacer',
                            bind: {
                                html: '<b>Документ:</b> {number}'
                            }
                        },
                        {
                            cls: 'status-field-spacer',
                            bind: {
                                html: '<b>Тип: </b> {type} {person}'
                            }
                        }]
                },
                {
                    xtype: 'container',
                    defaults: {
                        xtype: 'container'
                    },

                    layout: {
                        type: 'hbox',
                        pack: 'start'
                    },

                    items: [
                        {
                            cls: 'status-field-spacer',
                            bind: {
                                html: '<b>Статус: </b> {status}'
                            }
                        },
                        {
                            cls: 'status-field-spacer',
                            bind: {
                                html: '<b>Период: </b> с {periodBegin} по {periodEnd}'
                            },
                            width: 250
                        }]
                }]
        },
        {
            bind: {
                html: '<div class="status-round status-round-{statusColor}">&nbsp;</div> {status}'
            }
        }],

    /*
     * @param documentId {string} идентификатор документа
     * @param callback {()=>void} функция обратного вызова
     */
    databind: function (documentId, callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();
        var me = this;
        dataProvider.getDocument(documentId, function (document) {
            if (document) {
                var viewModel = me.getViewModel();
                if (viewModel) {
                    viewModel.set('number', document.get('C_Number'));
                    viewModel.set('type', document.get('F_Types').C_Name);
                    var personOnly = Ext.getConf('personOnly');
                    if (personOnly == true) {
                        viewModel.set('person', document.get('S_Person') != true ? 'ЮЛ' : '');
                    } else {
                        viewModel.set('person', document.get('S_Person') == true ? 'ФЛ' : 'ЮЛ');
                    }
                    viewModel.set('status', document.get('F_Statuses').C_Name);
                    viewModel.set('periodBegin', Ext.Date.format(document.get('D_Date'), 'd.m.Y'));
                    viewModel.set('periodEnd', Ext.Date.format(document.get('D_Date_End'), 'd.m.Y'));
                    viewModel.set('type', document.get('F_Types').C_Name);
                    viewModel.set('statusColor', document.get('F_Statuses').LINK);
                }
            }

            if (typeof callback == 'function')
                callback();

        });
    },

    setDefaultState: function () {

    }
});