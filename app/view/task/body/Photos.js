﻿/*
 * фотогалерея
 */
Ext.define('ARM.view.task.body.Photos', {
    extend: 'Ext.Panel',
    xtype: 'app-photos',
    defaultListenerScope: true,
    cls: 'app-photos',

    //padding: '30px 30px 0 0',

    layout: {
        type: 'vbox',
        pack: 'start'
    },

    width: '100%',

    defaults: {
        width: '100%',
    },

    photo_map: {},
    emptyImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAAE0lEQVR42mL4//8/AwAAAP//AwAI/AL+06V1YQAAAABJRU5ErkJggg==',

    /*
     * идентификатор активного изображения 
     */
    activeItemLink: null,

    viewModel: {
        data: {
            download_type: 'original',
            download_text: 'Оригинал',
            current_image: '',
            current_image_date: new Date(), // дата фотографирования
            total_images_count: 0, // общее кол-во изображений
            image_idx: 0 // текущий номер изображения
        },
        formulas: {
            download_href: function (get) {
                var view = this.getView();
                return view.getImageUrl(get('download_type'), get('current_image')); // Ext.String.format(Ext.getConf('fileUrl'), Ext.getConf('REMOTING_ADDRESS')) + '/' + get('current_image').replace('B-', get('download_type') + '-');
            },
            current_image_date_str: function (get) {
                return Ext.Date.format(get('current_image_date'), 'd.m.Y') + '<span>' + Ext.Date.format(get('current_image_date'), 'H:i:s') + '</span>'
            },
            preview_href: function (get) {
                var view = this.getView();
                return view.getImageUrl('small', get('current_image'));
            },
            empty_href: function (get) {
                return Ext.getCurrentApp().toAbsolutePath('resources/images/spacer.png');
            },
            idx: function (get) {
                return get('image_idx') + 1;
            },
            isPhotosExists: function (get) {
                return get('total_images_count') > 0;
            }
        }
    },

    items: [
        {
            // панель для вывода изображения
            xtype: 'panel',
            ui: 'white',
            height: 160,
            items: [
                {
                    bind: {
                        html: '<img alt="" src="{empty_href}" style="background:transparent url(\'{preview_href}\') no-repeat; background-size:cover;width:100%;height:160px;border:none">'
                    },
                    listeners: {
                        element: 'el',
                        click: 'onBigImageClick'
                    }
                }
            ],
            style: {
                'border': '1px solid #d4d4d4',
                'margin-bottom': '5px'
            },
            dockedItems: [
                {
                    width: 80,
                    dock: 'left',
                    xtype: 'button',
                    iconCls: 'x-fa fa-angle-left',
                    ui: 'action-link-black',
                    cls: 'arrow',
                    cfg: {
                        type: 'left'
                    },
                    handler: 'onArrowClick',
                    tooltip: 'Предыдущее изображение'
                },
                {
                    width: 80,
                    dock: 'right',
                    xtype: 'button',
                    iconCls: 'x-fa fa-angle-right',
                    ui: 'action-link-black',
                    cls: 'arrow',
                    cfg: {
                        type: 'right'
                    },
                    handler: 'onArrowClick',
                    tooltip: 'Следующее изображение'
                }
            ]
        },
        {
            // выбор изображений
            xtype: 'dataview',
            cls: 'white-scroll',
            style: {
                'white-space': 'nowrap',
                'margin-bottom': '10px'
            },
            tpl: [
                '<tpl for=".">',
                    '<div style="display:inline-block" class="thumb-wrap" data-id="{LINK}" data-idx="{idx}">',
                        '<img style="height:47px" src="{[this.getImagePath(values)]}" />',
                    '</div>',
                '</tpl>',
                {
                    getImagePath: function (values) {
                        return this.owner.up('app-photos').getImageUrl('small', values.LINK);
                    }
                }
            ],
            itemSelector: 'div.thumb-wrap',
            emptyText: 'Изображений нет',
            scrollable: 'x',
            height: 67,
            listeners: {
                itemclick: 'onImageClick'
            }
        },
        {
            // дата и количество
            xtype: 'container',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            defaults: {
                xtype: 'container'
            },
            items: [
                {
                    bind: {
                        html: '<div class="photo-date"><span class="x-fa fa-calendar-plus-o">&nbsp;</span> {current_image_date_str}</div>'
                    }
                },
                {
                    flex: 1
                },
                {
                    bind: {
                        html: '<div class="pager"><span>{idx}</span> / <span>{total_images_count}</span></div>'
                    }
                }
            ],
            style: {
                'margin-bottom': '10px'
            },
            bind: {
                hidden: '{!isPhotosExists}'
            }
        },
        {
            // скачать
            xtype: 'container',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            bind: {
                hidden: '{!isPhotosExists}'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Скачать',
                    ui: 'white-blue',
                    bind: {
                        href: '{download_href}'
                    }
                },
                {
                    xtype: 'button',
                    ui: 'action-link-red',
                    iconCls: 'x-fa fa-angle-down',
                    iconAlign: 'right',
                    bind: {
                        text: '{download_text}'
                    },
                    menu: {
                        defaults: {
                            handler: 'onDownloadImage'
                        },
                        items: [
                            {
                                text: 'Оригинал',
                                cfg: {
                                    type: 'original'
                                }
                            },
                            {
                                text: 'Сжатое',
                                cfg: {
                                    type: 'big'
                                }
                            }
                        ]
                    }
                }
            ]
        }
    ],


    /*
    * Конструктор
    */
    constructor: function () {
        this.callParent(arguments);

        this.gallery = Ext.create('ARM.view.task.body.Gallery');
    },

    /*
    * Десктруктор
    */
    doDestroy: function () {
        this.gallery.destroy();

        this.callParent(arguments);
    },


    /*
     * 
     * @param type {string} тип галареи (тип документа)
     * @param photos {any[]} записи для фотогалереи
     * @param callback {()=>void} функция обратного вызова
     */
    databind: function (type, photos, callback) {
        if (type == 'docout') {
            this.photo_map = {};
            var idx = 0;
            var me = this;
            photos.forEach(function (i) {
                i.set('idx', idx);
                me.photo_map[idx] = i;
                idx++;
            });
            var vm = this.getViewModel();
            vm.set('image_idx', 0);
            var store = Ext.create('ARM.store.File', {
                data: photos
            });
            var dataview = this.down('dataview');
            dataview.bindStore(store);

            this.photo_map.length = photos.length;
            vm.set('total_images_count', photos.length);
            if (photos.length > 0) {
                var item = this.photo_map[0];
                vm.set('current_image', item.get('LINK'));
                vm.set('current_image_date', item.get('D_Date'));

                this.setImageActive(item.get('LINK'));
                this.activeItemLink = item.get('LINK');
            }
            else {
                vm.set('current_image', me.emptyImage);
            }
        }
    },

    privates: {

        /*
        * Галерея
        */
        gallery: null,


        /*
        * Нажатие на большое изображение, открытие галереи
        */
        onBigImageClick: function () {
            var vm = this.getViewModel();
            var data = [];

            for (var i = 0; i < this.photo_map.length; i++) {
                var item = this.photo_map[i];
                var image = item.get('LINK');
                data.push({
                    date: Ext.Date.format(item.get('D_Date'), 'd.m.Y H:i:s'),
                    imageO: this.getImageUrl('original', image),
                    imageB: this.getImageUrl('big', image)
                });
            }
            this.gallery.setPictures(data, vm.get('image_idx'));
            /*this.gallery.setPictures(data);
            this.gallery.setActivePicture(active);*/
            this.gallery.show();
        },


        /*
         * нажатие на изображение в списке
         */
        onImageClick: function (sender, record, item, index, e, eOpts) {
            var me = this;
            var vm = me.getViewModel();
            vm.set('image_idx', record.get('idx'));
            vm.set('current_image', record.get('LINK'));
            vm.set('current_image_date', record.get('D_Date'));
            this.setImageActive(record.get('LINK'));
            this.activeItemLink = record.get('LINK');
        },

        /*
         * обработчик нажатия на кнопку выбора изображения (лево-право)
         */
        onArrowClick: function (btn) {
            var type = btn.cfg.type;
            var vm = this.getViewModel();
            var dataview = this.down('dataview');
            var idx = vm.get('image_idx');

            switch (type) {
                case 'left':
                    if (--idx < 0) {
                        idx = this.photo_map.length - 1;
                    }
                    break;

                case 'right':
                    if (++idx >= this.photo_map.length)
                        idx = 0;
                    break;
            }

            if (idx >= 0) {
                var item = this.photo_map[idx];
                vm.set('image_idx', idx);
                vm.set('current_image', item.get('LINK'));
                vm.set('current_image_date', item.get('D_Date'));
                this.setImageActive(item.get('LINK'));
                this.activeItemLink = item.get('LINK')
            }
        },
        /*
         * возвращается ссылка на изображение
         * @param type {string} тип изображения
         * @param id {string} LINK изображения
         */
        getImageUrl: function (size, id) {
            var src = this.emptyImage;
            if (id.length !== 0) {
                src = Ext.String.format(Ext.getConf('ws_url') + Ext.getConf('virtualDirPath') + '/image?type=photo&size={0}&id={1}', size, id);
            }
            return src;
        },
        /*
         * обработчик загрузки изображения
         */
        onDownloadImage: function (btn) {
            var vm = this.getViewModel();
            vm.set('download_text', btn.text);
            vm.set('download_type', btn.cfg.type);
        },

        /*
         * установить изображение как активное
         * @param link {string} идентификатор изображения
         */
        setImageActive: function (link) {
            var remove = this.el.query('div[data-id="' + this.activeItemLink + '"');
            if (remove.length > 0) {
                Ext.get(remove[0]).removeCls('active');
            }
            var add = this.el.query('div[data-id="' + link + '"');
            if (add.length > 0) {
                Ext.get(add[0]).addCls('active');
            }
        }
    }
});