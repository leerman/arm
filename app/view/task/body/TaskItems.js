﻿/*
 * элементы задания (маршрута)
 */
Ext.define('ARM.view.task.body.TaskItems', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-task-items',
    defaultListenerScope: true,

    requires: [
        'ARM.store.Detail',
        'Ext.toolbar.Paging',
        'Core.DefaultFilters',
        'Ext.grid.plugin.Exporter',
        'Core.data.proxy.Direct',
        'Ext.exporter.text.Html'
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeItemWidth: getResponsiveWidth(),
        smallItemWidth: '!largeItemWidth',
        largeItemHeight: getResponsiveHeight(),
        smallItemHeight: '!largeItemHeight'
    },

    cls: 'app-task-items white-scroll',
    loadMask: true,

    viewModel:{
        data: {
            total: 0
        }
    },

    /**
     * переменная для хранения параметров запроса по умолчанию
     */
    defaultParams: null,

    plugins: [
        {
            ptype: 'defaultfilters',
            pluginId: 'defaultfilters'
        },
        {
            ptype: 'gridexporter'
        },
        {
            ptype: 'gridfilters'
        },
        {
            ptype: 'responsive'
        }
    ],
    title: '',

    header: {
        layout: {
            type: 'hbox',
            pack: 'start'
        },
        padding: '5px 10px 5px 10px',

        items: [{
            xtype: 'button',
            iconCls: 'x-fa fa-sign-in',
            text: 'Экспорт данных',
            ui: 'action-dark',
            menu: {
                defaults: {
                    handler: 'exportTo'
                },
                items: [
                    {
                        text: 'Excel xlsx',
                        cfg: {
                            type: 'excel07',
                            ext: 'xlsx'
                        }
                    },
                    {
                        text: 'HTML',
                        cfg: {
                            type: 'html'
                        }
                    }
                ]
            }
        }]
    },

    columns: [
        { xtype: 'rownumberer', align: 'center' },
        {
            xtype: 'booleancolumn',
            text: 'Выполнено',
            dataIndex: 'IsDone',
            hideable: false,
            sortable: false,
            draggable: false,
            trueText: 'Да',
            falseText: 'Нет',
            align: 'center',
            filter: true
        },
        {
            text: 'Дата выполнения',
            dataIndex: 'D_Done_Date',
            xtype: 'datecolumn',
            format: 'd.m.Y H:i:s',
            hideable: false,
            sortable: false,
            draggable: false,
            align: 'center',
            width: 170,
            renderer: function (value) {
                return Ext.Date.format(value, 'd.m.Y') + '<span style="margin-left:10px;color:#959595">' + Ext.Date.format(value, 'H:i:s') + '</span>'
            },
            filter: true
        },
        {
            text: 'Владелец',
            dataIndex: 'Owner',
            hideable: false,
            sortable: false,
            draggable: false,
            align: 'left',
            width: 250,
            filter: true
        },
        {
            text: 'ПУ',
            dataIndex: 'C_Device_Number',
            hideable: false,
            sortable: false,
            draggable: false,
            filter: true
        },
        {
            text: 'ЛС',
            dataIndex: 'C_Subscr',
            hideable: false,
            sortable: false,
            draggable: false,
            filter: true
        },
        {
            text: 'Адрес',
            dataIndex: 'Address',
            hideable: false,
            sortable: false,
            draggable: false,
            minWidth: 80,
            flex: 1,
            filter: true
        },
        {
            xtype: 'booleancolumn',
            text: 'Принят',
            dataIndex: 'B_Received',
            hideable: false,
            sortable: false,
            draggable: false,
            trueText: 'Да',
            falseText: 'Нет',
            align: 'center',
            filter: true
        }
    ],

    bbar: {
        xtype: 'pagingtoolbar',
        ui: 'white-paging',
        cls: 'white-scroll',
        scrollable: 'x',

        plugins: 'responsive',
        responsiveConfig: {
            'largeItemWidth && largeItemHeight': {
                style: {
                    padding: '25px 0 6px 8px'
                }
            },
            'smallItemWidth || smallItemHeight': {
                style: {
                    padding: '0'
                }
            }
        },

        items: [
            '->',
            {
                xtype: 'container',
                bind: {
                    html: '<span class="task-info">Статистика (всего {total}):</span>'
                },
                margin: '0 30 0 0'
            },
            {
                xtype: 'core-progressbar',
                bgcolor: '#e6e6e6',
                color: '#5ac227',
                textColor: '#000000',

                itemId: 'taskDoneId',
                height: 34,
                width: 150,
                ui: 'white',
                margin: '0 10 0 0',
                listeners: {
                    el: {
                        click: 'onDoneClick'
                    }
                },
                style: {
                    'cursor':'pointer'
                }
            },
            {
                xtype: 'core-progressbar',
                bgcolor: '#e6e6e6',
                color: '#ea3a4a',
                textColor: '#000000',
                itemId: 'taskWorkId',

                height: 34,
                width: 150,
                ui: 'white',
                listeners: {
                    el: {
                        click: 'onWorkClick'
                    }
                },
                style: {
                    'cursor': 'pointer'
                }
            }
        ]
    },

    constructor: function (cfg) {
        cfg.store = Ext.create('ARM.store.ControlPoint', {
            suppressNextFilter: true,
        });
        this.callParent(arguments);
    },

    /*
     * отфильтровать по выполнено
     */
    onDoneClick: function () {
        this.getStore().filter('IsDone', true);
    },

    /*
     * отфильтровать по осталось (в работе)
     */
    onWorkClick: function () {
        this.getStore().filter('IsDone', false);
    },

    /*
     * 
     * @param documentId {string} идентификатор документа
     * @param doneCount {number} количество выполненых заданий
     * @param userId {string} идентификатор пользователя
     * @param callback {()=>void} функция обратного вызова
     */
    databind: function (documentId, doneCount, userId, callback) {
        this.setDefaultParams({
            filter: [
                {
                    property: 'DocLink',
                    value: documentId,
                    operator: '='
                },
                {
                    property: 'UserId',
                    value: userId,
                    operator: '='
                }
            ]
        });
        var me = this;
        var store = this.getStore();
        store.suppressNextFilter = false;
        store.currentPage = 1;
        store.load({
            callback: function () {
                var vm = me.getViewModel();
                var total = this.getTotalCount();
                if (vm) {
                    vm.set('total', total);
                    // здесь нужно сделать запрос на сервер, чтобы узначть сколько всего заданий выполнено
                    //var dataProvider = Ext.getCurrentApp().getDataProvider();
                    //dataProvider.getRouteDoneCount(documentId, userId, function (count) {

                    //});

                    var work = me.down('#taskWorkId');
                    if (work) {
                        work.setProgress(((total - doneCount) * 100) / total);
                        work.setText('Осталось ' + (total - doneCount));
                    }
                    var done = me.down('#taskDoneId');
                    if (done) {
                        done.setProgress((doneCount * 100) / total);
                        done.setText('Выполнено ' + doneCount);
                    }
                    if (typeof callback == 'function')
                        callback();
                }
            }
        });


    },

    setDefaultState: function () {

    },

    privates: {
        /*
         * экспортировать в...
         * @param menu {any} пункт меню который был нажат
         */
        exportTo: function (menu) {
            this.saveDocumentAs({
                type: menu.cfg.type,
                title: this.getTitle(),
                fileName: Ext.String.format('{0}.{1}', this.getTitle(), menu.cfg.ext || menu.cfg.type)
            });
        },
        /*
         * установить значение параметров по умолчанию
         * @param value {any} значение
         */
        setDefaultParams: function (value) {
            this.defaultParams = value;
        },
        /*
         * возвращаются значение параметров по умолчанию
         */
        getDefaultParams: function () {
            return this.defaultParams;
        }
    }
});