﻿/*
 * информация о задании
 */
Ext.define('ARM.view.task.body.TaskInfo', {
    extend: 'Ext.Panel',
    xtype: 'app-task-info',
    cls: 'white-scroll',
    defaultListenerScope: true,

    requires: [
        'ARM.view.task.body.Photos',
        'Core.data.proxy.Direct',
        'ARM.view.task.card.Selector',
        'ARM.view.task.card.DT_CONTROL_METER_READINGS',

        'ARM.view.task.card.Meters'
    ],
    mixins: ['Ext.mixin.Responsive'],

    map:{},

    responsiveFormulas: {
        largeInfoWidth: getResponsiveWidth(),
        smallInfoWidth: '!largeInfoWidth'
    },

    responsiveConfig: {
        'largeInfoWidth': {
            layout: {
                type: 'box',
                vertical: false,
                pack: 'start',
                align: 'top'
            },
            height: '100%'
        },
        'smallInfoWidth': {
            layout: {
                type: 'box',
                vertical: true,
                pack: 'start',
                align: 'center'
            },
            height: 'auto'
        }
    },
    scrollable: true,

    width: '100%',

    defaults:{
        xtype: 'panel',
        ui: 'white'
    },

    viewModel:{
        data: {
            document_text: ''
        }
    },
    items: [
        {
            plugins: 'responsive',
            responsiveConfig: {
                'largeInfoWidth': {
                    flex: 1,
                    height: '100%',
                    style: {
                        padding: '30px 30px 0 20px'
                    }
                },
                'smallInfoWidth': {
                    flex: 0,
                    height: 'auto',
                    style: {
                        padding: '10px'
                    }
                }
            },
            scrollable: 'y',
            width: '100%',
            defaults: {
                width: '100%'
            },

            layout: {
                type: 'vbox',
                pack: 'start'
            },

            items: [
                {
                    xtype: 'app-task-card-selector',
                    ui: 'white',
                    hidden: true
                },
                {
                    itemId: 'cardId',
                    xtype: 'panel',
                    ui: 'white',

                    plugins: 'responsive',
                    responsiveConfig: {
                        'largeInfoWidth': {
                            flex: 1
                        },
                        'smallInfoWidth': {
                            flex: 0
                        }
                    },
                    
                    scrollable: 'y',
                    cls: 'white-scroll'
                },
                {
                    xtype: 'app-task-card-meters',
                    hidden: true
                }
            ]
        },
        {
            width: 400,
            xtype: 'app-photos',

            plugins: 'responsive',
            responsiveConfig: {
                'largeInfoWidth': {
                    style: {
                        padding: '30px 30px 15px 0'
                    }
                },
                'smallInfoWidth': {
                    style: {
                        padding: '15px'
                    }
                }
            },

            hidden: true
        }
    ],

    /*
     * @param detailId {string} идентификатор строки документа
     * @param b_done {boolean} выполнение задания
     */
    databind: function (detailId, b_done, callback) {
        var me = this;
        this.map = {};
        var dataProvider = Ext.getCurrentApp().getDataProvider();
        // нужно найти все выходные документы у строки документа
        dataProvider.getDocOuts(detailId, function (docouts) {
            var selector = me.down('app-task-card-selector');
            if (docouts) {

                if (docouts.length == 0) {
                    me.emptyCard(b_done);
                } else {
                    if (selector.isHidden() == true)
                        selector.show();
                    var menuItems = [];
                    var idx = 0;
                    var vm = me.getViewModel();
                    if (vm) {
                        docouts.forEach(function (i) {
                            var c_const = i.get('F_Types___C_Const');
                            var name = i.get('F_Types').C_Name;
                            if (idx == 0) {
                                vm.set('document_text', name);
                                idx++;

                                me.setCard(c_const, i);
                            }

                            menuItems.push({
                                text: name,
                                cfg: {
                                    type: c_const
                                }
                            });

                            me.map[c_const] = i;
                        });

                        selector.databind(menuItems);
                    }
                }
            }

            if (typeof callback == 'function')
                callback();
        });
    },

    setDefaultState: function () {
        this.emptyCard();
    },

    privates: {
        /*
         * вывод сообщения что данных нет
         * @param b_done {boolean} выполнение задания
         */
        emptyCard: function (b_done) {
            var card = this.down('#cardId');
            card.removeAll(true);
            card.add({
                xtype: 'panel',
                html: '<center>' + ((b_done  == true) ? 'Задание выполнено, но нет данных' : 'Информация отсуствует т.к. задание не выполнено')+ '</center>',
                ui: 'white'
            });

            var array = [
                'app-task-card-meters',
                'app-task-card-selector',
                'app-photos'
            ];
            var me = this;
            array.forEach(function (i) {
                var view = me.down(i);
                if (view) {
                    view.hide();
                }
            });
        },

        /*
         * установка данных в карточку
         * @param type {string} тип документа
         * @param data {any} данные
         */
        setCard: function (type, data) {
            var card = this.down('#cardId');
            card.removeAll(true);
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            var _data = {};
            _data = data.getData();
            if (type == 'DT_CONTROL_CHECK') {
                dataProvider.getConnSeals(data.get('F_Doc_Details').LINK, function (seals) {

                    if (seals) {
                        var items = [];
                        Ext.each(seals, function (i) {
                            items.push(i.getData());
                        });

                        _data.seals = items;
                    }

                    card.add({
                        xtype: 'app-task-card-' + type,
                        data: _data,
                        ui: 'white'
                    });
                });
            } else {
                card.add({
                    xtype: 'app-task-card-' + type,
                    data: _data,
                    ui: 'white'
                });
            }

            var metersView = this.down('app-task-card-meters');
            if (metersView.isHidden() == true)
                metersView.show();
            metersView.mask('Загрузка показаний...');

            dataProvider.getMeters(data.get('F_Doc_Details').LINK, function (meters) {
                var store = Ext.create('ARM.store.Meter', {
                    data: meters
                });
                if (metersView.rendered == true) {
                    metersView.setStore(store);
                    metersView.unmask();
                }
            });

            var me = this;
            dataProvider.getFilesByDocout(data.get('LINK'), function (files) {
                var photos = me.down('app-photos');
                if (photos.rendered == true) {
                    if (photos.isHidden() == true)
                        photos.show();

                    photos.databind('docout', files, function () {

                    });
                }
            });
        },
        /*
         * обработчик выбора документа
         */
        onSelectDocument: function (menu) {
            var vm = this.getViewModel();
            vm.set('document_text', menu.text);
            // нужно еще изменить карточку
            this.setCard(menu.cfg.type, this.map[menu.cfg.type]);
        }
    }
});