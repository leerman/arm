Ext.define('ARM.view.task.body.Gallery', {
    extend: 'Ext.window.Window',
    xtype: 'g-mainwindow',
    defaultListenerScope: true,

    autoShow: false,
    closeAction: 'hide',
    draggable: false,
    resizable: false,
    shadow: false,
    width: 800,
    height: 600,
    layout: 'fit',


    viewModel: {
        data: {
            currentPage: 1,
            count: 0,
            date: '',
            image: {
                big: '',
                original: '',
                currentType: 'original',
                currentText: 'Оригинал'
            },
            border: {
            }
        },
        formulas: {
            imageHref: function (get) {
                return (get('image.currentType') == 'original' ? get('image.original') : get('image.big'));
            }
        }
    },

    
    header: {
        title: 'Закрыть',
        titleAlign: 'right',
        cls: 'galleryTitle'
    },
    closeToolText: 'Закрыть',
    style: {
        borderWidth: 0,
        borderRadius: 0,
        backgroundColor: 'transparent'
    },


    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        bodyPadding: 5,
        items: [{
            //Текущая страница
            xtype: 'tbtext',
            itemId: 'currentPage',
            cls: 'galleryText',
            bind: {
                text: '{currentPage}/{count}'
            }
        },
        {
            //Иконка календарика
            iconCls: 'x-fa fa-calendar-o',
            border: false,
            disabled: true,
            style: {
                opacity: 1,
                paddingTop: '2px',
                backgroundColor: 'transparent'
            }
        },
        {
            //Дата
            xtype: 'tbtext',
            itemId: 'date',
            cls: 'galleryText',
            style: {
                padding: '0'
            },
            bind: {
                text: '{date}'
            }
        },
        '  ',
        {
            //Скачать
            xtype: 'button',
            itemId: 'downloadPicture',
            text: 'Скачать',
            ui: 'white-blue',
            bind: {
                href: '{imageHref}'
            }
        },
        {
            //Размер фото
            xtype: 'button',
            ui: 'action-link-red',
            iconCls: 'x-fa fa-angle-down',
            iconAlign: 'right',
            bind: {
                text: '{image.currentText}'
            },
            menu: {
                defaults: {
                    handler: 'onImageType'
                },
                items: [
                    {
                        text: 'Оригинал',
                        cfg: {
                            type: 'original'
                        }
                    },
                    {
                        text: 'Сжатое',
                        cfg: {
                            type: 'big'
                        }
                    }
                ]
            }
        },
        {
            //Предыдущее фото
            iconCls: 'x-fa fa-angle-left fa-2x',
            itemId: 'prevImage',
            action: 'left',
            handler: 'onPrevNextImage'
        },
        {
            //Следующее фото
            iconCls: 'x-fa fa-angle-right fa-2x',
            itemId: 'nextImage',
            action: 'right',
            handler: 'onPrevNextImage'
        }]
    }],


    /* ПОЛЯ */

    pictures: null,     //Массив картинок
    activePicture: 1,   //Текущая картинка


    /* МЕТОДЫ */

    getPictures: function() {
        return this.pictures;
    },
    setPictures: function(pictures, active) {
        if (pictures) {
            var dataLength = pictures.length;
            if (dataLength > this.MAX_PREVIEW_COUNT)
                throw new Error('Слишком много фотографий(максимум ' + this.MAX_PREVIEW_COUNT + ')');

            this.pictures = pictures;

            this.setPreviewCount(dataLength);

            //Текущая картинка
            if (active != undefined)
                this.setActivePicture(active);
            else if (this.getActivePicture() > dataLength)
                this.setActivePicture(dataLength-1);
            else
                this.update();
        }
    },

    getActivePicture: function() {
        return this.activePicture;
    },
    setActivePicture: function(page) {
        if (page != undefined)
            page++;
        this.update(page);
    },

    /*
    * Конструктор
    */
    constructor: function(cfg) {
        Ext.apply(this, cfg);

        var preview = [];
        for (var i = 1; i <= this.MAX_PREVIEW_COUNT; i++) 
            preview.push({
                xtype: 'button',
                cls: 'galleryImage miniImage',
                number: i,
                bind: {
                    style: {
                        borderColor: '{border.m' + i + '}'
                    }
                },
                handler: 'onMiniImage'
            });

        this.items = [{
            xtype: 'panel',
            itemId: 'previewBar',
            bodyCls: 'previewPanel galleryImage',
            bind: {
                bodyStyle: {
                    backgroundImage: 'url({image.big})'
                }
            },
            layout: 'border',
            items: [{
                xtype: 'panel',
                region: 'south',
                width: 'auto',
                bodyStyle: {
                    backgroundColor: 'transparent'
                },
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                scrollable: 'x',
                items: preview
            }]
        }];

        this.callParent(arguments);
        
        var vm = this.getViewModel();
        for (var i = 1; i <= this.MAX_PREVIEW_COUNT; i++) {
            vm.set('border.m' + i, 'transparent');
        }

        this.initSpotlight();
    },

    privates: {

        /* ПОЛЯ */
        
        spotlight: null,
        MAX_PREVIEW_COUNT: 10,


        /* МЕТОДЫ */

        /*
        * Блокировка области за окном
        */
        initSpotlight: function() {
            if (!this.spotlight)
                this.spotlight = Ext.create('Ext.ux.Spotlight', {
                    animate: false
                });
        },
        showSpolight: function() {
            this.spotlight.show(this.id);
            this.spotlight.syncSize();
        },
        hideSpotlight: function() {
            this.spotlight.hide();
        },


        /*
        * Обновление галереи
        */
        update: function (page) {
            var data = this.getPictures();
            if (!data)
                return;
            var active = this.getActivePicture(),
                vm = this.getViewModel();
            if (page == undefined)
                page = active;
            else if (page <= 0)
                page = 1;
            else if (data.length < page)
                page = data.length;

            vm.set('currentPage', page);
            vm.set('date', data[page-1].date);
            vm.set('image.big', data[page-1].imageB);
            vm.set('image.original', data[page - 1].imageO);
            vm.set('border.m' + active, 'transparent');
            vm.set('border.m' + page, 'red');
            this.activePicture = page;
        },


        /*
        * Получить превьюшку
        */
        getPreviewBar: function() {
            return this.down('panel[itemId=previewBar]');
        },
        getPreviewImg: function(image, bar) {
            if (!bar)
                bar = this.getPreviewBar();
            return bar.down('button[number=' + image + ']');
        },


        /*
        * Указание количества видимых превьюшек
        */
        setPreviewCount: function(count) {
            if (count < 0)
                count = 0;
            if (count > this.MAX_PREVIEW_COUNT)
                count = this.MAX_PREVIEW_COUNT;
            
            var preview = this.getPreviewBar();
            var vm = this.getViewModel();

            //Видимые превьюшки
            for (var i = 1; i <= count; i++) {
                var image = this.getPreviewImg(i, preview);
                image.setStyle({
                    backgroundImage: 'url(' + this.pictures[i-1].imageB + ')',
                    display: 'inline-block'
                });
            }
            //Невидимые превьюшки
            for (var i = count+1; i <= this.MAX_PREVIEW_COUNT; i++) {
                var image = this.getPreviewImg(i, preview);
                image.setStyle({
                    display: 'none'
                });
            }
            //Количество страниц
            vm.set('count', count);
        },


        /*
        * Проверка, есть ли данные
        */
        checkPictures: function() {
            var download = this.down('button[itemId=downloadPicture]');
            if (!this.getPictures()) {
                this.setPreviewCount(0);
                download.setDisabled(true);
            }
            else
                download.setDisabled(false);
        },


        /*
        * Обработчики
        */

        //При открытии галереи, блок заднего фона
        onShow: function() {
            this.checkPictures();
            this.callParent(arguments);
            this.showSpolight();
        },
        //При закрытии галереи, разблокировка фона
        onHide: function() {
            this.callParent(arguments);
            this.hideSpotlight();
        },

        //Кнопка влево-вправо
        onPrevNextImage: function(button) {
            var pictures = this.getPictures();
            if (pictures) {
                var active = this.getActivePicture();
                var count = pictures.length;
                if (button.action == 'left')
                    this.update((active == 1) ? count : active-1);
                else
                    this.update((active == count) ? 1 : active+1);
            }
        },

        //Нажатие на миниатюру
        onMiniImage: function(button) {
            this.update(button.number);
        },

        //Выбор размера фото
        onImageType: function (button) {
            var vm = this.getViewModel();
            vm.set('image.currentText', button.text);
            vm.set('image.currentType', button.cfg.type);
        }
    }
});