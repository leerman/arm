﻿/*
 * акт контрольной проверки
 */
Ext.define('ARM.view.task.card.DT_CONTROL_METER_READINGS', {
    extend: 'Ext.Panel',
    xtype: 'app-task-card-DT_CONTROL_METER_READINGS',


    tpl: [
        '<div class="document-field">',
        '<div class="item"><b>Дата формирования:</b><span>{[this.getDate(values, "D_Date")]}</span></div>',
        '<div class="item"><b>Номер акта:</b><span>{C_Doc_Number}</span></div>',
        '<div class="item"><b>Лицевой счет:</b><span>{F_Doc_Details___C_Subscr}</span></div>',
        '<div class="item"><b>Потребитель:</b><span>{F_Doc_Details___C_Owner}</span></div>',
        '<div class="item"><b>Адрес объекта:</b><span>{F_Doc_Details___C_Address}</span></div>',
        '<div class="item"><b>Квартира:</b><span>{F_Doc_Details___N_Premise_Number}</span></div>',
        '<div class="item"><b>Тип ПУ:</b><span>{F_Doc_Details___C_Device_Types}</span></div>',
        '<div class="item"><b>Номер ПУ:</b><span>{F_Doc_Details___C_Number}</span></div>',
        '<div class="item"><b>Примечание:</b><span>{C_Note}</span></div>',
        '<tpl if="C_Signature != \'\'"><div class="item signature"><b>Подпись:</b><span><img src="{C_Signature}" style="width:80%" /></span></div></tpl>',

        '</div>',
        {
            getDate: function (values, field) {
                return Ext.Date.format(values[field], 'd.m.Y H:i:s');
            }
        }
    ]
});