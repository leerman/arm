﻿/*
 * таблица показаний
 */
Ext.define('ARM.view.task.card.Meters', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-task-card-meters',
    cls: 'app-task-items white-scroll',
    defaultListenerScope: true,
    features: [{
        ftype: 'summary'
    }],

    columns: [
        {
            text: 'Наименование',
            dataIndex: 'C_Name',
            hideable: false,
            sortable: false,
            draggable: false,
            minWidth: 120,
            flex: 1,
            summaryRenderer: function () {
                return 'АЭ-, Сумма (кВт.ч)';
            }
        },
        {
            text: 'Предыдущее<br />показание',
            dataIndex: 'N_Value_Prev',
            hideable: false,
            sortable: false,
            draggable: false,
            width: 120,
            align: 'right',
            summaryType: 'sum'
        },
        {
            text: 'Текущее<br />показание',
            dataIndex: 'N_Value',
            hideable: false,
            sortable: false,
            draggable: false,
            width: 120,
            align: 'right',
            renderer: 'onCurrentValueRenderer',
            summaryType: 'sum'
        },
        {
            text: 'Дата пред.<br />показание',
            xtype: 'datecolumn',
            dataIndex: 'D_Date_Prev',
            hideable: false,
            sortable: false,
            draggable: false,
            format: 'd.m.Y',
            width: 150,
            align: 'center'
        },
        {
            text: 'Дата тек.<br />показание',
            xtype: 'datecolumn',
            dataIndex: 'D_Date',
            hideable: false,
            sortable: false,
            draggable: false,
            format: 'd.m.Y',
            width: 150,
            align: 'center'
        }
    ],

    privates: {
        /*
         * отрисовка текущего показания
         */
        onCurrentValueRenderer: function (value, cell, record) {
            var cls = '';
            var qtip = '';
            var value_prev = record.get('N_Value_Prev');
            if (value < value_prev) {
                cls = 'rowclass-red';
                qtip = 'Текущее показание меньше предыдущего';
            }

            if (value == value_prev) {
                cls = 'rowclass-gray';
                qtip = 'Текущее и предыдущее показания равны';
            }

            if (value > value_prev) {
                cls = 'rowclass-green';
                qtip = 'Показание валидно';
            }

            return '<span data-qtip="' + qtip + '" class="' + cls + '">' + value + '</span>';
        },
        /**
         * Скрывает строку с суммой если запись 1 
         */
        setStore: function(store){
            var show = store.getCount() > 1;
            this.getView().features[0].toggleSummaryRow(show);
            this.callParent(arguments);
        }
    }
});