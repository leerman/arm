﻿/*
 * акт инструментальной проверки
 */
Ext.define('ARM.view.task.card.DT_CONTROL_CHECK', {
    extend: 'Ext.Panel',
    xtype: 'app-task-card-DT_CONTROL_CHECK',


    tpl: [
        '<div class="document-field">',
        '<div class="item"><b>Дата формирования:</b><span>{[this.getDate(values, "D_Date")]}</span></div>',
        '<div class="item"><b>Номер акта:</b><span>{C_Doc_Number}</span></div>',
        '<div class="item"><b>Лицевой счет:</b><span>{F_Doc_Details___C_Subscr}</span></div>',
        '<div class="item"><b>Потребитель:</b><span>{F_Doc_Details___C_Owner}</span></div>',
        '<div class="item"><b>Адрес объекта:</b><span>{F_Doc_Details___C_Address}</span></div>',
        '<div class="item"><b>Квартира:</b><span>{F_Doc_Details___N_Premise_Number}</span></div>',
        '<div class="item"><b>Тип ПУ:</b><span>{F_Doc_Details___C_Device_Types}</span></div>',
        '<div class="item"><b>Номер ПУ:</b><span>{F_Doc_Details___C_Number}</span></div>',
        '<div class="item"><b>Расчетный коэффициент ПУ:</b><span>{F_Doc_Details___N_Rate}</span></div>',

        '<tpl for="seals">',
            '<div class="item"><b>Тип пломбы №{[xindex]}:</b><span>{[this.withEmpty(values, "S_Stamp_Types_Prev.C_Name")]} | {[this.withEmpty(values, "F_Stamp_Types.C_Name")]}</span></div>',
            '<div class="item"><b>Номер пломбы №{[xindex]}:</b><span>{[this.withEmpty(values, "S_Number_Prev")]} | {[this.withEmpty(values, "F_Seals.C_Seal_Number")]}</span></div>',
            '<div class="item"><b>Место установки пломбы №{[xindex]}:</b><span>{[this.withEmpty(values, "S_Places_Prev.C_Name")]} | {[this.withEmpty(values, "F_Places.C_Name")]}</span></div>',
        '</tpl>',
        
        '<div class="item"><b>Тип нарушения:</b><span>{F_Doc_Details___F_Violations___C_Name}</span></div>',
        '<div class="item"><b>Тип нарушения 2:</b><span>{F_Doc_Details___F_Violations2___C_Name}</span></div>',
        '<div class="item"><b>Тип нарушения 3:</b><span>{F_Doc_Details___F_Violations3___C_Name}</span></div>',
        '<div class="item"><b>Характеристика нарушения:</b><span>{F_Doc_Details___C_Violation}</span></div>',
        '<div class="item"><b>Срок устранения:</b><span>{[this.getDate(values, "F_Doc_Details___D_Date_Elimination")]}</span></div>',

        '<div class="item"><b>Примечание:</b><span>{C_Note}</span></div>',
        '<tpl if="C_Signature != \'\'"><div class="item signature"><b>Подпись:</b><span><img src="{C_Signature}" style="width:80%" /></span></div></tpl>',

        '</div>',
        {
            getDate: function (values, field) {
                return Ext.Date.format(values[field], 'd.m.Y H:i:s');
            },
            withEmpty: function (values, field) {
                var array = field.split('.');
                var result = values;
                if (array.length > 0) {
                    for (var j = 0; j < array.length; j++) {
                        var name = array[j];
                        if (result[name])
                            result = result[name];
                        else {
                            result = null;
                            break;
                        }
                    }
                }
                return result || 'не указан';
            }
        }
    ]
});