﻿/*
 * акт инструментальной проверки
 */
Ext.define('ARM.view.task.card.DT_FAILURE', {
    extend: 'Ext.Panel',
    xtype: 'app-task-card-DT_FAILURE',


    tpl: [
        '<div class="document-field">',
        '<div class="item"><b>Дата формирования:</b><span>{[this.getDate(values, "D_Date")]}</span></div>',
        '<div class="item"><b>Номер акта:</b><span>{C_Doc_Number}</span></div>',
        '<div class="item"><b>Лицевой счет:</b><span>{F_Doc_Details___C_Subscr}</span></div>',
        '<div class="item"><b>Потребитель:</b><span>{F_Doc_Details___C_Owner}</span></div>',
        '<div class="item"><b>Адрес объекта:</b><span>{F_Doc_Details___C_Address}</span></div>',
        '<div class="item"><b>Квартира:</b><span>{F_Doc_Details___N_Premise_Number}</span></div>',
        '<div class="item"><b>Причина отказа:</b><span>{[this.getReason(values)]}</span></div>',
        '</div>',
        {
            getDate: function (values, field) {
                return Ext.Date.format(values[field], 'd.m.Y H:i:s');
            },
            getReason: function (values,field) {
                if (values.F_Violations && values.F_Violations.C_Name)
                    return values.F_Violations.C_Name;
                return "";
            }
        }        
    ]
});