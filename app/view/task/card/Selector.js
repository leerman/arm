﻿/*
 * выбор типав акта
 */
Ext.define('ARM.view.task.card.Selector', {
    extend: 'Ext.Panel',
    xtype: 'app-task-card-selector',

    items: [
        {
            cls: 'app-task-card-document',
            xtype: 'button',
            ui: 'action-link-red',
            iconCls: 'x-fa fa-angle-down',
            iconAlign: 'right',
            bind: {
                text: '{document_text}'
            }
        }
    ],

    /*
     * 
     * @param items {any[]} список документов
     */
    databind: function (items) {
        var button = this.down('button');
        if (items && items.length > 1) {
            var menu = {
                defaults: {
                    handler: 'onSelectDocument'
                },
                items: []
            };

            items.forEach(function (i) {
                menu.items.push(i);
            });

            button.setMenu(menu);
        } else {
            button.setIconCls('');
        }
    }
});