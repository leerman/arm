﻿/*
 * Акт о безучетном потреблении
 */
Ext.define('ARM.view.task.card.DT_NOT_CONTROL', {
    extend: 'Ext.Panel',
    xtype: 'app-task-card-DT_NOT_CONTROL',


    tpl: [
        '<div class="document-field">',
        '<div class="item"><b>Дата формирования:</b><span>{[this.getDate(values, "D_Date")]}</span></div>',
        '<div class="item"><b>Номер акта:</b><span>{C_Doc_Number}</span></div>',
        '<div class="item"><b>Номер бранка строгой отчетности:</b><span>{F_Blanks___C_Blank_Number}</span></div>',
        '<div class="item"><b>Тип нарушения:</b><span>{F_Doc_Details___F_Violations___C_Name}</span></div>',
        '<div class="item"><b>Период нарушения:</b><span>с {[this.getDate(values, "D_Begin_Violation")]} по {[this.getDate(values, "D_End_Violation")]}</span></div>',
        '<div class="item"><b>Характеристика нарушения:</b><span>{C_Violation}</span></div>',
        '<div class="item"><b>Примечание:</b><span>{C_Note}</span></div>',
        '<tpl if="C_Signature != \'\'"><div class="item signature"><b>Подпись:</b><span><img src="{C_Signature} style="width:80%" /></span></div></tpl>',

        '</div>',
        {
            getDate: function (values, field) {
                return Ext.Date.format(values[field], 'd.m.Y H:i:s');
            }
        }
    ]
});