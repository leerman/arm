﻿Ext.define('ARM.view.task.TaskController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.task',

    user: null,

    onAfterRender: function (sender) {
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('select', this.onUserSelect, sender);
            list.on('unselect', this.onUserUnSelect, sender);

            var selectCardView = list.getSelectCardView();
            if (selectCardView)
                list.fireEvent('select', list, selectCardView);

            list.on('action', this.onAction, sender);
            var mainfilter = sender.up('app-main').down('app-main-filter');
        }

        // уведомлем о готовности
        sender.setIsReady();
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;

        // теперь нужно обновить список пользователей у которых есть информация о их местоположении
        var view = this.getView();
        var me = this;
        if (view) {
            var list = view.getUserListComponent();
            if (list) {
                var routeOptions = view.getRouteOptions();

                users.forEach(function (i) {
                    var record = list.map_uuid[i.get('uuid')];
                    if (record)
                        list.setAccess(record.getId(), true);

                    if (routeOptions && record.getId() == routeOptions.userId) {
                        // здесь раскрываем карточку
                        list.fireEvent('select', list, list.map[record.getId()]);
                    }
                });
            }
        }
    },

    /*
     * обработчик выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserSelect: function (view, cardView) {
        this.showSelectUserInfo(false);
        this.showSelectRouteInfo(true);
        var filterPanel = this.down('app-task-workpanel-filter');
        var model = cardView.getModel();
        if (filterPanel && filterPanel.rendered == true) {
            var controller = this.getController();
            controller.user = model;
            var workPanel = this.down('app-task-workpanel');
            if (workPanel && workPanel.rendered == true) {
                if (workPanel.isHidden() == true)
                    workPanel.show();

                workPanel.setDefaultState();
            }

            var body = this.down('#body');
            if (body) {
                if (body.isHidden() == false)
                    body.hide();

                body.setDefaultState();
            }
            if (model)
                controller.onFilterApply(filterPanel.getFilterValues(), model.get('LINK'));
        } 
    },

    /**
     * отмена выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserUnSelect: function (view, cardView) {
        this.showSelectUserInfo(true);
        this.showSelectRouteInfo(false);
        var workPanel = this.down('app-task-workpanel');
        if (workPanel && workPanel.rendered == true) {
            if (workPanel.isHidden() == false)
                workPanel.hide();

            workPanel.setDefaultState();
        }

        var body = this.down('#body');
        if (body) {
            if (body.isHidden() == false)
                body.hide();

            body.setDefaultState();
        }
    },
    /*
     * обработчик выбора маршрута
     * @param record {any} текущая выбранная запись
     */
    onSelectRoute: function (record) {
        var taskItems = this.getView().down('app-task-items');
        taskItems.setTitle(record.get('name'));
        taskItems.databind(record.get('id'), record.get('done'), this.user.get('LINK'));

        var filterStatus = this.getView().down('app-task-filter-status');
        if (filterStatus)
            filterStatus.databind(record.get('id'));

        this.getView().showSelectRouteInfo(false);

        // теперь нужно вывести body
        var body = this.getView().down('#body');
        if (body) {
            if (body.isHidden() == true)
                body.show();

            body.setDefaultState();
        }

    },

    /*
     * применение фильтрации
     * @param values {any} значение фильтра
     * @param userLINK {string} идентификатор пользователя
     * @param fromPanel {boolean} данные пришли от панели
     */
    onFilterApply: function (values, userLINK, fromPanel) {
        var view = this.getView();
        if (fromPanel == true) {
            view.showSelectRouteInfo(true);

            var body = view.down('#body');
            if (body) {
                if (body.isHidden() == false)
                    body.hide();

                body.setDefaultState();
            }
        }

        if (userLINK) {
            var panel = view.down('app-task-workpanel');
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            panel.mask('Загрузка...');
            var me = this;
            dataProvider.getRoutesStat(userLINK, values.date, values.Person, values.Done, function (items) {
                if (items) {
                    if (panel.rendered == true) {
                        panel.updateData(items, userLINK);
                    }

                    // здесь нужно проверить что у нас вывод конкретного документа
                    var routeOptions = view.getRouteOptions();
                    if (routeOptions) {
                        var results = items.filter(function (i) { return i.id == routeOptions.docLink });
                        if (results.length > 0) {
                            var routeModel = Ext.create('ARM.model.RouteInfo');
                            for (var j in results[0]) {
                                routeModel.set(j, results[0][j]);
                            }
                            me.onSelectRoute(routeModel);
                        }
                    }
                }

                if (panel.rendered == true)
                    panel.unmask();
            });
        }
    },

    /*
     * очистка фильтра
     */
    onFilterClean: function () {

    },

    /**
     * обработчик нажатия на строку документа
     */
    onRowClick: function (sender, record, element, rowIndex, e, eOpts) {
        var info = this.getView().down('app-task-info');
        if (info) {
            info.mask('Загрузка...');
            var link = record.get('LINK');

            if (info.isHidden() == true)
                info.show();

            info.databind(link, record.get('IsDone'), function () {
                if (info.rendered == true)
                    info.unmask();
            });
        }
    },

    /*
     * обработчик действия с карточкой пользователя
     * @param list {any} представление со списком пользователей
     * @param action {string} действие
     * @param record {any} запись пользователя
     * @param card {any} карточка пользователя - представление
     */
    onAction: function (list, action, record, card) {
        var url = 'home/' + action + '/' + record.get('uuid');
        switch (action) {
            case 'user':
            case 'route':
            case 'tasks':
                Ext.getCurrentApp().redirectTo(url);
                break;
        }
    }
});