﻿/*
 * панель для фильтрации и вывода статистики
 */
Ext.define('ARM.view.task.filter.WorkPanel', {
    extend: 'Ext.Panel',
    xtype: 'app-task-workpanel',
    cls: 'app-task-workpanel',

    requires: [
        'ARM.view.task.filter.WorkPanelFilter',
        'ARM.view.task.filter.RouteGrid'
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeWorkHeight: getResponsiveHeight(),
        smallWorkHeight: '!largeWorkHeight'
    },

    hidden: true,
    layout: {
        type: 'vbox',
        pack: 'start'
    },

    defaults: {
        xtype: 'container',
        width: '100%'
    },

    viewModel: {
        data: {
            task_done: 0, // выполнено
            task_work: 0, // в работе (осталось)
        },
        formulas: {
            gettotal: function (get) {
                return get('task_done') + get('task_work');
            }
        }
    },

    items: [
        {
            xtype: 'app-task-workpanel-filter',
            cls: 'app-task-workpanel-filter',
            listeners: {
                apply: 'onFilterApply',
                clean: 'onFilterClean'
            }
        },
        {
            xtype: 'panel',
            ui: 'white',
            cls: 'stat',

            plugins: 'responsive',
            responsiveConfig: {
                'largeWorkHeight': {
                    bodyStyle: {
                        padding: '25px 20px 10px 20px'
                    }
                },
                'smallWorkHeight': {
                    bodyStyle: {
                        padding: '10px 10px 0 10px'
                    }
                }
            },

            defaults: {
                width: '100%'
            },
            layout: {
                type: 'vbox',
                padding: 0,
                pack: 'start'
            },
            items: [
                //#region статистика
                {
                    xtype: 'container',
                    cls: 'progress-title',
                    bind: {
                        html: 'Статистика обработки маршрутов'
                    }
                },
                {
                    xtype: 'core-progressbar',
                    bgcolor: '#e6e6e6',
                    color: '#5ac227',
                    textColor: '#000000',
                    itemId: 'taskDoneId',
                    height: 35,
                    ui: 'white',
                    cls: 'progress'
                },
                {
                    xtype: 'core-progressbar',
                    bgcolor: '#e6e6e6',
                    color: '#ea3a4a',
                    textColor: '#000000',
                    itemId: 'taskWorkId', //ea3a4a
                    height: 35,
                    ui: 'white',
                    cls: 'progress'
                },
                {
                    xtype: 'container',
                    cls: 'progress-title',
                    bind: {
                        html: '<div style="text-align:right">Всего, Т.У.: {gettotal}</div>'
                    }
                }
                //#endregion
            ]
        },
        {
            flex: 1,
            xtype: 'app-task-routegrid',
            emptyText: 'Нет данных',
            listeners: {
                select_route: 'onSelectRoute'
            }
        }
    ],

    /*
     * установить в состояние по умолчанию
     */
    setDefaultState: function () {
        this.down('app-task-workpanel-filter').setDefaultState();

        this.setProgress('taskDoneId', '', 0, 0);
        this.setProgress('taskWorkId', '', 0, 0);
    },

    /*
     * обновление данных на форме
     * @param items {any[]} информация о маршрутах
     * @param userLINK {string} идентификатор пользователя
     */
    updateData: function (items, userLINK) {
        this.down('app-task-routegrid').setStore(this.getRouteInfoStore(items));
        var filter = this.down('app-task-workpanel-filter');
        filter.setUserLink(userLINK);

        var data = { // временная переменная
            task_done: 0,
            task_work: 0
        };

        items.forEach(function (i) {
            data.task_done += i.done;
            data.task_work += i.old
        });

        var total_task = data.task_done + data.task_work;

        var vm = this.getViewModel();
        for (var i in data)
            vm.set(i, data[i]);

        this.setProgress('taskDoneId', 'Выполнено', data.task_done, total_task);
        this.setProgress('taskWorkId', 'В работе', data.task_work, total_task);
    },

    privates: {
        /*
         * возвращается хранилище с информацией о маршрутах
         * @param items {any[]} данные о маршрутах
         */
        getRouteInfoStore: function (items) {
            var store = Ext.create('ARM.store.RouteInfo');
            items.forEach(function (i) {
                store.add(i);
            });

            return store;
        },

        /*
         * устанавливаем параметры для строки состояния
         * @param itemId {string} идентификтаор строки состояния
         * @param msg {string} текст
         * @param current {number} текущий результат
         * @param total {number} общий результат
         */
        setProgress: function (itemId, msg, current, total) {
            var bar = this.down('#' + itemId);
            bar.setText(msg + ' ' + current);
            bar.setProgress((current * 100) / total);
        }
    }
});