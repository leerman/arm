﻿/*
 * таблица маршрутов
 */
Ext.define('ARM.view.task.filter.RouteGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-task-routegrid',
    cls: 'app-task-routegrid white-scroll',
    defaultListenerScope: true,

    /*
     * событие выбора маршрута
     */
    EVENT_SELECT: 'select_route',

    columns: [
        {
            text: 'Номер маршрута',
            dataIndex: 'number',
            hideable: false,
            sortable: false,
            resizable: false,
            draggable: false,
            flex: 1,
            renderer: 'onNumberRouteRenderer'
        },
        {
            text: 'Статистика<br />по маршруту',
            hideable: false,
            sortable: false,
            resizable: false,
            draggable: false,
            renderer: 'onStatRouteRenderer',
            width: 120,
            align: 'center'
        }
    ],

    privates:{
        /*
         * отрисовка колонки номер маршрута
         * @param value {any} значение колонки
         * @param cell {any} ячейка
         * @param record {any} запись
         */
        onNumberRouteRenderer: function (value, cell, record) {
            var done = record.get('done');
            var old = record.get('old');
            var b_done = record.get('b_done');
            var qtip = '';

            var cls = '';
            if (b_done != true) {
                if (done > 0) {
                    cls = 'work';
                    qtip = 'В работе';
                }
                if (done == 0) {
                    cls = 'empty';
                    qtip = 'Назначено';
                }
                if (old == 0) {
                    cls = 'done';
                    qtip = 'Выполнено';
                }
            } else {
                cls = 'done';
                qtip = 'Выполнено';
            }

            return '<span data-qtip="' + qtip + '" class="round-status ' + cls + '">&nbsp;</span> Маршрут ' + record.get('number');
        },

        /*
         * отрисовка колонки со статистикой
         * @param value {any} значение колонки
         * @param cell {any} ячейка
         * @param record {any} запись
         */
        onStatRouteRenderer: function (value, cell, record) {
            var done = record.get('done');
            var old = record.get('old');
            var today = record.get('today');

            var all = done + old;

            return '<div class="stat-column">' +
                '<span class="today" data-qtip="Выполнено сегодня">' + today + '</span> / ' +
                '<span class="done" data-qtip="Выполнено всего">' + done + '</span> / ' +
                '<span class="old" data-qtip="Осталось">' + old + '</span> / ' +
                '<span class="all" data-qtip="Всего">' + all + '</span>' +
                '</div>';
        },

        /*
         * обработчик выбора маршрута
         * @param sender {any} грид
         * @param record {any} запись
         */
        onSelect: function (sender, record, index, eOpts) {
            this.fireEvent(this.EVENT_SELECT, record);
        }
    },

    listeners: {
        select: 'onSelect' 
    }
});