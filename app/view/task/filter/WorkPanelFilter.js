﻿/*
 * панель фильтрации данных
 */
Ext.define('ARM.view.task.filter.WorkPanelFilter', {
    extend: 'Ext.form.Panel',
    ui: 'white',
    xtype: 'app-task-workpanel-filter',
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeWorkFilterHeight: getResponsiveHeight(),
        smallWorkFilterHeight: '!largeWorkFilterHeight'
    },

    layout:{
        type: 'vbox',
        pack: 'start'
    },
    width: '100%',
    defaultListenerScope: true,

    responsiveConfig: {
        'largeWorkFilterHeight': {
            bodyStyle: {
                padding: '25px 20px 10px 20px'
            }
        },
        'smallWorkFilterHeight': {
            bodyStyle: {
                padding: '10px 10px 5px 10px'
            }
        }
    },

    /*
     * событие применения фильтра
     */
    EVENT_APPLY: 'apply',

    /*
     * события очистки фильтра
     */
    EVENT_CLEAN: 'clean',

    /*
     * предыдущие значения
     */
    _lastValue: null,

    userLINK: null,

    viewModel:{
        data: {
            clean: false,
            apply: false,
            personOnly: false
        }
    },

    items: [
        {
            html: 'Актуальные маршруты по состоянию на:',
            xtype: 'panel',
            ui: 'white',
            bodyStyle: {
                'padding-bottom': '5px',
                'font-weight': 'bold !important'
            }
        },
        {
            plugins: 'responsive',
            responsiveConfig: {
                'largeWorkFilterHeight': {
                    margin: '0 0 20px 0'
                },
                'smallWorkFilterHeight': {
                    margin: '0 0 5px 0'
                }
            },

            xtype: 'datefield',
            width: '100%',
            emptyText: 'Дата',
            name: 'date',
            maxValue: new Date(),
            value: new Date(),
            format: 'd.m.Y',
            submitFormat:'m/d/Y',
            ui: 'white',
            flex: 1,
            listeners: {
                change: 'onDateChange'
            },
            triggers: {
                clear: {
                    cls: 'x-form-clear-trigger',
                    handler: 'onDateReset',
                    hidden: true
                }
            }
        },
        {
            hidden: true,
            bind: {
                hidden: '{personOnly}'
            },
            store: {
                data: [
                    {
                        Id: true,
                        Name: 'ФЛ'
                    },
                    {
                        Id: false,
                        Name: 'ЮЛ'
                    }
                ],
                proxy: {
                    type: 'memory'
                }
            },

            plugins: 'responsive',
            responsiveConfig: {
                'largeWorkFilterHeight': {
                    margin: '0 0 20px 0'
                },
                'smallWorkFilterHeight': {
                    margin: '0 0 5px 0'
                }
            },

            width: '100%',
            xtype: 'combo',
            name: 'Person',
            queryMode: 'local',
            displayField: 'Name',
            valueField: 'Id',
            emptyText: 'Субъекты (правовых отношений)',
            ui: 'white',
            listeners: {
                change: 'onPersonChange'
            },
            triggers: {
                clear: {
                    cls: 'x-form-clear-trigger',
                    handler: 'onPersonReset',
                    hidden: true
                }
            }
        },
        {
            store: {
                data: [
                    {
                        Id: true,
                        Name: 'Да'
                    },
                    {
                        Id: false,
                        Name: 'Нет'
                    }
                ],
                proxy: {
                    type: 'memory'
                }
            },

            plugins: 'responsive',
            responsiveConfig: {
                'largeWorkFilterHeight': {
                    margin: '0 0 20px 0'
                },
                'smallWorkFilterHeight': {
                    margin: '0 0 5px 0'
                }
            },

            width: '100%',
            xtype: 'combo',
            name: 'Done',
            queryMode: 'local',
            displayField: 'Name',
            valueField: 'Id',
            emptyText: 'Выполнено (заданий)',
            ui: 'white',
            listeners: {
                change: 'onDoneChange'
            },
            triggers: {
                clear: {
                    cls: 'x-form-clear-trigger',
                    handler: 'onDoneReset',
                    hidden: true
                }
            }
        },
        {
            plugins: 'responsive',
            responsiveConfig: {
                'largeWorkFilterHeight': {
                    margin: '0 0 20px 0'
                },
                'smallWorkFilterHeight': {
                    margin: '0 0 5px 0'
                }
            },

            xtype: 'container',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Применить',
                    ui: 'white-blue',
                    style: {
                        'margin-right': '20px'
                    },
                    handler: 'onApply',
                    bind: {
                        disabled: '{!apply}'
                    },
                    disabled: true
                },
                {
                    xtype: 'button',
                    text: 'Очистить',
                    ui: 'white-blue',
                    bind:{
                        disabled: '{!clean}'
                    },
                    disabled: true,
                    handler: 'onClean'
                }
            ]
        }
    ],

    constructor: function (cfg) {
        this.callParent(arguments);
        var vm = this.getViewModel();
        vm.set('personOnly', Ext.getConf('personOnly'));
    },

    /*
     * возвращаются условия фильтрации с учетом установленого фильтра
     */
    getFilterValues: function () {
        if (this.isFilter == true)
            return this.getValues() || {};

        return {};
    },

    /*
     * установить идентификатор пользователя
     * @param userLINK {string} идентификатор пользователя
     */
    setUserLink: function (userLINK) {
        this.userLINK = userLINK;
    },

    /*
     * установить в состояние по умолчанию
     */
    setDefaultState: function () {
        this.userLINK = null;
        this.onClean();
    },

    privates: {
        /*
         * обработчик изменения даты
         */
        onDateChange: function (sender, newValue) {
            this.onFilterChange(sender, newValue);
        },
        /*
         * изменения ФЛ/ЮЛ
         */
        onPersonChange: function (sender, newValue) {
            this.onFilterChange(sender, newValue);
        },
        /*
         * изменения выполнения
         */
        onDoneChange: function (sender, newValue) {
            this.onFilterChange(sender, newValue);
        },
        /*
         * перемена указывает что фильтр установлен
         */
        isFilter: false,

        /*
         * обработчик применения фильтрации
         */
        onApply: function () {
            this.isFilter = true;
            var vm = this.getViewModel();
            vm.set('clean', true);

            vm.set('apply', false);
            var values = this.getValues();
            if (values.date) {
                values.date = new Date(values.date);
            }
            this._lastValue = values

            this.fireEvent(this.EVENT_APPLY,values , this.userLINK, true);
        },

        /*
         * обработчик отмены фильтрации (очистки)
         */
        onClean: function () {
            this._lastValue = null;
            this.isFilter = false;
            this.reset();

            var vm = this.getViewModel();
            vm.set('clean', false);
            vm.set('apply', false);

            this.fireEvent(this.EVENT_CLEAN);
        },

        /*
         * обработчик изменения фильтра
         */
        onFilterChange: function (field, newValue) {
            var vm = this.getViewModel();
            // здесь проверка на то, что данные изменились
            var values = this.getValues();
            if (!Ext.Object.equals(values, this._lastValues)) {
                vm.set('apply', true);
            } else {
                vm.set('apply', false);
            }
            var clear = field.getTrigger('clear');
            if (clear)
                clear.show();
        },

        /*
         * обработчик очистки даты
         */
        onDateReset: function (field, sender, eOpts) {
            this.onTriggerReset(field, sender, eOpts);
        },
        /*
         * обработчик очистки выполнения
         */
        onDoneReset: function (field, sender, eOpts) {
            this.onTriggerReset(field, sender, eOpts);
        },
        /*
         * обработчик очистки ФЛ/ЮЛ
         */
        onPersonReset: function (field, sender, eOpts) {
            this.onTriggerReset(field, sender, eOpts);
        },

        /**
         * очистка значения в поле
         */
        onTriggerReset: function (field, sender, eOpts) {
            field.suspendEvent('change');
            field.reset();
            sender.hide();
            field.resumeEvent('change');

            var vm = this.getViewModel();
            vm.set('apply', true);

            var values = this.getValues();

            var isCleanActive = false;
            for (var i in values) {
                if (values[i]) {
                    isCleanActive = true;
                    this.fireEvent(this.EVENT_CLEAN, this);
                    break;
                }
            }

            vm.set('clean', isCleanActive);
        },
        /*
         * переопределенная функция
         * если не переопределять, то будет вызываться событие change
         */
        reset: function (resetRecord) {
            Ext.suspendLayouts();
            var fields = this.getForm().getFields().items,
                f,
                fLen = fields.length;
            for (f = 0; f < fLen; f++) {
                fields[f].suspendCheckChange = 1;
                if (fields[f].getTrigger) {
                    var clear = fields[f].getTrigger('clear');
                    if (clear)
                        clear.hide();
                }
            }
            Ext.resumeLayouts(true);
            var result = this.callParent(arguments);
            for (f = 0; f < fLen; f++) {
                fields[f].suspendCheckChange = 0;
                fields[f].lastValue = fields[f].getValue();
            }
            return result;
        }
    }
});