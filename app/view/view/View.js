﻿/*
 * Шаблонное представление
 */
Ext.define('ARM.view.view.View', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-view',
    subType: null,
    ui: 'white',
    scrollable: true,
    constructor: function (cfg) {
        if (cfg.subType) {
            cfg.items = [{
                xtype: 'app-' + cfg.subType
            }];
        }
        this.callParent(arguments);
    }
});