﻿Ext.define('ARM.view.login.LoginModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.login',

    data: {
        login: '',
        password: '',
        remember: localStorage.getItem('rememberMe') == 'true' ? true : false
    }
});