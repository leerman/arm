﻿Ext.define('ARM.view.login.LoginController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.login',

    onLoginClick: function () {
        var view = this.getView();
        view.mask('Авторизация...');
        var viewModel = view.getViewModel();
        var application = Ext.getCurrentApp();
        var authProvider = application.getAuthProvider();

        authProvider.singIn(viewModel.get('login'), viewModel.get('password'), viewModel.get('remember'), function (result) {
            if (result.success == true) {
                application.afterAuthLoadData(function () {
                    view.destroy();
                    if (application.getInitView()) {

                    } else {
                        application.setInitView(Ext.create({
                            xtype: 'app-main',
                            pageName: application.getPageName(),
                            subPageName: application.getSubPageName()
                        }));
                    }
                    application.fireEvent('authorize');
                });
            }
            else {
                view.unmask();
                Ext.Msg.alert('Ошибка авторизации', result.msg);
            }
        });
    },

    /**
     * нажатие на ENTER в поле пароль
     */
    onPswKeyPress: function (sender, e) {
        if (e.getKey() === e.ENTER) {
            this.onLoginClick();
        }
    },

    onAfterRender: function (sender) {
        Ext.defer(function () {
            var username = this.lookupReference('username');
            if (username) {
                username.focus();
            }
        }, 50, this);
    }
});