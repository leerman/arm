﻿Ext.define('ARM.view.login.Login', {
    extend: 'Ext.window.Window',
    xtype: 'app-login',
    layout:'fit',
    requires: [
        'ARM.view.login.LoginController',
        'ARM.view.login.LoginModel',
        'Ext.form.Panel'
    ],

    controller: 'login',
    viewModel: 'login',

    bodyPadding: 10,
    minWidth: 300,
    minHeight: 200,

    title: 'Авторизация',
    closable: false,
    autoShow: true,

    constructor: function () {
        this.callParent(arguments);

        var authProvider = Ext.getCurrentApp().getAuthProvider();
        if (authProvider.getRememberMe() == true) {
            var vm = this.getViewModel();
            vm.set('login', authProvider.getLogin());
            vm.set('rememberMe', authProvider.getRememberMe());
        }
    },

    items: {
        xtype: 'form',
        reference: 'form',
        layout: 'anchor',
        border: false,
        defaults: {
            anchor: '100%'
        },
        ui: 'body-white',
        items: [
            {
                xtype: 'textfield',
                name: 'username',
                fieldLabel: 'Логин',
                allowBlank: false,
                emptyText: 'Логин',
                bind: {
                    value: '{login}'
                },
                reference: 'username'
            },
            {
                xtype: 'textfield',
                name: 'password',
                inputType: 'password',
                fieldLabel: 'Пароль',
                allowBlank: false,
                emptyText: 'Пароль',
                bind: {
                    value: '{password}'
                },
                listeners: {
                    keypress: 'onPswKeyPress'
                },
                enableKeyEvents: true
            },
            {
                xtype: 'checkboxfield',
                name: 'rememberMe',
                fieldLabel: 'Запомнить меня',
                bind: {
                    value: '{remember}'
                }
            }
        ]
    },

    dockedItems: [
        {
            dock: 'bottom',
            xtype: 'toolbar',
            ui: 'dark',
            items: [
                '->',
                {
                    text: 'Войти',
                    ui: 'action-transparent',
                    formBind: true,
                    listeners: {
                        click: 'onLoginClick'
                    }
                }
            ]
        }
    ],

    listeners: {
        afterrender: 'onAfterRender'
    }
});