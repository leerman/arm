Ext.define('ARM.view.appoint.Appoint', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-appoint',
    ui: 'body',
    requires: [
        'ARM.view.appoint.AppointController',
        'ARM.view.appoint.AppointViewModel',
    ],

    controller: 'appoint',
    viewModel: 'appoint',
    reference: 'app-appoint',

    /*
     * объект для работы с картой
     */
    mapAPI: null,
    setMapAPI: function (value) {
        this.mapAPI = value;
    },
    getMapAPI: function () {
        return this.mapAPI;
    },

    listeners: {
        render: 'onRender',
        afterrender: 'onAfterRender',
        beforedestroy: 'onBeforeDestroy',
        setuserstore: 'onSetUserStore',
        boundselected: 'onBoundSelected'
    },

    constructor: function (cfg) {
        this.callParent(cfg);


        this.html = '<iframe src="' + Ext.getCurrentApp().toAbsolutePath('resources/pages/appoint') + '" style="width:100%;height:100%;border:none"><p>Ваш браузер не поддерживает iframe.</p></iframe>';
    },

    getRegions: function (callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();

        dataProvider.getRegions(function (items) {
            var regions = [];

            items.forEach(function (item) {
                regions.push(item.getRegionInfo());
            });
            if (typeof callback == 'function')
                callback(regions);
        });
    },

    addRegion: function (region, callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();

        dataProvider.addRegion(region, callback);
    },

    removeRegion: function (link, callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();

        dataProvider.removeRegion(link, callback);
    },

    updateRegion: function (link, region, callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();

        dataProvider.updateRegion(link, region, callback);
    },

    getTasks: function (users, callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();
        var tasks = new It.Core.Thread.Tasks(this);
        var items = [];
        Ext.each(users, function (user) {
            tasks.Add(function (sender, callback, options) {
                if (!options) // пропускаем пользоватеоей с пустыми IMEI
                    return callback();

                var date = dataProvider.getCurrentDate();

                dataProvider.getUserTasks(options, date, function (results) {
                    Ext.each(results, function (i) {
                        items.push(i);
                    });

                    callback();
                });

            }, null, user.get('uuid'));
        }, this);

        tasks.Run(function () {
            if (typeof callback == 'function')
                callback(items);
        });
    },

    renderLeftblockItems: function (sender) {
        var mainApp = this.up('app-main');
        if (mainApp) {
            var leftblock = mainApp.down('#leftblock');
            if (leftblock) {
                this.leftblock = Ext.create('Ext.Container', {
                    controller: this.getController(),
                    height: 140,
                    cls: 'appoint-leftblock-create-region',
                    padding: '0 20 0 20',
                    items: [
                        {
                            xtype: 'container',
                            html: 'Создание и закрепление участков работы',
                            cls: 'appoint-leftblock-create-region-text',
                            height: 70
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'button',
                                    cls: 'appoint-leftblock-create-region-button',
                                    text: 'Создать',
                                    tooltip: 'Создать новый участок работы',
                                    handler: 'onCreateNewRegion',
                                    width: 100
                                },
                                {
                                    xtype: 'container',
                                    flex:1
                                },
                                {
                                    xtype: 'button',
                                    cls: 'appoint-leftblock-create-region-button',
                                    text: 'Назначить',
                                    tooltip: 'Назначить все непривязанные задания',
                                    handler: 'onAssignZoneTasks',
                                    width: 100
                                }
                            ]
                        }
                    ]
                });

                leftblock.insert(1, this.leftblock);
            }
        }
    },

    /*
     * обработчик отмены фильтрации
     */
    onMainFilterClean: function () {
        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();
    },

    destroyLeftBlockItems: function () {

        if (this.leftblock) {
            this.leftblock.destroy();
        }
    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();

        Ext.getCurrentApp().un('snSignal', controller.onSignal, this);

        if (list) {
            list.un('action', controller.onAction, this);
        }

        this.callParent(arguments);
    }
});