﻿Ext.define('ARM.view.appoint.RegionsMixin', {
    extend: 'Ext.Mixin',

    mixinConfig: {
        id: 'appointregion'
    },

    /**
     * Создание нового региона
     * @param {any} sender - вьюха
     * @param {any} mapAPI - ссылка на объект для работы с картой
     * @param {()=>void} onAddRegion - функция добавления региона
     * @param {(title, msg)=>void} onError - функция вывода сообщения
     */
    createNewRegion: function (sender, mapAPI, onAddRegion, onError) {
        var me = this;
        if (sender) {
            var mapAPI = sender.getMapAPI();

            //вывести форму создания региона
            mapAPI.createNewRegion(function (newRegion, removeTempRegion) {
                //тут нажата кнопка ок
                var coordsStr = [];
                for (var i = 0; i < newRegion.coords.length; i++) {
                    coordsStr.push(newRegion.coords[i][0] + ';' + newRegion.coords[i][1]);
                }

                sender.mask('Добавление нового региона...');
                if (typeof onAddRegion == 'function') {
                    //вызов серверной функции добавления
                    onAddRegion({
                        C_Area: coordsStr.join(','),
                        C_Color: newRegion.color,
                        C_Name: newRegion.displayName,
                        C_Note: newRegion.description
                    }, function () {
                        sender.unmask();
                        //удаляем временный регион
                        if (typeof removeTempRegion == 'function')
                            removeTempRegion();
                    });
                }
            }, function (error) {
                if (typeof onError == 'function')
                    onError('Ошибка создания области контроля', error);
            });
        }
    },

    /***
     * Вывести регионы на карту
     * @param {any} sender - вьюха
     * @param {any[]} regions - список регионов
     * @param {any} mapAPI - ссылка на объект для работы с картой
     * @param {any} functions - функции для региона
     */
    renderRegions: function (sender, regions, mapAPI, functions) {
        var me = this;

        var updateRegion = functions.updateRegion;
        var removeRegion = functions.removeRegion;
        var onError = functions.onError;
        var getUsers = functions.getUsers;
        var changeRegionUser = functions.changeRegionUser;

        var links = [];

        Ext.each(regions, function (region) {
            region.onEdit = function (link) {
                me.editRegion(sender, link, region, mapAPI, updateRegion, onError);
            }

            region.onRemove = function (link) {
                var msg = 'Удалить область контроля?';
                if (region.users) {
                    msg = 'К области контроля назначен обходчик. Все равно удалить?';
                }

                Ext.Msg.confirm('Сообщение', msg, function (btn) {
                    if (btn == 'yes' && typeof removeRegion == 'function')
                        removeRegion(link, function () {
                            mapAPI.removeRegion(link);
                        });
                });
            }

            region.onAppointToUser = function (region) {
                me.appointRegionToUser(sender, region, mapAPI, getUsers, changeRegionUser, onError);
            }

            mapAPI.addRegion(region.link, region.coords, region);
            links.push(region.link);
        });
        return links;
    },

    /**
     * Изменить регион
     * @param {any} sender - вьюха
     * @param {number} link - идентификатор региона
     * @param {any} region - область контроля
     * @param {any} mapAPI - ссылка на объект для работы с картой
     * @param {()=>void} updateRegion - функция обновления региона
     * @param {(title, msg)=>void} onError - функция вывода сообщения
     */
    editRegion: function (sender, link, region, mapAPI, updateRegion, onError) {
        var me = this;

        //вывод формы редактирования региона
        mapAPI.editRegion(link, region, function (newRegion, removeTempRegion) {
            //тут нажали на кнопку ок
            var coordsStr = [];
            for (var i = 0; i < newRegion.coords.length; i++) {
                coordsStr.push(newRegion.coords[i][0] + ';' + newRegion.coords[i][1]);
            }

            sender.mask('Обновление области контроля...');

            updateRegion(link, {
                C_Area: coordsStr.join(','),
                C_Color: newRegion.color,
                C_Name: newRegion.displayName,
                C_Note: newRegion.description
            }, function (loadRegions) {
                //после обновления региона на сервере удаляем ее
                if (typeof removeTempRegion == 'function') {
                    removeTempRegion();
                    //после удаления загружаем регионы(возможно надо грузить лишь измененный регион)
                    if (typeof loadRegions == 'function')
                        loadRegions(function () {
                            sender.unmask();
                        });
                }
            });
        }, function () {
            //тут нажата кнопка отмена
            //отмена всех изменений
            region.force = true;
            mapAPI.addRegion(link, region.coords, region);
        }, function (error) {
            if (typeof onError == 'function')
                onError('Ошибка редактирования области контроля', error);
        });
    },

    /**
     * Назначить пользователя на регион
     * @param {any} sender - вьюха
     * @param {any} region - область контроля
     * @param {any} mapAPI - ссылка на объект для работы с картой
     * @param {(users)=>void} getUsers - функция для получения обходчиков
     * @param {(users)=>void} changeRegionUser - функция для привязки региона к пользователю
     * @param {(title, msg)=>void} onError - функция вывода сообщения
     */
    appointRegionToUser: function (sender, region, mapAPI, getUsers, changeRegionUser, onError) {

        sender.mask('Получение списка обходчиков...');

        if (typeof getUsers == 'function') {
            getUsers(function (users) {
                sender.unmask();

                //выводим форму назначения региона 
                mapAPI.appointRegionToUser(region, users, function (newUser) {
                    sender.mask('Назначение области контроля обходчику...');

                    if (typeof changeRegionUser == 'function') {
                        var updatedRegion = {
                            LINK: region.link,
                            C_Area: region.coords,
                            C_Color: region.color,
                            C_Name: region.displayName,
                            C_Note: region.description,
                            F_Managers: newUser
                        }
                        changeRegionUser(updatedRegion, function () {
                            //тут обновляем данные региона
                            region.force = true;
                            region.users = newUser;
                            mapAPI.addRegion(region.link, region.coords, region);
                            sender.unmask();
                        });
                    }
                }, null, function (error) {
                    if (typeof onError == 'function')
                        onError('Ошибка назначения области контроля', error);
                });
            });
        }
    },

})