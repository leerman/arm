﻿Ext.define('ARM.view.appoint.TasksMixin', {
    extend: 'Ext.Mixin',

    mixinConfig: {
        id: 'appointtasks'
    },

    /**
     * Прорисовать задания
     * @param tasks {any[]} задания
     * @param mapAPI {any} ссылка на карту
     * @param getUsers {(users)=>void} функция для получения пользователей
     * @param createDocument {(user,links[])=>void} функция обратного вызова
     * @param updatePart {bool} обновить только указанные задания
     */
    renderTasks: function (tasks, mapAPI, getUsers, createDocument, updatePart) {
        var me = this;
        mapAPI.addAppointTask(tasks, function (selectedTasks) {
            getUsers(function (users) {
                me._appointTasksToUser(selectedTasks, mapAPI, users, createDocument);
            });
        }, updatePart);
    },

    /**
     * Привязать документ с заданиями на пользователя
     * @param selectedTasks {any[]} задания
     * @param mapAPI {any} ссылка на карту
     * @param users {any[]} список пользователей
     * @param createDocument {()=>void} функция обратного вызова(создать документ)
     */
    _appointTasksToUser: function (selectedTasks, mapAPI, users, createDocument) {
        var me = this;
        var tasks = [];
        for (var i = 0; i < selectedTasks.length; i++) {
            tasks.push({
                doc: selectedTasks[i].get('DocName'),
                status: selectedTasks[i].get('StatusName'),
                address: selectedTasks[i].get('Address'),
                user: selectedTasks[i].get('UserFIO')
            });
        }
        //выводим форму назначения задач к пользователям
        mapAPI.appointTasksToUser(tasks, users, function (user) {
            var links = [];
            var appointedTasks = [];
            for (var i = 0; i < selectedTasks.length; i++) {
                links.push(selectedTasks[i].get('LINK'));
                appointedTasks.push(selectedTasks[i].getData());
            }

            if (typeof createDocument == 'function')
                createDocument(user, links, appointedTasks, function () {
                    me.changeTasksUser(user, links, mapAPI);
                });
        }, function () {

        }, function (error) {
            Ext.Msg.alert('Ошибка назначения заданий', error);
        });

    },

    /**
     * Изменить у заданий кто выполняет 
     */
    changeTasksUser: function (user, links, mapAPI) {
        mapAPI.changeTasksOwner(user, links);
    }
})