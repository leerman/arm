/**
 * @flow
 */

Ext.define('ARM.view.appoint.AppointController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.appoint',
    mixins: [
        'ARM.view.appoint.RegionsMixin',
        'ARM.view.appoint.TasksMixin'
    ],

    onRender: function (sender) {

        Ext.getCurrentApp().on('snSignal', this.onSignal, sender);

        var myMask = new Ext.LoadMask({
            msg: 'Загрузка...',
            target: sender
        });
        myMask.show();


        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('action', this.onAction, sender);
        }

        var iframe = sender.el.query('iframe')[0];
        if (iframe) {
            var me = this;
            iframe.onload = function () {
                sender.setMapAPI(new this.contentWindow.mapAPI({
                    id: 'map',
                    pointConterter: new this.contentWindow.converter(),
                    defaultControls: ['zoomControl', 'searchControl', 'fullscreenControl'],
                    iconGenerator: Ext.conf().getIconPath(),
                    mapOptions: {
                        zoom: Ext.getConf('map_zoom'),
                        center: [Ext.getConf('map_latitude'), Ext.getConf('map_longitude')]
                    },

                    onUserClick: function (data) {
                        me.onUserClick(data);
                    },

                    //выбор заданий правой кнопкой
                    onBoundsSelect: me.onBoundSelected.bind(me, sender)
                }, function () {
                    sender.setIsReady(true);
                    myMask.destroy();

                    sender.mask('Добавление регионов...');
                    me.addRegions(sender, function () {
                        sender.unmask();
                    });
                }));
            }
        }
    },

    /*
     * обработчик сигналов
     */
    onSignal: function (socket, data, tid) {
        if (data.type == 'received') { // уведомления о принятии задания
            try {
                var details = data.data.split(',');
                if (details.length > 0) {
                    PN.Domain.DD_Doc_Details.SetReceived(details, function (records, options, success) {
                        // здесь нужно добавить уведомление на то что задание получено пользователем
                    });
                }
            } catch (exc) {

            }
        }
    },

    onAfterRender: function (sender) {
        sender.renderLeftblockItems();
    },

    onBeforeDestroy: function (sender) {
        sender.destroyLeftBlockItems();
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var filter = sender.up('app-main').down('app-main-filter');
        var values = filter.getLastValues();
        var filterValues = {
            S_MainDivision: -1,
            S_Division: 0,
            S_SubDivision: 0,
            F_Managers: 0
        };
        if (values) {
            filterValues = {
                S_MainDivision: typeof values.MainDivision == 'number' ? values.MainDivision : 0,
                S_Division: typeof values.Division == 'number' ? values.Division : 0,
                S_SubDivision: typeof values.SubDivision == 'number' ? values.SubDivision : 0,
                F_Managers: typeof values.Managers == 'number' ? values.Managers : 0
            };
        }

        var users = userStore.getData().items;
        var userslinks = [];
        for (var i = 0; i < users.length; i++) {
            userslinks.push(users[i].get('LINK'));
        }

        var me = this;
        sender.mask('Обработка элементов на карте');
        _.series(
            [
                // нужно чтобы иконки в списке обновились (обработались)
                function (callback) {
                    sender.mask('Обработка<br />обходчиков...');
                    me.processingPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    sender.mask('Обработка<br />заданий...');
                    me.addTasks(sender, userslinks, filterValues, function () {
                        if (sender.rendered === true) {
                            sender.unmask();
                            callback();
                        }
                    });
                }
            ]);
    },

    /**
     * Добавить задания на карту
     */
    addTasks: function (sender, users, filters, callback) {
        var me = this;
        this.getTasks(sender, users, filters, function (tasks) {
            if (sender.rendered === true)
                me._addTasks(sender, tasks, callback);
        });
    },

    /**
     * Выбор правой кнопкой области с кнопками
     * @params {any[]} tasks - список заданий
     */
    onBoundSelected: function (sender, tasks) {
        var me = this;
        var mapAPI = sender.getMapAPI();
        var mixin = me.mixins.appointtasks;

        if (tasks.length > 0 && mapAPI && mixin && typeof mixin.renderTasks == 'function') {
            this.getUsers(function (users) {
                //показать форму назначения
                mixin._appointTasksToUser(tasks, mapAPI, users, me.appointTasksToUser.bind(me));
            })
        }
    },

    /**
     * Назначить задачи на пользователя
     * @params {any} user - Пользователь к которому привязывается задания
     * @params {string[]} links - идентификаторы заданий для привязки
     * @params {()=>void} callback - функция обратного вызова
     */
    appointTasksToUser: function (user, links, tasks, callback) {
        var me = this;
        var dataProvider = Ext.getCurrentApp().getDataProvider();

        dataProvider.createDocument(user.LINK, links.join(','), function (success, error) {
            if (success === false) {
                Ext.Msg.alert('Ошибка создания документа', error);
            }
            else {
                me.sendSocketTaskUpdate(user, tasks, links);

                if (callback)
                    callback();
            }
        });
    },

    /**
     * Получить список заданий
     */
    getTasks: function (sender, users, filters, callback) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();

        dataProvider.getUnAssignedTasks(users, filters, function (result) {
            callback(result);
        });
    },

    /**
     * Нажатие на кнопку создания региона
     */
    onCreateNewRegion: function () {
        var me = this;
        var main = Ext.getCurrentApp().getInitView();
        if (main) {
            var appoint = main.down('app-appoint');
            var mixin = me.mixins.appointregion;

            if (appoint && mixin && typeof mixin.createNewRegion == 'function') {
                var mapApi = appoint.getMapAPI();

                //рисуем форму для создания нового региона
                mixin.createNewRegion(appoint, mapApi, function (newRegionData, afterAdd) {
                    //тут отправляем новый регион на сервер
                    Ext.getCurrentApp().getDataProvider().addRegion(newRegionData, function () {
                        //загружаем регионы
                        me.addRegions(appoint, function (links) {
                            newRegionData.LINK = links[0];

                            //уведомляем всех диспечеров о добавлении региона
                            me.sendSocketRegionAdd(newRegionData);

                            if (typeof afterAdd == 'function')
                                afterAdd();
                        });
                    });
                }, me.onError);
            }
        }
    },

    /**
     * Получение региона с сокета(добавление или обновление)
     * @param {any} region - области контроля
     */
    responseSocketRegionUpdate: function (region) {
        region.force = true;
        this._addRegions(this.getView(), [region]);
    },

    /**
     * Получение запроса на удаление региона
     * @param {number} link - идентификатор области контроля
     */
    responseSocketRegionRemove: function (link) {
        this.getView().getMapAPI().removeRegion(link);
    },

    /**
     * получение изменений заданий с сокета
     * @param {any[]} tasks - задачи
     */
    responseSocketTasksUpdate: function (tasks) {
        this._addTasks(this.getView(), tasks, null, true);
    },

    /**
     * Отправка по сокету информации о созданном регионе
     * @param {any} region - область контроля
     * пример
     * region = {
     *      LINK: 'some guid'
     *      C_Area: coordsStr.join(','),
     *      C_Color: newRegion.color,
     *      C_Name: newRegion.displayName,
     *      C_Note: newRegion.description
     * }
     */
    sendSocketRegionAdd: function (region) {

    },

    /**
     * Отправка по сокету информации о изменении региона
     * @param {any} region - область контроля
     */
    sendSocketRegionUpdate: function (region) {

    },

    /**
     * Отправка по сокету информации о изменении региона
     * @param {number} link - идентификатор области контроля
     */
    sendSocketRegionRemove: function (link) {

    },

    /**
     * Oтправка по сокету информации о изменениях заданий
     * @param {any} user - пользователь к которому привязывается задания
     * @param {any[]} tasks - задачи
     * @param {string[]} links - идентификаторы задач
     */
    sendSocketTaskUpdate: function (user, tasks, links) {
        var me = this;
        var app = Ext.getCurrentApp();
        var socket = app.getSocket();
        if (socket) {
            var appointFromUser = tasks.filter(function (item) {
                return !Ext.isEmpty(item.UserId);
            });

            if (appointFromUser.length === 0) {
                var count = tasks.length;
                if (count > 0)
                    socket.notifUser(user.uuid,
                        'add_details',
                        'Вам назначен' + (count === 1 ? 'о' : 'ы') + ' ' + count + ' нов' + (count === 1 ? 'ое' : 'ых') + ' задан' + (count === 1 ? 'ие' : 'ий'),
                        links.join(','), function () {
                            me.onTooltip(count + ' задани' + (count === 1 ? 'е' : 'й') + ' доставлено ' + user.C_Fio);
                        });
            }
            else {
                var notUserDocs = tasks.filter(function (item) {
                    return item.UserId !== user.LINK;
                });

                var count = notUserDocs.length;
                if (count > 0)
                    socket.notifUser(user.uuid,
                        'add_details',
                        'Вам назначен' + (count === 1 ? 'о' : 'ы') + ' ' + count + ' нов' + (count == 1 ? 'ое' : 'ых') + ' задан' + (count == 1 ? 'ие' : 'ий'),
                        links.join(','), function () {
                            me.onTooltip(count + ' задани' + (count === 1 ? 'е' : 'й') + ' доставлено ' + user.C_Fio);
                        });

                var unicDocs = {};
                Ext.each(tasks, function (task) {
                    var link = task.UserId;
                    if (!Ext.isEmpty(link)) {
                        //уже был привязан пользователь
                        if (unicDocs[link])
                            unicDocs[link] = {
                                count: unicDocs[link].count + 1
                            }
                        else
                            unicDocs[link] = {
                                link: link,
                                count: 1
                            }
                    }
                });

                this.getUsers(function (usersList) {
                    for (var i in unicDocs) {
                        if (i !== user.LINK) {
                            var count = unicDocs[i].count;
                            var uuid = usersList.filter(function (item) { return item.value.LINK == i })[0].value.uuid;
                            socket.notifUser(uuid,
                                'remove_tasks',
                                'Диспетчером передан' + (count == 1 ? 'о' : 'ы') + ' ' + count + ' задан' + (count == 1 ? 'ие' : 'ий') + ' другому пользователю',
                                links.join(','));
                        }
                    }
                });
            }
        }
    },

    /*
     * обработчик действия с карточкой пользователя
     * @param list {any} представление со списком пользователей
     * @param action {string} действие
     * @param record {any} запись пользователя
     * @param card {any} карточка пользователя - представление
     */
    onAction: function (list, action, record, card) {
        var url = 'home/' + action + '/' + record.get('uuid');
        switch (action) {
            case 'user':
            case 'route':
            case 'tasks':
                Ext.getCurrentApp().redirectTo(url);
                break;
        }
    },

    /**
     * Назначить задания находящиеся внутри зоны
     * @returns {} 
     */
    onAssignZoneTasks: function () {
        var me = this;
        var main = Ext.getCurrentApp().getInitView();
        if (main) {
            var appoint = main.down("app-appoint");
            var mapApi = appoint.getMapAPI();

            var unassignedTasks = mapApi.getUnAssignedTasks();
            if (unassignedTasks.length > 0) {
                appoint.mask('Назначение заданий обходчикам...');
                var assingnParalel = [];
                for (var i = 0; i < unassignedTasks.length; i++) {
                    var task = unassignedTasks[i];
                    assingnParalel.push(function (callback) {
                        var usr = this.user;
                        var lnks = this.links;
                        me.appointTasksToUser(this.user, this.links, this.tasks, function () {
                            mapApi.changeTasksOwner(usr, lnks);
                            callback();
                        });
                    }.bind(task));
                }

                _.parallel(assingnParalel, function () {
                    appoint.unmask();
                });
            }
        }
    },

    privates: {

        /**
         * Вывод ошибок
         * @param {string} title - заголовок сообщения
         * @param {string} msg - сообщение
         */
        onError: function (title, msg) {
            Ext.Msg.alert(title, msg)
        },

        /**
         * Добавление областей контроля
         * @param {any} sender - вьюха
         * @param {()=>viod} callback - функция обратного вызова
         */
        addRegions: function (sender, callback) {
            var me = this;
            sender.getRegions(function (results) {
                var renderedLinks = me._addRegions(sender, results);

                if (typeof callback == 'function')
                    callback(renderedLinks);
            })
        },

        /**
         * Добвить области контроля на карту
         * @param {any} sender - вьюха
         * @param {any[]} regions - области контроля
         */
        _addRegions: function (sender, regions) {
            var me = this;
            var mapAPI = sender.getMapAPI();
            if (mapAPI) {
                var mixin = me.mixins.appointregion;
                if (mixin && typeof mixin.renderRegions == 'function') {

                    //функции для регионов
                    var functions = {
                        //обработка ошибок
                        onError: me.onError,
                        //обновление региона
                        updateRegion: function (link, region, afterUpdateRegion) {
                            sender.updateRegion(link, region, function () {
                                afterUpdateRegion(function (afterLoadRegions) {
                                    me.addRegions(sender, function () {
                                        region.LINK = link;
                                        //уведомляем диспечеров о изменении данных региона
                                        me.sendSocketRegionUpdate(region);

                                        if (typeof afterLoadRegions == 'function')
                                            afterLoadRegions();
                                    });
                                })
                            });
                        },
                        //удаление региона
                        removeRegion: function (link, afterRemove) {
                            sender.removeRegion(link, function () {
                                me.sendSocketRegionRemove(link);

                                if (typeof afterRemove == 'function')
                                    afterRemove();
                            });
                        },
                        //получить список обходчиков
                        getUsers: me.getUsers,
                        //привязать обходчика к региону
                        changeRegionUser: function (region, callback) {

                            Ext.getCurrentApp().getDataProvider().changeRegionUser(region.F_Managers, region.LINK, function () {

                                //уведомляем диспечеров об изменениях
                                me.sendSocketRegionUpdate(region);

                                if (typeof callback == 'function')
                                    callback();
                            })
                        }
                    }

                    //выводим регионы
                    var renderedLinks = mixin.renderRegions(sender, regions, mapAPI, functions);

                    return renderedLinks;
                }
            }
        },

        /**
         * Добавление заданий на карту
         */
        _addTasks: function (sender, tasks, callback, updatePart) {
            var me = this;
            var dataWorker = sender.getYandexDataWorker();

            // тут нужно прочитать все записи и определить есть ли у них адреса
            // затем нужно добавить им координаты
            var geoData = {};
            for (var i = 0; i < tasks.length; i++) {
                var point = tasks[i];
                var address = point.get('Address');
                if (address) {
                    if (point.isCoordExist() != true) {
                        var hash = md5(address);
                        point.set('hash', hash);
                        geoData[hash] = address;
                    }
                } else {
                    // здесь нужно что-то делать с точка у которых нет адреса
                    new Exception(new Error('Адрес не указан'));
                }
            }

            var count = Object.keys(geoData).length;

            if (sender.showProgressBar) {
                var isShowProgressBar = false;
                var timeout = setTimeout(function () {
                    isShowProgressBar = true;
                }, 2000);
                var msg = 'Обработка геоданных. Подождите';
                dataWorker.onTask({ type: 'geocoder', data: geoData }, function (results, options) {
                    if (results == null) {
                        var percent = ((options * 100) / count).toFixed(0);
                        if (isShowProgressBar == true)
                            sender.showProgressBar(msg + ' ' + percent + '%', percent);
                        // значит это только итерация
                        return;
                    } else {
                        sender.showProgressBar();
                        clearTimeout(timeout);
                    }

                    // нужно сгруппировать точки по координатам
                    for (var i = 0; i < tasks.length; i++) {
                        var point = tasks[i];
                        var hash = point.get('hash');

                        if (results[hash])
                            point.setCoord(results[hash]);
                    }

                    // тут обработали все координаты
                    var mapAPI = sender.getMapAPI();
                    var mixin = me.mixins.appointtasks;
                    if (mapAPI && mixin && typeof mixin.renderTasks == 'function') {
                        mixin.renderTasks(tasks, mapAPI, me.getUsers, me.appointTasksToUser.bind(me), updatePart);

                        if (typeof callback == 'function')
                            callback();
                    }
                });
            }
        },

        /**
         * Получить список обходчиков
         * @param {()=>void} функция обратного вызова
         */
        getUsers: function (callback) {
            var initView = Ext.getCurrentApp().getInitView();
            var users = initView.down('app-user-list').store.getData();

            var usersList = [];
            for (var i = 0; i < users.length; i++) {
                var user = users.getAt(i);
                usersList.push({
                    name: user.get('C_Fio'),
                    value: {
                        LINK: user.get('LINK'),
                        C_Fio: user.get('C_Fio'),
                        uuid: user.get('uuid'),
                    }
                });
            }

            if (typeof callback == 'function')
                callback(usersList);
        },

        /*
         * Обработка пользователей
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        processingPlacemarks: function (sender, users, callback) {
            var list = sender.getUserListComponent();
            if (list) {
                users.forEach(function (i) {
                    var record = list.map_uuid[i.get('uuid')];
                    if (record)
                        list.setAccess(record.getId(), true);
                });
            }

            if (typeof callback == 'function')
                callback();
        },

        /*
         * обработчик уведомлений
         * @param txt {string} текст сообщения
         */
        onTooltip: function (txt) {
            var app = Ext.getCurrentApp();
            app.showTooltip('Уведомление', txt);
        }
    }
});