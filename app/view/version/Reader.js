﻿/*
 * предназначен для чтения данных о версии
 */
Ext.define('ARM.view.version.Reader', {
    extend: 'Ext.data.reader.Json',
    alias: 'reader.md_reader',

    getResponseData: function (response) {
        var versions = [];
        try {
            var str = response.responseText;
            var i = str.indexOf('### ');
            do {
                i = str.indexOf('### ', i + 1);
                if (i > 0) {
                    var j = str.indexOf('---', i);

                    var verStr = str.substr(i, j - i);

                    // достаем версию
                    var descStart = verStr.indexOf('####');
                    var versionNumber = verStr.substr(0, descStart).replace('### ', '').replace('\n', '');

                    var date = Ext.Date.parse(verStr.substr(descStart + 5, 19), 'd.m.Y H:i:s');
                    var text = verStr.substr(descStart + 25, j - (descStart + 25)).replace('\n', '');
                    versions.push({
                        id: versionNumber,
                        date: date,
                        message: text
                    });
                    i = j;
                }

            } while (i > 0);

            return versions;
        } catch (ex) {
            Ext.Logger.warn('Unable to parse the JSON returned by the server');
            return this.createReadError(ex.message);
        }
    }
});