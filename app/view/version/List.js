﻿Ext.define('ARM.view.version.List', {
    extend: 'Ext.Panel',
    xtype: 'app-version',
    ui: 'white',
    bodyPadding: 10,
    layout: {
        type: 'vbox',
        pack: 'start'
    },
    defaultListenerScope: true,

    requires: [
        'ARM.store.Version'
    ],

    defaults: {
        width: '100%'
    },

    initComponent: function () {
        var me = this;
        this.callParent(arguments);

        var store = Ext.create('ARM.store.Version');

        this.add({
            html: '<h2 style="text-align:center">История изменений</h2>',
            xtype: 'panel',
            ui: 'white'
        });

        this.add(
            {
                flex: 1,
                xtype: 'dataview',
                infinite: true,
                itemHeight: 100,
                store: store,
                disableSelection: true,
                style: {
                    background: '#ffffff'
                },
                itemTpl: [
                    '<div style="padding-right:50px;position:relative;color:black" class="item">'
                    + '<b>{id}</b><br /><i>{[this.getDate(values)]}</i> {message}</div><hr />',
                    {
                        getDate: function (values) {
                            return Ext.Date.format(values.date, 'd.m.Y H:i:s');
                        }
                    }
                ]
            }
        );
        // нужно проверить что будет если записей больше 25
        store.sort('date', 'DESC');
        store.load({
            limit: 10000,
            callback: function (items) {

            }
        });
    },

    destroy: function () {
        this.callParent(arguments);
    }
});