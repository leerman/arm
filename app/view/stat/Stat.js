﻿/*
 * Статистика
 */
Ext.define('ARM.view.stat.Stat', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-stat',
    requires: [
        'ARM.view.stat.StatController',
        'ARM.view.stat.StatModel',

        'Ext.chart.CartesianChart',

        'ARM.view.stat.store.TaskByDay',

        'ARM.view.stat.Filter',
        'ARM.view.stat.RightStat'
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeStatHeight: getResponsiveHeight(),
        smallStatHeight: '!largeStatHeight',
        largeStatWidth: getResponsiveWidth(),
        smallStatWidth: '!largeStatWidth'
    },

    ui: 'white',
    controller: 'stat',
    viewModel: 'stat',
    layout: {
        type: 'vbox',
        pack: 'start'
    },
    
    listeners: {
        render: 'onRender',
        setuserstore: 'onSetUserStore'
    },
    items: [
        {
            xtype: 'panel',
            ui: 'white',

            plugins: 'responsive',
            responsiveConfig: {
                'largeStatHeight && largeStatWidth': {
                    style: {
                        padding: '10px'
                    }
                },
                'smallStatHeight || smallStatWidth': {
                    style: {
                        padding: '0 10px 0 10px'
                    }
                }
            },

            html: '<h2>График плана и фактически выполненной работы по точкам</h2>',
            width: '100%'
        },
        {
            xtype: 'app-stat-filter',
            listeners: {
                change: 'onFilterChange'
            }
        },
        {
            padding:10,
            xtype: 'app-stat-mainchart',
            reference: 'chart',
            width: '100%',
            flex: 1
        }
    ],

    onMainFilterClean: function () {
        this.onMainChartUpdate(function () { });
    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);

            list.un('select', controller.onUserSelect, this);
            list.un('unselect', controller.onUserUnSelect, this);
        }

        this.callParent(arguments);
    },

    privates: {
        /*
         * обработчик обновление главного графика
         * @param callback {()=>void} функция обратного вызова
         */
        onMainChartUpdate: function (callback) {
            var controller = this.getController();
            var chart = this.lookup('chart');
            var data = controller.getFilter();
            if (chart) {
                var now = new Date();
                var startDate = new Date(now.getFullYear(), now.getMonth(), 1);
                var endDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59);
                chart.dataBind(data.mainDivision, data.division, data.subDivision, data.res, data.user, startDate, endDate, callback);
            }

            var rightPanel = this.getRightPanel();
            if (rightPanel) {
                rightPanel.showPanel(data, null);
            }
        }
    }
});