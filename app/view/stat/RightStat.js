﻿/*
 * правая панель
 */
Ext.define('ARM.view.stat.RightStat', {
    extend: 'ARM.shared.BaseRightPanel',
    ui: 'right-stat',
    title: 'Статистика по эффективности обхода точек',
    //layout: 'fit',
    items: [
        {
            xtype: 'grid',
            emptyText: 'Нет данных',
            cls: 'grid-right-stat',
            padding: '5px',
            scrollable: true,
            store: Ext.create('ARM.view.stat.store.UserRate'),
            columns: [
                {
                    xtype: 'rownumberer',
                    width: 25,
                    align: 'center'
                },
                {
                    text: 'ФИО',
                    dataIndex: 'C_Fio',
                    flex: 1,
                    hideable: false,
                    sortable: false,
                    resizable: false,
                    draggable: false,
                    renderer: function (value, cell, record) {
                        var mainDivision = record.get('C_MainDivision');
                        var division = record.get('C_Division');
                        var subDivision = record.get('C_SubDivision');
                        var html = '';
                        if (mainDivision)
                            html += mainDivision + '<br />';
                        if (division)
                            html += division + '<br />';
                        if (subDivision)
                            html += subDivision + '<br />';
                        return '<div>' + value + '<div class="user-rate-info">' + html + '</div></div>';
                    }
                },
                {
                    text: 'Кол-во<br />точек',
                    dataIndex: 'N_Count',
                    hideable: false,
                    sortable: false,
                    resizable: false,
                    draggable: false,
                    //width: 35,
                    //align: 'right',
                    renderer: function (value, cell, record) {
                        return '<div style="text-align:center;margin-right:5px">' + value + '</div>';
                    }
                }
            ]
        }
    ],

    /**
     * обновление содержимой панели
     * @param options {any} дополнительные опции
     * @param callback {()=>void} функция обраного вызова
     */
    refreshPanel: function (options, callback) {
        // тут можно достать информацию по пользователю
        options = options || this.getOptions();
        var me = this;
        var mask = new Ext.LoadMask({
            target: this.getGrid(),
            msg: 'Загрузка...'
        });

        Ext.defer(function () {
            mask.show();

            if (options) {
                var data = options.view;
                me.dataBind(data.mainDivision, data.division, data.subDivision, data.res, data.start, data.end, function () {
                    mask.destroy();
                    if (typeof callback == 'function')
                        callback();
                });
            }
        }, 50);
    },

    privates: {
        getGrid: function () {
            return this.down('grid');
        },

        /*
         * добавление данных
         * @param mainDivision {number} филиал
         * @param division {number} отделение
         * @param subDivision {number} участок
         * @param res {number} рес
         * @param dateStart {Date} дата начала
         * @param dateEnd {Date} дата завершения
         * @param callback {()=>void} функция обратного вызова
         */
        dataBind: function (mainDivision, division, subDivision, res, dateStart, dateEnd, callback) {
            var store = this.getGrid().getStore();
            var me = this;
            store.load({
                limit: 10000,
                params: {
                    "params": [mainDivision, division, subDivision, res, dateStart, dateEnd]
                },
                callback: function (items) {
                    if (typeof callback == 'function')
                        callback();
                }
            });
        }
    }
});