﻿/*
 * главнй график
 */
Ext.define('ARM.view.stat.MainChart', {
    extend: 'Ext.chart.CartesianChart',
    xtype: 'app-stat-mainchart',

    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeChartWidth: getResponsiveWidth(),
        smallChartWidth: '!largeChartWidth'
    },

    defaultListenerScope: true,

    DT_CONTROL_METER_READINGS_TEXT: 'Акт снятия контрольного показания',
    DT_CONTROL_CHECK_TEXT: 'Акт инструментальной проверки',
    DT_NOT_CONTROL_TEXT: 'Акт безучетного потребления',

    viewModel: {
        data: {

        }
    },

    legend: {
        docked: 'top',
        border: {
            xtype: 'legendborder',
            strokeOpacity: 0,
            radius: 0
        }
    },
    colors: ['#ffb912', 'blue', '#3fbcef', '#f25858', '#2cb316', '#596ad7'],
    animation: {
        duration: 200
    },

    responsiveConfig: {
        'largeChartWidth': {
            innerPadding: {
                left: 40,
                right: 40
            }
        },
        'smallChartWidth': {
            innerPadding: {
                left: 20,
                right: 20
            }
        }
    },

    axes: [{
        type: 'numeric',
        position: 'left',
        grid: true,
        renderer: 'onAxisLabelRender',
        title: {
            text: 'Кол-во заданий',
            fontSize: 12
        }
    }, {
        type: 'category',
        position: 'bottom',
        grid: true,
        label: {
            rotate: {
                degrees: -45
            }
        },
        title: {
            text: 'День',
            fontSize: 12
        }
    }],
    
    constructor: function (cfg) {
        cfg.series = [{
            type: 'line',
            xField: 'Day',
            yField: 'Total_Plan',
            style: {
                lineWidth: 2
            },
            marker: {
                radius: 3,
                lineWidth: 1
            },
            label: {
                field: 'Total_Plan',
                display: 'over',
                color: 'transparent'
            },
            highlight: {
                fillStyle: '#ffb912',
                radius: 3,
                lineWidth: 1,
                strokeStyle: '#fff'
            },
            tooltip: {
                trackMouse: true,
                showDelay: 0,
                dismissDelay: 0,
                hideDelay: 0,
                renderer: 'onSeriesTooltipRender'
            },
            title: 'Запланировано'
        },
        {
            type: 'line',
            xField: 'Day',
            yField: 'Today',
            style: {
                lineWidth: 2
            },
            marker: {
                radius: 3,
                lineWidth: 1
            },
            label: {
                field: 'Today',
                display: 'over',
                color: 'transparent'
            },
            highlight: {
                fillStyle: 'blue',
                radius: 3,
                lineWidth: 1,
                strokeStyle: '#fff'
            },
            tooltip: {
                trackMouse: true,
                showDelay: 0,
                dismissDelay: 0,
                hideDelay: 0,
                renderer: 'onSeriesTooltipRender'
            },
            title: 'Назначено'
        },
        {
            type: 'line',
            xField: 'Day',
            yField: 'Total_Fact',
            style: {
                lineWidth: 2
            },
            marker: {
                radius: 3,
                lineWidth: 1
            },
            label: {
                field: 'Total_Fact',
                display: 'over',
                color: 'transparent'
            },
            highlight: {
                fillStyle: '#3fbcef',
                radius: 3,
                lineWidth: 1,
                strokeStyle: '#fff'
            },
            tooltip: {
                trackMouse: true,
                showDelay: 0,
                dismissDelay: 0,
                hideDelay: 0,
                renderer: 'onSeriesTooltipRender'
            },
            title: 'Выполнено'
        }];
        this.getUserDocuments().forEach(function (i) {
            cfg.series.push(i)
        });

        this.callParent(arguments);
    },

    /*
     * добавление данных
     * @param mainDivision {number} филиал
     * @param division {number} отделение
     * @param subDivision {number} участок
     * @param res {number} рес
     * @param user {string} пользователь
     * @param dateStart {Date} дата начала
     * @param dateEnd {Date} дата завершения
     * @param callback {()=>void} функция обратного вызова
     */
    dataBind: function (mainDivision, division, subDivision, res, user, dateStart, dateEnd, callback) {
        var store = Ext.create('ARM.store.GetCommonStatisticsQuery');
        var me = this;
        store.load({
            limit: 10000,
            params: {
                "params": [mainDivision, division, subDivision, res, user, dateStart, dateEnd]
            },
            callback: function (items) {
                var data = [];
                var item = null;
                Ext.each(items, function (i) {
                    if (!i.get('C_Const')) {
                        if (item) {
                            data.push(item);
                            item = null;
                        }
                        item = {
                            Date: i.get('D_Date'),
                            Day: Ext.Date.format(i.get('D_Date'), 'd'),
                            Total_Plan: i.get('N_Plan'),
                            Total_Fact: i.get('N_Fact'),
                            Today: i.get('N_Today')
                        };
                    } else {
                        item['Total_Plan_' + i.get('C_Const')] = i.get('N_Plan');
                        item['Total_Fact_' + i.get('C_Const')] = i.get('N_Fact');
                    }
                });
                var storeResult = Ext.create('ARM.view.stat.store.TaskByDay');
                storeResult.setData(data);
                me.setStore(storeResult);

                if (typeof callback == 'function')
                    callback();
            }
        });
    },

    /*
     * очистка данных - графика
     */
    clean: function () {
        var storeResult = Ext.create('ARM.view.stat.store.TaskByDay');
        this.setStore(storeResult);
    },

    onAxisLabelRender: function (axis, label, layoutContext) {
        // Custom renderer overrides the native axis label renderer.
        // Since we don't want to do anything fancy with the value
        // ourselves except appending a '%' sign, but at the same time
        // don't want to loose the formatting done by the native renderer,
        // we let the native renderer process the value first.
        return layoutContext.renderer(label);
    },

    onSeriesTooltipRender: function (tooltip, record, item) {
        if (record) {

            switch (item.field) {
                case 'Today':
                    tooltip.setHtml(Ext.String.format('{0} было {1} {2} задания', Ext.Date.format(record.get('Date'), 'd M'), 'назначено в этот день', record.get(item.field)));
                    break;

                case 'Total_Plan':
                    tooltip.setHtml(Ext.String.format('{0} было {1} {2} заданий', Ext.Date.format(record.get('Date'), 'd M'), 'активно', record.get(item.field)));
                    break;

                default:
                    tooltip.setHtml(Ext.String.format('{0} было {1} {2} заданий', Ext.Date.format(record.get('Date'), 'd M'), item.field.indexOf('Total_Plan') >= 0 ? 'загружено' : 'выполнено', record.get(item.field)));
                    break;
            }
        } else {
            tooltip.setHtml('Информация не найдена');
        }
    },

    privates: {
        /*
         * вовзвращаются списка выходных документов 
         */
        getUserDocuments: function () {
            return [{
                type: 'line',
                xField: 'Day',
                yField: 'Total_Fact_DT_CONTROL_METER_READINGS',
                style: {
                    lineWidth: 1
                },
                marker: {
                    radius: 3,
                    lineWidth: 1
                },
                label: {
                    field: 'Total_Fact_DT_CONTROL_METER_READINGS',
                    display: 'over',
                    color: 'transparent'
                },
                highlight: {
                    fillStyle: '#f25858',
                    radius: 3,
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tooltip: {
                    trackMouse: true,
                    showDelay: 0,
                    dismissDelay: 0,
                    hideDelay: 0,
                    renderer: 'onSeriesTooltipRender'
                },
                title: this.DT_CONTROL_METER_READINGS_TEXT
            },
            {
                type: 'line',
                xField: 'Day',
                yField: 'Total_Fact_DT_CONTROL_CHECK',
                style: {
                    lineWidth: 1
                },
                marker: {
                    radius: 3,
                    lineWidth: 1
                },
                label: {
                    field: 'Total_Fact_DT_CONTROL_CHECK',
                    display: 'over',
                    color: 'transparent'
                },
                highlight: {
                    fillStyle: '#2cb316',
                    radius: 3,
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tooltip: {
                    trackMouse: true,
                    showDelay: 0,
                    dismissDelay: 0,
                    hideDelay: 0,
                    renderer: 'onSeriesTooltipRender'
                },
                title: this.DT_CONTROL_CHECK_TEXT
            },
            {
                type: 'line',
                xField: 'Day',
                yField: 'Total_Fact_DT_NOT_CONTROL',
                style: {
                    lineWidth: 1
                },
                marker: {
                    radius: 3,
                    lineWidth: 1
                },
                label: {
                    field: 'Total_Fact_DT_NOT_CONTROL',
                    display: 'over',
                    color: 'transparent'
                },
                highlight: {
                    fillStyle: '#596ad7',
                    radius: 3,
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tooltip: {
                    trackMouse: true,
                    showDelay: 0,
                    dismissDelay: 0,
                    hideDelay: 0,
                    renderer: 'onSeriesTooltipRender'
                },
                title: this.DT_NOT_CONTROL_TEXT
            }];
        }
    }
});