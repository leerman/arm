﻿/*
 * Статистика по пользователям за указанный период
 */
Ext.define('ARM.view.stat.store.UserRate', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.User',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Doc_Details.GetBestUsersQuery'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});