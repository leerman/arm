﻿/*
 * статистика за день
 */
Ext.define('ARM.view.stat.store.TaskByDay', {
    extend: 'Ext.data.Store',
    model: 'ARM.view.stat.model.TaskByDay',

    proxy:{
        type: 'memory'
    }
});