﻿/*
 * компонент для вывода статистики по версиям приложения
 */
Ext.define('MobileService.view.stat.MobileService', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.chart.PolarChart',
        'Ext.chart.interactions.Rotate',
        'Ext.chart.series.Pie3D'
    ],
    title: 'Статистика приложений',
    modal: true,
    maximizable: false,
    minimizable: false,
    closeAction: 'method-destroy',
    width: '90%',
    height: '90%',
    defaultListenerScope: true,
    layout: {
        type: 'vbox',
        pack: 'start'
    },
    viewModel: {
        data: {
            version: ''
        }
    },

    users: {
        map: {},
        /*
         * возвращается общее кол-во записей
         */
        getCount: function () {
            var count = 0;
            for (var i in this.map)
                count += this.map[i];
            return count;
        }
    },

    constructor: function (cfg) {
        this.callParent(arguments);

        var me = this;
        me.loadData(function () {
            me.down('#chart').setStore(me.getInsertStore(me.users));
        });

        me.getActualApkVersion();
    },

    defaults: {
        width: '100%'
    },

    items: [
        {
            ui: 'white',
            xtype: 'panel',
            style: {
                'text-align': 'center'
            },
            bind: {
                html: 'Актуальная версия: <b>{version}</b>' 
            }
        },
        {
            flex: 1,
            xtype: 'polar',
            itemId: 'chart',
            innerPadding: 40,
            store: Ext.create('Ext.data.Store', {
                fields: ['version', 'count'],
                data: []
            }),
            interactions: ['itemhighlight', 'rotate'],
            legend: {
                type: 'sprite',
                docked: 'bottom'
            },
            series: [
                {
                    type: 'pie3d',
                    angleField: 'count',
                    donut: 30,
                    distortion: 0.6,
                    highlight: {
                        margin: 40
                    },
                    label: {
                        field: 'version'
                    },
                    tooltip: {
                        trackMouse: true,
                        renderer: 'onSeriesTooltipRender'
                    }
                }
            ]
        }
    ],

    privates: {
        /*
         * загрузка данных
         * @param callback {()=>void} функция обратного вызова
         */
        loadData: function (callback) {
            var app = Ext.getCurrentApp();
            var me = this;
            app.getDataProvider().getWalkers(function (items) {
                me.users.map = {};
                Ext.each(items, function (i) {
                    var version = i.get('C_Mobile_Version') || 'неизвестно';
                    if (!me.users.map[version])
                        me.users.map[version] = 0;
                    ++me.users.map[version];
                });
                if (typeof callback == 'function')
                    callback();
            });
        },

        onSeriesTooltipRender: function (tooltip, record, item) {
            tooltip.setHtml(record.get('version') + ' кол-во ' + record.get('count'));
        },

        /*
         * возвращается хранилище для добавления
         * @param data {any} данные
         */
        getInsertStore: function (data) {
            var items = [];
            for (var i in data.map) {
                items.push({
                    version: Ext.String.format('{0} ({1}%)', i, ((data.map[i] * 100) / data.getCount()).toFixed(0)),
                    count: data.map[i]
                });
            }

            return Ext.create('Ext.data.Store', {
                fields: ['version', 'count'],
                data: items
            });
        },

        /*
         * чтение актуальной версии apk
         * @param callback {()=>void} функция обратного вызова
         */
        getActualApkVersion: function () {
            var me = this;
            Ext.Ajax.request({
                url: Ext.getConf('ws_url') + Ext.getConf('virtualDirPath') + '/apk',
                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    var vm = me.getViewModel();
                    if (vm) {
                        vm.set('version', obj.version);
                    }
                },

                failure: function (response, opts) {
                    Ext.Msg.alert('server-side failure with status code ' + response.status);
                }
            })
        }
    }

});