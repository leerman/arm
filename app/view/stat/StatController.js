﻿Ext.define('ARM.view.stat.StatController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.stat',

    onRender: function (sender) {
        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('select', this.onUserSelect, sender);
            list.on('unselect', this.onUserUnSelect, sender);

            list.on('action', this.onAction, sender);
            var selectCardView = list.getSelectCardView();
            if (selectCardView) {
                list.fireEvent('select', list, selectCardView);
            }
        }

        // встраиваем свой компонент в правую часть
        var initView = sender.up('app-main');
        if (initView) {
            var rightPanel = initView.lookupReference('rightpanel');
            if (rightPanel) {
                rightPanel.refreshPanel('ARM.view.stat.RightStat');
                var data = this.getFilter();
                rightPanel.showPanel(data, null);
            }
        }

        sender.setIsReady();
    },

    /*
     * обработчик изменение фильтра
     * @param values {any} данные по фильтру
     */
    onFilterChange: function (values) {
        var chart = this.lookupReference('chart');
        if (chart) {
            var data = this.getFilter();
            chart.dataBind(data.mainDivision, data.division, data.subDivision, data.res, data.user, data.start, data.end);
            var rightPanel = this.getView().getRightPanel();
            if (rightPanel) {
                rightPanel.showPanel(data, null);
            }
        }
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;
        var me = this;
        _.series(
            [
                // нужно чтобы иконки в списке обновились (обработались)
                function (callback) {
                    sender.mask('Обработка<br />обходчиков...');
                    me.processingPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    sender.mask('Загрузка данных<br />для графика...');
                    sender.onMainChartUpdate(callback);
                },
                function (callback) {
                    sender.unmask();
                    callback();
                }
            ]);
    },

    /*
     * обработчик выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserSelect: function (view, cardView) {
        var model = cardView.getModel();
        if (model) {
            var chart = this.lookupReference('chart');
            if (chart) {
                var data = this.getController().getFilter();
                chart.dataBind(data.mainDivision, data.division, data.subDivision, data.res, model.get('LINK'), data.start, data.end);
            }
        }
    },

    /**
     * отмена выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserUnSelect: function (view, cardView) {
        var chart = this.lookupReference('chart');
        if (chart) {
            var data = this.getController().getFilter();
            chart.dataBind(data.mainDivision, data.division, data.subDivision, data.res, null, data.start, data.end);
        }
    },

    privates: {
        /*
         * возвращается фильтр для построения запроса
         * @param values {any} данные с панели фильтрации
         */
        getFilter: function (values) {
            var data = {};
            var view = this.getView();
            if (view) {
                // тут нужна обработка фильтра
                var filter = view.up('app-main').down('app-main-filter');
                var filter_values = null;
                if (filter_values = filter.getLastValues()) {
                    data = {
                        mainDivision: filter_values.MainDivision,
                        division: filter_values.Division,
                        subDivision: filter_values.SubDivision,
                        res: filter_values.RES
                    };
                }

                var local_filter = view.down('app-stat-filter');
                var local_filter_values = null;
                if (local_filter_values = local_filter.getValues()) {
                    data.start = Ext.Date.parse(local_filter_values.dateBeginStart, 'd.m.Y');
                    data.end = Ext.Date.parse(local_filter_values.dateBeginFinish, 'd.m.Y');
                }

                var list = view.getUserListComponent();
                if (list) {
                    var selectCardView = list.getSelectCardView();
                    if (selectCardView) {

                        var model = selectCardView.getModel();
                        if (model) {
                            data.user = model.get('LINK');
                        }
                    }
                }
            }
            return data;
        },
        /*
         * обработчик действия с карточкой пользователя
         * @param list {any} представление со списком пользователей
         * @param action {string} действие
         * @param record {any} запись пользователя
         * @param card {any} карточка пользователя - представление
         */
        onAction: function (list, action, record, card) {
            var url = 'home/' + action + '/' + record.get('uuid');
            switch (action) {
                case 'user':
                case 'route':
                case 'tasks':
                    Ext.getCurrentApp().redirectTo(url);
                    break;
            }
        },

        /*
         * Обработка пользователей
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        processingPlacemarks: function (sender, users, callback) {
            var view = this.getView();
            if (view) {
                var list = view.getUserListComponent();
                if (list) {
                    users.forEach(function (i) {
                        var record = list.map_uuid[i.get('uuid')];
                        if (record)
                            list.setAccess(record.getId(), true);
                    });
                }

                if (typeof callback == 'function')
                    callback();
            }
        }
    }
});