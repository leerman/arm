﻿/*
 * панель фильтрации
 */
Ext.define('ARM.view.stat.Filter', {
    extend: 'Ext.Panel',
    xtype: 'app-stat-filter',
    ui: 'white',

    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeStatFilterHeight: getResponsiveHeight(),
        smallStatFilterHeight: '!largeStatFilterHeight',
        largeStatFilterWidth: getResponsiveWidth(),
        smallStatFilterWidth: '!largeStatFilterWidth'
    },

    responsiveConfig: {
        'largeStatFilterHeight && largeStatFilterWidth': {
            bodyStyle: {
                padding: '20px 20px 0 20px'
            }
        },
        'smallStatFilterHeight || smallStatFilterWidth': {
            bodyStyle: {
                padding: '0 10px 0 10px'
            }
        }
    },

    defaultListenerScope: true,
    /*
     * событие изменения фильтра
     */
    EVENT_FILTER_CHANGE: 'change',

    constructor: function (cfg) {
        this.items = [
        {
            xtype: 'form',
            ui: 'white',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Период',
                    labelWidth: 100,
                    layout: 'hbox',
                    labelStyle: 'color:#808080;padding-top:9px',
                    items: [
                        {
                            xtype: 'datefield',
                            emptyText: 'с',
                            name: 'dateBeginStart',
                            format: 'd.m.Y',
                            ui: 'white',
                            maxValue: new Date(),
                            allowBlank: false,
                            listeners: {
                                change: 'onFilterChange'
                            },
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    handler: 'onReset',
                                    hidden: true
                                }
                            },
                            value: this.getCurrentPeriodStart(),
                            validator: function (value) {
                                if (value) {
                                    // данная дата должна быть меньше другой
                                    var form = this.up('form');
                                    if (form) {
                                        var finish = Ext.Date.parse(form.getValues().dateBeginFinish, 'd.m.Y');
                                        var currentDate = Ext.Date.parse(value, 'd.m.Y');
                                        if (currentDate) {
                                            if (finish == currentDate) {
                                                return 'Дата начала и даты завершения периода не должны быть равны';
                                            }
                                            if (finish > currentDate)
                                                return true;
                                            else
                                                return 'Дата начала периода должна быть меньше даты завершения периода';
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            width: 10
                        },
                        {
                            xtype: 'datefield',
                            emptyText: 'по',
                            name: 'dateBeginFinish',
                            allowBlank: false,
                            format: 'd.m.Y',
                            maxValue: new Date(),
                            ui: 'white',
                            listeners: {
                                change: 'onFilterChange'
                            },
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    handler: 'onReset',
                                    hidden: true
                                }
                            },
                            value: new Date(),
                            validator: function (value) {
                                // данная дата должна быть больше другой
                                if (value) {
                                    // данная дата должна быть меньше другой
                                    var form = this.up('form');
                                    if (form) {
                                        var start = Ext.Date.parse(form.getValues().dateBeginStart, 'd.m.Y');

                                        var currentDate = Ext.Date.parse(value, 'd.m.Y');
                                        if (currentDate) {
                                            if (start == currentDate) {
                                                return 'Дата начала и даты завершения периода не должны быть равны';
                                            }
                                            if (start < currentDate)
                                                return true;
                                            else
                                                return 'Дата завершения периода должна быть больше даты начала периода';
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        }
        ];
        this.callParent(arguments);
    },

    /*
     * значения с формы
     */
    getValues: function () {
        return this.getForm().getValues();
    },

    privates: {
        /*
         * возвращается начало предыдущего периода
         */
        getCurrentPeriodStart: function () {
            var CurrentDate = new Date();
            CurrentDate.setMonth(CurrentDate.getMonth());
            CurrentDate.setDate(1);

            return CurrentDate;
        },

        /*
         * возвращается конец предыдущего периода
         */
        getCurrentPeriodEnd: function () {
            var date = this.getCurrentPeriodStart();
            var d = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            return d;
        },

        /*
         * возвращается форма 
         */
        getForm: function () {
            return this.down('form');
        },

        /*
         * обработчик изменения фильтра
         */
        onFilterChange: function (sender, newValue) {
            var form = this.getForm();
            var clear = sender.getTrigger('clear');
            if (clear)
                clear.show();
            if (form.isValid() == true) {
                this.onCallChangeEvent();
            }
        },

        /**
         * очистка значения в поле
         */
        onReset: function (field, sender, eOpts) {
            field.suspendEvent('change');
            field.reset();
            sender.hide();
            field.resumeEvent('change');
            var form = this.getForm();
            if (form.isValid() == true) {
                this.onCallChangeEvent();
            }
        },

        /*
         * вызов события изменения
         */
        onCallChangeEvent: function () {
            var values = this.getValues();

            for (var i in values) {
                values[i] = Ext.Date.parse(values[i], 'd.m.Y');
            }

            this.fireEvent(this.EVENT_FILTER_CHANGE, this, values);
        }
    }
});