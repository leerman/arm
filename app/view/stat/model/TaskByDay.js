﻿/*
 * заданий за день
 */
Ext.define('ARM.view.stat.model.TaskByDay', {
    extend: 'ARM.model.Base',
    idProperty: 'Day',

    fields: [
        {
            name: 'Date',
            type: 'date'
        },
        {
            name: 'Day',
            type: 'string'
        },
        {
            name: 'Total_Plan',
            type: 'number'
        },
        {
            name: 'Total_Fact',
            type: 'number'
        }
    ]
});