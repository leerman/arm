﻿/*
 * комопнент для вывода карточки пользователя (дополнительно строиться со статистикой)
 */
Ext.define('ARM.view.main.UserCard', {
    extend: 'ARM.shared.UserCard',
    /*
     * идет загрузка статистики
     */
    statisticLoading: false,
    width: '100%',
    /*
     * загрзка статистики завершена
     */
    statisticLoaded: false,
    is_header_click: true,

    constructor: function (cfg) {
        cfg.items = [{
            cls: 'user-card-body',

            margin: '0 10px 0 10px',
            plugins: 'responsive',
            responsiveConfig: {
                'largeUserCard': {
                    style: {
                        padding: '15px 0 15px 0'
                    }
                },
                'smallUserCard': {
                    style: {
                        padding: '5px 0 5px 0'
                    }
                }
            },

            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    width: 85,
                    bind: {
                        html: '<div style="position:relative;width:62px;height:62px;border-radius:31px;overflow:hidden;" title="{name}">' +
                        '<img style="position:absolute;max-width:62px;min-height:62px" alt="" src="{photo}" />'
                        + '</div>'
                    }
                },
                {
                    flex: 1,
                    bind: {
                        html: '<div class="user-card-info">' +
                            '<p data-qtip="Идентификатор устройства"><span>Серийный номер</span> {uuid}</p>' +
                            '<p><span>Телефон</span> <a href="tel:{phone}">{phone}</a></p>' +
                            '<p><span>E-mail</span> <a href="mailto:{email}">{email}</p>' +
                            '</div>'
                    }
                }
            ],
            dockedItems: [
                {
                    dock: 'bottom',
                    cls: 'user-card-statistic',

                    plugins: 'responsive',
                    responsiveConfig: {
                        'largeUserCard': {
                            style: {
                                padding: '15px 0 0 0',
                                marginTop: '10px'
                            },

                        },
                        'smallUserCard': {
                            style: {
                                padding: '5px 0 0 0',
                                marginTop: '0'
                            }
                        }
                    },

                    bind: {
                        // вывод информации по статистике пользователя
                        html: Ext.getConf('personOnly') == true ? this.getPersonStatictic() : this.getAllStatictic()
                    },
                    itemId: 'statistic',
                    minHeight: 90,
                    viewModel: {
                        data: {
                            date: new Date(),
                            // ФЛ
                            person: {
                                done_today: 0, // завершено сегодня
                                done_total: 0, // всего завершено
                                old: 0, // осталось
                                all: 0 // общее кол-во
                            },
                            // ЮЛ
                            noperson: {
                                done_today: 0, // завершено сегодня
                                done_total: 0, // всего завершено
                                old: 0, // осталось
                                all: 0 // общее кол-во
                            }
                        },
                        formulas: {
                            getdate: function (get) {
                                return Ext.Date.format(get('date'), 'd.m.Y');
                            }
                        }
                    }
                }
            ]
        }];
        this.callParent(arguments);
    },

    /**
     * установка режима отображения
     * @param val {string} режим (maximize, maximize)
     */
    setMode: function (val, suspend) {
        if (val == 'maximize') {
            if (this.statisticLoading == false &&
                this.statisticLoaded == false)
                this.loadStatstic();
        }

        this.callParent(arguments);
    },

    privates: {
        /*
         * загрузка статистической информации
         * @param flush {boolean} принудительное обновление (true)
         */
        loadStatstic: function (flush) {
            this.statisticLoading = true;
            var userList = this.up('app-user-list');
            if (userList) {
                var filterValues = userList.getFilterValues();

                var _filterValuesClone = Object.assign({}, filterValues);

                _filterValuesClone.User = this.getModel().getId();
                Ext.defer(function () {
                    var statistic = this.down('#statistic');
                    var mask = new Ext.LoadMask({
                        msg: 'Обновление статистики...',
                        target: statistic
                    });
                    
                    var dataProvider = Ext.getCurrentApp().getDataProvider();
                    var me = this;

                    var timeout = setTimeout(function () {
                        mask.show();
                    }, 500);

                    dataProvider.getUserStatistic(_filterValuesClone, function (data) {

                        clearTimeout(timeout);

                        if (statistic.rendered == true) { // нужно убедиться, что элемент еще существует
                            var vm = statistic.getViewModel();

                            for (var i in data)
                                vm.set(i, data[i]);

                            mask.destroy();
                            me.statisticLoading = false;
                            me.statisticLoaded = true;
                        }
                    });
                }, 50, this);
            }
        },

        /*
         * возвращается статистика 
         */
        getAllStatictic: function () {
            return '<div class="user-card-statistic-data">' +
                    '<div class="header"><span>Статистика на {getdate}</span><span>ФЛ/ЮЛ</span></div>' +
                    '<div class="body">' +
                    '<div class="item">' +
                    '<span>Выполнено за сегодня</span><span>{person.done_today}/{noperson.done_today}</span>' +
                    '</div>' +
                    '<div class="item">' +
                    '<span>Выполнено всего</span><span>{person.done_total}/{noperson.done_total}</span>' +
                    '</div>' +
                    '<div class="item">' +
                    '<span>Осталось</span><span>{person.old}/{noperson.old}</span>' +
                    '</div>' +
                    '<div class="item">' +
                    '<span>Общее количество</span>' +
                    '<span>{person.all}/{noperson.all}</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
        },

        /*
         * вовзвращается статистика для физ. лиц 
         */
        getPersonStatictic: function () {
            return '<div class="user-card-statistic-data">' +
                    '<div class="header"><span>Статистика на {getdate}</span><span>ФЛ</span></div>' +
                    '<div class="body">' +
                    '<div class="item">' +
                    '<span>Выполнено за сегодня</span><span>{person.done_today}</span>' +
                    '</div>' +
                    '<div class="item">' +
                    '<span>Выполнено всего</span><span>{person.done_total}</span>' +
                    '</div>' +
                    '<div class="item">' +
                    '<span>Осталось</span><span>{person.old}</span>' +
                    '</div>' +
                    '<div class="item">' +
                    '<span>Общее количество</span>' +
                    '<span>{person.all}</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
        }
    }
});