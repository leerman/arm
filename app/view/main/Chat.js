﻿/*
 * компонент для вывода чата
 */
Ext.define('ARM.view.main.Chat', {
    extend: 'Ext.Window',
    defaultListenerScope: true,
    cls: 'chat',
    minWidth: 390,
    height: 620,
    ui: 'white',
    closeToolText: 'Скрыть чат',
    xtype: 'app-chat',
    closeAction: 'method-hide',
    /*
     * количество записей в чате
     */
    CHAT_COUNT_ITEMS: 100,

    requires: [
        'ARM.store.Message',
        'ARM.Socket'
    ],

    currentUser: null,

    viewModel: {
        data: {
            text: '',
            status: ''
        }
    },

    config: {
        layout: {
            type: 'vbox',
            pack: 'start'
        }
    },

    defaults: {
        width: '100%'
    },

    /*
     * текущий выбранный пользователь
     */
    selectedUser: null,
    setSelectedUser: function (value) {
        this.selectedUser = value;
    },
    getSelectedUser: function () {
        return this.selectedUser;
    },

    constructor: function (cfg) {
        var me = this;
        cfg.items = [
            {
                ui: 'white',
                xtype: 'tabpanel',
                height: '100%',
                items: [
                    //#region users
                    {
                        xtype: 'container',
                        layout: {
                            type: 'vbox',
                            pack: 'start'
                        },
                        defaults: {
                            width: '100%'
                        },
                        items: [
                            {
                                cls: 'search-field header-separator',
                                height: 60,
                                xtype: 'textfield',
                                triggers: {
                                    search: {
                                        cls: 'x-form-search-trigger',
                                        handler: function () {

                                        }
                                    }
                                },
                                listeners: {
                                    change: 'onUserSearch'
                                }
                            },
                            {
                                xtype: 'container',
                                scrollable: 'y',
                                flex: 1,
                                items: [
                                    {
                                        // список пользователей
                                        xtype: 'dataview',
                                        itemId: 'userList',
                                        store: this.createStore(),
                                        tpl: [
                                            '<tpl for=".">',
                                            '<div data-id="{LINK}" class="thumb-wrap">',
                                                '<table style="border:0;width: 100%">' +
                                                '<body><tr>' +
                                                '<td><div style="position:relative;width:62px;height:62px;border-radius:31px;overflow:hidden">' +
                                                '<img style="position:absolute;max-width:62px;min-height:62px" alt="{C_Fio}" title="{C_Fio}" src="{[this.getPhoto(values)]}" /></div>' +
                                                '</td>' +
                                                '<td>{C_Fio}</td>' +
                                                '<td>{[this.getCount(values)]}</td>' +
                                                '<td>{[this.getLastTime(values)]}</td>' +
                                                '</tr></body>' +
                                                '</table>',
                                            '</div>',
                                            '</tpl>',
                                            {
                                                getDate: function (values, fieldName) {
                                                    return Ext.Date.format(new Date(values[fieldName]), 'd.m.Y H:i:s');
                                                },
                                                getPhoto: function (values) {
                                                    if (values.C_Photo)
                                                        return me.getPhotoPath(values.LINK);
                                                    else return me.getPhotoPath('');
                                                },
                                                /*
                                                 * возвращается время последнего выхода на связь
                                                 */
                                                getLastTime: function (values) {

                                                },
                                                /*
                                                 * кол-во не прочитанных сообщений
                                                 */
                                                getCount: function (values) {

                                                }
                                            }
                                        ],
                                        itemSelector: 'div.thumb-wrap',
                                        emptyText: '<center>Пользователей не найдено</center>',
                                        listeners: {
                                            itemclick: 'onUserClickItem'
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    //#endregion
                    //#region chat
                    {
                        xtype: 'panel',
                        ui: 'white',
                        layout: {
                            type: 'vbox',
                            pack: 'start'
                        },
                        defaults: {
                            width: '100%'
                        },
                        viewModel: {
                            photo: '',
                            name: ''
                        },
                        items: [
                            {
                                padding: 7,
                                xtype: 'container',
                                cls: 'header-separator',
                                layout: {
                                    type: 'hbox',
                                    pack: 'start'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        iconCls: 'x-fa fa-arrow-left',
                                        ui: 'action-link-black',
                                        style: {
                                            'margin-top': '20px',
                                            'margin-right': '10px'
                                        },
                                        handler: 'onBackToList'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        bind: {
                                            html: '<div style="position:relative;width:62px;height:62px;border-radius:31px;overflow:hidden;float:left;margin-right:10px;">' +
                                                '<img style="position:absolute;max-width:62px;min-height:62px" alt="{name}" title="{name}" src="{photo}" /></div><div style="margin-top:5px">{name}<br />Email: <a href="mailto:{C_Email}">{C_Email}</a><br/>Тел.: <a href="tel:{C_Telephone}">{C_Telephone}</a></div>'
                                        }
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'container',
                                scrollable: 'y',
                                items: [
                                    {
                                        xtype: 'dataview',
                                        itemId: 'messages',
                                        store: Ext.create('ARM.store.Message'),
                                        tpl: [
                                            '<tpl for=".">',
                                            '<div data-id="{id}" class="thumb-wrap" style="margin-left:5px;margin-right:5px;border-bottom:1px solid rgba(0,0,0,0.08);margin-bottom:5px;">',
                                                '<div style="font-size:0.8em;margin-left:10px">{userNameBegin} <span class="x-fa fa-arrow-{[this.getArrow(values)]}" style="color:{[this.getArrowColor(values)]}"></span> {userNameEnd}</div>{text}<div style="font-size:0.8em;overflow:hidden;margin-top:10px;"><div style="float:left">Статус: {[this.getStatus(values)]}</div><div style="float:right">{[this.getDate(values, "created")]}</div></div>',
                                            '</div>',
                                            '</tpl>',
                                            {
                                                getDate: function (values, fieldName) {
                                                    return Ext.Date.format(new Date(values[fieldName]), 'd.m.Y H:i:s');
                                                },
                                                getArrow: function (values) {
                                                    return 'right';
                                                },
                                                getArrowColor: function (values) {
                                                    if (values.sended == true || values.in == true)
                                                        return values.in == true ? 'red' : 'green';
                                                    else
                                                        return 'gray';
                                                },
                                                getStatus: function (values) {
                                                    switch (values.status) {
                                                        case 'SUCCESS':
                                                            return 'Доставлено';

                                                        case 'SERVER_APPLY':
                                                            return 'Принято сервером';

                                                        case 'SENDING':
                                                            return 'Отправка';

                                                        case 'SENDED':
                                                            return 'Принято';

                                                        default:
                                                            return ' Неизвестно';
                                                    }
                                                }
                                            }
                                        ],
                                        itemSelector: 'div.thumb-wrap',
                                        emptyText: '<center>Сообщений не найдено</center>'
                                    }
                                ]
                            },
                            {
                                height: 80,
                                dock: 'bottom',
                                xtype: 'form',
                                ui: 'white',
                                width: '100%',
                                padding: 5,
                                layout: {
                                    type: 'hbox',
                                    pack: 'start'
                                },
                                items: [
                                    {
                                        flex: 1,
                                        xtype: 'textareafield',
                                        grow: true,
                                        name: 'message',
                                        cls: 'text-message',
                                        emptyText: 'текстовое сообщение',
                                        bind: {
                                            value: '{text}'
                                        }
                                    },
                                    {
                                        width: 10
                                    },
                                    {
                                        xtype: 'button',
                                        itemId: 'sendId',
                                        ui: 'white-blue',
                                        iconCls: 'x-fa fa-paper-plane',
                                        tooltip: 'Отправить сообщение',
                                        handler: 'onSend'
                                    }
                                ]
                            }
                        ]
                    }
                    //#endregion
                ]
            },
            {
                width: '100%',
                style: {
                    'font-size': '10px',
                    'text-align': 'center'
                },
                bind: {
                    html: '{status}'
                }
            }
        ];
        this.callParent(arguments);
        this.insertDataInUserStore(function () {
            // устанавливаем заначение по умолчанию
            //me.down('#users').setValue('-1');
        });
        var app = Ext.getCurrentApp();

        var dataview = this.down('#messages');
        var store = dataview.getStore();
        store.sort('created', 'DESC');
        store.load({
            callback: function (items) {
                var max = me.CHAT_COUNT_ITEMS;
                if (items.length > max) {
                    for (var i = items.length - max; i < items.length; i++) {
                        store.remove(items[i]);
                    }

                    store.sync({
                        callback: function () {

                        }
                    });
                }
            }
        });

        this.setCurrentUser(app.getUser());
        app.on('snRegistreted', this.unLock, this);
        app.on('unavailable', this.unavailable, this);
        app.on('snChat', this.onResponseChat, this);

        var scroller = dataview.getScrollable();
        if (scroller) {
            scroller.on('scrollend', this.onScrollEnd, this);
        }
    },

    unLock: function () {
        this.down('#sendId').setDisabled(false);
        this.getViewModel().set('status', '');
    },

    /*
     * сервер не доступен
     */
    unavailable: function () {
        this.getViewModel().set('status', '<span style="color:red">сервер не доступен</span>');
    },

    onScrollEnd: function (scroller, x, y) {
        var dataview = this.down('#messages');
        var store = dataview.getStore();

        if (y >= scroller.getMaxPosition().y && store.getTotalCount() > store.data.items.length) {
            dataview.mask('Загрузка...');
            store.nextPage({
                addRecords: true, callback: function () {
                    dataview.unmask();
                }
            });
        }
    },

    onSend: function () {
        var user = this.getSelectedUser();
        var value = user.get('uuid');

        var vm = this.getViewModel();
        var msg = vm.get('text');
        var currentUser = this.getCurrentUser();
        if (currentUser) {
            if (msg) {
                var model = Ext.create('ARM.model.Message', {});
                model.set('id', Utilits.GetGUID());
                model.set('text', msg);
                model.set('created', Date.now());
                model.set('modifed', null);
                model.set('check', false);
                model.set('userBegin', this.getUserId());
                model.set('userNameEnd', user.get('C_Fio'));
                model.set('userNameBegin', currentUser.getUserName());
                model.set('userEnd', value);
                model.set('in', false);
                model.set('sended', false);
                model.set('status', 'SENDING');

                this.addMessage(model, function () {
                    vm.set('text', '');
                });
            } else {
                alert('Сообщение', 'Текст сообщения не указан');
                this.down('textareafield').focus();
            }
        } else {
            alert('Сообщение', 'Не удалось получить информацию о текущем пользователе');
        }
    },

    /*
     * написать указанному пользователю
     * @param model {any} пользователь
     */
    sendBy: function (model) {
        if (model) {
            //var store = this.down('#userList').getStore();
            //store.clearFilter();
            //store.filterBy(function (item) {
            //    return item.get('LINK') == model.get('LINK');
            //});
            this.onUserClickItem(this.down('#userList'), model);
        }
    },

    /*
     * написать любому
     */
    sendAll: function () {
        var tabpanel = this.down('tabpanel');
        tabpanel.setActiveItem(0);
        var store = this.down('#userList').getStore();
        store.clearFilter();
    },

    destroy: function () {
        Ext.getCurrentApp().un('snRegistreted', this.unLock, this);
        Ext.getCurrentApp().un('unavailable', this.unLock, this);
        Ext.getCurrentApp().un('snChat', this.onResponseChat, this);
        var scroller = this.down('#messages').getScrollable();
        if (scroller) {
            scroller.un('scrollend', this.onScrollEnd, this);
        }

        this.callParent(arguments);
    },

    privates: {
        /*
         * вернуться назад к списку пользователей
         */
        onBackToList: function () {
            var tabpanel = this.down('tabpanel');
            tabpanel.setActiveItem(0);
        },
        /*
         * возвращается путь к изображению
         * @param photo {string} путь к изображению 
         */
        getPhotoPath: function (id) {
            if (id) {
                return Ext.String.format(Ext.getConf('ws_url') + Ext.getConf('virtualDirPath') + '/image?type=avatar&size=small&id={0}', id);
            }
            return Ext.getCurrentApp().toAbsolutePath('resources/images/avatar.png');
        },
        /*
         * обработчик поиска пользователя
         */
        onUserSearch: function (sender, newValue) {
            var store = this.down('#userList').getStore();

            store.clearFilter();
            if (newValue != '') {

                store.filterBy(function (item) {
                    return item.get('C_Fio').toLowerCase().indexOf(newValue) >= 0;
                });
            }
        },
        /*
         * обработчик нажатия на пользователе в списке
         */
        onUserClickItem: function (sender, record) {
            this.setSelectedUser(record);
            var tabpanel = this.down('tabpanel');
            tabpanel.setActiveItem(1);
            var tab = tabpanel.getActiveTab();
            if (tab) {
                var vm = tab.getViewModel();
                var data = record.getData();
                for (var i in data) {
                    vm.set(i, data[i]);
                }
                vm.set('name', record.get('C_Fio'));
                vm.set('photo', this.getPhotoPath(record.get('LINK')));

                var messageStore = tab.down('#messages').getStore();
                messageStore.clearFilter();

                var uuid = record.get('uuid');
                messageStore.filterBy(function (item) {
                    return item.get('userEnd') == uuid || item.get('userBegin') == uuid;
                });
            }
        },

        getCurrentUser: function () {
            return this.currentUser;
        },

        setCurrentUser: function (val) {
            this.currentUser = val;
        },

        onResponseChat: function (sender, eventResult, tid) {
            eventResult.in = true;
            var model = Ext.create('ARM.model.Message', {});
            model.set('id', tid);
            model.set('text', eventResult.text);
            model.set('created', eventResult.created);
            model.set('modifed', null);
            model.set('check', false);
            model.set('userBegin', eventResult.userBegin);
            model.set('userNameEnd', eventResult.userNameEnd);
            model.set('userNameBegin', eventResult.userNameBegin);
            model.set('userEnd', eventResult.userEnd);
            model.set('in', true);
            model.set('sended', false);
            model.set('status', 'SENDED');

            this.addMessage(model);
            var app = Ext.getCurrentApp();
            Ext.getCurrentApp().getSocket().sendNotif(eventResult.userBegin, 'SUCCESS', tid);
            app.showTooltip('Сообщение от ' + eventResult.userNameBegin, eventResult.text);
        },

        getUserId: function () {
            return this.getCurrentUser().getId();
        },

        /**
         * добавление сообщения
         * @param data {any} данные сообщения
         * @param callback {()=>void} функция обратной связи
         */
        addMessage: function (data, callback) {
            var me = this;
            var store = this.down('#messages').getStore();
            var record = store.getById(data.get('id'));
            if (!record) {
                store.add(data);
            }
            store.sort('created', 'DESC');
            store.sync({
                callback: function () {
                    if (data.get('in') == false) {
                        Ext.getCurrentApp().getSocket().sendChat(data.get('userEnd'),
                            data.getData(),
                            function (status, options) {
                                var record = store.getById(options.id);
                                if (record) {
                                    switch (status) {
                                        case 'SUCCESS':
                                            record.set('sended', true);
                                            record.set('modifed', true);
                                            break;
                                    }

                                    record.set('status', status);

                                    store.sync({
                                        callback: function () {

                                        }
                                    });
                                }
                            });
                    }
                    if (typeof callback == 'function')
                        callback();
                }
            });
        },

        /*
         * возвращается хранилище с пользователями
         */
        getUserStore: function () {
            return this.down('#userList').getStore();
        },

        /*
         * создание хранилища
         */
        createStore: function () {
            var store = Ext.create('Ext.data.Store', {
                model: 'ARM.model.User',
                data: [],
                proxy: {
                    type: 'memory'
                }
            });
            return store;
        },

        /*
         * добавление активных пользователей (те на которых можно назначить задание)
         * @param callback {()=>void} функция обратного вызова
         */
        insertDataInUserStore: function (callback) {
            var me = this;
            Ext.getCurrentApp().getDataProvider().getWalkers(function (points) {
                me.getUserStore().setData(points);
                if (typeof callback == 'function')
                    callback();
            });
        }
    }
});