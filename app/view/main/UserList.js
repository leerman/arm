﻿/*
 * комопнент для вывода списка пользователей
 */
Ext.define('ARM.view.main.UserList', {
    extend: 'Ext.Panel',
    xtype: 'app-user-list',
    cls: 'userlist',
    cardView: null,
    defaultListenerScope: true,
    // хранилище с пользователями
    store: null,
    /*
     * данные о пользователях которые находятся в online
     */
    onLineUsers: {},

    lastCardView: null,
    nextCardView: null,
    selectCardView: null,

    /**
     * событие выбора пользователя
     */
    EVENT_SELECT: 'select',

    /*
     * событие отмены выбора пользователя
     */
    EVENT_UNSELECT: 'unselect',

    /*
     * событие выполнения какого либо действия
     */
    EVENT_ACTION: 'action',

    /**
     * мапинг записей на элементы dom
     */
    map: {},
    /*
     * мапинг записей
     */
    map_uuid: {},

    TEXT_EMPTY: 'Фильтр не указан',

    /*
     * условие фильтрации
     */
    filterValues: null,

    constructor: function (cfg) {
        this.callParent(arguments);

        this.empty();
        this.initListeners();
    },

    /*
     * устанавливаются условия фильтрации
     * @param values {any} условия фильтрации
     */
    setFilterValues: function (values) {
        this.filterValues = values;
    },

    /*
     * возвращается условие фильтрации
     */
    getFilterValues: function () {
        return this.filterValues;
    },

    /**
     * очистка данных
     */
    empty: function () {
        this.toEmpty(true);
        this.add({
            html: '<center style="display:block;margin-top:10px">' + this.TEXT_EMPTY + '</center>'
        });
    },

    /*
     * добавление элемента
     * @param record {any} запись
     */
    addItem: function (record) {
        var el = Ext.create(this.cardView, {
            model: record,
            icon_generator: Ext.conf().getIconPath(),
            listeners: {
                maximize: 'onCardViewMaximize',
                minimize: 'onCardViewMinimize',
                actionheader: 'onActionHeader' // действия
            }
        });
        this.map[record.getId()] = el;
        this.map_uuid[record.get('uuid')] = record;
        return el;
    },

    /*
     * установка видимости
     * @param recordId {string} идентификатор записи
     * @param online {boolean} параметр видимости
     */
    setOnline: function (recordId, online) {
        var store = this.store;
        if (store) {
            var record = store.getById(recordId);
            if (record)
                this.updateItem(record, online);
        }
    },

    /*
     * установка досупности
     * @param recordId {string} идентификатор записи
     * @param access {boolean} параметр доступности
     */
    setAccess: function (recordId, access) {
        var store = this.store;
        if (store) {
            var record = store.getById(recordId);
            if (record)
                this.updateItem(record, null, access);
        }
    },

    /**
     * загрузка элементов
     * @param store {any} хранилище пользователей
     * @param callback {()=>void} функця обратного вызова
     */
    load: function (store, callback) {
        this.toEmpty(true);
        this.store = store;

        var me = this;
        store.load({
            callback: function (items) {
                var elements = [];
                Ext.each(items, function (item) {
                    elements.push(me.addItem(item));
                });
                me.add(elements);
                me.onUserOnlineStatistic(null, me.onLineUsers);
                if (typeof callback == 'function')
                    callback();
            }
        });
    },

    /*
     * устанавливается шаблон для отрисовки спика пользователей
     * @param viewName {string} имя шаблона
     */
    //setCardView: function (viewName) {
    //    this.cardView = viewName;
    //},

    /*
     * возвращается текущая выбранная карточка
     */
    getSelectCardView: function () {
        if (this.selectCardView && this.selectCardView.rendered == true)
            return this.selectCardView;

        return null;
    },

    destroy: function () {
        this.toEmpty();
        this.deleteListeners();
        this.callParent(arguments);
    },

    privates: {
        /*
         * очистка данных
         * @param remove {boolean} удалять дочернии элементы
         */
        toEmpty: function (remove) {
            this.filterValues = null;
            this.map = {};
            this.map_uuid = {};
            this.lastCardView = null;
            this.nextCardView = null;
            //this.onLineUsers = {};
            this.store = null;

            if (remove == true)
                this.removeAll(true);
        },
        /*
         * инициализация событий
         */
        initListeners: function () {
            var app = Ext.getApplication();
            app.on('snStatistic', this.onUserOnlineStatistic, this);
            app.on('snDisconnect', this.onSocketDisconnect, this);
        },

        /*
         * очистка событий
         */
        deleteListeners: function () {
            var app = Ext.getApplication();
            app.un('snStatistic', this.onUserOnlineStatistic, this);
            app.un('snDisconnect', this.onSocketDisconnect, this);
        },

        //#region сокет
        /**
         * обработчик отключения сокета
         */
        onSocketDisconnect: function () {
            if (this.map) {
                for (var i in this.map)
                    this.updateItem(this.map[i].model, false);
            }
        },

        /**
         * обработчик для нахождения кто из пользователей находиться online
         * @param socket {any} соединение (не используется)
         * @param data {any} данные для обработки
         */
        onUserOnlineStatistic: function (socket, data) {
            var removed = {};
            var me = this;
            var user_items = this.onLineUsers.items;
            if (user_items && data.options && data.options.removed) {
                // нужно по предыдущим данным достать информацию о пользователе
                data.options.removed.forEach(function (i) {
                    if (me.onLineUsers.items[i])
                        removed[i] = me.onLineUsers.items[i];
                });
            }
            this.onLineUsers = data;
            user_items = this.onLineUsers.items;
            var changed = false;
            var store = null;
            for (var i in user_items) {
                var record = this.map_uuid[user_items[i].uuid];
                if (record) {
                    var info = user_items[i].info;
                    if (record.get('C_Mobile_Version') != info.androidAppVersion) {
                        record.set('C_Mobile_Version', info.androidAppVersion);
                        record.set('C_Mobile_Info', JSON.stringify(info));
                        changed = true;
                        store = record.store;
                    }
                    this.setOnline(record.getId(), true, record.get('S_Date'));
                }
            }

            if (changed == true && store) {
                store.sync({
                    callback: function () {

                    }
                });
            }

            if (removed) {
                for (var j in removed) {
                    var record = me.map_uuid[removed[j].uuid];
                    if (record)
                        me.setOnline(record.getId(), false);
                }
            }
        },
        //#endregion
        /*
         * обновление записи
         * @param record {any} запись
         * @param online {boolean} онлай или оффлайн
         * @param access {boolean} доступность
         */
        updateItem: function (record, online, access) {
            if (record && record.getId) {
                var id = record.getId();
                var node = this.map[id];
                if (node && online != null) {
                    node.setOnline(online, record.get('S_Date'));
                }

                if (access != undefined)
                    node.setAccess(access);
            }
        },
        /**
         * обработчик раскрытия карточки
         * @param sender {any} карточка
         */
        onCardViewMaximize: function (sender) {
            var lastView = this.getLastCardView();
            if (lastView && lastView.getId() != sender.getId()) {
                // второй параметр для запрета события
                lastView.setMode('minimize', true);
            }
            this.setLastCardView(sender);
            this.selectCardView = sender;
            // теперь нужно найти соседний элемент и поменять ему стить

            if (this.nextCardView)
                this.nextCardView.removeCls('next-card-view');

            this.nextCardView = sender.nextNode();
            if (this.nextCardView)
                this.nextCardView.addCls('next-card-view');
            if (sender.is_header_click == true)
                this.fireEvent(this.EVENT_SELECT, this, sender);
        },

        /**
         * обработчик закрытия карточки
         * @param sender {any} карточка
         */
        onCardViewMinimize: function (sender) {
            this.selectCardView = null;
            var lastView = this.getLastCardView();
            if (lastView && lastView.getId() == sender.getId()) {
                if (this.nextCardView)
                    this.nextCardView.removeCls('next-card-view');
            }
            if (sender.is_header_click == true)
                this.fireEvent(this.EVENT_UNSELECT, this, sender);
        },
        /**
         * устанавливается последняя карточка
         * @param view {any} карточка
         */
        setLastCardView: function (view) {
            this.lastCardView = view;
        },

        /*
         * возвращается последняя открытая карточка
         */
        getLastCardView: function () {
            return this.lastCardView;
        },

        /*
         * обработчик нажатия кнопок в шапке панели (карточка пользователя)
         * @param action {string} имя действия
         * @param btn {any} кнопка
         * @param record {any} запись
         * @param sender {any} карточка
         */
        onActionHeader: function (action, btn, record, sender) {
            this.fireEvent(this.EVENT_ACTION, this, action, record, sender);
        }
    }
});