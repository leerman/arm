﻿/*
 * консоль для ввода команд
 */
Ext.define('ARM.view.main.CommandConsole', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-command-console',
    bodyPadding: 5,

    defaultListenerScope: true,

    viewModel: {
        data: {
            command: ''
        }
    },

    items: [
        {
            fieldLabel: '~ ',
            labelWidth: 20,
            labelSeparator: '',
            labelStyle: 'padding-top:10px',
            xtype: 'textfield',
            width: '100%',
            ui: 'white',
            emptyText: 'ввести команду и нажать ENTER',
            margin: 0,
            enableKeyEvents: true,
            itemId: 'commandfield',
            bind: {
                value: '{command}'
            },
            listeners: {
                keypress: 'onKeyPress'
            },
            allowBlank: false
        }
    ],

    dockedItems: [
        {
            dock: 'right',
            xtype: 'panel',
            ui: 'white',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-play',
                    ui: 'white-blue',
                    margin: 5,
                    tooltip: 'Выполнить введенную команду',
                    handler: 'onRun'
                },
                {
                    xtype: 'container',
                    width: 5
                },
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-remove',
                    ui: 'white-blue',
                    margin: 5,
                    tooltip: 'Закрыть панель для ввода команд',
                    handler: 'onClose'
                }
            ]
        }
    ],

    onClose: function () {
        this.hide();
    },

    /*
     * обработчик нажатия на ENTER в поле пользователи
     */
    onKeyPress: function (sender, e, eOpts) {
        if (e.getKey() == e.ENTER) {
            // выполняем команду
            this.onCommand(this.getViewModel().get('command'));
        }
    },

    /*
     * обработчик выполнения команды
     */
    onRun: function () {
        var cmd = this.down('#commandfield');
        if (cmd.isValid() == true)
            this.onCommand(this.getViewModel().get('command'));
    },

    /*
     * обработчик выполнения команды
     * @param command {string} текст команды
     */
    onCommand: function (command) {
        this.getViewModel().set('command', '');
        Ext.getCurrentApp().getChiter().runCommand('~' + command);
    }
});