﻿/*
 * компонент для отображения главного блока фильтраций
 */
Ext.define('ARM.view.main.MainFilter', {
    extend: 'Ext.form.Panel',
    xtype: 'app-main-filter',

    defaultListenerScope: true,

    requires: [
        'ARM.store.MainDivision',
        'ARM.store.Division',
        'ARM.store.SubDivision',
        'ARM.store.User',
        'ARM.store.RES'
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeMainFilter: getResponsiveHeight(),
        smallMainFilter: '!largeMainFilter'
    },

    // событие применения фильтра
    EVENT_APPLY_FILTER: 'apply',
    // событие изменения фильтра
    EVENT_CHANGE_FILTER: 'change',
    // событие очистки фильтра
    EVENT_CLEAN_FILTER: 'clean',

    PAGESIZE: 25,
    //emptyText у Division
    DIVISION_LABEL: 'ПО',
    //emptyText у SubDivision
    SUBDIVISION_LABEL: 'Участок',
    //emptyText у MainDivision
    MAINDIVISION_LABEL: 'Филиал',
    //emptyText у РЭС
    RES_LABEL: 'РЭС',

    MANAGER_LABEL:'Диспетчер',

    layout: 'anchor',
    defaults: {
        anchor: '100%',
        plugins: 'responsive',
        responsiveConfig: {
            'largeMainFilter': {
                style: {
                    padding: '0 0 15px 0',
                }
            },
            'smallMainFilter': {
                style: {
                    padding: '0 0 5px 0',
                }
            }
        },
        listConfig: { // только для выпадающих списков
            minWidth: 400
        }
    },

    responsiveConfig: {
        'largeMainFilter': {
            bodyStyle: {
                padding: '20px 20px 0 20px',
            }
        },
        'smallMainFilter': {
            bodyStyle: {
                padding: '10px 10px 0 10px',
            }
        }
    },

    defaultType: 'combo',

    viewModel: {
        data: {
            apply: false,
            clean: false
        }
    },

    /*
     * предыдущие значения формы
     */
    _lastValues: null,

    constructor: function (cfg) {

        this.stores.MainDivision = Ext.create('ARM.store.MainDivision', { pageSize: this.PAGESIZE });
        this.stores.Division = Ext.create('ARM.store.Division', { pageSize: this.PAGESIZE });
        this.stores.SubDivision = Ext.create('ARM.store.SubDivision', { pageSize: this.PAGESIZE });
        this.stores.Managers = Ext.create('ARM.store.GetManagersQuery', { pageSize: this.PAGESIZE });
        this.stores.RES = Ext.create('ARM.store.RES', { pageSize: this.PAGESIZE });
        this.stores.User = Ext.create('ARM.store.GetWalkersQuery', { pageSize: 10000 });

        // Нужно ли скрыть поле Филиал
        var configs = Ext.getCurrentApp().getConfigs();
        var hideMainDivision = false;
        if (configs && configs.data)
            hideMainDivision = (configs.data.B_MainDivision !== true);
        var hideRES = false;
        if (configs && configs.data)
            hideRES = (configs.data.B_Networks !== true);

        cfg.items = [
            {
                store: this.stores.MainDivision,
                name: 'MainDivision',
                hidden: hideMainDivision,
                queryMode: 'remote',
                displayField: 'C_Name',
                valueField: 'LINK',
                emptyText: this.MAINDIVISION_LABEL,
                pageSize: this.PAGESIZE,
                forceSelection: true, // нужно чтобы можно было вводить произольные данные
                triggers: {
                    clear: {
                        cls: 'x-form-clear-trigger',
                        handler: 'onMainDivisionReset',
                        hidden: true
                    }
                },
                listeners: {
                    select: 'onMainDivisionSelect'
                }
            },
            {
                store: this.stores.Division,
                name: 'Division',
                queryMode: 'remote',
                displayField: 'C_Name',
                valueField: 'LINK',
                emptyText: this.DIVISION_LABEL,
                pageSize: this.PAGESIZE,
                forceSelection: true,
                triggers: {
                    clear: {
                        cls: 'x-form-clear-trigger',
                        handler: 'onDivisionReset',
                        hidden: true
                    }
                },
                listeners: {
                    select: 'onDivisionSelect'
                }
            },
            {
                store: this.stores.SubDivision,
                name: 'SubDivision',
                queryMode: 'remote',
                displayField: 'C_Name',
                valueField: 'LINK',
                emptyText: this.SUBDIVISION_LABEL,
                pageSize: this.PAGESIZE,
                forceSelection: true,
                triggers: {
                    clear: {
                        cls: 'x-form-clear-trigger',
                        handler: 'onSubDivisionReset',
                        hidden: true
                    }
                },
                listeners: {
                    select: 'onSubDivisionSelect'
                }
            },
            {
                store: this.stores.RES,
                name: 'RES',
                hidden: hideRES,
                queryMode: 'remote',
                displayField: 'C_Name',
                valueField: 'LINK',
                emptyText: this.RES_LABEL,
                pageSize: this.PAGESIZE,
                forceSelection: true,
                triggers: {
                    clear: {
                        cls: 'x-form-clear-trigger',
                        handler: 'onRESReset',
                        hidden: true
                    }
                },
                listeners: {
                    select: 'onRESSelect'
                }
            },
            {
                store: this.stores.Managers,
                name: 'Managers',
                queryMode: 'remote',
                displayField: 'C_Fio',
                valueField: 'LINK',
                emptyText: this.MANAGER_LABEL,
                pageSize: this.PAGESIZE,
                forceSelection: true,
                triggers: {
                    clear: {
                        cls: 'x-form-clear-trigger',
                        handler: 'onManagersReset',
                        hidden: true
                    }
                },
                listeners: {
                    select: 'onManagersSelect'
                }
            },
            {
                xtype: 'textfield',
                name: 'User',
                emptyText: 'Поиск по ФИО',
                enableKeyEvents: true,
                triggers: {
                    clear: {
                        cls: 'x-form-clear-trigger',
                        handler: 'onUserReset',
                        hidden: true
                    }
                },
                listeners: {
                    change: 'onUserChange',
                    keypress: 'onUserKeyPress'
                }
            },
            {
                xtype: 'hiddenfield',
                name: 'uuid'
            },
            {
                xtype: 'hiddenfield',
                name: 'userId'
            }
        ];

        cfg.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',

                plugins: 'responsive',
                responsiveConfig: {
                    'largeMainFilter': {
                        style: {
                            padding: '0 0 0 20px',
                        }
                    },
                    'smallMainFilter': {
                        style: {
                            padding: '0 0 0 10px',
                        }
                    }
                },

                items: [
                    {
                        xtype: 'button',
                        text: 'Применить',
                        disabled: true,
                        bind: {
                            disabled: '{!apply}'
                        },
                        itemId: 'applyId',
                        tooltip: 'Применить условие фильтрации',
                        handler: 'onApplyFilter'
                    },
                    {
                        xtype: 'container',
                        width: 8
                    },
                    {
                        xtype: 'button',
                        text: 'Очистить',
                        disabled: true,
                        bind: {
                            disabled: '{!clean}'
                        },
                        tooltip: 'Отменить условие фильтрации',
                        handler: 'onCleanFilter'
                    }
                ]
            }
        ];

        this.callParent(arguments);
    },

    /**
     * возвращается хранилище с пользователями
     */
    getUserStore: function () {
        var values = this.getValues();

        this.stores.User.clearFilter(true); // пользователь
        var filters = [];
        for (var i in values) {
            if (values[i]) {
                if (i != 'User' && i != 'uuid' && i != 'RES' && i != 'userId') {
                    filters.push({
                        property: (i == 'Managers' ? 'F_' : 'S_') + i,
                        value: values[i],
                        operator: '='
                    });
                } else {
                    if (i == 'User') {
                        filters.push({
                            property: 'C_Fio',
                            value: values[i],
                            operator: 'like'
                        });
                    }

                    if (i == 'uuid') {
                        filters.push({
                            property: 'uuid',
                            value: values[i],
                            operator: '='
                        });
                    }

                    if (i == 'userId') {
                        filters.push({
                            property: 'LINK',
                            value: values[i],
                            operator: '='
                        });
                    }
                }
            }
        }

        this.stores.User.filter(filters, null, true);

        return this.stores.User;
    },

    /*
     * информирование панели, что центральная часть готова и фильтр установлен
     */
    placeHolderReady: function () {
        // нужно разрешить нажатие на кнопку применить, но при этом оставить фильтр
        var vm = this.getViewModel();
        vm.set('apply', false);
        vm.set('clean', true);
    },

    /*
     * возвращается последние введеные данные в фильтре
     */
    getLastValues: function () {
        return this._lastValues;
    },

    /*
     * установлен ли фильтр
     */
    getFiltered: function () {
        var values = this.getLastValues();

        var isCleanActive = false;
        for (var i in values) {
            if (values[i]) {
                isCleanActive = true;
                break;
            }
        }

        return isCleanActive;
    },

    /*
     * установка фио пользователя
     * @param name {string} имя пользователя
     * @param record {any} запись
     */
    setUserName: function (name, record) {
        var field = this.getField('User');
        var clear = field.getTrigger('clear');
        if (field) {
            field.suspendEvent('change');

            if (name) {
                field.setValue(name);
                if (clear)
                    clear.show();

                // добавляем событие
                this.fireEvent('singleuser', name, record, this);
            } else {
                field.setValue(name);
                if (clear)
                    clear.hide();

                // добавляем событие
                this.fireEvent('cleanuser', "", record, this);
            }

            field.resumeEvent('change');
        }
    },

    destroy: function () {
        this.stores = null;
        this._lastValues = null;
        this.callParent(arguments);
    },

    privates: {
        stores: {
            MainDivision: null,
            Division: null,
            SubDivision: null,
            User: null,
            RES: null,
            Managers: null
        },

        /*
         * возвраащется поле
         * @param name {string} имя поля
         */
        getField: function (name) {
            var fields = this.getForm().getFields();
            for (var i = 0; i < fields.items.length; i++) {
                var field = fields.items[i];
                if (field.getName() == name)
                    return field;
            }

            return null;
        },

        /*
         * переопределенная функция
         * если не переопределять, то будет вызываться событие change
         */
        reset: function (resetRecord) {
            Ext.suspendLayouts();
            var fields = this.getForm().getFields().items,
                f,
                fLen = fields.length;
            for (f = 0; f < fLen; f++) {
                fields[f].suspendCheckChange = 1;
                if (fields[f].getTrigger) {
                    var clear = fields[f].getTrigger('clear');
                    if (clear)
                        clear.hide();
                }
            }
            Ext.resumeLayouts(true);
            var result = this.callParent(arguments);
            for (f = 0; f < fLen; f++) {
                fields[f].suspendCheckChange = 0;
                fields[f].lastValue = fields[f].getValue();
            }
            return result;
        },

        /**
         * сбросить значение с полей
         * @param fields {string[]} список полей
         */
        resetFields: function (fields) {
            var me = this;
            Ext.each(fields, function (name) {
                var field = me.getField(name);
                if (field) {
                    field.suspendEvent('change');
                    field.reset();
                    var clear = field.getTrigger('clear');
                    if (clear)
                        clear.hide();
                    field.resumeEvent('change');
                }
            });
        },

        /*
         * сбросить филиал
         */
        onMainDivisionReset: function (field, sender, eOpts) {
            var fields = ['Division', 'SubDivision', 'RES', 'Managers', 'User'];
            this.resetFields(fields);

            this.onTriggerReset(field, sender, eOpts);

            this.onMainDivisionSelect(field, null, eOpts);

            var vm = this.getViewModel();
            vm.set('apply', false);
            vm.set('clean', true);
        },
        /*
        * сбросить по
        */
        onDivisionReset: function (field, sender, eOpts) {
            var fields = ['SubDivision', 'RES', 'Managers', 'User'];
            this.resetFields(fields);

            this.onTriggerReset(field, sender, eOpts);

            this.onDivisionSelect(field, null, eOpts);
        },
        /*
        * сбросить участок
        */
        onSubDivisionReset: function (field, sender, eOpts) {
            var fields = ['RES', 'Managers', 'User'];
            this.resetFields(fields);

            this.onTriggerReset(field, sender, eOpts);

            this.onSubDivisionSelect(field, null, eOpts);
        },
        /*
        * сбросить РЭС
        */
        onRESReset: function (field, sender, eOpts) {
            var fields = ['Managers', 'User'];
            this.resetFields(fields);

            this.onTriggerReset(field, sender, eOpts);

            this.onRESSelect(field, null, eOpts);
        },
        /*
        * сбросить диспетчер
        */
        onManagersReset: function (field, sender, eOpts) {
            var fields = ['User'];
            this.resetFields(fields);

            this.onTriggerReset(field, sender, eOpts);

            this.onManagersSelect(field, null, eOpts);
        },
        /*
        * сбросить фио
        */
        onUserReset: function (field, sender, eOpts) {
            this.onTriggerReset(field, sender, eOpts);
        },

        /**
         * очистка значения в поле
         */
        onTriggerReset: function (field, sender, eOpts) {
            field.suspendEvent('change');
            field.reset();
            sender.hide();
            field.resumeEvent('change');

            var vm = this.getViewModel();
            vm.set('apply', true);

            var values = this.getValues();

            var isCleanActive = false;
            for (var i in values) {
                if (values[i]) {
                    isCleanActive = true;
                    this.fireEvent(this.EVENT_CLEAN_FILTER, this);
                    break;
                }
            }

            vm.set('clean', isCleanActive);
        },

        /**
         * обработчик сброса фильтрации
         */
        onCleanFilter: function () {
            this._lastValues = null;
            var vm = this.getViewModel();
            vm.set('apply', false);
            vm.set('clean', false);
            // сбрасываем значения в полях
            this.reset();
            var mainDivisionField = this.getField('MainDivision');
            if (mainDivisionField)
                mainDivisionField.focus();

            this.fireEvent(this.EVENT_CLEAN_FILTER, this);
        },

        /**
         * обработчик применения фильтрации
         */
        onApplyFilter: function () {
            var vm = this.getViewModel();
            vm.set('clean', true);
            vm.set('apply', false);
            this._lastValues = this.getValues();

            var isCleanActive = false;
            for (var i in this._lastValues) {
                if (this._lastValues[i]) {
                    isCleanActive = true;
                    break;
                }
            }
            vm.set('clean', isCleanActive);

            this.fireEvent(this.EVENT_APPLY_FILTER, this, this._lastValues);
        },

        /**
         * обработчик изменения поля для фильтрации
         * @param sender {any} поле
         * @param newValue {any} новое значение
         */
        onFilterChange: function (sender, newValue) {
            var vm = this.getViewModel();
            // здесь проверка на то, что данные изменились
            var values = this.getValues();
            if (!Ext.Object.equals(values, this._lastValues)) {
                vm.set('apply', true);
            } else {
                vm.set('apply', false);
            }
            var clear = sender.getTrigger('clear');
            if (clear)
                clear.show();

            this.fireEvent(this.EVENT_CHANGE_FILTER, this, sender, newValue);
        },
        /**
         * изменение филиала
         */
        onMainDivisionSelect: function (sender, newValue, eOpts) {
            this.onFilterChange(sender, newValue, null);

            var values = this.getValues();

            this.stores.Division.clearFilter(); // ПО
            this.stores.Division.filter([{
                property: 'F_Division',
                value: values.MainDivision,
                operator: '='
            }]);

            this.stores.SubDivision.clearFilter();  // участок
            this.stores.SubDivision.filter([{
                property: 'F_Division_Ref.F_Division',
                value: values.MainDivision,
                operator: '='
            }]);

            this.stores.RES.clearFilter(); // РЭС
            this.stores.RES.filter([{
                property: 'F_MainDivision',
                value: values.MainDivision,
                operator: '='
            }]);

            this.stores.Managers.clearFilter(); // диспетчер
            this.stores.Managers.filter([{
                property: 'S_MainDivision',
                value: values.MainDivision,
                operator: '='
            }]);

            this.onResetUser();

            Ext.defer(function () {
                this.getField('Division').focus();
            }, 50, this);
        },

        /**
         * изменение ПО
         */
        onDivisionSelect: function (sender, newValue, eOpts) {
            this.onFilterChange(sender, newValue);

            var values = this.getValues();
            this.stores.SubDivision.clearFilter();  // участок
            this.stores.SubDivision.filter([{
                property: 'F_Division',
                value: values.Division,
                operator: '='
            }]);

            this.stores.RES.clearFilter(); // РЭС
            this.stores.RES.filter([{
                property: 'F_MainDivision',
                value: values.MainDivision,
                operator: '='
            }, {
                property: 'F_Division',
                value: values.Division,
                operator: '='
            }]);

            this.stores.Managers.clearFilter(); // диспетчер
            this.stores.Managers.filter([{
                property: 'S_MainDivision',
                value: values.MainDivision,
                operator: '='
            }, {
                property: 'S_Division',
                value: values.Division,
                operator: '='
            }]);

            this.onResetUser();

            Ext.defer(function () {
                this.getField('SubDivision').focus();
            }, 50, this);
        },

        /**
         * изменение Участка
         */
        onSubDivisionSelect: function (sender, newValue, eOpts) {
            this.onFilterChange(sender, newValue);

            var values = this.getValues();

            this.stores.RES.clearFilter(); // РЭС
            this.stores.RES.filter([{
                property: 'F_MainDivision',
                value: values.MainDivision,
                operator: '='
            }, {
                property: 'F_Division',
                value: values.Division,
                operator: '='
            }, {
                property: 'F_SubDivision',
                value: values.SubDivision,
                operator: '='
            }]);

            this.stores.Managers.clearFilter(); // диспетчер
            this.stores.Managers.filter([{
                property: 'S_MainDivision',
                value: values.MainDivision,
                operator: '='
            }, {
                property: 'S_Division',
                value: values.Division,
                operator: '='
            }, {
                property: 'S_SubDivision',
                value: values.SubDivision,
                operator: '='
            }]);

            this.onResetUser();

            Ext.defer(function () {
                this.getField('Managers').focus();
            }, 50, this);
        },

        /**
         * изменение РЭС
         */
        onRESSelect: function (sender, newValue, eOpts) {
            this.onFilterChange(sender, newValue);

            this.onResetUser();
        },

        /**
         * изменение диспетчер
         */
        onManagersSelect: function (sender, newValue, eOpts) {
            this.onFilterChange(sender, newValue);

            this.onResetUser();

            Ext.defer(function () {
                this.down('#applyId').focus();
            }, 50, this);
        },

        /**
         * изменение фио
         */
        onUserChange: function (sender, newValue, oldValue, eOpts) {
            this.onFilterChange(sender, newValue, oldValue);
        },

        /*
         * обработчик нажатия на ENTER в поле пользователи
         */
        onUserKeyPress: function (sender, e, eOpts) {
            if (e.getKey() == e.ENTER) {
                this.onApplyFilter();
            }
        },

        /*
         * сбросить пользователя
         */
        onResetUser: function () {
            this.resetFields(['User']);
            this.getField('User').setValue(null);
        }
    }
});