﻿///*
// * Главное меню приложения
// */
Ext.define('ARM.view.main.MainMenuBar', {
    extend: 'Ext.Container',
    xtype: 'app-mainmenu-bar',
    cls: 'app-mainmenu-bar',
    bodyStyle: 'margin: 10px 0 0 0;',
    defaultListenerScope: true,
    requires: [
        'ARM.view.main.ActionButton'
    ],

    viewModel: {
        data: {
            userName: '',
        }
    },

    layout: {
        type: 'fit',
        align: 'center',
    },


    /**
     * событие нажатия на пункт
     */
    EVENT_ACTION: 'action',
    EVENT_LOADED: 'loaded',

    rawMenuItems: null,

    config: {
        menuItems: [],
        userRole: [],
        securityItems: []
    },
    setUserInfo: function(name, role) {
        Ext.each(role, function(item, id) {
            role[id] = item.toLowerCase();
        });
        this.setUserRole(role);
        this.getViewModel().set('userName', name);
    },

    loadMenuItems: function() {
        var me = this;
        if (this.rawMenuItems && this.rawMenuItems.length > 0) {
            me.setSecurityItems(me.prepareSecurityItems(this.rawMenuItems));
            var root = me.selectAllowedNodes(this.rawMenuItems);
            var menu = Ext.create('Ext.menu.Bar', {
                items: root,
                floating: false,
                visibility: true,
                plain: true,
                ui: 'dark'
            });

            me.add(menu);
            me.setMenuItems(root);
            me.fireEvent(me.EVENT_LOADED, root);
        } else {
            throw new Error('Не заданы элементы навигации')
        }
    },

    privates: {
        /*
         * получить массив безопасности из меню
         */
        prepareSecurityItems: function(root) {
            var me = this;
            var securityItems = [];
            Ext.each(root, function(rootNode) {
                var itemRoles = [];
                if (rootNode.Roles) {
                    itemRoles = rootNode.Roles.toLowerCase().split(",");
                }
                if (rootNode.menu) {
                    var innerItems = [];
                    if (rootNode.menu.items) {
                        innerItems = me.prepareSecurityItems(rootNode.menu.items);
                    } else {
                        innerItems = me.prepareSecurityItems(rootNode.menu);
                    }
                    if (innerItems.length > 0)
                        securityItems = securityItems.concat(innerItems);
                }
                var action = rootNode.actionId;
                if (action && itemRoles.length > 0) {
                    var securityNode = {
                        "part": '#' + action,
                        "access": "allow",
                        "users": [],
                        "roles": itemRoles,
                    }
                }
                if (securityNode) {
                    securityItems.push(securityNode);
                }
            });
            return securityItems;
        },

        /*
         * получить меню без закрытых элементов
         */
        selectAllowedNodes: function(root) {
            var me = this;
            var newRoot = [];
            Ext.each(root, function(rootNode) {
                var itemRoles = [];
                if (rootNode.Roles) {
                    itemRoles = rootNode.Roles.toLowerCase().split(",");
                }
                //если в элементе не указаны роли или доступ разрешен
                if (itemRoles.length == 0 || me.compareRoles(itemRoles)) {
                    if (rootNode.menu) {
                        if (rootNode.menu.items) {
                            rootNode.menu.items = me.selectAllowedNodes(rootNode.menu.items);
                        } else {
                            rootNode.menu = me.selectAllowedNodes(rootNode.menu);
                        }
                    }
                    newRoot.push(rootNode);
                }
            });
            if (newRoot.length > 0)
                return newRoot;
        },
        /*
         * проверить доступ
         */
        compareRoles: function(itemRoles) {
            var intersec = Utilits.intersecArrays(itemRoles, this.getUserRole());
            return intersec.length > 0;
        },
        /*
         * предыдущий активный элемент
         */
        lastActiveItem: null,

        /*
         * обрабтчик ручной синхронизации
         */
        onManualSync: function() {

            var win = Ext.create('ARM.view.main.ManualSync', {});
            win.show();
        },

        /*
         * написать сообщение
         */
        onChat: function() {
            var win = Ext.getCurrentApp().getChat();
            win.sendAll();
            win.show();

        },

        /**
         * обработчик выход пользователя
         */
        onExit: function() {
            Ext.getCurrentApp().getAuthProvider().singOut();
            location.reload();
        },

        /**
         * Изменить выделение пункта меню
         */
        changeActiveMark: function(actionId) {
            if (this.lastActiveItem) {
                this.lastActiveItem.removeCls('active');
            } else {
                if (this.el) {
                    var el = this.el.down('.x-btn.active');
                    if (el) {
                        el.removeCls('active');
                    }
                }
            }

            if (actionId) {
                var items = Ext.all('menuitem', this);
                var btn = items.filter(function(item) {
                    return item.actionId && item.actionId.indexOf(actionId) > -1
                })[0];

                if (btn) {
                    var menuitem = btn.up('menuitem');
                    if (!menuitem) {
                        menuitem = btn;
                    }
                    menuitem.addCls('active');

                    this.lastActiveItem = menuitem;
                }
            }
        },

        /**
         * обработчик нажатия на пункт меню
         * @param btn {any} кнопка 
         */
        onAction: function(btn) {
            var action = btn.getAction();
            if (action) {
                this.fireEvent(this.EVENT_ACTION, action, btn, this);
            }
        }
    }
});