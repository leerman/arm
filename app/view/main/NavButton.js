﻿/*
 * конопка с действиями
 */
Ext.define('ARM.view.main.NavButton', {
    extend: 'Ext.menu.Item',
    xtype: 'app-nav-button',

    action: null,
    crs: 'x-btn-wrap',

    setAction: function (val) {
        this.action = val;
    },

    getAction: function () {
        return this.action;
    },

    /*
     * устанаовка класса
     */
    setCls: function (value) {
        this.cls = value;
        this.addCls(value);
    }
});