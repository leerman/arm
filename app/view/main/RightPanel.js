﻿/*
 * панель для вывода дополнительной информации
 */
Ext.define('ARM.view.main.RightPanel', {
    extend: 'Ext.Panel',
    xtype: 'app-rightpanel',

    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeRightWidth: getResponsiveWidth(),
        smallRightWidth: '!largeRightWidth'
    },

    responsiveConfig: {
        'largeRightWidth': {
            width: 390
        },
        'smallRightWidth': {
            width: 300
        }
    },

    layout: 'fit',
    /**
     * имя компонента для отображения справо
     */
    viewPanelName: null,

    constructor: function (cfg) {
        Ext.apply(this, cfg);
        this.callParent(arguments);
    },

    showPanel: function (view, cardView) {
        if (this.viewPanelName) {
            if (!this.getViewPanel()) {
                // код для добавления своего компонента в правую часть
                this.setViewPanel(Ext.create(this.viewPanelName, {
                    container: this,
                    options: {
                        view: view,
                        cardView: cardView
                    },
                    listeners: {
                        afterrender: function (sender) {
                            sender.refreshPanel({
                                view: view,
                                cardView: cardView
                            });
                        }
                    }
                }));

                this.add(this.getViewPanel());
            } else {
                Ext.defer(function () {
                    this.getViewPanel().refreshPanel({
                        view: view,
                        cardView: cardView
                    });
                }, 100, this);
            }

            this.show();
        } else {
            new Exception(new Error('Имя представления не указано'));
        }
    },

    hidePanel: function (view, cardView) {
        this.hide();
    },

    /*
     * обновление содержимого панели
     * @param viewPanelName {string} наименование представления  
     */
    refreshPanel: function (viewPanelName) {
        this.viewPanel = null;
        this.viewPanelName = viewPanelName;
        this.removeAll(true);
    },

    privates:{
        viewPanel: null,
        getViewPanel: function () {
            return this.viewPanel;
        },
        setViewPanel: function (value) {
            this.viewPanel = value;
        }
    },

    destroy: function () {
        var viewPanel = this.getViewPanel();
        if (viewPanel) {
            viewPanel.destroy();
            this.setViewPanel(null);
        }
        this.callParent(arguments);
    }
});