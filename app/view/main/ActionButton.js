﻿/*
 * конопка с действиями
 */
Ext.define('ARM.view.main.ActionButton', {
    extend: 'Ext.button.Button',
    xtype: 'app-action-button',
    ui: 'action-link',
    action: null,

    setAction: function (val) {
        this.action = val;
    },

    getAction: function () {
        return this.action;
    },

    /*
     * устанаовка класса
     */
    setCls: function (value) {
        this.cls = value;
        this.addCls(value);
    }
});