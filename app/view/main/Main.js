/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('ARM.view.main.Main', {
    extend: 'Ext.Panel',
    xtype: 'app-main',
    ui: 'body',
    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'ARM.view.main.MainFilter',
        'ARM.view.main.UserList',
        'ARM.view.main.RightPanel',
        'ARM.view.main.UserCard',

        'ARM.view.main.MainController',
        'ARM.view.main.MainModel',

        'ARM.view.home.Home',
        'ARM.view.about.About',
        'ARM.view.task.Task',
        'ARM.view.appoint.Appoint',
        'ARM.view.develop.Develop',
        'ARM.view.history.History',
        'ARM.view.documents.Documents',
        'ARM.view.admin.Admin',
        'ARM.view.stat.Stat',
        'ARM.view.report.Report',
        'ARM.view.main.CommandConsole',
        'ARM.Diagnostic',
        'ARM.view.version.List'
    ],
    mixins: ['Ext.mixin.Responsive'],
    
    responsiveFormulas: {
        largeMainHeight: getResponsiveHeight(),
        smallMainHeight: '!largeMainHeight',
        largeMainWidth: getResponsiveWidth(),
        smallMainWidth: '!largeMainWidth'
    },

    controller: 'main',
    viewModel: 'main',
    plugins: 'viewport',

    dockedItems: [
        {
            xtype: 'app-diagnostic',
            dock: 'top',
            hidden: true,
            height: 2
        },
        {
            xtype: 'app-command-console',
            dock: 'bottom',
            hidden: localStorage.getItem('terminal') != null ? false : true,
            ui: 'white'
        },
        { //левая панель
            xtype: 'panel',
            reference: 'leftblock',
            itemId:'leftblock',
            dock: 'left',
            layout: {
                type: 'vbox',
                pack: 'start'
            },
            defaults: {
                width: '100%'
            },
            width: 390,
            items: [
                {
                    xtype: 'app-main-filter',

                    plugins: 'responsive',
                    responsiveConfig: {
                        'largeMainHeight': {
                            style: {
                                padding: '0 0 35px 0'
                            }
                        },
                        'smallMainHeight': {
                            style: {
                                padding: '0 0 20px 0'
                            }
                        }
                    },

                    listeners: {
                        apply: 'onFilterApply',
                        clean: 'onFilterClean',
                        change: 'onFilterChange',

                        render: 'onMainFilterRender'
                    }
                    /*
                     * listeners:{
                     *      apply:(sender, values) - событие применения фильтра
                     *      change:(sender, field, newValue, oldValue) - изменение фильтра
                     *      clean: (sender) - очистка фильтра
                     * }
                     */
                },
                {
                    xtype: 'app-user-list',
                    cardView: 'ARM.view.main.UserCard',
                    scrollable: 'y',
                    reference: 'app-user-list',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        pack: 'start'
                    },
                    defaults: {
                        width: '100%'
                    }
                }
            ]
        }
    ],
    layout: 'fit',

    constructor: function (cfg) {
        Ext.apply(this, cfg);

        this.items = [{
            layout: 'fit',
            reference: 'placeHolder',
            dockedItems: [
                {
                    // верхнее меню
                    xtype: 'app-mainmenu-bar',
                    reference: 'app-mainmenu',
                    dock: 'top',
                    ui: 'body',

                    plugins: 'responsive',
                    responsiveConfig: {
                        'largeMainHeight && largeMainWidth': {
                            height: 70
                        },
                        'smallMainHeight || smallMainWidth': {
                            height: 50
                        }
                    },

                    listeners: {
                        action: 'onAction'
                    }
                },
                {
                    xtype: 'app-rightpanel',
                    reference: 'rightpanel',
                    dock: 'right',
                    hidden: true,
                    viewPanelName: 'ARM.view.home.RightStat'
                }
            ],
            items: [
                {
                    xtype: 'app-' + (this.pageName || 'home'),
                    subType: this.subPageName
                }
            ]
        }];
        this.callParent(arguments);
    },

    /*
     * применение условия фильтрации
     * @param filterPanel {any} панель фильтрации
     * @param values {any} данные для фильтрации
     */
    applyFilter: function (filterPanel, values) {
        this.getController().onFilterApply(filterPanel, values);
    },

    listeners: {
        // событие готовности предствления для вывода данных (страницы)
        placeholderready: 'onPlaceHolderReady'
    },

    destroy: function () {
        var controller = this.getController();
        var list = this.down('app-user-list');
        if (list) {
            list.un('select', controller.onSelectUser, this);
            list.un('unselect', controller.onUnSelectUser, this);
        }

        this.callParent(arguments);
    }
});
