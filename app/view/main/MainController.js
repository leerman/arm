﻿/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('ARM.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onMainFilterRender: function (sender) {
        var view = this.getView();
        var list = view.down('app-user-list');
        if (list) {
            list.on('select', this.onSelectUser, view, sender);
            list.on('unselect', this.onUnSelectUser, view, sender);
        }
    },

    /*
     * обработчик нажатия на пункты меню
     */
    onAction: function (action, btn, mainmenu) {
        this.redirectTo(action);
    },

    /*
     * обработчик применения фильтрации
     * @param sender {any} панель фильтрации
     * @param values {any} значения полей фильтрации
     */
    onFilterApply: function (sender, values) {
        var list = this.lookupReference('app-user-list');
        if (list) {

            var mask = new Ext.LoadMask({
                msg: 'Загрузка...',
                target: list
            });

            mask.show();
            var me = this;
            list.load(sender.getUserStore(), function () {
                list.setFilterValues(values);
                // здесь передаем дополнительно данные в компонент, чтобы понимать с какими пользователями работаем
                var placeHolder = me.lookupReference('placeHolder');
                if (placeHolder) {
                    var appPlaceHolder = placeHolder.down('app-placeholder');
                    if (appPlaceHolder.isReady == true)
                        me.onPlaceHolderReady(sender);

                }
                mask.destroy();
            });
        }
    },

    /*
     * обработчик готовности контейнера
     * @param sender {any} панель фильтрации
     */
    onPlaceHolderReady: function (sender) {
        var store = sender.getUserStore();

        var placeHolder = this.lookupReference('placeHolder');
        if (placeHolder) {
            var appPlaceHolder = placeHolder.down('app-placeholder');
            if (appPlaceHolder && appPlaceHolder.setUserStore) {
                appPlaceHolder.setUserStore(store);
            }
        }
    },

    /*
     * обработчик очистки фильтра
     * @param sender {any} панель фильтрации
     */
    onFilterClean: function (sender) {
        var list = this.lookupReference('app-user-list');
        if (list) {
            list.empty();
        }

        var rightPanel = this.lookupReference('rightpanel');
        if (rightPanel) {
            rightPanel.hidePanel(list, null);
        }

        var placeHolder = this.lookupReference('placeHolder');
        if (placeHolder) {
            var appPlaceHolder = placeHolder.down('app-placeholder');
            if (appPlaceHolder && appPlaceHolder.onMainFilterClean) {
                appPlaceHolder.onMainFilterClean();
            }
        }
    },

    /**
     * обработчик изменения фильтра 
     * @param filterPanel {any} панель фильтрации
     * @param field {any} поле
     * @param value {any} значение поля
     */
    onFilterChange: function(filterPanel, field, value){
        switch (field.getName()) {
            case 'User':

                break;
        }
    },

    privates: {
        /*
         * обработчик выбора пользователя
         * @param view {any} панель с пользователями
         * @param cardView {any} карточка пользователя
         * @param mainfilter {any} панель фильтрации
         */
        onSelectUser: function (view, cardView, mainfilter) {
            if (cardView) {
                var model = cardView.getModel();
                if (model) {
                    mainfilter.setUserName(model.get('C_Fio'), model);
                }
            }
        },
        /*
         * обработчик выбора пользователя
         * @param view {any} панель с пользователями
         * @param cardView {any} карточка пользователя
         * @param mainfilter {any} панель фильтрации
         */
        onUnSelectUser: function (view, cardView, mainfilter) {
            if (cardView) {
                var model = cardView.getModel();
                if (model) {
                    mainfilter.setUserName("", model);
                }
            }
        }
    }
});
