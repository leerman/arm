﻿Ext.define('ARM.view.home.HomeController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.home',
    // наименования поля в настройках для определения цвета по умолчанию
    defaultColorFieldName: 'C_Default_Color',

    onRender: function (sender) {
        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('select', this.onUserSelect, sender);
            list.on('unselect', this.onUserUnSelect, sender);
        }

        var mainfilter = sender.up('app-main').down('app-main-filter');
        if (mainfilter) {
            mainfilter.on('singleuser', this.onSingleUser, sender);
            mainfilter.on('cleanuser', this.onSingleUser, sender);
        }

        // встраиваем свой компонент в правую часть
        var initView = sender.up('app-main');
        if (initView) {
            var rightPanel = initView.lookupReference('rightpanel');
            if (rightPanel) {
                rightPanel.refreshPanel('ARM.view.home.RightStat');
            }
        }

        var mask = new Ext.LoadMask({
            msg: 'Загрузка...',
            target: sender
        });
        mask.show();

        var iframe = sender.el.query('iframe')[0];
        if (iframe) {
            var me = this;
            iframe.onload = function () {
                sender.setMapAPI(new this.contentWindow.mapAPI({
                    id: 'map',
                    pointConterter: new this.contentWindow.converter(),
                    defaultControls: ['zoomControl', 'searchControl', 'fullscreenControl'],
                    iconGenerator: Ext.conf().getIconPath(),
                    mapOptions: {
                        zoom: Ext.getConf('map_zoom'),
                        center: [Ext.getConf('map_latitude'), Ext.getConf('map_longitude')]
                    },

                    onUserClick: function (data) { me.onUserClick(data); }, // добавляем обработчик при нажатии на пользователе
                    onTaskClick: function (data) { me.onTaskClick(data); }
                }, function (mapAPI) {
                    // карта создана и готова к использованию

                    // теперь нужно подписаться на событие action у списка пользователей
                    var list = sender.getUserListComponent();
                    if (list) {
                        list.on('action', me.onAction, sender);
                        var selectCardView = list.getSelectCardView();
                        if (selectCardView)
                            list.fireEvent('select', list, selectCardView);
                    }
                    sender.setIsReady();
                    mask.destroy();
                }));
            }
        }
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;
        var routeOptions = sender.routeOptions;
        if (routeOptions && routeOptions.uuid) {
            users = users.filter(function (i) { return i.get('uuid') == routeOptions.uuid; });
        }

        var me = this;

        // нужно очистить данные с карты
        var mapAPI = sender.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();

        sender.mask('Обработка элементов на карте');
        _.series(
            [
                function (callback) {
                    sender.mask('Добавление<br />обходчиков...');
                    me.addPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    var txt = 'Добавление<br />маршрутов';
                    sender.mask(txt + '...');
                    me.addRoutes(sender, users, function () {
                        callback();
                    }, function (percent) {
                        sender.mask(txt + ' ' + percent.toFixed(0) + '%');
                    });
                },
                function (callback) {
                    var txt = 'Добавление<br />заданий';
                    sender.mask(txt + '...');
                    me.addTasks(sender, users, function () {
                        callback();
                    }, function (percent) {
                        sender.mask(txt + ' ' + percent.toFixed(0) + '%');
                    });
                },
                function (callback) {
                    sender.unmask();
                    sender.fireEvent(sender.EVENT_MAP_ITEMS_READY, sender);
                    callback();
                }
            ]);
    },

    /*
     * обработчик выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserSelect: function (view, cardView) {
        var mainView = this.up('app-main');
        var right = mainView.lookupReference('rightpanel');
        if (right) {
            right.showPanel(view, cardView);
        }
    },

    /**
     * отмена выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserUnSelect: function (view, cardView) {
        var mainView = this.up('app-main');
        var right = mainView.lookupReference('rightpanel');
        if (right)
            right.hidePanel(view, cardView);
    },

    /*
     * обработчик выбора одного пользователя
     * @param name {string} имя пользователя
     * @param record {any} запись
     */
    onSingleUser: function (name, record, mainfilter) {
        if (name) {
            this.getController().onSetUserStore(this, {
                getData: function () {
                    return {
                        items: [record]
                    }
                }
            });
        } else {
            var store = mainfilter.getUserStore();
            this.getController().onSetUserStore(this, store);
        }
    },

    privates: {

        /*
         * обработчик при готовности карты и элементов
         */
        onMapItemsReady: function (sender) {
            var route_options = sender.getRouteOptions();
            if (route_options) {
                var type = route_options.type;
                var uuid = route_options.uuid;
                if (type && uuid) {
                    // теперь устаналиваем фокус на карте
                    var mapAPI = sender.getMapAPI();
                    if (mapAPI) {
                        mapAPI.focusObject(route_options.type, route_options.uuid)
                    }
                }
            }
        },

        /*
         * обработчик действия с карточкой пользователя
         * @param list {any} представление со списком пользователей
         * @param action {string} действие
         * @param record {any} запись пользователя
         * @param card {any} карточка пользователя - представление
         */
        onAction: function (list, action, record, card) {
            var mapAPI = this.getMapAPI();
            if (mapAPI) {
                // здесь нужно сделать ожидение
                var result = mapAPI.focusObject(action, record.get('uuid'));
                if (!result) {
                    var msg = '';
                    switch (action) {
                        case 'user':
                            msg = 'Местоположение обходчика не найдено';
                            break;

                        case 'route':
                            msg = 'Маршрут обходчика не найден';
                            break;

                        case 'tasks':
                            msg = 'Задания обходчика не найдены';
                            break;
                    }
                    if (msg) {
                        Ext.Msg.show({
                            title: 'Сообщение',
                            message: msg,
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.INFO
                        });
                    }
                }
            }
        },

        /*
         * обработчик нажатия на пользователе
         * @param data {any} данные о пользователе (результат который возвращает метод DefaultDataProvider.getUsersPosition)
         */
        onUserClick: function (data) {
            // нужно найти панель с пользователями
            var list = this.getView().getUserListComponent();
            if (list) {
                var uuid = data.uuid;
                if (uuid) {
                    var record = list.map_uuid[uuid];
                    if (record) {
                        var card = list.map[record.getId()];
                        if (card) {
                            card.setMode('maximize');

                            var scroll = list.getScrollable();
                            scroll.scrollIntoView(card.el.dom);
                        }
                    }
                }
            }
        },

        /*
         * обработчик нажатия на переход документа
         * @param url {string} ссылка документа
         */
        onTaskClick: function (url) {
            this.redirectTo(url);
        },

        /*
         * добавление меток на карту
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        addPlacemarks: function (sender, users, callback) {
            var me = this;
            this.getPlacemarks(users, function (results) {
                // теперь нужно обновить список пользователей у которых есть информация о их местоположении
                var view = me.getView();
                if (view) {
                    var list = view.getUserListComponent();
                    if (list) {
                        results.forEach(function (i) {
                            var record = list.map_uuid[i.get('uuid')];
                            if (record)
                                list.setAccess(record.getId(), true);
                        });
                    }

                    if (typeof callback == 'function')
                        callback();
                }
            });
        },

        /*
         * добавление маршрута на карту
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         * @param itemCallback {(percent)=>void} функция обратного вызова при прохождении итерации
         */
        addRoutes: function (sender, users, callback, itemCallback) {
            var me = this;
            // добавление маршрута следования
            this.getRoutes(users, function (results) {
                var mapAPI = sender.getMapAPI();
                if (mapAPI) {
                    Ext.each(users, function (user) {
                        var uuid = user.get('uuid');
                        var color = user.get('C_Icon_Color');
                        if (uuid && color) {
                            var array = [];
                            Ext.each(results[uuid], function (i) {
                                array.push(i.getData());
                            });
                            if (array && array.length > 0) {
                                mapAPI.addRoute(uuid, array, Ext.getGlobalParams(this.defaultColorFieldName) || color, user.get('C_Fio'), false);
                            }
                        }
                    }, me);

                    if (typeof callback == 'function')
                        callback();
                }
            }, itemCallback);
        },

        /*
         * добавление заданий на карту
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         * @param itemCallback {(percent)=>void} функция обратного вызова при прохождении итерации
         */
        addTasks: function (sender, users, callback, itemCallback) {
            var me = this;
            this.getTasks(users, function (tasks) {
                var dataWorker = sender.getYandexDataWorker();

                if (tasks && tasks.length > 0) {
                    // тут нужно прочитать все записи и определить есть ли у них адреса
                    // затем нужно добавить им координаты
                    var geoData = {};
                    for (var i = 0; i < tasks.length; i++) {
                        var point = tasks[i];
                        var address = point.get('Address');
                        if (address) {
                            if (point.isCoordExist() != true) {
                                var hash = md5(address);
                                point.set('hash', hash);

                                geoData[hash] = address;
                            }
                            
                        } else {
                            // здесь нужно что-то делать с точка у которых нет адреса
                            new Exception(new Error('Адрес не указан'));
                        }
                    }

                    var count = 0;
                    for (var i in geoData)
                        count++;
                    if (sender.showProgressBar) {
                        var isShowProgressBar = false;
                        var timeout = setTimeout(function () {
                            isShowProgressBar = true;
                        }, 2000);
                        var msg = 'Обработка геоданных. Подождите';

                        dataWorker.onTask({ type: 'geocoder', data: geoData }, function (results, options) {
                            if (results == null) {
                                var percent = ((options * 100) / count).toFixed(0);
                                if (isShowProgressBar == true)
                                    sender.showProgressBar(msg + ' ' + percent + '%', percent);
                                // значит это только итерация
                                return;
                            } else {
                                sender.showProgressBar();
                                clearTimeout(timeout);
                            }
                            // нужно сгруппировать точки по координатам
                            var groups = {};
                            for (var i = 0; i < tasks.length; i++) {
                                var point = tasks[i];
                                var hash = point.get('hash');

                                if (results[hash])
                                    point.setCoord(results[hash]);

                                var coordHash = point.getCoordHash();
                                // добавляем индексирование
                                var uuid = [point.get('PhoneIMEI')];
                                if (!groups[uuid]) {
                                    groups[uuid] = {
                                        /*
                                         * возвращается общее количество заданий
                                         * @param coordHash {string} хэш-сумма координат. не обязателен
                                         */
                                        getTotalCount: function (coordHash) {
                                            var count = 0;

                                            for (var i in this) {
                                                if (typeof this[i] != 'function') {
                                                    if (coordHash) {
                                                        if (coordHash == i) {
                                                            count += this[i].length;
                                                            break;
                                                        }
                                                        continue;
                                                    }

                                                    count += this[i].length;
                                                }
                                            }
                                            return count;
                                        },

                                        /**
                                         * возвращается количество элементов которые были выполнены
                                         * @param coordHash {string} хэш-сумма координат. не обязателен
                                         */
                                        getDoneCount: function (coordHash) {
                                            var idx = 0;
                                            for (var i in this) {

                                                if (typeof this[i] != 'function') {

                                                    if (coordHash) {
                                                        if (coordHash == i) {
                                                            for (var j = 0; j < this[i].length; j++) {
                                                                var item = this[i][j];
                                                                if (item.IsDone == true)
                                                                    idx++;
                                                            }

                                                            break;
                                                        }
                                                        continue;
                                                    }

                                                    for (var j = 0; j < this[i].length; j++) {
                                                        var item = this[i][j];
                                                        if (item.IsDone == true)
                                                            idx++;
                                                    }
                                                }
                                            }

                                            return idx;
                                        }
                                    };
                                }

                                if (!groups[uuid][coordHash])
                                    groups[uuid][coordHash] = [];

                                groups[uuid][coordHash].push(point.getData());
                            }

                            // тут обработали все координаты
                            var mapAPI = sender.getMapAPI();
                            if (mapAPI) {
                                Ext.each(users, function (user) {
                                    var uuid = user.get('uuid');
                                    var color = user.get('C_Icon_Color');
                                    if (uuid && color) {
                                        if (groups[uuid]) {
                                            //var path = Ext.String.format(Ext.getConf('RPC_URL').replace('/rpc', '/'), Ext.getConf('REMOTING_ADDRESS'));
                                            mapAPI.addTask(uuid, groups[uuid], Ext.getGlobalParams(this.defaultColorFieldName) || color);
                                        }
                                    }
                                }, me);

                                if (typeof callback == 'function')
                                    callback();
                            }
                        });
                    }
                } else {
                    if (typeof callback == 'function')
                        callback();
                }
            }, itemCallback);
        },

        /*
        * возвращаются метоки на карту (пользователи)
        * @param users {ARM.model.User[]} список пользователей 
        * @param callback {()=>void} функция обратного вызова
        */
        getPlacemarks: function (users, callback) {
            // данные о пользователях получены
            var view = this.getView();
            if (view) {
                var mapAPI = view.getMapAPI();
                if (mapAPI) {
                    Ext.each(users, function (user) {
                        if (user.isWorkingToday() == true) {
                            mapAPI.addUser(user.getData(), Ext.getGlobalParams(this.defaultColorFieldName));
                        } else {
                            //mapAPI.addUser(user.getData(), '00ff00');
                        }
                    }, this);
                    mapAPI.focusByUsers();
                } else {
                    new Exception(new Error('Компонента mapAPI для работы с картой не найдено.'));
                }

                if (typeof callback == 'function')
                    callback(users);
            }
        },

        /**
         * возвращаются задания на карту для указанных пользователей
         * @param users {ARM.model.User[]} список пользователей
         * @param callback {(ARM.model.Task[])=>void} функция обратного вызова
         * @param itemCallback {(percent)=>void} функция обратного вызова при прохождении итерации
         */
        getTasks: function (users, callback, itemCallback) {
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            var list = [];

            users.forEach(function (i) {
                list.push(i.getId());
            });

            dataProvider.getUsersTasks(list, function (items) {
                if (typeof callback == 'function')
                    callback(items);
            });

        },

        /**
         * возвращаются маршруты для указанных пользователей
         * @param users {ARM.model.User[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         * @param itemCallback {(percent)=>void} функция обратного вызова при прохождении итерации
         */
        getRoutes: function (users, callback, itemCallback) {
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            var list = [];
            users.forEach(function (i) {
                var uuid = i.get('uuid');
                if (uuid && i.isWorkingToday()) {
                    list.push(uuid);
                }
            });

            dataProvider.getUsersRoutes(list, function (items) {
                if (typeof callback == 'function')
                    callback(items);
            });
        }
    }
});