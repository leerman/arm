﻿Ext.define('ARM.view.home.Home', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-home',
    requires: [
        'ARM.view.home.HomeController',
        'ARM.view.home.HomeModel',
        'ARM.view.home.RightStat'
    ],
    layout: 'fit',
    /*
     * событие гтовности карты с элементами
     */
    EVENT_MAP_ITEMS_READY: 'mapitemsready',

    /*
     * объект для работы с картой
     */
    mapAPI: null,
    setMapAPI: function (value) {
        this.mapAPI = value;
    },
    getMapAPI: function () {
        return this.mapAPI;
    },

    controller: 'home',
    viewModel: 'home',

    listeners: {
        render: 'onRender',
        setuserstore: 'onSetUserStore',
        mapitemsready: 'onMapItemsReady'
    },

    /*
     * дополнительные параметры в роутинге
     */
    routeOptions: null,

    constructor: function () {
        this.callParent(arguments);

        this.html = '<iframe src="' + Ext.getCurrentApp().toAbsolutePath('resources/pages/ymaps') + '" style="width:100%;height:100%;border:none"><p>Ваш браузер не поддерживает iframe.</p></iframe>';
    },

    /*
     * передача дополнительных параметров через роутинг 
     * Вызывается в тот момент когда передаются дополнительные параметры
     * @param options {any} дополнительные параметры
     */
    onRouteOptions: function (options) {
        this.callParent(arguments);
        if (options && options.type != 'home') {

            this.routeOptions = options;

            var initView = Ext.getCurrentApp().getInitView();
            if (initView) {
                var filter = initView.down('app-main-filter');

                if (filter.getFiltered() != true) {
                    var uuid = options.uuid;
                    filter.getField('uuid').setValue(uuid);
                    initView.applyFilter(filter, {
                        userId: uuid
                    });

                    filter.placeHolderReady();
                }
            }
        }
    },

    /*
     * обработчик отмены фильтрации
     */
    onMainFilterClean: function () {
        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();
    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);

            list.un('select', controller.onUserSelect, this);
            list.un('unselect', controller.onUserUnSelect, this);
        }

        var mainfilter = this.getMainFilter();
        if (mainfilter) {
            mainfilter.un('singleuser', controller.onSingleUser, this);
            mainfilter.un('cleanuser', controller.onSingleUser, this);
        }

        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.destroy();
        this.callParent(arguments);
    }
});