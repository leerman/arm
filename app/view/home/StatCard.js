﻿/*
 * карточка статистики обхода
 */
Ext.define('ARM.view.home.StatCard', {
    extend: 'Ext.Panel',
    xtype: 'app-home-statcard',
    ui: 'right-stat',
    defaultListenerScope: true,
    requires: [
        'ARM.store.StatCard'
    ],

    viewModel: {
        data: {
            noperson: true
        }
    },

    items: [
        {
            xtype: 'grid',
            emptyText: 'Нет данных',
            cls: 'grid-right-stat',
            padding: '10px',
            itemId: 'maingrid',
            columns: [
                {
                    text: 'Субъекты (правовых отношений)',
                    dataIndex: 'title',
                    flex: 1,
                    hideable: false,
                    sortable: false,
                    resizable: false
                },
                {
                    text: 'ФЛ',
                    dataIndex: 'person',
                    width: '40px',
                    hideable: false,
                    sortable: false,
                    resizable: false,
                    align: 'center',
                    renderer: 'onNoPersonRenderer'
                },
                {
                    text: 'ЮЛ',
                    dataIndex: 'noperson',
                    width: '40px',
                    hideable: false,
                    sortable: false,
                    resizable: false,
                    align: 'center',
                    renderer: 'onPersonRenderer',
                    hidden: true,
                    bind: {
                        hidden: '{!noperson}'
                    }
                }
            ]
        }
    ],

    constructor: function (cfg) {
        this.callParent(arguments);
        var vm = this.getViewModel();
        var personOnly = Ext.getConf('personOnly');
        if (personOnly == true) {
            vm.set('noperson', false);
        }
    },

    /*
     * установка хранилища
     * @param store {any} хранилище
     */
    setStore: function (store) {
        var grid = this.getComponent('maingrid');
        if (grid)
            grid.setStore(store);
        else {
            new Exception(new Error('Грид для вывода данных не найден'));
        }
    },

    privates: {
        /*
         * ФЛ
         */
        onPersonRenderer: function (value, cell, record) {
            return this.getColored(value, record.get('name'));
        },

        /*
         * ЮЛ
         */
        onNoPersonRenderer: function (value, cell, record) {
            return this.getColored(value, record.get('name'));
        },

        /**
         * добавлена цвета в зависимости от типа строки
         * @param value {number} значение строки
         * @param type {string} тип строки
         */
        getColored: function (value, type) {
            var color = 'white';
            switch (type) {
                case 'today':
                    color = '#269eff';
                    break;

                case 'done':
                    color = '#5ac227';
                    break;

                case 'old':
                    color = '#da2435';
                    break;

                case 'total':
                    color = '#808080';
                    break;
            }

            return '<div style="color:' + color + '">' + value + '</div>';
        }
    }
});