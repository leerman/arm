﻿/*
 * компонент для вывода статистики в виде progressbar
 */
Ext.define('ARM.view.home.ProgressBars', {
    extend: 'Ext.Panel',
    xtype: 'app-home-progressbars',
    cls: 'progressbars',
    requires: [
        'Core.ProgressBar'
    ],

    viewModel: {
        data: {
            day: { // данные по текущему дню
                all: 0,
                done: 0
            },
            total: { // данные за все дни (по документам)
                all: 0,
                done: 0
            }
        }
    },
    layout:{
        type: 'vbox',
        pack: 'start'
    },
    defaults:{
        width: '100%',
        flex: 1,
        xtype: 'core-progressbar',
        bgcolor: '#454545',
        color: '#269eff',
        cls: 'bar',
        padding: 10
    },
    items: [
        {
            itemId: 'day',
            dockedItems: [
                {
                    padding: '0 0 7px 0',
                    bind: {
                        html: 'Выполненные задачи за день <span class="numbers">({day.done} из {day.all})</span>'
                    }
                }
            ]
        },
        {
            itemId: 'total',
            dockedItems: [
                {
                    padding: '0 0 7px 0',
                    bind: {
                        html: 'Выполненные задачи по документам <span class="numbers">({total.done} из {total.all})</span>'
                    }
                }
            ]
        }
    ],

    /**
     * устанавливаем значение
     * @param value {any} данные
     */
    setData: function (value) {
        var vm = this.getViewModel();
        for (var i in value) {
            vm.set(i, value[i]);
        }

        var day = this.getComponent('day');
        if (day && value.day) {
            day.setProgress((100 * value.day.done) / value.day.all);
        }

        var total = this.getComponent('total');
        if (total && value.total) {
            total.setProgress((100 * value.total.done) / value.total.all);
        }
    }
})