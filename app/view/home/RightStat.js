﻿/*
 * правая панель со статистикой на главной странице
 */
Ext.define('ARM.view.home.RightStat', {
    extend: 'ARM.shared.BaseRightPanel',

    requires: [
        'ARM.view.home.StatCard',
        'ARM.view.home.ProgressBars'
    ],

    viewModel: {
        data: {

        }
    },

    items: [
        {
            xtype: 'app-home-statcard',
            title: 'Статистика по допуску ПУ в эксплуатацию',
            itemId: 'DT_METER_JOB',
            hidden: true,
            bind: {
                hidden: '{DT_METER_JOB}'
            }
        },
        {
            xtype: 'app-home-statcard',
            title: 'Статистика по контрольным обходам',
            itemId: 'DT_CONTROL_VISITS',
            hidden: true,
            bind: {
                hidden: '{DT_CONTROL_VISITS}'
            }
        },
        {
            xtype: 'app-home-progressbars',
            itemId: 'bars',
            height: 175
        }
    ],

    /**
     * обновление содержимой панели
     * @param options {any} дополнительные опции
     * @param callback {()=>void} функция обраного вызова
     */
    refreshPanel: function (options, callback) {
        // чтобы скрывать лишнии документы
        this.showDocuments();

        // тут можно достать информацию по пользователю
        options = options || this.getOptions();
        var me = this;
        var mask = new Ext.LoadMask({
            target: this,
            msg: 'Загрузка...'
        });
        
        Ext.defer(function () {
            mask.show();

            var dataProvider = Ext.getCurrentApp().getDataProvider();
            if (options.cardView) {
                var model = options.cardView.getModel();
                if (model) {
                    var link = model.get('LINK'); //'7F7E2A7E-9096-4B61-8724-3541A20816F3'
                    dataProvider.getStat(link, new Date(), function (statdata) {
                        statdata.forEach(function (i) {
                            var view = me.getComponent(i.get('C_Const'));
                            if (view) {
                                var store = Ext.create('ARM.store.StatCard');
                                store.loadData([
                                    {
                                        name: 'today',
                                        title: 'Выполнено за сегодня',
                                        person: i.get('N_Done_Today_PE'),
                                        noperson: i.get('N_Done_Today_EE')
                                    },
                                    {
                                        name: 'done',
                                        title: 'Выполнено всего',
                                        person: i.get('N_Done_PE'),
                                        noperson: i.get('N_Done_EE')
                                    },
                                    {
                                        name: 'old',
                                        title: 'Осталось',
                                        person: i.get('N_NotDone_PE'),
                                        noperson: i.get('N_NotDone_EE')
                                    },
                                    {
                                        name: 'total',
                                        title: 'Общее количество',
                                        person: i.get('N_Total_PE'),
                                        noperson: i.get('N_Total_EE')
                                    }
                                ]);
                                view.setStore(store);
                            }
                        });

                        var bars = me.getComponent('bars');
                        if (bars) {
                            var data = statdata[0].getData();
                            bars.setData({
                                total: {
                                    all: data.N_Total_All,
                                    done: data.N_Done_All
                                },
                                day: {
                                    all: data.N_Total_Today_All,
                                    done: data.N_Done_Today_All
                                }
                            });
                        }

                        mask.destroy();
                        if (typeof callback == 'function')
                            callback();
                    });
                } else {
                    mask.destroy();
                    if (typeof callback == 'function')
                        callback();
                }
            }
        }, 50);
    },

    privates: {
        /*
         * выводить документы
         */
        showDocuments: function () {
            // чтобы скрывать лишнии документы
            var vm = this.getViewModel();
            Ext.getCurrentApp().getDataProvider().getDocumentsType(function (types) {
                if (vm) {
                    types.forEach(function (type) {
                        vm.set(type.get('C_Const'), false);
                    });
                }
            });
        }
    },

    destroy: function () {
        this.callParent(arguments);
    }
});