﻿/*
 * история маршрутов
 */
Ext.define('ARM.view.history.History', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-history',
    requires: [
        'ARM.view.history.HistoryController',
        'ARM.view.history.HistoryModel',
        'ARM.view.history.FilterPanel'
    ],

    controller: 'history',
    viewModel: 'history',

    /*
     * событие гтовности карты с элементами
     */
    EVENT_MAP_ITEMS_READY: 'mapitemsready',

    /*
     * объект для работы с картой
     */
    mapAPI: null,
    setMapAPI: function (value) {
        this.mapAPI = value;
    },
    getMapAPI: function () {
        return this.mapAPI;
    },

    layout: 'fit',
    listeners: {
        render: 'onRender',
        setuserstore: 'onSetUserStore',
        mapitemsready: 'onMapItemsReady'
    },

    constructor: function () {
        this.callParent(arguments);

        this.html = '<iframe src="' + Ext.getCurrentApp().toAbsolutePath('resources/pages/ymaps') + '" style="width:100%;height:100%;border:none"><p>Ваш браузер не поддерживает iframe.</p></iframe>';
    },

    /*
     * обработчик отмены фильтрации
     */
    onMainFilterClean: function () {
        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();
    },

    destroy: function () {

        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);

            list.un('select', controller.onUserSelect, this);
            list.un('unselect', controller.onUserUnSelect, this);
        }

        var initView = Ext.getCurrentApp().getInitView();
        if (initView) {
            var rightPanel = initView.lookupReference('rightpanel');
            if (rightPanel) {
                var panel = rightPanel.getViewPanel();
                if (panel)
                    panel.un('change', controller.onFilterChange, this);
            }
        }

        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.destroy();
        this.callParent(arguments);
    }
});