﻿Ext.define('ARM.view.history.HistoryController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.history',
    // наименования поля в настройках для определения цвета по умолчанию
    defaultColorFieldName: 'C_Default_Color',

    onRender: function (sender) {
        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('select', this.onUserSelect, sender);
            list.on('unselect', this.onUserUnSelect, sender);
        }

        // встраиваем свой компонент в правую часть
        var initView = sender.up('app-main');
        if (initView) {
            var rightPanel = initView.lookupReference('rightpanel');
            if (rightPanel) {
                rightPanel.refreshPanel('ARM.view.history.FilterPanel');
            }
        }

        var mask = new Ext.LoadMask({
            msg: 'Загрузка...',
            target: sender
        });
        mask.show();

        var iframe = sender.el.query('iframe')[0];
        if (iframe) {
            var me = this;
            iframe.onload = function () {
                sender.setMapAPI(new this.contentWindow.mapAPI({
                    id: 'map',
                    pointConterter: new this.contentWindow.converter(),
                    defaultControls: ['zoomControl', 'searchControl', 'fullscreenControl'],
                    iconGenerator: Ext.conf().getIconPath(),
                    mapOptions: {
                        zoom: Ext.getConf('map_zoom'),
                        center: [Ext.getConf('map_latitude'), Ext.getConf('map_longitude')]
                    },

                    onUserClick: function (data) { me.onUserClick(data); }, // добавляем обработчик при нажатии на пользователе
                    onTaskClick: function (data) { me.onTaskClick(data); }
                }, function (mapAPI) {
                    // карта создана и готова к использованию

                    // теперь нужно подписаться на событие action у списка пользователей
                    var list = sender.getUserListComponent();
                    if (list) {
                        list.on('action', me.onAction, sender);
                        var selectCardView = list.getSelectCardView();
                        if (selectCardView)
                            list.fireEvent('select', list, selectCardView);
                    }
                    sender.setIsReady();
                    mask.destroy();
                }));
            }
        }
    },

    /*
     * изменение данные в панели фильтрации
     * @param sender {any} панель фильтрации
     * @param values {any} текущие значения в фильтре
     * @param newData {any} новые данные
     */
    onFilterChange: function (sender, values, newData) {
        var controller = this.getController();
        var list = this.getUserListComponent();
        if (list) {
            var selectCardView = list.getSelectCardView();

            var mainView = this.up('app-main');
            var right = mainView.lookupReference('rightpanel');
            // обновляем данные в панели фильтрации
            if (right) {
                if (selectCardView){
                    right.showPanel(list, selectCardView);
                    var user = selectCardView.getModel();
                    var uuid = user.get('uuid');

                    var mapAPI = this.getMapAPI();
                    if (mapAPI) {
                        mapAPI.removeObject('route', uuid);
                        mapAPI.removeObject('tasks', uuid);

                        this.mask('Обработка элементов на карте');
                        var me = this;
                        _.series(
                            [
                                function (callback) {
                                    if (values.geo_show == true || values.geo_show == 'on') {
                                        var txt = 'Добавление<br />маршрута';
                                        me.mask(txt + '...');
                                        controller.addRoutes(me, user, Ext.Date.parse(values.task_date, 'd.m.Y'), function () {
                                            callback();
                                        }, function (percent) {
                                            me.mask(txt + ' ' + percent.toFixed(0) + '%');
                                        });
                                    } else {
                                        callback();
                                    }
                                },
                                function (callback) {
                                    var txt = 'Добавление<br />задания';
                                    me.mask(txt + '...');
                                    controller.addTasks(me, user, Ext.Date.parse(values.task_date, 'd.m.Y'), function () {
                                        callback();
                                    }, function (percent) {
                                        me.mask(txt + ' ' + percent.toFixed(0) + '%');
                                    });
                                },
                                function (callback) {
                                    me.unmask();
                                    me.fireEvent(me.EVENT_MAP_ITEMS_READY, me, user, newData);
                                    callback();
                                }
                            ]);
                    }
                }
            }



            // нужно обновить маршрут

            // нужно обновить гео
        }
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;
        var me = this;

        // нужно очистить данные с карты
        var mapAPI = sender.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();

        sender.mask('Обработка элементов на карте');
        _.series(
            [
                // нужно чтобы иконки в списке обновились (обработались)
                function (callback) {
                    sender.mask('Обработка<br />обходчиков...');
                    me.processingPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    sender.unmask();
                    sender.fireEvent(sender.EVENT_MAP_ITEMS_READY, sender, null, null);
                    callback();
                }
            ]);
    },

    /*
     * обработчик выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserSelect: function (view, cardView) {
        var mainView = this.up('app-main');
        var right = mainView.lookupReference('rightpanel');
        var controller = this.getController();
        if (right) {
            right.showPanel(view, cardView);
            var panel = right.getViewPanel();
            if (!panel.hasListeners['change'])
                panel.on('change', controller.onFilterChange, this);
        }

        // тут еще нужно вывести данные на карту
        var users = [cardView.getModel()];
        var me = this;

        // нужно очистить данные с карты
        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();

        this.mask('Обработка элементов на карте');
        _.series(
            [
                function (callback) {
                    var txt = 'Добавление<br />маршрута';
                    me.mask(txt + '...');
                    controller.addRoutes(me, users[0], new Date(), function () {
                        callback();
                    }, function (percent) {
                        me.mask(txt + ' ' + percent.toFixed(0) + '%');
                    });
                },
                function (callback) {
                    var txt = 'Добавление<br />задания';
                    me.mask(txt + '...');
                    controller.addTasks(me, users[0], new Date(), function () {
                        callback();
                    }, function (percent) {
                        me.mask(txt + ' ' + percent.toFixed(0) + '%');
                    });
                },
                function (callback) {
                    me.unmask();
                    me.fireEvent(me.EVENT_MAP_ITEMS_READY, me, users[0], null);
                    callback();
                }
            ]);
    },

    /**
     * отмена выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserUnSelect: function (view, cardView) {
        var mainView = this.up('app-main');
        var right = mainView.lookupReference('rightpanel');
        if (right)
            right.hidePanel(view, cardView);

        // тут еще нужно вывести очистить данные с карты

        var mapAPI = this.getMapAPI();
        if (mapAPI)
            mapAPI.removeAll();
    },

    privates: {

        /*
         * обработчик при готовности карты и элементов
         * 
         * @param user {any} обходчик
         * @param newData {any} информация о фильтрах
         */
        onMapItemsReady: function (sender, user, newData) {
            var mapAPI = sender.getMapAPI();

            var array = [];
            var r = 'route';
            var t = 'tasks';
            if (newData) {
                if (newData.task_date) {
                    array.push(t);
                    array.push(r);
                }
            } else {
                array.push(r);
                array.push(t);
            }
            if (mapAPI && user) {
                for (var i = 0; i < array.length; i++) {
                    var item = mapAPI.focusObject(array[i], user.get('uuid'));
                    if (item) {
                        break;
                    }

                    if (i == array.length - 1) {
                        Ext.Msg.show({
                            title: 'Сообщение',
                            message: 'Данные не найдены. Установите другой фильтр',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.INFO
                        });
                    }
                }
            }
        },

        /*
         * обработчик действия с карточкой пользователя
         * @param list {any} представление со списком пользователей
         * @param action {string} действие
         * @param record {any} запись пользователя
         * @param card {any} карточка пользователя - представление
         */
        onAction: function (list, action, record, card) {
            var mapAPI = this.getMapAPI();
            if (mapAPI) {
                if (action == 'user') {
                    var url = 'home/user/' + record.get('uuid');
                    Ext.getCurrentApp().redirectTo(url);
                } else {
                    // здесь нужно сделать ожидение
                    var result = mapAPI.focusObject(action, record.get('uuid'));
                    if (!result) {
                        var msg = '';
                        switch (action) {
                            case 'route':
                                msg = 'Маршрут обходчика не найден';
                                break;

                            case 'tasks':
                                msg = 'Задания обходчика не найдены';
                                break;
                        }
                        if (msg) {
                            Ext.Msg.show({
                                title: 'Сообщение',
                                message: msg,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.INFO
                            });
                        }
                    }
                }
            }
        },

        /*
         * обработчик нажатия на пользователе
         * @param data {any} данные о пользователе (результат который возвращает метод DefaultDataProvider.getUsersPosition)
         */
        onUserClick: function (data) {
            // нужно найти панель с пользователями
            var list = this.getView().getUserListComponent();
            if (list) {
                var uuid = data.uuid;
                if (uuid) {
                    var record = list.map_uuid[uuid];
                    if (record) {
                        var card = list.map[record.getId()];
                        if (card) {
                            card.setMode('maximize');

                            var scroll = list.getScrollable();
                            scroll.scrollIntoView(card.el.dom);
                        }
                    }
                }
            }
        },

        /*
         * обработчик нажатия на переход документа
         * @param url {string} ссылка документа
         */
        onTaskClick: function (url) {
            this.redirectTo(url);
        },

        /*
         * Обработка пользователей
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        processingPlacemarks: function (sender, users, callback) {
            var view = this.getView();
            if (view) {
                var list = view.getUserListComponent();
                if (list) {
                    users.forEach(function (i) {
                        var record = list.map_uuid[i.get('uuid')];
                        if (record)
                            list.setAccess(record.getId(), true);
                    });
                }

                if (typeof callback == 'function')
                    callback();
            }
        },

        /*
         * добавление маршрута на карту
         * @param sender {any} текущее представление
         * @param user {any} пользователь
         * @param date {Date} дата
         * @param callback {()=>void} функция обратного вызова
         */
        addRoutes: function (sender, user, date, callback) {
            var me = this;
            // добавление маршрута следования
            var uuid = user.get('uuid');
            if (uuid) {
                this.getRoutes(uuid, date, function (results) {
                    var mapAPI = sender.getMapAPI();
                    if (mapAPI) {
                        var color = user.get('C_Icon_Color');
                        if (uuid && color) {
                            var array = [];
                            Ext.each(results, function (i) {
                                array.push(i.getData());
                            });
                            if (array && array.length > 0) {
                                mapAPI.addRoute(uuid, array, Ext.getGlobalParams(me.defaultColorFieldName) || color, user.get('C_Fio'), true);
                            }
                        }

                        if (typeof callback == 'function')
                            callback();
                    }
                });
            }
        },

        /*
         * добавление заданий на карту
         * @param sender {any} текущее представление
         * @param user {any} пользователь
         * @param date {Date} дата
         * @param callback {()=>void} функция обратного вызова
         */
        addTasks: function (sender, user, date, callback) {
            var me = this;
            var uuid = user.get('uuid');
            if (uuid) {
                this.getTasks(uuid, date, function (tasks) {
                    var dataWorker = sender.getYandexDataWorker();

                    if (tasks.length > 0) {
                        // тут нужно прочитать все записи и определить есть ли у них адреса
                        // затем нужно добавить им координаты
                        var geoData = {};
                        for (var i = 0; i < tasks.length; i++) {
                            var point = tasks[i];
                            var address = point.get('Address');
                            if (address) {
                                if (point.isCoordExist() != true) {
                                    var hash = md5(address);
                                    point.set('hash', hash);

                                    geoData[hash] = address;
                                }

                            } else {
                                // здесь нужно что-то делать с точка у которых нет адреса
                                new Exception(new Error('Адрес не указан'));
                            }
                        }

                        var count = 0;
                        for (var i in geoData)
                            count++;
                        if (sender.showProgressBar) {
                            var isShowProgressBar = false;
                            var timeout = setTimeout(function () {
                                isShowProgressBar = true;
                            }, 2000);
                            var msg = 'Обработка геоданных. Подождите';

                            dataWorker.onTask({ type: 'geocoder', data: geoData }, function (results, options) {
                                if (results == null) {
                                    var percent = ((options * 100) / count).toFixed(0);
                                    if (isShowProgressBar == true)
                                        sender.showProgressBar(msg + ' ' + percent + '%', percent);
                                    // значит это только итерация
                                    return;
                                } else {
                                    sender.showProgressBar();
                                    clearTimeout(timeout);
                                }
                                // нужно сгруппировать точки по координатам
                                var groups = {};
                                for (var i = 0; i < tasks.length; i++) {
                                    var point = tasks[i];
                                    var hash = point.get('hash');

                                    if (results[hash])
                                        point.setCoord(results[hash]);

                                    var coordHash = point.getCoordHash();
                                    // добавляем индексирование
                                    var uuid = [point.get('PhoneIMEI')];
                                    if (!groups[uuid]) {
                                        groups[uuid] = {
                                            /*
                                             * возвращается общее количество заданий
                                             * @param coordHash {string} хэш-сумма координат. не обязателен
                                             */
                                            getTotalCount: function (coordHash) {
                                                var count = 0;

                                                for (var i in this) {
                                                    if (typeof this[i] != 'function') {
                                                        if (coordHash) {
                                                            if (coordHash == i) {
                                                                count += this[i].length;
                                                                break;
                                                            }
                                                            continue;
                                                        }

                                                        count += this[i].length;
                                                    }
                                                }
                                                return count;
                                            },

                                            /**
                                             * возвращается количество элементов которые были выполнены
                                             * @param coordHash {string} хэш-сумма координат. не обязателен
                                             */
                                            getDoneCount: function (coordHash) {
                                                var idx = 0;
                                                for (var i in this) {

                                                    if (typeof this[i] != 'function') {

                                                        if (coordHash) {
                                                            if (coordHash == i) {
                                                                for (var j = 0; j < this[i].length; j++) {
                                                                    var item = this[i][j];
                                                                    if (item.IsDone == true)
                                                                        idx++;
                                                                }

                                                                break;
                                                            }
                                                            continue;
                                                        }

                                                        for (var j = 0; j < this[i].length; j++) {
                                                            var item = this[i][j];
                                                            if (item.IsDone == true)
                                                                idx++;
                                                        }
                                                    }
                                                }

                                                return idx;
                                            }
                                        };
                                    }

                                    if (!groups[uuid][coordHash])
                                        groups[uuid][coordHash] = [];

                                    groups[uuid][coordHash].push(point.getData());
                                }

                                // тут обработали все координаты
                                var mapAPI = sender.getMapAPI();
                                if (mapAPI) {
                                    var color = user.get('C_Icon_Color');
                                    if (uuid && color) {
                                        if (groups[uuid]) {
                                            mapAPI.addTask(uuid, groups[uuid], Ext.getGlobalParams(me.defaultColorFieldName) || color);
                                        }
                                    }
                                    if (typeof callback == 'function')
                                        callback();
                                }
                            });
                        }
                    } else {
                        if (typeof callback == 'function')
                            callback();
                    }
                });
            }
        },

        /**
         * возвращаются задания на карту для указанного пользователя
         * @param uuid {string} идентификатор устройства
         * @param date {Date} дата
         * @param callback {(ARM.model.Task[])=>void} функция обратного вызова
         */
        getTasks: function (uuid, date, callback) {
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            dataProvider.getUserTasks(uuid, date, function (items) {
                if (typeof callback == 'function')
                    callback(items);
            });

        },

        /**
         * возвращаются маршруты на карту для указанного пользователя
         * @param uuid {string} идентификатор устройства
         * @param date {Date} дата
         * @param callback {(any[])=>void} функция обратного вызова
         */
        getRoutes: function (uuid, date, callback) {
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            dataProvider.getUserRoutes(uuid, date, function (items) {
                if (typeof callback == 'function')
                    callback(items);
            });
        }
    }
});
