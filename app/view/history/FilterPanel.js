﻿/*
 * правая панель фильтрации
 */
Ext.define('ARM.view.history.FilterPanel', {
    extend: 'ARM.shared.BaseRightPanel',

    requires: [
        'ARM.view.home.StatCard'
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeRightFilterWidth: getResponsiveWidth(),
        smallRightFilterWidth: '!largeRightFilterWidth'
    },

    layout: {
        type: 'vbox',
        pack: 'start'
    },
    height: '100%',
    width: '100%',

    defaultListenerScope: true,

    /*
     * событие изменения фильтра
     */
    EVENT_FILTER_CHANGE: 'change',

    /*
     * предыдущие данные
     */
    _lastValues: {
        task_date: Ext.Date.format(new Date(), 'd.m.Y'),
        geo_show: true
    },

    viewModel: {
        data: {

        }
    },

    constructor: function (cfg) {
        var self = this;
        cfg.items = [{
            bodyPadding: '20px 20px 0 20px',
            xtype: 'form',
            layout: {
                type: 'vbox',
                pack: 'start'
            },
            defaults: {
                anchor: '100%',
                padding: '0 0 15px 0',
                listConfig: { // только для выпадающих списков
                    minWidth: 400
                },
                plugins: 'responsive',
                responsiveConfig: {
                    'largeRightFilterWidth': {
                        labelWidth: 150
                    },
                    'smallRightFilterWidth': {
                        labelWidth: 90
                    }
                }
                //labelWidth: 100
            },
            items: [
                {
                    xtype: 'checkboxfield',
                    name: 'geo_show',
                    fieldLabel: 'Показать координаты',
                    itemId: 'geo_show',
                    checked: true,
                    labelStyle: 'color:white',
                    listeners: {
                        change: 'onGeoShowChange',
                        render: 'onGeoShowRender',
                        destroy: 'onGeoShowDestroy'
                    }
                },
                {
                    xtype: 'datefield',
                    anchor: '100%',
                    fieldLabel: 'Задания за',
                    name: 'task_date',
                    itemId: 'task_date',
                    maxValue: new Date(),
                    value: new Date(),
                    format: 'd.m.Y',
                    labelStyle: 'color:white;padding-top:9px',
                    tooltip: 'Вывод заданий за указанную дату',
                    listeners: {
                        change: 'onTaskDateChange'
                    },
                    flex: 1
                }
            ]
        },
        {

            width: '100%',
            flex: 1,
            layout: {
                type: 'vbox',
                pack:'start'
            },
            defaults: {
                width: '100%'
            },
            margin: '20px 0 0 0',
            items: [
                {
                    xtype: 'app-home-statcard',
                    title: 'Статистика по допуску ПУ в эксплуатацию',
                    itemId: 'DT_METER_JOB',
                    hidden: true,
                    bind: {
                        hidden: '{DT_METER_JOB}'
                    }
                },
                {
                    xtype: 'app-home-statcard',
                    title: 'Статистика по контрольным обходам',
                    itemId: 'DT_CONTROL_VISITS',
                    hidden: true,
                    bind: {
                        hidden: '{DT_CONTROL_VISITS}'
                    }
                }
            ]
        }];

        this.callParent(arguments);
    },

    /**
     * обновление содержимой панели
     * @param options {any} дополнительные опции
     * @param callback {()=>void} функция обраного вызова
     */
    refreshPanel: function (options, callback) {
        // чтобы скрывать лишнии документы
        this.showDocuments();

        // тут можно достать информацию по пользователю
        options = options || this.getOptions();
        var me = this;
        var mask = new Ext.LoadMask({
            target: this,
            msg: 'Загрузка...'
        });

        var values = this.down('form').getValues();

        Ext.defer(function () {
            mask.show();
            var dataProvider = Ext.getCurrentApp().getDataProvider();
            var link = options.cardView.getModel().get('LINK');

            _.series([
                function (callback) {
                    dataProvider.getStat(link, Ext.Date.parse(values.task_date, "d.m.Y"), function (statdata) {

                        statdata.forEach(function (i) {
                            var view = me.down('#' + i.get('C_Const'));
                            if (view) {
                                var store = Ext.create('ARM.store.StatCard');
                                store.loadData([
                                    {
                                        name: 'today',
                                        title: 'Выполнено за сегодня',
                                        person: i.get('N_Done_Today_PE'),
                                        noperson: i.get('N_Done_Today_EE')
                                    },
                                    {
                                        name: 'done',
                                        title: 'Выполнено всего',
                                        person: i.get('N_Done_PE'),
                                        noperson: i.get('N_Done_EE')
                                    },
                                    {
                                        name: 'old',
                                        title: 'осталось',
                                        person: i.get('N_NotDone_PE'),
                                        noperson: i.get('N_NotDone_EE')
                                    },
                                    {
                                        name: 'total',
                                        title: 'Общее количество',
                                        person: i.get('N_Total_PE'),
                                        noperson: i.get('N_Total_EE')
                                    }
                                ]);
                                view.setStore(store);
                            }
                        });

                        if (typeof callback == 'function')
                            callback();
                    });
                },
                function (callback) {
                    dataProvider.getGeoDate(link, function (dates) {
                        var values = {};
                        dates.forEach(function (i) {
                            values[i.get('Date').getTime()] = i.get('Date');
                        });
                        //me.down('#geo_date').getPicker().setMarkers(values);
                        if (typeof callback == 'function')
                            callback();

                    });
                },
                function (callback) {
                    dataProvider.getTaskDate(link, function (dates) {
                        var values = {};
                        dates.forEach(function (i) {
                            values[i.get('Date').getTime()] = i.get('Date');
                        });
                        me.down('#task_date').getPicker().setMarkers(values);
                        if (typeof callback == 'function')
                            callback();

                    });
                }
            ], function () {
                mask.destroy();
                if (typeof callback == 'function')
                    callback();
            });

            

        }, 50);
    },

    privates: {
        /*
         * выводить документы
         */
        showDocuments: function () {
            // чтобы скрывать лишнии документы
            var vm = this.getViewModel();
            Ext.getCurrentApp().getDataProvider().getDocumentsType(function (types) {
                if (vm) {
                    types.forEach(function (type) {
                        vm.set(type.get('C_Const'), false);
                    });
                }
            });
        },
        /*
         * изменения даты заданий
         */
        onTaskDateChange: function (sender, newValue) {
            this.onFilterChange(sender, newValue);
        },

        onGeoShowChange: function (sender, newValue) {
            this.onFilterChange(sender, newValue);
        },

        /*
         * обработчик изменения фильтра
         */
        onFilterChange: function (sender, newValue) {
            var values = this.down('form').getValues();
            if (sender.getName() == 'geo_show')
                values['geo_show'] = newValue;
            var newData = {};
            if (this._lastValues) {
                for (var i in this._lastValues) {
                    if (this._lastValues[i] != values[i])
                        newData[i] = values[i];
                }
            }

            this.fireEvent(this.EVENT_FILTER_CHANGE, this, values, newData);

            this._lastValues = values;
        },

        /*
         * обработчик для вывода маршрута
         */
        onGeoShowRender: function (sender) {
            var tip = Ext.create('Ext.tip.ToolTip', {
                target: sender,
                html: 'Показать координаты'
            });
            sender.tooltip = tip;
        },

        onGeoShowDestroy: function () {
            if (this.tooltip)
                this.tooltip.destroy();
        }
    }
});