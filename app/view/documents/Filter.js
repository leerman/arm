﻿/*
 * панель фильтрации
 */
Ext.define('ARM.view.documents.Filter', {
    extend: 'Ext.Panel',
    xtype: 'app-documents-filter',
    ui: 'white',
    bodyPadding: '20px 20px 0 20px',
    defaultListenerScope: true,
    /*
     * событие изменения фильтра
     */
    EVENT_FILTER_CHANGE: 'change',

    items: [
        {
            xtype: 'form',
            ui: 'white',
            width: '100%',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Дата',
                    labelWidth: 100,
                    layout: 'hbox',
                    labelStyle: 'color:#808080;padding-top:9px',
                    items: [
                        {
                            xtype: 'datefield',
                            emptyText: 'Период с',
                            name: 'dateBeginStart',
                            format: 'd.m.Y',
                            ui: 'white',
                            maxValue: new Date(),
                            listeners: {
                                change: 'onFilterChange'
                            },
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    handler: 'onReset',
                                    hidden: true
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            width: 10
                        },
                        {
                            xtype: 'datefield',
                            emptyText: 'по',
                            name: 'dateBeginFinish',
                            format: 'd.m.Y',
                            ui: 'white',
                            listeners: {
                                change: 'onFilterChange'
                            },
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    handler: 'onReset',
                                    hidden: true
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    width: 10
                },
                {
                    flex: 1,
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Срок',
                    labelWidth: 100,
                    layout: 'hbox',
                    labelStyle: 'color:#808080;padding-top:9px',
                    items: [
                        {
                            xtype: 'datefield',
                            emptyText: 'Период с',
                            name: 'dateEndStart',
                            format: 'd.m.Y',
                            ui: 'white',
                            listeners: {
                                change: 'onFilterChange'
                            },
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    handler: 'onReset',
                                    hidden: true
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            width: 10
                        },
                        {
                            xtype: 'datefield',
                            emptyText: 'по',
                            name: 'dateEndFinish',
                            format: 'd.m.Y',
                            ui: 'white',
                            listeners: {
                                change: 'onFilterChange'
                            },
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    handler: 'onReset',
                                    hidden: true
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ],

    /*
     * значения с формы
     */
    getValues: function () {
        return this.getForm().getValues();
    },

    privates: {
        /*
         * возвращается форма 
         */
        getForm: function () {
            return this.down('form');
        },

        /*
         * обработчик изменения фильтра
         */
        onFilterChange: function (sender, newValue) {

            var clear = sender.getTrigger('clear');
            if (clear)
                clear.show();

            this.onCallChangeEvent();
        },

        /**
         * очистка значения в поле
         */
        onReset: function (field, sender, eOpts) {
            field.suspendEvent('change');
            field.reset();
            sender.hide();
            field.resumeEvent('change');

            this.onCallChangeEvent();
        },

        /*
         * вызов события изменения
         */
        onCallChangeEvent: function () {
            var values = this.getValues();

            for (var i in values) {
                values[i] = Ext.Date.parse(values[i], 'd.m.Y');
            }

            this.fireEvent(this.EVENT_FILTER_CHANGE, this, values);
        }
    }
});