﻿/*
 * 
 */
Ext.define('ARM.view.documents.Table', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-documents-table',
    cls: 'app-task-items white-scroll',
    loadMask: true,
    requires: [
        'Core.DefaultFilters',
        'Ext.selection.CellModel'
    ],
    defaultListenerScope: true,
    /**
     * переменная для хранения параметров запроса по умолчанию
     */
    defaultParams: null,
    plugins: [
        {
            ptype: 'defaultfilters'
        }, {
            ptype: 'rowediting',
            clicksToEdit: 2,
            listeners: {
                edit: 'onEdit',
                beforeedit: 'onBeforeEdit'
            },
            cancelBtnText: 'Отмена',
            saveBtnText: 'Применить'
        },
        {
            ptype: 'gridfilters'
        }
    ],
    selModel: {
        type: 'rowmodel'
    },

    /*
     * было выполнено изменение
     */
    is_edit_change: false,

    columns: {
        defaults: {
            draggable: false,
            align: 'center',
            filter: true,
            flex: 1,
        },
        items: [
            {
                xtype: 'rownumberer',
                width: 40,
                align: 'center',
                text: '№',
                flex:0
            },
            {
                dataIndex: 'F_Statuses',
                text: 'Статус',
                renderer: function (value, cell, record) {
                    return value.C_Name;
                }            
            },
            {
                dataIndex: 'F_Types',
                text: 'Тип маршрута',
                renderer: function (value, cell, record) {
                    return value.C_Name;
                }
            },
            {
                dataIndex: 'D_Date',
                text: 'Дата формирования',
                renderer: function (value, cell, record) {
                    return Ext.Date.format(value, 'd.m.Y');
                }
            },
            {
                dataIndex: 'D_Date_End',
                text: 'Срок действия',
                renderer: function (value, cell, record) {
                    return Ext.Date.format(value, 'd.m.Y');
                },
                editor: {
                    xtype: 'datefield',
                    ui: 'white',
                    format: 'd.m.Y'
                }
            },
            {
                dataIndex: 'C_Number',
                text: 'Номер маршрута',
                renderer: function (value, cell, record) {
                    return value;
                }
            },
            {
                dataIndex: 'F_Users',
                text: 'Исполнитель',
                renderer: function (value, cell, record) {
                    var store = Ext.data.StoreManager.lookup('users-list');
                    if (store && typeof value == 'string') {
                        var record = store.getById(value);
                        if (record) {
                            return record.get('C_Fio');
                        }
                        else {
                            return value;
                        }
                    }
                    return value ? value.C_Fio : '';
                },
                editor: {
                    xtype: 'combo',
                    typeAhead: true,
                    ui: 'white',
                    pageSize: 25,
                    displayField: 'C_Fio',
                    valueField: 'LINK',
                    msgTarget: 'none',
                    store: Ext.create('ARM.store.GetWalkersQuery', {
                        storeId: 'users-list'
                    }),
                    listConfig: { // только для выпадающих списков
                        minWidth: 450
                    },
                    //Филиал: ' + obj.C_MainDivision + '<br />Отделение: ' + obj.C_Division + '<br />Участок: ' + obj.C_SubDivision + '<br />IMEI: ' + obj.uuid
                    tpl: Ext.create('Ext.XTemplate',
                        '<ul class="x-list-plain"><tpl for=".">',
                            '<li role="option" class="x-boundlist-item"><center><b>{C_Fio}</b></center><br /><b>Филиал:</b> {C_MainDivision}<br /><b>Отделение:</b> {C_Division}<br /><b>Участок:</b> {C_SubDivision}<br /><b>Серийный номер:</b> {uuid}</li>',
                        '</tpl></ul>'
                    ),
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            handler: 'onUserReset'
                        }
                    },
                    listeners: {
                        /*render: function (sender) {
                            debugger;
                            var record = sender.ownerCt.context.record;
                            if (record) {
                                sender.setValue(record.get('F_Users'));
                            }
                        },*/
                        change: 'onUserChange'
                    }
                }
            },
            {
                dataIndex: 'DetailsCount',
                text: 'Количество заданий'
            }
        ]
    },

    bbar: {
        xtype: 'pagingtoolbar',
        ui: 'white-paging',
        displayInfo: true,
        displayMsg: 'Отображается элементы {0} - {1} из {2}',
        emptyMsg: "Информация отсуствует",
    },

    initComponent: function () {
        var store = Ext.create('ARM.store.Doc');
        store.proxy.extraParams['select'] = store.getSelectFields();
        this.store = store;
        Ext.data.StoreManager.lookup('users-list').load();
        this.callParent(arguments);
    },

    listeners: {
        itemcontextmenu: 'onItemContextMenu'
    },

    /*
    * установить значение параметров по умолчанию
    * @param value {any} значение
    */
    setDefaultParams: function (value) {
        this.defaultParams = value;
    },
    /*
     * возвращаются значение параметров по умолчанию
     */
    getDefaultParams: function () {
        return this.defaultParams;
    },

    /*
     * очистка пользователя
     */
    onUserReset: function (field, sender, eOpts) {
        this.is_edit_change = true;
        field.setValue(null);
    },

    /**
     * обработчик завершения редактирования поля
     */
    onEdit: function (editor, context, eOpts) {
        var me = this;
        var app = Ext.getCurrentApp();
        var socket = app.getSocket();
        var dataProvider = app.getDataProvider();
        var record = context.record;
        // тут нужен механизм уведомлений
        var currentUser = record.get('F_Users');
        var lastUser = record.modified.F_Users;
        var regex = RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
        
        if (currentUser != null && !regex.test(currentUser)) {
            Ext.Msg.alert('Ошибка', 'Выбрано неверное имя пользователя');
            context.grid.is_edit_change = false;
        }
        else if (!record.get('D_Date_End')) {
            Ext.Msg.alert('Ошибка', 'Не указан срок действия');
            context.grid.is_edit_change = false;
        }

        if (context.grid.is_edit_change == true) {
            context.grid.mask('Обновление...');
            /*
            * сохраняем результат
            */
            Ext.Msg.confirm('Вопрос', 'Обновить запись?', function (btn) {
                if (btn == 'yes') {



                    record.save({
                        callback: function (result, operation, success) {
                            // уведомление обходчика
                            if (currentUser && lastUser) {
                                var currentLINK = currentUser.LINK ? currentUser.LINK : currentUser;
                                var lastLINK = lastUser.LINK ? lastUser.LINK : lastUser;
                                if (currentLINK != lastLINK) {
                                    // здесь дважды вызывается одна и таже функция
                                    dataProvider.getUserInfo(currentLINK, function (currentUserRecord) {
                                        if (currentUserRecord) {
                                            var number = null;
                                            var data = null;
                                            if (record) {
                                                number = record.get('C_Number');
                                                data = record.getData();
                                            }
                                            socket.notifUser(currentUserRecord.get('uuid'), 'add_documents', 'Вам назначен новый документ' + (number ? ' под номером ' + number : ''), (data ? JSON.stringify(data) : {}), function () {

                                                me.onTooltip('Документ ' + (number ? ' под номером ' + number : '') + ' доставлен ' + currentUserRecord.get('C_Fio'));
                                            });
                                        }
                                    });

                                    dataProvider.getUserInfo(lastLINK, function (lastUserRecord) {
                                        if (lastUserRecord) {
                                            var number = null;
                                            var data = null;
                                            if (record) {
                                                number = record.get('C_Number');
                                                data = record.getData();
                                            }
                                            socket.notifUser(lastUserRecord.get('uuid'), 'remove_documents', 'Документ' + (number ? ' под номером ' + number : '') + ' был передан другому пользователю ', (data ? JSON.stringify(data) : {}), function () {
                                                me.onTooltip('Документ ' + (number ? ' под номером ' + number : '') + ' передан ' + lastUserRecord.get('C_Fio'));
                                            });
                                        }
                                    });
                                }
                            } else {
                                if (currentUser) {
                                    var currentLINK = currentUser.LINK ? currentUser.LINK : currentUser;
                                    dataProvider.getUserInfo(currentLINK, function (currentUserRecord) {
                                        if (currentUserRecord) {
                                            var number = null;
                                            var data = null;
                                            if (record) {
                                                number = record.get('C_Number');
                                                data = record.getData();
                                            }
                                            socket.notifUser(currentUserRecord.get('uuid'), 'add_documents', 'Вам назначен новый документ' + (number ? ' под номером ' + number : ''), (data ? JSON.stringify(data) : {}), function () {
                                                me.onTooltip('Документ ' + (number ? ' под номером ' + number : '') + ' передан ' + currentUserRecord.get('C_Fio'));
                                            });
                                        }
                                    });
                                    // назначили задание пользователю
                                }

                                if (lastUser) {
                                    // забрали задание у пользователя
                                    var lastLINK = lastUser.LINK ? lastUser.LINK : lastUser;
                                    dataProvider.getUserInfo(lastLINK, function (lastUserRecord) {
                                        if (lastUserRecord) {
                                            var number = null;
                                            var data = null;
                                            if (record) {
                                                number = record.get('C_Number');
                                                data = record.getData();
                                            }
                                            socket.notifUser(lastUserRecord.get('uuid'), 'remove_documents', 'Документ' + (number ? ' под номером ' + number : '') + ' был отменен', (data ? JSON.stringify(data) : {}), function () {
                                                me.onTooltip('Документ ' + (number ? ' под номером ' + number : '') + ' отвязан от ' + lastUserRecord.get('C_Fio'));
                                            });
                                        }
                                    });
                                }
                            }

                            if (context.grid.rendered == true)
                                context.grid.unmask();
                        }
                    });
                }
                else {
                    context.grid.getStore().rejectChanges();
                    if (context.grid.rendered == true)
                        context.grid.unmask();
                }
            });
        } else {
            context.grid.getStore().rejectChanges();
        }
    },

    /*
     * обработчик перед изменением данных
     */
    onBeforeEdit: function (editor, context, eOpts) {
        context.grid.is_edit_change = false;
        var record = context.record;
        var validLINK = [3, 1, 4]; // 1 - в работе, 3 - назначено, 4 - не назначено
        var status = record.get('F_Statuses');
        if (status) {
            var id = status.LINK;
            if (id) {
                // чтобы сделать проверку на редактирование строки
                return validLINK.filter(function (i) { return id == i; }).length > 0;
            }
        }
        return false;
    },

    /*
     * обработчик изменения пользователя
     */
    onUserChange: function (sender, newValue) {
        this.is_edit_change = true;
    },

    /*
     * обработчик уведомлений
     * @param txt {string} текст сообщения
     */
    onTooltip: function (txt) {
        var app = Ext.getCurrentApp();
        app.showTooltip('Уведомление', txt);
    },

    /**
     * при нажати правой кнопкой выводить контекстное меню с предложением удалить
     */
    onItemContextMenu: function (sender, record, item, index, e) {
        var configs = Ext.getCurrentApp().getConfigs();
        if (configs && configs.get('B_System_Edit')) {
            var me = this;
            var menu = Ext.create('Ext.menu.Menu', {
                width: 200,
                cls: 'menu-without-icons',
                items: [{
                    text: 'Удалить маршрут',
                    handler: function () {
                        me.removeRoute(menu, record, sender, index);
                    }
                }]
            }).showAt(e.getXY());
            e.stopEvent();
        }
    },

    /**
     * удалить маршрут
     */
    removeRoute: function (sender, record, grid, index) {
        var store = grid.getStore();
        store.removeAt(index);
        store.sync();
    }
});