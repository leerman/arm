﻿Ext.define('ARM.view.documents.DocumentsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.documents',


    onRender: function (sender) {

        Ext.getCurrentApp().on('snSignal', this.onSignal, sender);

        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('select', this.onUserSelect, sender);
            list.on('unselect', this.onUserUnSelect, sender);

            list.on('action', this.onAction, sender);
            var selectCardView = list.getSelectCardView();
            if (selectCardView) {
                list.fireEvent('select', list, selectCardView);
                // пользователь был выбран
            } else {
                //// пользователь не был выбран
                //sender.loadGrid();
            }
        }

        // встраиваем свой компонент в правую часть
        var initView = sender.up('app-main');
        if (initView) {
            var rightPanel = initView.lookupReference('rightpanel');
            if (rightPanel) {
                rightPanel.refreshPanel('ARM.view.history.FilterPanel');
            }
        }

        sender.setIsReady();
    },

    /*
     * обработчик сигналов
     */
    onSignal: function (socket, data, tid) {
        if (data.type == 'received') { // уведомления о принятии задания
            try{
                var doc = JSON.parse(data.data);
                if (doc) {
                    PN.Domain.DD_Docs.SetReceived([doc.LINK], function (records, options, success) {
                        // здесь нужно добавить уведомление на то что задание получено пользователем
                    });
                }
            } catch (exc) {

            }
        }
    },

    /*
     * обработчик изменения фильтра
     * @param filter {any} панель фильтрации
     * @param values {any} данные для фильтрации
     */
    onFilterChange: function (filter, values) {
        var view = this.getView();
        if (view) {
            view.loadGrid(this.getFilter(values));
        }
    },

    /*
     * обработчик изменения таба
     * @param tabPanel {any}
     */
    onTabChange: function ( tabPanel, newCard, oldCard, eOpts ) {
        var table = oldCard.down('app-documents-table');
        table.cfg.type = newCard.typeId;
        newCard.insert(0, table);
        var view = this.getView();
        if (view) {
            view.loadGrid(this.getFilter(view.down('app-documents-filter').getValues()));
        }
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;
        var me = this;
        sender.loadGrid(this.getFilter([]));
        _.series(
            [
                // нужно чтобы иконки в списке обновились (обработались)
                function (callback) {
                    sender.mask('Обработка<br />обходчиков...');
                    me.processingPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    sender.unmask();
                    callback();
                }
            ]);
    },

    /*
     * обработчик выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserSelect: function (view, cardView) {
        var model = cardView.getModel();
        if (model) {
            this.loadGrid([
                {
                    property: 'F_Users',
                    value: model.get('LINK'),
                    operator: '='
                }
            ]);
        }
    },

    /**
     * отмена выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserUnSelect: function (view, cardView) {
        this.loadGrid();
    },

    privates: {
        /*
         * возвращается фильтр для построения запроса
         * @param values {any} данные с панели фильтрации
         */
        getFilter: function (values) {
            var filters = [];
            var view = this.getView();
            if (view) {

                // тут нужна обработка фильтра
                var filter = view.up('app-main').down('app-main-filter');
                var filter_values = null;
                if (filter_values = filter.getLastValues()) {
                    var usersGrid = view.down('app-documents-table');
                    if (usersGrid) {
                        var data = {
                            S_MainDivision: filter_values.MainDivision,
                            S_Division: filter_values.Division,
                            S_SubDivision: filter_values.SubDivision,
                            F_Managers: filter_values.Managers,
                            C_Fio: filter_values.User
                        };
                        var filters = [];

                        for (var i in data) {
                            if (data[i]) {

                                if (i == 'C_Fio') {
                                    // нужно проверить, что не гуид
                                    if (/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(data[i])) {
                                        continue;
                                    }
                                }
                                if (i == 'F_Managers') {
                                    filters.push({
                                        property: 'F_Users_Ref.F_Managers',
                                        value: data[i],
                                        operator: '='
                                    });
                                } else {
                                    filters.push({
                                        property: i == 'C_Fio' ? 'F_Users_Ref.C_Fio' : i,
                                        value: data[i],
                                        operator: i == 'C_Fio' ? 'like' : '='
                                    });
                                }
                            }
                        }
                    }
                }


                var list = view.getUserListComponent();
                if (list) {
                    var selectCardView = list.getSelectCardView();
                    if (selectCardView) {

                        var model = selectCardView.getModel();
                        if (model) {
                            filters.push({
                                property: 'F_Users',
                                value: model.get('LINK'),
                                operator: '='
                            });
                        }

                    }
                    if (values) {
                        for (var i in values) {
                            if (values[i]) {
                                switch (i) {
                                    case 'dateBeginStart':
                                        filters.push({
                                            property: 'D_Date',
                                            value: values[i],
                                            operator: '>='
                                        });
                                        break;

                                    case 'dateBeginFinish':
                                        filters.push({
                                            property: 'D_Date',
                                            value: values[i],
                                            operator: '<='
                                        });
                                        break;

                                    case 'dateEndStart':
                                        filters.push({
                                            property: 'D_Date_End',
                                            value: values[i],
                                            operator: '>='
                                        });
                                        break;

                                    case 'dateEndFinish':
                                        filters.push({
                                            property: 'D_Date_End',
                                            value: values[i],
                                            operator: '<='
                                        });
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            return filters;
        },
        /*
         * обработчик действия с карточкой пользователя
         * @param list {any} представление со списком пользователей
         * @param action {string} действие
         * @param record {any} запись пользователя
         * @param card {any} карточка пользователя - представление
         */
        onAction: function (list, action, record, card) {
            var url = 'home/' + action + '/' + record.get('uuid');
            switch (action) {
                case 'user':
                case 'route':
                case 'tasks':
                    Ext.getCurrentApp().redirectTo(url);
                    break;
            }
        },

        /*
         * Обработка пользователей
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        processingPlacemarks: function (sender, users, callback) {
            var view = this.getView();
            if (view) {
                var list = view.getUserListComponent();
                if (list) {
                    users.forEach(function (i) {
                        var record = list.map_uuid[i.get('uuid')];
                        if (record)
                            list.setAccess(record.getId(), true);
                    });
                }

                if (typeof callback == 'function')
                    callback();
            }
        }
    }
});
