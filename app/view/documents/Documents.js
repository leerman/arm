﻿/*
 * документы
 */
Ext.define('ARM.view.documents.Documents', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-documents',
    requires: [
        'ARM.view.documents.DocumentsController',
        'ARM.view.documents.DocumentsModel',

        'Ext.toolbar.Paging',
        'ARM.view.documents.Filter',
        'ARM.view.documents.Table',
        'Ext.tab.Panel'
    ],

    controller: 'documents',
    viewModel: 'documents',

    layout: {
        type: 'vbox',
        pack: 'start'
    },
    height: '100%',
    width: '100%',
    defaults: {
        width: '100%'
    },
    ui: 'white',
    items: [
        {
            xtype: 'app-documents-filter',
            listeners: {
                change: 'onFilterChange'
            },
            hidden: true // специально убрали
        },
        {
            padding: '20px 20px 0 20px',
            flex: 1,
            ui: 'tab-white',
            xtype: 'tabpanel',
            defaults: {
                xtype: 'panel',
                layout: 'fit',
                ui: 'white'
            },
            items: [
                {
                    typeId: 5,
                    title: 'Все',
                    itemId: 'ALL', // 5
                    items: [
                        {
                            xtype: 'app-documents-table',
                            cfg: {
                                type: 5
                            }
                        }
                    ]
                },
                {
                    title: 'Назначено',
                    typeId: 3
                },
                {
                    typeId: 1,
                    title: 'В работе',
                    itemId: 'DS_IN_WORK' //1
                },
                {
                    typeId: 2,
                    title: 'Выполнено',
                    itemId: 'DS_DONE' // 2
                },
                {
                    typeId: 4,
                    title: 'Просрочено',
                    itemId: 'DS_EXPIRED' // 4
                }
            ],
            listeners: {
                tabchange: 'onTabChange'
            }
        }
    ],

    listeners: {
        render: 'onRender',
        setuserstore: 'onSetUserStore'
    },

    /*
     * загрузка данных в грид
     * @param filter {any[]} параметры фильтрации
     */
    loadGrid: function (filter) {
        var dataProvider = Ext.getCurrentApp().getDataProvider();
        var grid = this.down('app-documents-table');
        if (grid) {
            var type = grid.cfg.type;

            var store = grid.getStore();
            var filters = [];

            if (filter) {
                filter.forEach(function (i) {
                    filters.push(i);
                });
            }

            if (type <= 3) { // это типы из БД 
                filters.push({
                    property: 'F_Statuses',
                    value: type,
                    operator: '='
                });
                if (type == 3 && type == 1) {
                    var result = null;
                    if ((result = filters.filter(function (i) { return i.property == 'D_Date_End' && i.operator == '>=' })).length > 0) {
                        result[0].value = dataProvider.getToday(new Date());
                        result[0].operator = '>';
                    } else {
                        filters.push({
                            property: 'D_Date_End',
                            value: dataProvider.getToday(new Date()),
                            operator: '>'
                        });
                    }
                }
            } else { // просрочено и все
                switch (type) {
                    case 4:
                        if (filters.filter(function (i) { return i.property == 'D_Date_End' && i.operator == '<=' }).length == 0) {
                            filters.push({
                                property: 'D_Date_End',
                                value: dataProvider.getToday(new Date()),
                                operator: '<'
                            });
                        }
                        filters.push({
                            property: 'F_Statuses',
                            value: 2, // Выполнено
                            operator: '!='
                        });
                        break;

                    case 5:
                        break;
                }
            }
            grid.setDefaultParams({
                filter: filters
            });

            store.load({
                page: 1,
                callback: function (items) {

                }
            });
        }
    },

    onMainFilterClean: function () {
        this.loadGrid([]);
    },

    destroy: function () {

        var list = this.getUserListComponent();
        var controller = this.getController();

        Ext.getCurrentApp().un('snSignal', controller.onSignal, this);

        if (list) {
            list.un('action', controller.onAction, this);

            list.un('select', controller.onUserSelect, this);
            list.un('unselect', controller.onUserUnSelect, this);
        }

        this.callParent(arguments);
    }
});