﻿/*
 * 
 */
Ext.define('ARM.view.admin.Users', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-admin-users',
    cls: 'app-task-items white-scroll',
    loadMask: true,

    requires: [
        'ARM.store.User'
    ],

    /**
     * переменная для хранения параметров запроса по умолчанию
     */
    defaultParams: null,
    store: Ext.create('ARM.store.User'),
    plugins: [
        {
            ptype: 'gridfilters'
        },
        {
            ptype: 'defaultfilters',
            pluginId: 'defaultfilters'
        }
    ],

    MANAGER_LABEL:'Диспетчер',

    constructor: function(cfg) {
        this.columns = this.getUserColumns();
        this.callParent(arguments);
    },

    getUserColumns: function() {
        return [
            {
                xtype: 'rownumberer',
                width: 40,
                align: 'center',
                text: '№'
            },
            {
                dataIndex: 'C_Fio',
                text: 'ФИО',
                draggable: false,
                flex: 1,
                renderer: function(value, cell, record) {
                    return '<span style="border-radius:8px;margin-right: 10px;display:inline-block;width:16px;height:16px;background-color:#' + record.get('C_Icon_Color') + '">&nbsp;</span> ' + value
                },
                filter: true
            },
            {
                dataIndex: 'C_Login',
                text: 'Логин',
                draggable: false,
                flex: 1,
                align: 'center',
                filter: true
            },
            {
                dataIndex: 'C_Pwd',
                text: 'Пароль',
                draggable: false,
                flex: 1,
                align: 'center'
            },
            {
                dataIndex: 'C_Telephone',
                text: 'Телефон',
                draggable: false,
                renderer: function(value, cell, record) {
                    if (value) {
                        return '<a href="tel:' + value + '">' + value + '</a>';
                    }
                    return '';
                },
                flex: 1,
                align: 'center',
                filter: true
            },
            {
                dataIndex: 'C_Email',
                text: 'Почта',
                draggable: false,
                align: 'center',
                renderer: function(value, cell, record) {
                    if (value) {
                        return '<a href="mailto:' + value + '">' + value + '</a>';
                    }
                    return '';
                },
                flex: 1,
                filter: true
            },
            {
                dataIndex: 'uuid',
                text: 'Серийный номер',
                draggable: false,
                align: 'center',
                width: 130,
                filter: true
            },
            {
                dataIndex: 'C_MainDivision',
                text: 'Филиал',
                draggable: false,
                width: 200,
                align: 'center',
                hidden: true,
                filter: true
            },
            {
                dataIndex: 'C_Division',
                text: 'Отделение',
                draggable: false,
                width: 200,
                align: 'center',
                hidden: true,
                filter: true
            },
            {
                dataIndex: 'C_SubDivision',
                text: 'Участок',
                draggable: false,
                width: 200,
                align: 'center',
                hidden: true,
                filter: true
            },
            {
                dataIndex: 'B_Manager',
                text: this.MANAGER_LABEL,
                draggable: false,
                width: 200,
                align: 'center',
                hidden: true,
                filter: true,
                xtype: 'booleancolumn',
                trueText: 'Да',
                falseText: 'Нет'
            },
            {
                dataIndex: 'B_Admin',
                text: 'Администратор',
                draggable: false,
                width: 200,
                align: 'center',
                hidden: true,
                filter: true,
                xtype: 'booleancolumn',
                trueText: 'Да',
                falseText: 'Нет'
            },
            {
                dataIndex: 'B_Chief',
                text: 'Руководитель',
                draggable: false,
                width: 200,
                align: 'center',
                hidden: true,
                filter: true,
                xtype: 'booleancolumn',
                trueText: 'Да',
                falseText: 'Нет'
            },
            {
                dataIndex: 'C_Mobile_Version',
                text: 'Версия<br />приложения',
                align: 'center',
                filter: true
            }
        ]
    },

    bbar: {
        xtype: 'pagingtoolbar',
        ui: 'white-paging',
        displayInfo: true,
        displayMsg: 'Отображается элементы {0} - {1} из {2}',
        emptyMsg: "Информация отсутствует",
    },

    /*
    * установить значение параметров по умолчанию
    * @param value {any} значение
    */
    setDefaultParams: function(value) {
        this.defaultParams = value;
    },
    /*
     * возвращаются значение параметров по умолчанию
     */
    getDefaultParams: function() {
        return this.defaultParams;
    },

    /*
     * загрузка данных в грид
     * @param filter {any} параметры фильтрации
     */
    loadGrid: function(filter) {
        var store = this.getStore();
        this.setDefaultParams({
            filter: filter
        });

        store.load({
            page: 1
        });
    }
});