﻿/*
 * Администрирование
 */
Ext.define('ARM.view.admin.Admin', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-admin',

    requires: [
        'ARM.view.admin.AdminController',
        'ARM.view.admin.AdminModel',
        'ARM.view.admin.Users',
        'ARM.view.admin.UserItem',
        'ARM.view.admin.Error'
    ],
    ui: 'white',
    controller: 'admin',
    viewModel: 'admin',
    layout: 'fit',
    items: [
        {
            xtype: 'tabpanel',
            padding: '20px 20px 0 20px',
            ui: 'tab-white',
            xtype: 'tabpanel',
            defaults: {
                xtype: 'panel',
                layout: 'fit',
                ui: 'white'
            },
            items: [
                {
                    title: 'Исполнители',
                    xtype: 'app-admin-users',
                    listeners: {
                        itemdblclick: 'onUserItemClick'
                    }
                },
                {
                    title: 'Журнал ошибок',
                    xtype: 'app-admin-error',
                    listeners: {
                        itemdblclick: 'onErrorItemClick'
                    }
                },
                {
                    title: 'Тип изображений',
                    hidden: true
                }
            ]
        }
    ],

    onMainFilterClean: function () {
        var users = this.down('app-admin-users');
        if (users)
            users.loadGrid(this.getController().getFilter());
    },

    listeners: {
        render: 'onRender',
        setuserstore: 'onSetUserStore'
    },

    /*
     * возвращается список пользователей
     * @param view {any} представленив котором требуется производить поиск. По умолчанию 
     */
    getUserList: function (view) {

    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);

            list.un('select', controller.onUserSelect, this);
            list.un('unselect', controller.onUserUnSelect, this);
        }

        this.callParent(arguments);
    }
});