﻿/*
 * Администрирование (карточка пользователя)
 */
Ext.define('ARM.view.admin.UserItem', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-admin-useritem',
    requires: [
        'ARM.view.admin.AdminController',
        'Ext.ux.colorpick.Field'
    ],
    ui: 'white',
    controller: 'admin',
    layout: {
        type: 'vbox',
        pack: 'start'
    },

    store: null,

    //label у Division
    DIVISION_LABEL: 'ПО',
    //label у SubDivision
    SUBDIVISION_LABEL: 'Участок',
    //label у Division
    MAINDIVISION_LABEL: 'Филиал',
    //label у SubDivision
    RES_LABEL: 'РЭС',

    //label для диспечера
    MANAGER_LABEL:'Диспетчер',

    MANAGER_TOOLTIP:'ФИО диспетчера, к которому будет привязан обходчик',

    //всплывающая подсказка для цвета обходчика
    COLOR_TOOLTIP: 'Цвет метки обходчика на карте. Для администратора и диспетчера не обязателен',

    viewModel: {
        data: {
            LINK: '',
            C_Fio: '',
            C_Telephone: '',
            C_Email: '',
            C_Note: '',
            F_Brigades: null,
            uuid: '',
            C_Icon_Color: '',
            C_Pwd: '',
            C_Login: '',
            F_Managers: '',
            B_Admin: false,
            B_Manager: false,
            C_Mobile_Version: '',
            C_Mobile_Info: '',
            defaultColor: false, // применять цвет по умолчанию
            C_Photo: '',
            B_Chief: false,
            filePhoto: ''
        },

        formulas: {
            getCreator: function (get) {
                var date = get('S_Create_Date');
                var name = get('S_Creator');

                if (date && name) {
                    return name + ' создал ' + Ext.Date.format(date, 'd.m.Y H:i:s');
                }

                return '';
            },

            getOwner: function (get) {
                var date = get('S_Modif_Date');
                var name = get('S_Owner');

                if (date && name) {
                    return name + ' обновил ' + Ext.Date.format(date, 'd.m.Y H:i:s');
                }

                return '';
            },

            getMobileInfo: function (get) {
                var info = get('C_Mobile_Info');
                if (info) {
                    var data = JSON.parse(info);
                    return '<div class="info-card-item"><b>Платформа:</b> ' + data.platform + '</div>' +
                        '<div class="info-card-item"><b>Версия платформы:</b> ' + data.version + '</div>' +
                        '<div class="info-card-item"><b>Модель:</b> ' + data.model + '</div>';
                }

                return '';
            },
            getPhotoExist: function (get) {
                return ((get('filePhoto') || get('C_Photo')) ? true : false);
            },
            getPhotoSrc: function (get) {
                var filephoto = get('filePhoto');
                var src = Ext.getCurrentApp().toAbsolutePath('resources/images/avatar.png');
                if (filephoto) {
                    src = filephoto;
                } else {
                    var C_Photo = get('C_Photo');
                    if (C_Photo) {
                        src = Ext.String.format(Ext.getConf('ws_url') + Ext.getConf('virtualDirPath') + '/image?type=avatar&size=small&id={0}', get('LINK'));;
                    }
                }
                return src;
            }
        }
    },

    constructor: function (cfg) {
        this.store = Ext.create('ARM.store.EditUser');

        cfg.items = [{
            scrollable: true,
            bind: {
                title: '{C_Fio}'
            },
            xtype: 'form',
            width: '100%',
            ui: 'right-stat-white',
            header: {
                style: {
                    padding: '10px 0 10px 0'
                }
            },

            padding: '20px 20px 20px 20px',
            bodyPadding: '20px 0 0 0',
            flex: 1,
            defaults: {
                width: '100%',
                labelWidth: 200,
                labelStyle: 'padding:10px 0 0 0'
            },
            layout: 'anchor',
            items: [
                this.getUserInfo(),
                this.getBindings(),
                this.getSecurity(),
                this.additionalInfo()
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    ui: 'footer',
                    dock: 'bottom',
                    items: [
                        '->',
                        {
                            xtype: 'button',
                            ui: 'white-blue',
                            text: 'Применить',
                            tooltip: 'Применить изменения',
                            handler: 'applyChanges'
                        }
                    ]
                }
            ]
        }
        ];

        this.callParent(arguments);
    },


    /*
	 * Возвращает данные пользователя и фото
	 */
    getUserInfo: function() {
        return {
            xtype: 'fieldcontainer',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'anchor',
                    flex: 1,
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            bind: {
                                value: '{LINK}'
                            }
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Серийный номер',
                            bind: {
                                value: '{uuid}'
                            },
                            emptyText: '000000000000000',
                            validator: function (value) {
                                var view = this.up('app-admin-useritem');
                                if (view) {
                                    var vm = view.getViewModel();
                                    if (vm && typeof vm.get === 'function') {
                                        var isDispecher = vm.get('B_Manager');
                                        var isAdmin = vm.get('B_Admin');
                                        var isChef = vm.get('B_Chief');
                                        if (vm.get('B_Manager') != true && vm.get('B_Admin') != true && vm.get('B_Chief') != true && value.length == 0)
                                            return 'Серийный номер не должен быть пустым';
                                        return true;
                                    }
                                }
                                return /^[0-9a-z]+$/i.test(value) == true ? true : 'Серийный номер должен содержать только числа и символы';
                            },
                            tooltip: 'Идентификатор устройства выводиться при авторизации на устройстве'
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Телефон',
                            bind: {
                                value: '{C_Telephone}'
                            },
                            emptyText: 'номер телефона'
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Email',
                            vtype: 'email',
                            bind: {
                                value: '{C_Email}'
                            },
                            emptyText: 'адрес эл. почты'
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Описание',
                            bind: {
                                value: '{C_Note}'
                            },
                            emptyText: 'Краткая информация'
                        },
                        {
                            ui: 'white',
                            xtype: 'colorfield',
                            fieldLabel: 'Цвет',
                            bind: {
                                value: '{C_Icon_Color}',
                                hidden: '{defaultColor}'
                            },
                            emptyText: '000000',
                            listeners: {
                                change: 'onUserColorSelect'
                            },
                            tooltip: this.COLOR_TOOLTIP
                        }
                    ]
                },
                {
                    xtype: 'container',
                    width: 20
                },
                {
                    height: '100%',
                    width: 200,
                    xtype: 'container',
                    ui: 'white',
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'fieldset',
                            cls: 'user-photo',

                            items: [
                                {
                                    xtype: 'image',
                                    bind: {
                                        src: '{getPhotoSrc}'
                                    }

                                },
                                {
                                    xtype: 'button',
                                    cls: 'button-trash',
                                    iconCls: 'fa fa-trash',
                                    handler: 'onClearUserImage',
                                    bind: {
                                        disabled: '{!getPhotoExist}'
                                    }
                                }, {
                                    xtype: 'filefield',
                                    name: 'photo',
                                    buttonConfig: {
                                        iconCls: 'fa fa-folder-open',
                                        text: ''
                                    },
                                    fieldStyle: 'display: none',
                                    accept: 'image',

                                    listeners: {
                                        change: 'onPhotoFieldChanged',
                                    }
                                }
                            ],
                        }
                    ]
                },
                {
                    xtype: 'container',
                    width: 10
                }
            ]
        }
    },


    /*
	 * Возвращает привязки: участок работы и диспетчера
	 */
    getBindings: function () {
        return {
            style: 'margin-top:20px',
            xtype: 'fieldcontainer',
            layout: 'vbox',
            fieldLabel: 'Привязки',
            labelAlign: 'top',
            labelStyle: 'font-weight:bold',
            defaults: {
                width: '100%',
                labelWidth: 200,
                labelStyle: 'padding:10px 0 0 0'
            },
            items: [
                {
                    ui: 'white',
                    xtype: 'combobox',
                    typeAhead: true,
                    store: Ext.create('ARM.store.Brigade'),
                    fieldLabel: 'Участок работы',
                    valueField: 'LINK',
                    displayField: 'C_Name',
                    pageSize: 25,
                    bind: {
                        value: '{F_Brigades}'
                    },
                    emptyText: 'Наименование',
                    tooltip: 'Наименование участка к которому будет привязан обходчик для упрощенного назначения задания'
                },
                {
                    ui: 'white',
                    xtype: 'combobox',
                    store: Ext.create('ARM.store.User'),
                    fieldLabel: this.MANAGER_LABEL,
                    valueField: 'LINK',
                    displayField: 'C_Fio',
                    typeAhead: true,
                    listConfig: {
                        // только для выпадающих списков
                        minWidth: 450
                    },
                    tpl: Ext.create('Ext.XTemplate',
                        '<ul class="x-list-plain"><tpl for=".">',
                        '<li role="option" class="x-boundlist-item"><center><b>{C_Fio}</b></center><br /><b>' + this.MAINDIVISION_LABEL + ':</b> {C_MainDivision}<br /><b>Отделение:</b> {C_Division}<br /><b>Участок:</b> {C_SubDivision}<br /><b>Серийный номер:</b> {uuid}</li>',
                        '</tpl></ul>'
                    ),
                    bind: {
                        value: '{F_Managers}'
                    },
                    emptyText: 'ФИО',
                    tooltip: this.MANAGER_TOOLTIP,
                    pageSize: 25
                }
            ]
        }
    },

    /*
	 * Возвращает логин, пароль и роли пользователя
	 */
    getSecurity: function () {
        return {
            style: 'margin-top:20px',
            xtype: 'fieldcontainer',
            layout: 'vbox',
            fieldLabel: 'Безопасность',
            labelAlign: 'top',
            labelStyle: 'font-weight:bold',
            defaults: {
                width: '100%'
            },
            items: [
                {
                    xtype: 'container',
                    layout: 'hbox',
                    defaults: {
                        labelWidth: 200,
                        labelStyle: 'padding:10px 0 0 0'
                    },
                    items: [
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Логин',
                            bind: {
                                value: '{C_Login}'
                            },
                            emptyText: 'логин',
                            flex: 1,
                            readOnly: true,
                            tooltip: 'Логин для входа в мобильное приложение'
                        },
                        {
                            xtype: 'container',
                            width: 30
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Пароль',
                            bind: {
                                value: '{C_Pwd}'
                            },
                            emptyText: 'пароль',
                            flex: 1,
                            tooltip: 'Пароль для входа в мобильное приложение'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    margin: '10px 0 0 0',
                    defaults: {
                        labelWidth: 200,
                        labelStyle: 'padding:10px 0 0 0'
                    },
                    items: [
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: this.MANAGER_LABEL,
                            bind: {
                                value: '{B_Manager}'
                            },
                            flex: 1
                        },
                        {
                            xtype: 'container',
                            width: 30
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Администратор',
                            bind: {
                                value: '{B_Admin}'
                            },
                            flex: 1
                        },
                        {
                            xtype: 'container',
                            width: 30
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Руководитель',
                            bind: {
                                value: '{B_Chief}'
                            },
                            flex: 1
                        }
                    ]
                }
            ]
        }
    },

    /*
	 * Вернуть дополнительную информацию
	 */
    additionalInfo: function () {
        return {
            xtype: 'panel',
            ui: 'white',
            bind: {
                html: '<div class="info-card">' +
                    '<div class="info-card-title">Дополнительная информация</div>' +
                    //'<div class="info-card-item"><b>' + this.MAINDIVISION_LABEL + ':</b> {S_MainDivision___C_Name}</div>' +
                    '<div class="info-card-item"><b>' + this.DIVISION_LABEL + ':</b> {S_Division___C_Name}</div>' +
                    '<div class="info-card-item"><b>' + this.SUBDIVISION_LABEL + ':</b> {S_SubDivision___C_Name}</div>' +
                    '<div class="info-card-item" style="margin-top:10px">{getCreator}</div>' +
                    '<div class="info-card-item">{getOwner}</div>' +
                    '<hr />' +
                    '<div class="info-card-title">Информация об устройстве</div>' +
                    '{getMobileInfo}' +
                    '<div class="info-card-item"><b>Версия приложения:</b> {C_Mobile_Version}</div>' +
                    '</div>'
            }
        }
    },

    dockedItems: [
        {
            xtype: 'toolbar',
            ui: 'footer',
            style: {
                'background-color': 'white'
            },
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    ui: 'white-blue',
                    text: 'Перейти к списку пользователей',
                    tooltip: 'Перейти к списку пользователей',
                    handler: 'onUsersList',
                    iconCls: 'x-fa fa-angle-left'
                },
                '->'
            ]
        }
    ],

    listeners: {
        render: 'onRender',
        afterRender: 'initPhotoComponent',
        setuserstore: 'onSetUserStore'
    },

    /*
     * передача дополнительных параметров через роутинг 
     * Вызывается в тот момент когда передаются дополнительные параметры
     * @param options {any} дополнительные параметры
     */
    onRouteOptions: function (options) {
        this.callParent(arguments);
        if (options) {
            this.routeOptions = options;
            var store = this.store;
            this.mask('Загрузка...');
            var me = this;
            store.load({
                params: {
                    select: store.getSelectFields(),
                    filter: [
                        {
                            property: 'LINK',
                            value: options.id
                        }
                    ]
                },
                callback: function (items) {
                    if (items && Array.isArray(items)) {
                        var item = items[0].getData();
                        var vm = me.getViewModel();
                        if (vm) {
                            for (var i in item) {
                                vm.set(i, item[i]);
                            }
                        }
                    }
                    if (me.rendered == true)
                        me.unmask();
                }
            });
        }
    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);
        }

        var fieldset = this.down('fieldset');
        if (fieldset) {
            fieldset.el.removeListener('mouseenter', controller.showButtons, fieldset);
            fieldset.el.removeListener('mouseleave', controller.hideButtons, fieldset);
        }

        this.callParent(arguments);
    }
});