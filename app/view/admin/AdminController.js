﻿Ext.define('ARM.view.admin.AdminController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.admin',

    onRender: function (sender) {
        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('action', this.onAction, sender);

            list.on('select', this.onUserSelect, sender);
            list.on('unselect', this.onUserUnSelect, sender);
        }

        sender.setIsReady();
        if (Ext.getGlobalParams('C_Default_Color')) {
            sender.getViewModel().set('defaultColor', true);
        }
    },

    /*
     * обработчик выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserSelect: function (view, cardView) {
        var model = cardView.getModel();
        if (model) {
            var users = this.down('app-admin-users');
            if (users)
                users.loadGrid(this.getController().getFilter());
        }
    },

    /**
     * отмена выбора пользователя
     * @param view {any} панель с пользователями
     * @param cardView {any} карточка пользователя
     */
    onUserUnSelect: function (view, cardView) {
        var users = this.down('app-admin-users');
        if (users)
            users.loadGrid(this.getController().getFilter());
    },

    /*
     * выбор пользователя из списка
     */
    onUserItemClick: function (sender, record, item, index, e, eOpts) {
        this.redirectTo('#admin/admin-useritem/' + record.get('LINK'));
    },

    /*
     * обработчик выбора цвета для пользователя
     */
    onUserColorSelect:function (field, selColor) { 

    },

    applyChanges: function () {
        var me = this;
        var view = this.getView();        
        var field = view.down('filefield');
        var file = field.getEl().down('input[type=file]').dom.files[0];
        if (file) {
            var vm = view.getViewModel(),
                values = vm.getData(),
                url = Ext.String.format(Ext.getConf('RPC_URL').replace('/rpc', '/img/saveavatar'), Ext.getConf('REMOTING_ADDRESS')),
                link = values.LINK;
                imgName = link + '.jpg';
            var http = new XMLHttpRequest();
            var formData = new FormData();
            formData.append('id', link);
            formData.append('filename', imgName);
            formData.append('file', file);
            formData.append('size', file.size);
            formData.append('date', Ext.Date.now());
            http.open('POST', url);

            view.mask('Сохранение изображения...');

            http.onload = function (e) {
                view.unmask();
                me.onUserApply();
            };

            http.send(formData);
        } else {
            me.onUserApply();
        }
    },

    /*
     * применение данных по пользователю
     */
    onUserApply: function () {
        var view = this.getView();
        var values = view.getViewModel().getData();
        // нужно проверить, что IMEI доступен
        var dataProvider = Ext.getCurrentApp().getDataProvider();
        dataProvider.uuidExists(values.uuid, function (exists, record) {
            var form = view.down('form');
            var change = function () {
                if (form.isValid() == true) {
                    var record = view.store.getById(values.LINK);
                    if (record) {
                        for (var i in values)
                            record.set(i, values[i]);
                        if (record.modified) {
                            view.mask('Сохранение...');

                            view.store.sync({
                                callback: function () {
                                    view.unmask();
                                    Ext.Msg.alert('Уведомление', 'Данные успешно сохранены');
                                }
                            });
                        } else {
                            Ext.Msg.alert('Сообщение', 'Изменений на форме не найдено');
                        }
                    }
                }
            };

            if (exists == true && record.get('LINK') != values.LINK) {
                Ext.Msg.confirm('Предупреждение', 'Указанный "Серийный номер" уже привязан к пользователю. Продолжить?', function (btn) {
                    if (btn == 'yes') {
                        change();
                    }
                });
            } else {
                change();
            }
        });
    },

    /*
     * вернуться в список пользователей
     */
    onUsersList: function () {
        var len = Ext.util.History.length;
        if (len > 2) {
            Ext.util.History.back();
        } else {
            this.redirectTo('#admin');
        }
    },

    /*
     * обработчик нажания на текст ошибки
     */
    onErrorItemClick: function (sender, record, item, index, e, eOpts) {
        Ext.Msg.alert('Текст ошибки', record.get('C_Message'));
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;
        var me = this;

        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            var selectCardView = list.getSelectCardView();
            if (selectCardView) {
                list.fireEvent('select', list, selectCardView);
            } else {
                var usersGrid = sender.down('app-admin-users');
                if (usersGrid) {
                    usersGrid.loadGrid(this.getFilter());
                }
            }
        }

        _.series(
            [
                // нужно чтобы иконки в списке обновились (обработались)
                function (callback) {
                    sender.mask('Обработка<br />обходчиков...');
                    me.processingPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    sender.unmask();
                    callback();
                }
            ]);
    },
    initPhotoComponent: function () {
        var view = this.getView();
        var fieldset = view.down('fieldset');
        fieldset.el.addListener('mouseenter', this.showButtons, fieldset);
        fieldset.el.addListener('mouseleave', this.hideButtons, fieldset);
    },

    showButtons: function () {
        var fieldset = this;
        var btn = fieldset.down('button');
        var filefield = fieldset.down('filefield');
        var animIn = {
            opacity: 1,
            easing: 'easeIn',
            duration: 500
        };
        btn.el.fadeIn('t', animIn);
        filefield.el.fadeIn('t', animIn);

    },

    hideButtons: function () {
        var fieldset = this;
        var btn = fieldset.down('button');
        var filefield = fieldset.down('filefield');
        var animOut = {
            opacity: 0,
            easing: 'easeOut',
            duration: 700,
            remove: false,
            useDisplay: false
        };
        btn.el.fadeOut('t', animOut);
        filefield.el.fadeOut('t', animOut);
    },

    onPhotoFieldChanged: function (field, path) {
        var view = this.getView();
        var vm = view.getViewModel();
        var values = vm.getData();
        var file = field.getEl().down('input[type=file]').dom.files[0];
        if (file) {
            var reader = new FileReader();
            var image = view.down('image');

            reader.onload = function (e) {
                vm.set('filePhoto', e.target.result);
            }
            reader.readAsDataURL(file);
        }
    },

    onClearUserImage: function () {
        var me = this;
        var mess = Ext.Msg.show({
            title: 'Удалить фото',
            message: 'Вы действительно хотите удалить фото?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            closeToolText: 'Закрыть',
            fn: function (btn) {
                if (btn === 'yes') {
                    var view = me.getView();
                    var vm = view.getViewModel();
                    var filefield = view.down('filefield');
                    filefield.reset();
                    filefield.getEl().down('input[type=file]').dom.files[0] = undefined;
                    vm.set('C_Photo', '');
                    vm.set('filePhoto', '');
                }
            }
        });
    },

    privates: {
        /*
         * возвращаются фильтры
         */
        getFilter: function () {
            var sender = this.getView();
            var filters = [];
            var filter = sender.up('app-main').down('app-main-filter');
            var values = null;
            if (values = filter.getLastValues()) {
                var usersGrid = sender.down('app-admin-users');
                if (usersGrid) {

                    var data = {
                        S_MainDivision: values.MainDivision,
                        S_Division: values.Division,
                        S_SubDivision: values.SubDivision,
                        F_Managers: values.Managers,
                        C_Fio: values.User
                    };

                    var list = sender.getUserListComponent();
                    if (list) {
                        var selectCardView = list.getSelectCardView();
                        if (selectCardView && selectCardView.rendered == true) {

                            var model = selectCardView.getModel();
                            if (model) {
                                filters.push({
                                    property: 'LINK',
                                    value: model.get('LINK'),
                                    operator: '='
                                });
                            }

                        }
                    }

                    for (var i in data) {
                        if (data[i]) {

                            if (i == 'C_Fio') {
                                var operator = '';
                                var property = '';
                                // нужно проверить, что не гуид
                                if (/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(data[i])) {
                                    operator = '=';
                                    property = 'LINK';
                                } else {
                                    operator = 'like';
                                    property = 'C_Fio';
                                }

                                filters.push({
                                    property: property,
                                    value: data[i],
                                    operator: operator
                                });
                            } else {
                                filters.push({
                                    property: i,
                                    value: data[i],
                                    operator: i == 'C_Fio' ? 'like' : '='
                                });
                            }
                        }
                    }
                }
            }

            return filters;
        },
        /*
         * обработчик действия с карточкой пользователя
         * @param list {any} представление со списком пользователей
         * @param action {string} действие
         * @param record {any} запись пользователя
         * @param card {any} карточка пользователя - представление
         */
        onAction: function (list, action, record, card) {
            var url = 'home/' + action + '/' + record.get('uuid');
            switch (action) {
                case 'user':
                case 'route':
                case 'tasks':
                    Ext.getCurrentApp().redirectTo(url);
                    break;
            }
        },

        /*
         * Обработка пользователей
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        processingPlacemarks: function (sender, users, callback) {
            var view = this.getView();
            if (view) {
                var list = view.getUserListComponent();
                if (list) {
                    users.forEach(function (i) {
                        var record = list.map_uuid[i.get('uuid')];
                        if (record)
                            list.setAccess(record.getId(), true);
                    });
                }

                if (typeof callback == 'function')
                    callback();
            }
        }
    }
});
