﻿/*
 * 
 */
Ext.define('ARM.view.admin.Error', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-admin-error',
    cls: 'app-task-items white-scroll',
    loadMask: true,

    requires: [
        'ARM.store.Error'
    ],

    /**
     * переменная для хранения параметров запроса по умолчанию
     */
    defaultParams: null,
    store: Ext.create('ARM.store.Error'),
    plugins: [
        {
            ptype: 'gridfilters'
        }
    ],

    columns: [
        {
            xtype: 'rownumberer', width: 40, align: 'center', text: '№'
        },
        {
            xtype: 'numbercolumn', dataIndex: 'LINK', text: 'Идентификатор', filter: true, hidden: true, flex: 1,
            renderer: function (value) {
                return value.toFixed(0);
            }
        },
        { dataIndex: 'C_Exception', text: 'Исключение', filter: true, hidden: true, flex: 1 },
        { xtype: 'datecolumn', dataIndex: 'S_Date', text: 'Дата-Время', format: 'd.m.Y H:i:s', filter: true, hidden: false, flex: 1 },
        { dataIndex: 'C_Level', text: 'Тип', filter: true, hidden: false, flex: 1 },
        { dataIndex: 'C_Message', text: 'Сообщение', filter: true, hidden: false, flex: 2 },
        { dataIndex: 'C_User', text: 'Пользователь', filter: true, hidden: false, flex: 1 }
    ],

    bbar: {
        xtype: 'pagingtoolbar',
        ui: 'white-paging',
        displayInfo: true,
        displayMsg: 'Отображается элементы {0} - {1} из {2}',
        emptyMsg: "Информация отсутствует",
    }
});