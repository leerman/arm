﻿/*
 * Отчеты
 */
Ext.define('ARM.view.report.Report', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-report',
    requires: [
        'ARM.view.report.ReportController',
        'ARM.view.report.ReportModel',

        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.series.Line',
        'Ext.chart.interactions.ItemHighlight'
    ],
    ui: 'white',
    controller: 'report',
    viewModel: 'report',
    layout: {
        type: 'hbox',
        pack: 'start'
    },

    items: [
        {
            padding: 10,
            xtype: 'panel',
            ui: 'white',
            reference: 'list',
            flex: 1
        }
    ],

    listeners: {
        'afterrender': 'onRender'
    },

    destroy: function () {
        var controller = this.getController();
        var app_main = Ext.getCurrentApp().getInitView();
        if (app_main) {
            var menu = app_main.down('app-mainmenu-bar');
            if (menu) {
                menu.un('loaded', controller.onMenuLoaded, this);
            }
        }        
        this.callParent(arguments)
    }
});