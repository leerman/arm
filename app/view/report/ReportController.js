﻿Ext.define('ARM.view.report.ReportController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.report',

    onRender: function (sender) {
        var menu = sender.up('app-main').down('app-mainmenu-bar');
        if (menu) {
            if (menu.menuItems.length > 0) {
                this.onMenuLoaded(menu.menuItems);
            } else {
                menu.on('loaded', this.onMenuLoaded, sender);
            }
        }
    },

    onMenuLoaded: function (items) {
        var reportMenu = null;
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (item.actionId == 'report') {
                reportMenu = item.children;
                break;
            }
        }
        if (reportMenu) {
            var tpl = new Ext.XTemplate(
                '<ul>',
                '<tpl for=".">',       // process the data.kids node
                    '<li><a target="_blank" href="{actionId}">{text}</a></li>',  // use current array index to autonumber
                '</tpl>',
                '</ul>');
            var html = tpl.apply(reportMenu);
            var list = this.lookupReference('list');
            list.setHtml(html);
        }
    }
});