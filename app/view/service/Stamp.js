Ext.define('ARM.view.service.Stamp', {
    extend: 'ARM.view.service.BaseServiceTab',
    xtype: 'app-service-stamp',
    requires: [
        'ARM.store.Stamp'
    ],
    viewModel: {
        stores: {
            items: {}
        }
    },
    items: [  
        {
            xtype: 'grid',
            cls: 'app-task-items white-scroll',
            loadMask: true,
            bind: {
                store: '{items}'
            },
            plugins: [
                {
                    ptype: 'gridfilters'
                },
                {
                    ptype: 'defaultfilters',
                    pluginId: 'defaultfilters'
                }
            ],
            columns: [
                {
                    xtype: 'rownumberer', width: 40, align: 'center', text: '№'
                },
                {
                    dataIndex: 'F_Stamp_Types___C_Name',
                    text: 'Тип пломбы',
                    draggable: false,
                    flex: 1,
                    sorter: {
                        property: 'F_Stamp_Types_Ref.C_Name'
                    },
                    filter: {
                        dataIndex: 'F_Stamp_Types_Ref.C_Name'
                    }
                },
                {
                    dataIndex: 'C_Seal_Number',
                    text: 'Номер пломбы',
                    draggable: false,
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                    
                },
                {
                    dataIndex: 'F_Users___C_Fio',
                    text: 'ФИО',
                    draggable: false,
                    flex: 2,
                    sorter: {
                        property: 'F_Users_Ref.C_Fio'
                    },
                    filter: {
                        dataIndex: 'F_Users_Ref.C_Fio'
                    }
                },
                {
                    dataIndex: 'F_Users___C_Login',
                    text: 'Логин',
                    align: 'center',
                    draggable: false,
                    sorter: {
                        property: 'F_Users_Ref.C_Login'
                    },
                    filter: {
                        dataIndex: 'F_Users_Ref.C_Login'
                    }
                },
                {
                    dataIndex: 'B_Use',
                    align: 'center',
                    flex: 1,
                    text: 'Установлена',
                    draggable: false,
                    filter: {
                        type: 'boolean',
                        yesText: 'Да',
                        noText: 'Нет'
                    },
                    xtype: 'booleancolumn',
                    trueText: 'Да',
                    falseText: 'Нет'
        
                }
            ],
            bbar: {
                xtype: 'pagingtoolbar',
                ui: 'white-paging',
                displayInfo: true,
                displayMsg: 'Отображается элементы {0} - {1} из {2}',
                emptyMsg: "Информация отсуствует"
            }
        }
    ],

    initComponent: function () {
        this.callParent(arguments);

        var vm = this.getViewModel();
        var store = Ext.create('ARM.store.Stamp');
        store.proxy.extraParams['select'] = store.getSelectFields();
        vm.set('items', store);
    }
});