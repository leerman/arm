Ext.define('ARM.view.service.BaseServiceTab', {
    extend: 'ARM.shared.PlaceHolder',
    config: {
        refreshing: false
    },
    /**
     * Переопределено
     */
    onRender: function () {
        this.callParent(arguments);
        var list = this.up('app-main').down('app-user-list');
        if (list) {
            list.on('select', this.onUserSelect, this);
            list.on('unselect', this.onUserUnSelect, this);
            var selectCardView = list.getSelectCardView();
            if (selectCardView) {
                list.fireEvent('select', list, selectCardView);
            } 
        }
        this.setIsReady();
    },
    /**
     * Переопределено
     */
    onShow: function (){
        this.callParent(arguments);
        if (this.getRefreshing()){
            var list = this.up('app-main').down('app-user-list');
            if (list) {
                var selectCardView = list.getSelectCardView();
                if (selectCardView) {
                    this.usersFilter(selectCardView.getModel())
                } else {
                    this.usersFilterClear();
                }
            }
            this.setRefreshing(false)
        }
    },

    destroy: function () {
        var list = this.getUserListComponent();
        if (list) {
            list.un('select', this.onUserSelect, this);
            list.un('unselect', this.onUserUnSelect, this);
        }
        this.callParent(arguments);
    },
    /**
     * сбросить все фильтры по клику на кнопке "очистить"
     */
    clearAllFilters: function(){
        this.down('grid').filters.clearFilters();
        this.usersFilterClear();
    },
    privates: {
        /** 
        * обработчик выбора пользователя
        * @param view {any} панель с пользователями
        * @param cardView {any} карточка пользователя
        */
        onUserSelect: function (view, cardView) {
            var model = cardView.getModel();
            if (model) {
                if (this.isHidden() == false){
                    this.usersFilter(model);
                }else{
                    this.setRefreshing(true);
                }
            }
        },
        /**
         * отмена выбора пользователя
         * @param view {any} панель с пользователями
         * @param cardView {any} карточка пользователя
         */
        onUserUnSelect: function (view, cardView) {
            if (this.isHidden() == false){
                this.usersFilterClear();
            }else{
                this.setRefreshing(true);
            }
        },
        /**
         * фильтр по пользователям 
         */
        usersFilter: function (model){
            this.down('gridpanel').getStore().filter([
                {
                    id: 'UsersFilter',
                    property: 'F_Users',
                    value: model.get('LINK'),
                    operator: '='
                }
            ]);
        },
        /**
         * сбросить фильтр по пользователям
         */
        usersFilterClear: function (){
            this.down('gridpanel').getStore().removeFilter('UsersFilter');
        }
    }
});