﻿/*
* Служебное
*/
Ext.define('ARM.view.service.Service', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-service',
    ui: 'white',
    layout: 'fit',

    constructor: function (cfg) {
        var checkBlankModule = Ext.getConf('checkBlankModule');
        var tabItems = [];
        tabItems.push({
            title: 'Пломбы',
            xtype: 'app-service-stamp'
        });
        if (checkBlankModule == true) {
            tabItems.push({
                title: 'Бланки',
                xtype: 'app-service-blank'
            });
        }
        cfg.items = [{
            xtype: 'tabpanel',
            padding: '20px 20px 0 20px',
            ui: 'tab-white',
            defaults: {
                layout: 'fit',
                ui: 'white'
            },
            items: tabItems
        }];
        this.callParent(arguments);
    },

    /**
     * Переопределено
     */
    onMainFilterClean: function () {
        this.down('tabpanel').items.items.forEach(function(element) {
            element.clearAllFilters();
        });
    }
});