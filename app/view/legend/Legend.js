﻿/*
 * Настройки
 */
Ext.define('ARM.view.legend.Legend', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-legend',

    ui: 'white',
    cls: 'fields',
    layout: {
        type: 'vbox',
        pack: 'start'
    },
    
    scrollable: 'vertical',

    /**
     * Вернуть разметку описания точки пользователя
     */
    getMainUser: function () {
        var main_user_url = Ext.conf().getIconPath().replace('{1}', 'placemark').replace('{0}', '7f7f7f');
        var main_user = '<div style="height:30px;position:relative;display:flex;justify-content:center;align-items:center;">\
                            <img style="position:absolute;" src="' + main_user_url + '">\
                            <img style="position:absolute;" src="resources/pages/ymaps/images/placemark_user.png">\
                        </div>';

        return {
            xtype: 'container',
            layout: 'hbox',
            style: 'font-weight: normal; font-size: 14px; padding-top: 10px;display:flex;justify-content:center;align-items:center;',
            items: [
                {
                    xtype: 'container',
                    html: main_user,
                    width: 100
                },
                {
                    xtype: 'container',
                    padding: '8 0 0 0',
                    html: 'Местоположение обходчика'
                }
            ]
        }
    },

    /**
     * Вернуть разметку геокоординат
     */
    getMainUserGeo: function () {
        var url = Ext.conf().getIconPath().replace('{1}', 'geo').replace('{0}', '7f7f7f');
        var main_user = '<div style="height:30px;position:relative;display:flex;justify-content:center;align-items:center;">\
                            <img style="position:absolute;" src="' + url + '">\
                        </div>';

        return {
            xtype: 'container',
            layout: 'hbox',
            style: 'font-weight: normal; font-size: 14px; padding-top: 10px;display:flex;justify-content:center;align-items:center;',
            items: [
                {
                    xtype: 'container',
                    html: main_user,
                    width: 100
                },
                {
                    xtype: 'container',
                    padding: '8 0 0 0',
                    html: 'Местоположение обходчика во время отправки геокоординат'
                }
            ]
        }
    },

    /**
     * Вернуть разметку одной точки
     */
    getSmallImageLegend: function (backgroundColor, statusNumber, comment) {
        var url = Ext.conf().getIconPath().replace('{1}', 'task').replace('{0}', backgroundColor);
        var html = '<div style="height:30px;position:relative;display:flex;justify-content:center;align-items:center;">\
                            <img style="position:absolute;" src="' + url + '">\
                            <img style="position:absolute;" src="resources/pages/appoint/images/status-' + statusNumber + '.png" />\
                        </div>';

        return {
            xtype: 'container',
            layout: 'hbox',
            style: 'font-weight: normal; font-size: 14px; padding-top: 10px;display:flex;justify-content:center;align-items:center;',
            items: [
                {
                    xtype: 'container',
                    html: html,
                    width: 100
                },
                {
                    xtype: 'container',
                    padding: '8 0 0 0',
                    html: comment
                }
            ]
        }
    },

    /**
     * Вернуть разметку области
     */
    getBigImageLegend: function (imageName, label) {
        var html = '<div style="height:54px;position:relative;display:flex;justify-content:center;align-items:center;">\
                            <img src="resources/images/' + imageName + '.png" />\
                        </div>';

        return {
            xtype: 'container',
            layout: 'hbox',
            style: 'font-weight: normal; font-size: 14px; padding-top: 10px;display:flex;justify-content:center;align-items:center;',
            items: [
                {
                    xtype: 'container',
                    html: html,
                    width: 100
                },
                {
                    xtype: 'container',
                    padding: '16 0 0 0',
                    html: label
                }
            ]
        }
    },

    getRoute: function (name, label) {
        var html = '<div style="height:30px;position:relative;display:flex;justify-content:center;align-items:center;">\
                            <img style="position:absolute;" src="resources/images/' + name + '-container.png">\
                            <img style="position:absolute;padding-bottom:7px;" src="resources/images/' + name + '.png" />\
                        </div>';

        return {
            xtype: 'container',
            layout: 'hbox',
            style: 'font-weight: normal; font-size: 14px; padding-top: 10px;display:flex;justify-content:center;align-items:center;',
            items: [
                {
                    xtype: 'container',
                    html: html,
                    width: 100
                },
                {
                    xtype: 'container',
                    padding: '16 0 0 0',
                    html: label
                }
            ]
        }
    },

    constructor: function (cfg) {

        this.items = [
        {
            xtype: 'container',
            width: '100%',
            html: 'Информация о метках',
            cls: 'center',
            style: 'font-weight: bold; font-size: 20px; padding-top: 10px;',
        },
        {
            xtype: 'container',
            html: 'Главная',
            width: '100%',
            cls: 'center',
            style: 'font-weight: bold; font-size: 18px; padding-top: 20px;'
        },
        this.getMainUser(),
        this.getMainUserGeo(),
        this.getSmallImageLegend('d3d3d3', 3, 'Задания в точке выполнены'),
        this.getBigImageLegend('legend-done', 'Все задания в области выполнены'),
        this.getSmallImageLegend('d3d3d3', 2, 'Задания в точке находятся в работе'),
        this.getBigImageLegend('legend-inwork', 'Все задания в области находятся в работе'),
        this.getSmallImageLegend('d3d3d3', 1, 'Задания в точке не выполнены'),
        this.getBigImageLegend('legend-not-done', 'Задания в области не выполнены'),
        this.getBigImageLegend('legend-main-multi', 'В области присутствуют находящиеся в работе, не выполненные и выполненные задания'),
        {
            xtype: 'container',
            html: 'Назначение заданий',
            width: '100%',
            cls: 'center',
            style: 'font-weight: bold; font-size: 18px; padding-top: 20px;'
        },
        this.getSmallImageLegend('0A0909', 4, 'Задание не назначено обходчику'),
        this.getBigImageLegend('legend-not-assigned', 'Задания в области не назначены обходчику'),
        this.getSmallImageLegend('404DDD', 4, 'Задание назначено обходчику'),
        this.getBigImageLegend('legend-assigned', 'Задания в области назначены обходчику'),
        this.getSmallImageLegend('DF0024', 4, 'Задание выделено для назначения'),
        this.getBigImageLegend('legend-not-done', 'Задания в области выделены для назначения'),
        {
            xtype: 'container',
            html: 'История маршрутов',
            width: '100%',
            cls: 'center',
            style: 'font-weight: bold; font-size: 18px; padding-top: 20px;'
        },
        this.getMainUser(),
        this.getMainUserGeo(),
        this.getRoute('a', 'Начальная точка обходчика при выполнении заданий'),
        this.getRoute('b', 'Конечная точка обходчика при выполнении заданий'),
        this.getSmallImageLegend('d3d3d3', 3, 'Задания в точке выполнены'),
        this.getBigImageLegend('legend-done', 'Все задания в области выполнены'),
        this.getSmallImageLegend('d3d3d3', 2, 'Задания в точке находятся в работе'),
        this.getBigImageLegend('legend-inwork', 'Все задания в области находятся в работе'),
        this.getSmallImageLegend('d3d3d3', 1, 'Задания в точке не выполнены'),
        this.getBigImageLegend('legend-not-done', 'Задания в области не выполнены'),
        {
            xtype: 'container',
            style:'padding-bottom:20px'
        }
        ]
        this.callParent(cfg);
    }
});