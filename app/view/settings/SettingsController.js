﻿Ext.define('ARM.view.settings.SettingsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.settings',

    onRender: function (sender) {
        var store = sender.store;
        sender.mask('Загрузка...');
        store.load({
            callback: function (items) {
                if (items && Array.isArray(items)) {
                    var item = items[0].getData();
                    var vm = sender.getViewModel();
                    if (vm) {
                        for (var i in item) {
                            vm.set(i, item[i]);
                        }
                    }
                }
                if (sender.rendered == true)
                    sender.unmask();
            }
        });

        // здесь подписываемся на события выбора пользователя в списке
        var list = sender.up('app-main').down('app-user-list');
        if (list) {
            list.on('action', this.onAction, sender);
        }

        sender.setIsReady();
    },

    /*
     * применить
     */
    onApply: function () {
        var view = this.getView();

        var form = view.down('form');
        if (form.isValid() == true) {
            var values = view.getViewModel().getData();
            var record = view.store.getById(values.LINK);
            if (record) {
                view.mask('Сохранение...');
                for (var i in values)
                    record.set(i, values[i]);

                view.store.sync({
                    callback: function () {
                        view.unmask();
                        Ext.Msg.alert('Уведомление', 'Данные успешно сохранены');
                    }
                });
            }
        }
    },

    /*
     * обработчик установки хранилища
     * @param sender {any} текущее представление
     * @param store {any} установленное хранилище
     */
    onSetUserStore: function (sender, userStore) {
        var users = userStore.getData().items;
        var me = this;

        _.series(
            [
                // нужно чтобы иконки в списке обновились (обработались)
                function (callback) {
                    sender.mask('Обработка<br />обходчиков...');
                    me.processingPlacemarks(sender, users, function () {
                        callback();
                    });
                },
                function (callback) {
                    sender.unmask();
                    callback();
                }
            ]);
    },

    /*
     * очистка цвета
     */
    onColorReset: function (sender) {
        sender.setValue(null);
    },

    privates: {
        /*
         * обработчик действия с карточкой пользователя
         * @param list {any} представление со списком пользователей
         * @param action {string} действие
         * @param record {any} запись пользователя
         * @param card {any} карточка пользователя - представление
         */
        onAction: function (list, action, record, card) {
            var url = 'home/' + action + '/' + record.get('uuid');
            switch (action) {
                case 'user':
                case 'route':
                case 'tasks':
                    Ext.getCurrentApp().redirectTo(url);
                    break;
            }
        },

        /*
         * Обработка пользователей
         * @param sender {any} текущее представление
         * @param users {any[]} список пользователей
         * @param callback {()=>void} функция обратного вызова
         */
        processingPlacemarks: function (sender, users, callback) {
            var view = this.getView();
            if (view) {
                var list = view.getUserListComponent();
                if (list) {
                    users.forEach(function (i) {
                        var record = list.map_uuid[i.get('uuid')];
                        if (record)
                            list.setAccess(record.getId(), true);
                    });
                }

                if (typeof callback == 'function')
                    callback();
            }
        }
    }
});
