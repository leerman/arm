﻿/*
 * Настройки
 */
Ext.define('ARM.view.settings.Settings', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-settings',
    requires: [
        'ARM.view.settings.SettingsController',
        'ARM.view.settings.SettingsModel'
    ],
    ui: 'white',
    controller: 'settings',
    viewModel: 'settings',
    cls: 'fields',
    layout: {
        type: 'vbox',
        pack: 'start'
    },

    store: null,

    constructor: function (cfg) {
        this.store = Ext.create('ARM.store.GlobalParams');
        this.callParent(arguments);
    },

    items: [
        {
            xtype: 'form',
            width: '100%',
            ui: 'white',

            padding: '20px 20px 20px 20px',
            flex: 1,
            defaults: {
                width: '100%',
                labelWidth: 200
            },
            layout: 'anchor',
            items: [
                {
                    xtype: 'hiddenfield',
                    bind: {
                        value: '{LINK}'
                    }
                },
                {
                    ui: 'white',
                    xtype: 'textfield',
                    fieldLabel: 'URL web-приложения',
                    allowBlank: false,
                    vtype: 'url',
                    bind: {
                        value: '{C_Url}'
                    },
                    emptyText: 'http://localhost:123'
                },
                {
                    ui: 'white',
                    xtype: 'textfield',
                    fieldLabel: 'Интервал передачи геоданных с телефона в сек.',
                    bind: {
                        value: '{N_GeoInterval}'
                    },
                    validator: function (value) {
                        return /^\d+$/gi.test(value) == true ? true : 'Должно быть указано числовое значение';
                    },
                    emptyText: '123'
                },
                {
                    xtype: 'checkboxfield',
                    fieldLabel: 'Проверять разрядность',
                    bind: {
                        value: '{B_ValidatorDigits}'
                    }
                },
                {
                    xtype: 'checkboxfield',
                    fieldLabel: 'Разрешить удаление созданных актов',
                    bind: {
                        value: '{B_Remove_Record}'
                    }
                },
                {
                    ui: 'white',
                    xtype: 'colorfield',
                    fieldLabel: 'Цвет по умолчанию',
                    bind: {
                        value: '{C_Default_Color}'
                    },
                    emptyText: '000000',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            handler: 'onColorReset'
                        }
                    }
                },
                {
                    xtype: 'container',
                    height: '40px'
                },
                {
                    xtype: 'fieldcontainer',
                    labelWidth: 200,
                    fieldLabel: 'Настройки почтового сервера',
                    labelAlign: 'top',
                    // The body area will contain three text fields, arranged
                    // horizontally, separated by draggable splitters.
                    layout: 'vbox',
                    defaults: {
                        width: '100%',
                        labelWidth: 200
                    },
                    labelStyle: 'font-weight:bold',
                    items: [
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Сервер smtp',
                            bind: {
                                value: '{C_Smtp_Host}'
                            },
                            emptyText: 'smpt.localhost.ru'
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Пользователь smtp',
                            bind: {
                                value: '{C_Smtp_User}'
                            },
                            emptyText: 'user'
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Пароль smtp',
                            bind: {
                                value: '{C_Smtp_Pwd}'
                            },
                            inputType: 'password',
                            emptyText: ''
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Smtp e-mail от',
                            bind: {
                                value: '{C_Smtp_From}'
                            },
                            emptyText: 'from@localhost.ru'
                        },
                        {
                            ui: 'white',
                            xtype: 'textfield',
                            fieldLabel: 'Порт smtp',
                            bind: {
                                value: '{N_Smtp_Port}'
                            },
                            emptyText: '25',
                            validator: function (value) {
                                return /^\d+$/gi.test(value) == true ? true : 'Должно быть указано числовое значение';
                            }
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    ui: 'footer',
                    dock: 'bottom',
                    padding: '0 0 0 20px',
                    items: [
                        '->',
                        {
                            xtype: 'button',
                            text: 'Применить',
                            itemId: 'applyId',
                            tooltip: 'Применить условие фильтрации',
                            handler: 'onApply'
                        }
                    ]
                }
            ]
        }
    ],

    listeners: {
        render: 'onRender',
        setuserstore: 'onSetUserStore'
    },

    destroy: function () {
        var list = this.getUserListComponent();
        var controller = this.getController();
        if (list) {
            list.un('action', controller.onAction, this);
        }

        this.callParent(arguments);
    }
});