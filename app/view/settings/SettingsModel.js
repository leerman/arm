﻿Ext.define('ARM.view.settings.SettingsModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.settings',

    data: {
        LINK: '',
        C_Url: '', // URL web-приложения
        N_GeoInterval: 0, // Интервал передачи геоданных с телефона в сек.
        B_ValidatorDigits: false, // Проверять разрядность
        B_Remove_Record: false, // Разрешить удаление созданных актов

        C_Smtp_Host: '', // Сервер smtp
        C_Smtp_User: '', // Пользователь smtp
        C_Smtp_Pwd: '', // Пароль smtp
        C_Smtp_From: '', // Smtp e-mail от
        N_Smtp_Port: '', // Порт smtp
        C_Default_Color: '' // цвет по умоланию
    }
});