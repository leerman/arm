﻿Ext.define('ARM.view.subscrs.Subscrs', {
    extend: 'ARM.shared.PlaceHolder',
    xtype: 'app-subscrs',
    requires: [
        'ARM.view.subscrs.SubscrsGrid',
        'ARM.view.subscrs.SubscrsConstoller'
    ],

    controller: 'subscrs',

    ui: 'white',
    scrollable: false,
    layout: 'fit',

    items: [{
        xtype: 'subscrs-grid',
        emptyText: 'Нет лицевых счетов',
        width: '100%',
        listeners: {
            itemdblclick: 'onGridItemDblClick'
        }
    }],

    listeners: {
        setuserstore: 'onSetUserStore',
        render: 'onViewRender',
        reloadgrid: 'onReloadGrid'
    },

    /*
     * Дополнительные фильтры
     */
    filterValues: {},
    getFilterValues: function () {
        return this.filterValues;
    },

    setFilterValues: function (value) {
        this.filterValues = value;
    },

    /**
     * Вернуть грид с лицевыми счетами
     */
    getGrid: function () {
        return this.down('subscrs-grid');
    },

    /**
     * Обработка события изменения фильтров на левой панели
     */
    setUserStore: function () {
        var filter = this.up('app-main').down('app-main-filter');
        if (filter) {
            var values = filter.getLastValues();
            this.setFilterValues(values);
        }
        this.getController().onReloadGrid();
    }
});