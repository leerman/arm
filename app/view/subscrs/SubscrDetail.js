﻿Ext.define('ARM.view.subscrs.SubscrDetail', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-subscr-detail',
    requires: [
        'ARM.view.subscrs.LastCheck'
    ],

    padding: 10,
    ui: 'tab-white',
    defaults: {
        xtype: 'panel',
        layout: 'fit',
        ui: 'white'
    },
    height: '100%',
    width: '100%',

    constructor: function (cfg) {
        this.items = [{
            title: 'Предыдущие проверки',
            items: {
                xtype: 'subscr-last-check',
                link: cfg.link
            }
        }, {
            title: 'Информация о ЛС',
            xtype: 'subscrs-info',
            subscrInfo: cfg.subscrInfo

        }];
        this.callParent(arguments);
    }
});