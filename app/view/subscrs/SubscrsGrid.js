﻿Ext.define('ARM.view.subscrs.SubscrsGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'subscrs-grid',
    requires: [
        'ARM.view.subscrs.SubscrDetail'
    ],

    store: Ext.create('ARM.store.Subscrs'),
    cls: 'white-scroll app-task-items',
    style: 'background: white;',
    ui: 'white',
    scrollable: true,

    plugins: [{
        ptype: 'defaultfilters',
        pluginId: 'defaultfilters'
    }, {
        ptype: 'gridfilters'
    }, {
        ptype: 'responsive'
    }],

    columns: [
            {
                xtype: 'rownumberer',
                align: 'center',
                width: 50,
            },
            {
                dataIndex: 'LINK',
                text: 'Идентификатор',
                filter: true,
                hidden: true
            },
            {
                dataIndex: 'C_Family',
                text: 'Фамилия',
                filter: true,
                hidden: true
            },
            {
                dataIndex: 'C_Name',
                text: 'Имя',
                filter: true,
                hidden: true
            },
            {
                dataIndex: 'C_Otchestvo',
                text: 'Отчество',
                filter: true,
                hidden: true
            },
            {
                dataIndex: 'C_Fio',
                text: 'ФИО',
                filter: true,
                hidden: false,
                minWidth: 250
            },
            {
                dataIndex: 'C_Code',
                text: 'Номер ЛС',
                filter: true,
                hidden: false,
                minWidth: 100
            },
            {
                dataIndex: 'C_Address',
                text: 'Адрес',
                filter: true,
                hidden: false,
                flex: 1
            },
            {
                dataIndex: 'C_Address_Short',
                text: 'Короткий адрес',
                filter: true,
                hidden: true
            },
            {
                dataIndex: 'C_Appart_Number',
                text: '№ помещения',
                filter: true,
                hidden: false,
                align: 'center'
            },
            {
                dataIndex: 'C_Telephone',
                text: 'Телефон',
                filter: true,
                hidden: false
            },
            {
                dataIndex: 'C_Email',
                text: 'Электронная почта',
                filter: true,
                hidden: false
            },
            {
                dataIndex: 'C_INN',
                text: 'ИНН',
                filter: true,
                hidden: false
            },
            {
                dataIndex: 'C_Note',
                text: 'Описание',
                filter: true,
                hidden: false
            },
            {
                xtype: 'booleancolumn',
                dataIndex: 'B_Disable',
                text: 'Отключено',
                filter: true,
                hidden: false,
                trueText: 'Да',
                falseText: 'Нет'
            }
    ],

    bbar: {
        xtype: 'pagingtoolbar',
        ui: 'white-paging',
        cls: 'white-scroll',
        scrollable: 'x',

        plugins: 'responsive',
        responsiveConfig: {
            'largeItemWidth && largeItemHeight': {
                style: {
                    padding: '25px 0 6px 8px'
                }
            },
            'smallItemWidth || smallItemHeight': {
                style: {
                    padding: '0'
                }
            }
        }
    },

    defaultParams: null,

    privates: {
        /*
         * установить значение параметров по умолчанию
         * @param value {any} значение
         */
        setDefaultParams: function (value) {
            this.defaultParams = value;
        },
        /*
         * возвращаются значение параметров по умолчанию
         */
        getDefaultParams: function () {
            return this.defaultParams;
        }
    }

})