﻿/*
 * элементы задания (маршрута)
 */
Ext.define('ARM.view.subscrs.DocDetails', {
    extend: 'Ext.grid.Panel',
    xtype: 'subscr-doc-details',
    defaultListenerScope: true,

    requires: [
        'ARM.store.Detail',
        'Ext.toolbar.Paging',
        'Core.DefaultFilters',
        'Core.data.proxy.Direct',
    ],
    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeItemWidth: getResponsiveWidth(),
        smallItemWidth: '!largeItemWidth',
        largeItemHeight: getResponsiveHeight(),
        smallItemHeight: '!largeItemHeight'
    },

    cls: 'app-task-items white-scroll',
    loadMask: true,
    ui: 'white',
    style: 'background:white;',

    /**
     * переменная для хранения параметров запроса по умолчанию
     */
    defaultParams: null,

    plugins: [
        {
            ptype: 'defaultfilters',
            pluginId: 'defaultfilters'
        },
        {
            ptype: 'gridfilters'
        },
        {
            ptype: 'responsive'
        }
    ],
    title: '',

    columns: [
        { xtype: 'rownumberer', align: 'center' },
        {
            xtype: 'booleancolumn',
            text: 'Выполнено',
            dataIndex: 'IsDone',
            hideable: false,
            sortable: false,
            draggable: false,
            trueText: 'Да',
            falseText: 'Нет',
            align: 'center',
            filter: true
        },
        {
            text: 'Дата выполнения',
            dataIndex: 'D_Done_Date',
            xtype: 'datecolumn',
            format: 'd.m.Y H:i:s',
            hideable: false,
            sortable: false,
            draggable: false,
            align: 'center',
            width: 170,
            renderer: function (value) {
                return Ext.Date.format(value, 'd.m.Y') + '<span style="margin-left:10px;color:#959595">' + Ext.Date.format(value, 'H:i:s') + '</span>'
            },
            filter: true
        },
        {
            text: 'Владелец',
            dataIndex: 'Owner',
            hideable: false,
            sortable: false,
            draggable: false,
            align: 'left',
            width: 250,
            filter: true
        },
        {
            text: 'ПУ',
            dataIndex: 'C_Device_Number',
            hideable: false,
            sortable: false,
            draggable: false,
            filter: true
        },
        {
            text: 'Исполнитель',
            dataIndex: 'UserFIO',
            hideable: false,
            sortable: false,
            draggable: false,
            align: 'left',
            width: 250,
            filter: true
        },
        {
            text: 'Тип обхода',
            dataIndex: 'TypeName',
            hideable: false,
            sortable: false,
            draggable: false,
            align: 'left',
            width: 250,
            filter: true
        },
        {
            xtype: 'booleancolumn',
            text: 'Принят',
            dataIndex: 'B_Received',
            hideable: false,
            sortable: false,
            draggable: false,
            trueText: 'Да',
            falseText: 'Нет',
            align: 'center',
            filter: true
        },
        {
            xtype: 'booleancolumn',
            text: 'Просрочен',
            dataIndex: 'B_Overdue',
            hideable: false,
            sortable: false,
            draggable: false,
            trueText: 'Да',
            falseText: 'Нет',
            align: 'center',
            filter: true
        }
    ],

    bbar: {
        xtype: 'pagingtoolbar',
        ui: 'white-paging',
        cls: 'white-scroll',
        scrollable: 'x',

        plugins: 'responsive',
        responsiveConfig: {
            'largeItemWidth && largeItemHeight': {
                style: {
                    padding: '25px 0 6px 8px'
                }
            },
            'smallItemWidth || smallItemHeight': {
                style: {
                    padding: '0'
                }
            }
        }
    },

    constructor: function (cfg) {
        cfg.store = Ext.create('ARM.store.ControlPoint', {
            suppressNextFilter: true,
        });
        this.callParent(arguments);
    },

    listeners: {
        render: function (sender) {
            this.databind(this.link, true);
        }
    },

    /*
     * 
     * @param documentId {string} идентификатор документа
     * @param doneCount {number} количество выполненых заданий
     * @param userId {string} идентификатор пользователя
     * @param callback {()=>void} функция обратного вызова
     */
    databind: function (subscrId, showOverdue, callback) {
        var filters = [{
            property: 'F_Subscrs',
            value: subscrId,
            operator: '='
        }];
        if (showOverdue == false) {
            filters.push({
                property: 'B_Overdue',
                value: showOverdue,
                operator: '='
            });
        }
        this.setDefaultParams({
            filter: filters
        });
        var me = this;
        var store = this.getStore();
        store.suppressNextFilter = false;
        store.load({
            callback: callback
        });
    },

    privates: {
        /*
         * установить значение параметров по умолчанию
         * @param value {any} значение
         */
        setDefaultParams: function (value) {
            this.defaultParams = value;
        },
        /*
         * возвращаются значение параметров по умолчанию
         */
        getDefaultParams: function () {
            return this.defaultParams;
        }
    }
});