﻿Ext.define('ARM.view.subscrs.LastCheck', {
    extend: 'Ext.Container',
    xtype: 'subscr-last-check',
    requires: [
        'ARM.view.subscrs.DocDetails'
    ],
    ui: 'white',
    defaultListenerScope: true,
    layout: 'vbox',
    defaults: {
        width: '100%',
    },

    constructor: function (cfg) {
        this.items = [{
            xtype: 'panel',
            ui: 'white',
            padding: 10,
            items: [{
                xtype: 'checkbox',
                boxLabel: 'Выводить просроченные документы',
                inputValue: true,
                uncheckedValue: false,
                value: true,
                handler: 'onFilterChange'
            }]
        }, {
            xtype: 'container',
            layout: 'fit',
            items: {
                xtype: 'subscr-doc-details',
                link: cfg.link,
                emptyText:'Нет созданных документов',
                listeners: {
                    itemclick: 'onDocDetailClick'
                }
            }
        },
        {
            xtype: 'app-task-info',
            itemId: 'docInfo',
            ui: 'white',
            responsiveConfig: {
                'largeInfoWidth': {
                    layout: {
                        type: 'box',
                        vertical: false,
                        pack: 'start',
                        align: 'top'
                    },
                    flex: 1
                },
                'smallInfoWidth': {
                    layout: {
                        type: 'box',
                        vertical: false,
                        pack: 'start',
                        align: 'center'
                    },
                    height: 'auto'
                }
            }
        }
        ]
        this.callParent(arguments)
    },

    /**
     * Вывести информация о маршруте
     */
    onDocDetailClick: function (sender, record) {
        var info = this.down('#docInfo');
        if (info) {
            info.databind(record.get('LINK'), record.get('IsDone'));
        }
    },

    /**
     * Изменение фильтра
     */
    onFilterChange: function (sender,value) {
        var details = this.down('subscr-doc-details');
        if (details) {
            details.databind(this.link, value);
        }
    }
})