﻿Ext.define('ARM.view.subscrs.SubscrsConstoller', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.subscrs',

    onViewRender: function (sender) {
        sender.setIsReady(true);
    },

    /**
     * Вывести карточку потебителя
     */
    onGridItemDblClick: function (sender, record) {
        var cmp = Ext.create('Ext.Window', {
            scrollable: 'y',
            layout: 'fit',
            height: 700,
            width: 900,
            title:'Детализация ЛС',
            items: [
                {
                    xtype: 'app-subscr-detail',
                    link: record.get('LINK'),
                    subscrInfo: record.getData()
                }
            ]
        }).show();
    },

    /**
     * Перезагрузить список потребителей
     */
    onReloadGrid: function () {
        var view = this.getView();
        if (view) {
            var grid = view.getGrid();
            var store = grid.getStore();
            var values = view.getFilterValues();
            var filter = [];
            if (values) {
                if (values.MainDivision != '')
                    filter.push({
                        property: 'S_MainDivision_Ref.LINK',
                        value: values.MainDivision,
                        operator: '='
                    });

                if (values.Division != '')
                    filter.push({
                        property: 'S_Division_Ref.LINK',
                        value: values.Division,
                        operator: '='
                    });

                if (values.SubDivision != '')
                    filter.push({
                        property: 'S_SubDivision_Ref.LINK',
                        value: values.SubDivision,
                        operator: '='
                    });
            }
            grid.setDefaultParams({ filter: filter });
            store.load();
        }
    }
});