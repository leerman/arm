﻿Ext.define('ARM.view.subscrs.SubscrInfo', {
    extend: 'Ext.form.Panel',
    xtype: 'subscrs-info',
    ui: 'white',
    viewModel: {
        data: {}
    },

    items: [{
        title: '',
        ui: 'white',
        scrollable: true,
        items:
            [{
                xtype: 'fieldset',
                border: false,
                ui: 'white',
                defaults: { width: '100%', editable: false, xtype: 'textfield' },
                items: [
			            { name: 'C_Family', fieldLabel: 'Фамилия', bind: { value: '{C_Family}' } },
			            { name: 'C_Name', fieldLabel: 'Имя', bind: { value: '{C_Name}' } },
			            { name: 'C_Otchestvo', fieldLabel: 'Отчество', bind: { value: '{C_Otchestvo}' } },
			            { name: 'C_Address', fieldLabel: 'Адрес', bind: { value: '{C_Address}' } },
			            { name: 'C_Address_Short', fieldLabel: 'Короткий адрес', bind: { value: '{C_Address_Short}' } },
			            { name: 'C_Appart_Number', fieldLabel: '№ помещения', bind: { value: '{C_Appart_Number}' } },
			            { name: 'C_Telephone', fieldLabel: 'Телефон', bind: { value: '{C_Telephone}' } },
			            { name: 'C_Email', fieldLabel: 'Электронная почта', bind: { value: '{C_Email}' } },
			            { name: 'C_INN', fieldLabel: 'ИНН', bind: { value: '{C_INN}' } },
			            { name: 'C_Note', fieldLabel: 'Описание', bind: { value: '{C_Note}' } },
                        { xtype: 'checkboxfield', name: 'B_Additional_Agreement', fieldLabel: 'Наличие доп. соглашения', uncheckedValue: 'off', disabled: true, bind: { value: '{B_Additional_Agreement}' } },
                        { xtype: 'checkboxfield', name: 'B_Power_Attorney', fieldLabel: 'Наличие доверенности на съем показаний', uncheckedValue: 'off', disabled: true, bind: { value: '{B_Power_Attorney}' } },
			            { xtype: 'checkboxfield', name: 'B_Disable', fieldLabel: 'Отключено', uncheckedValue: 'off', disabled: true, bind: { value: '{B_Disable}' } }
                ]
            }]
    }],

    listeners: {
        render: function () {
            this.getViewModel().setData(this.subscrInfo);
        }
    }
});