﻿/*
 * страница недоступна
 */
Ext.define('ARM.view.access.Access', {
    extend: 'Ext.Panel',
    xtype: 'app-access',
    layout: 'fit',

    html: '<center style="margin-top:20px"><b>Страница недоступна</b></center>'
});