﻿/*
 * комопнент для вывода карточки пользователя - это базовый компонент
 */
Ext.define('ARM.shared.UserCard', {
    extend: 'Ext.Panel',
    ui: 'card',
    defaultListenerScope: true,

    mixins: ['Ext.mixin.Responsive'],

    responsiveFormulas: {
        largeUserCard: getResponsiveHeight(),
        smallUserCard: '!largeUserCard'
    },

    // путь к генератору изображений, пример http://localhost:3000/icon-generator?c=000000&t=phone
    icon_generator: '',
    // режим отображения (minimize, maximize)
    mode: 'minimize',

    model: null, // данные

    collapsed: true,
    responsiveConfig: {
        'largeUserCard': {
            style: {
                padding: '10px'
            }
        },
        'smallUserCard': {
            style: {
                padding: '5px'
            }
        }
    },
    width: '100%',

    /**
     * текст при отстуствие текста
     */
    UNKNOW_TEXT: 'не указан',

    /**
     * событие действия в шапке
     */
    EVENT_ACTION_HEADER: 'actionheader',

    is_online: false,
    /*
     * использовать обработчик при нажатие на шапку
     */
    is_header_click: true,

    viewModel: {
        data: {
            online: false, // устройство находиться в online или offline
            name: '', // фио
            photo: '', // путь к аватарке
            uuid: '', // идентификатор устройства
            phone: '', // номер телефона
            email: '', // эл. почта
            color: '', // цвет пользователя
            status: '', // статус пользователя; online, no_geo, offline
            access: false, // указывается доспупен ли компонент для обработки данных на карте
            personOnly: false
        },
        formulas: {
            getPhoneColor: function (get) {
                switch (get('status')) {
                    case 'online':
                        return '1db655';

                    case 'no_geo':
                        return 'd5d523';

                    case 'offline':
                        return 'e9432d';
                }
            },
            getStatusTooltip: function (get) {
                switch (get('status')) {
                    case 'online':
                        return 'Онлайн';

                    case 'no_geo':
                        return 'Геолокация отключена';

                    case 'offline':
                        return 'Оффлайн';
                }
            },
            isGeoExists:function(get) {
                if (get('status') === 'no_geo')
                    return false;
                return true;
            }
        }
    },

    /**
     * установка режима отображения
     * @param val {string} режим (maximize, maximize)
     */
    setMode: function (val, suspend) {
        this.mode = val;
        if (val == 'maximize') {
            this.expand(false);
            this.addCls('maximize');
        }
        else {
            this.collapse(null, false);
            this.removeCls('maximize');
        }
        if (!suspend)
            this.fireEvent(this.mode, this);
    },

    /*
     * установка видимости
     * @param value {boolean} значение
     */
    setOnline: function (value, geoDate) {
        this.is_online = value;
        var vm = this.getViewModel();
        vm.set('online', value);

        if (value == true) {
            if (geoDate && new Date(geoDate) > Ext.getCurrentApp().getDataProvider().getToday(new Date())) {
                vm.set('status', 'online');
            } else {
                vm.set('status', 'no_geo');
            }
        } else {
            vm.set('status', 'offline');
        }
    },

    /*
     * установить доступ
     * @param value {boolean} значение
     */
    setAccess: function (value) {
        var vm = this.getViewModel();
        vm.set('access', value);
    },

    /*
     * возвращается режим отображения
     */
    getMode: function () {
        return this.mode;
    },

    constructor: function (cfg) {
        Ext.apply(this, cfg);

        cfg.header = {
            plugins: 'responsive',
            responsiveConfig: {
                'largeUserCard': {
                    height: 56
                },
                'smallUserCard': {
                    height: 45
                }
            },
            bind: {
                title: '{name}'
            },
            titlePosition: 1,
            items: [
                {
                    xtype: 'button',
                    border: false,
                    style: {
                        paddingLeft: 0
                    },
                    bind: {
                        icon: Ext.String.format(this.icon_generator, '{getPhoneColor}', 'phone'),
                        tooltip: '{getStatusTooltip}'
                    }
                },
                {
                    xtype: 'app-action-button',
                    ui: 'geo-link',
                    action: 'user',
                    bind: {
                        icon: Ext.String.format(this.icon_generator, '{color}', 'user'),
                        hidden: '{!isGeoExists}'
                    },
                    handler: 'onAction',
                    tooltip: 'Показать местоположение'
                },
                {
                    xtype: 'app-action-button',
                    ui: 'geo-link',
                    action: 'route',
                    bind: {
                        icon: Ext.String.format(this.icon_generator, '{color}', 'route'),
                        hidden: '{!isGeoExists}'
                    },
                    handler: 'onAction',
                    tooltip: 'Показать маршрут'
                },
                {
                    xtype: 'app-action-button',
                    ui: 'geo-link',
                    action: 'tasks',
                    bind: {
                        icon: Ext.String.format(this.icon_generator, '{color}', 'task_16'),
                        hidden: '{!access}'
                    },
                    handler: 'onAction',
                    tooltip: 'Показать задания'
                },
                {
                    xtype: 'app-action-button',
                    ui: 'geo-online',
                    action: 'online',
                    cls: 'user-offline',
                    bind: {
                        iconCls: 'x-fa fa-eye-slash',
                        hidden: '{access}'
                    },
                    hidden: true,
                    handler: 'onAction',
                    border: false,
                    tooltip: 'Пользователь находится в offline'
                }
            ],
            listeners: {
                click: 'onHeaderClick'
            }
        };


        this.callParent(arguments);
        // обработка модели
        var vm = this.getViewModel();
        vm.set('name', this.model.get('C_Fio'));

        var photo = this.model.get('C_Photo');
        if (photo) {
            photo = Ext.String.format(Ext.getConf('ws_url') + Ext.getConf('virtualDirPath') + '/image?type=avatar&size=small&id={0}', this.model.get('LINK'));
        }
        vm.set('photo', photo || Ext.getCurrentApp().toAbsolutePath('resources/images/avatar.png')); // здесь статичная аватарка, потом нужно предусмотреть путь к серверу
        vm.set('uuid', this.model.get('uuid') || this.UNKNOW_TEXT);
        vm.set('phone', this.model.get('C_Telephone') || this.UNKNOW_TEXT);
        vm.set('email', this.model.get('C_Email') || this.UNKNOW_TEXT);
        var color = Ext.getGlobalParams('C_Default_Color') || this.model.get('C_Icon_Color');
        if (!color || (color && !/^[0-9|a-f]{3,6}$/gi.test(color)))
            color = 'ff0000';
        vm.set('color', color);
        vm.set('status', 'offline');
    },

    items: [
        {
            cls: 'user-card-body',
            margin: '0 10px 0 10px',
            padding: '15px 0 15px 0',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                {
                    width: 85,
                    bind: {
                        html: '<div style="position:relative;width:62px;height:62px;border-radius:31px;overflow:hidden;" title="{name}">' +
                        '<img style="position:absolute;max-width:62px;min-height:62px" alt="" src="{photo}" />'
                        + '</div>'
                    }
                },
                {
                    flex: 1,
                    bind: {
                        html: '<div class="user-card-info">' +
                            '<p data-qtip="Идентификатор устройства"><span>Серийный номер</span> {uuid}</p>' +
                            '<p><span>Телефон</span> <a href="tel:{phone}">{phone}</a></p>' +
                            '<p><span>E-mail</span> <a href="mailto:{email}">{email}</p>' +
                            '</div>'
                    }
                }
            ]
        }
    ],

    dockedItems: [
        {
            cls: 'user-card-footer',
            dock: 'bottom',
            xtype: 'toolbar',
            ui: 'footer',

            plugins: 'responsive',
            responsiveConfig: {
                'largeUserCard': {
                    style: {
                        padding: '0 10px 10px 10px'
                    }
                },
                'smallUserCard': {
                    style: {
                        padding: '0 10px 0 10px'
                    }
                }
            },
            defaults: {
                plugins: 'responsive',
                responsiveConfig: {
                    'largeUserCard': {
                        margin: '35px 0 0 0'
                    },
                    'smallUserCard': {
                        margin: '20px 0 0 0'
                    }
                },
            },

            items: [
                {
                    xtype: 'button',
                    text: 'Закрыть',
                    ui: 'action-transparent',
                    tooltip: 'Свернуть',
                    handler: 'onClose'
                },
                '->',
                {
                    xtype: 'button',
                    iconCls: 'x-fa fa-commenting-o',
                    tooltip: 'Отправить сообщение',
                    style: {
                        'border-radius': '20px'
                    },
                    bind: {
                        disabled: '{!online}'
                    },
                    handler: 'onChat'
                }
            ]
        }
    ],

    getModel: function () {
        return this.model;
    },

    privates: {
        isActionClick: false,
        /*
         * написать сообщение
         */
        onChat: function () {
            var model = this.getModel();
            if (model) {
                var chat = Ext.getCurrentApp().getChat();
                if (chat) {
                    chat.sendBy(model);
                    chat.show();
                }
            }
        },
        /**
         * обработчик нажатия на кнопку закрыть
         */
        onClose: function () {
            this.setMode('minimize');
        },
        /**
         * нажатие на шапку панели
         */
        onHeaderClick: function (sender) {
            if (this.isActionClick == false) {
                var panel = sender.container.component;

                var mode = panel.getMode();
                if (mode == 'minimize')
                    panel.setMode('maximize');
                else
                    panel.setMode('minimize');
            } else {
                this.isActionClick = false;
            }
        },

        /*
         * обработчик действия
         * @param btn {any} кнопка
         */
        onAction: function (btn) {
            this.isActionClick = true;
            this.fireEvent(this.EVENT_ACTION_HEADER, btn.getAction(), btn, this.model, this)
        }
    }
});