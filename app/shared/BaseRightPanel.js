﻿/*
 * базовый класс для правых панелей 
 */
Ext.define('ARM.shared.BaseRightPanel', {
    extend: 'Ext.Panel',

    xtype: 'base-right-panel',

    container: null,

    options: null,

    /*
     * возвращается родительский контейнер
     */
    getContainer: function () {
        return this.container;
    },

    /*
     * возвращаются дополнительные опции
     */
    getOptions: function(){
        return this.options;
    },

    setOptions: function (value) {
        this.options = value;
    },

    /**
     * обновление содержимой панели
     * @param options {any} дополнительные опции
     * @param callback {()=>void} функция обраного вызова
     */
    refreshPanel: function (options, callback) {

    }
});