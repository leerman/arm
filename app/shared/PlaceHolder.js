﻿/*
 * Базовый класс для вывода информации 
 */
Ext.define('ARM.shared.PlaceHolder', {
    extend: 'Ext.Panel',
    xtype: 'app-placeholder',
    cls: 'app-placeholder',
    
    requires: [
        'Core.ProgressBar'
    ],

    mixins: [
        'Core.security.Security'
    ],

    /*
     * событие готовности компонента
     */
    EVENT_ONREADY: 'placeholderready',

    routeOptions: null,

    /*
     * событие установки хранилища
     */
    EVENT_SET_STORE: 'setuserstore',

    userStore: null,
    isReady: false,

    /*
     * элемент для вывода процесса обработки
     */
    progressBar: null,

    yandexDataWorker: null, // worker для работы в параллельном потоке

    onRender: function () {
        Ext.getCurrentApp().on('security', this.onSecurity, this);
        this.callParent(arguments);
    },

    /*
     * обработчик безопасности
     */
    onSecurity: function () {
        // тут нужно проверить безопасность
        if (this.isAccess && this.isAccess() == false) {
            location.href = '#access';
        }
    },

    /*
     * возвращаются дополнительные парамеры которые были переданы через роутинг
     */
    getRouteOptions: function () {
        return this.routeOptions;
    },

    /**
     * возвращается воркер
     */
    getYandexDataWorker: function () {
        if (this.yandexDataWorker)
            return this.yandexDataWorker;
        else {
            return this.yandexDataWorker = Ext.create('Core.DataWorker', {
                path: Ext.getCurrentApp().toAbsolutePath('resources/workers/yandexWorker.js')
            });
        }
    },

    /*
     * устанавливаем хранилище
     */
    setUserStore: function (userStore) {
        this.userStore = userStore;
        // добавляем событие что были переданы данные о пользователях
        this.fireEvent(this.EVENT_SET_STORE, this, userStore);
    },

    /*
     * установить готовность компонента
     */
    setIsReady: function () {
        this.isReady = true;
        var main = this.up('app-main');
        main.fireEvent(this.EVENT_ONREADY, main.down('app-main-filter'));
    },

    /*
     * возвращается главный фильтр
     */
    getMainFilter: function () {
        var main = this.up('app-main');
        if (main)
            return main.down('app-main-filter');

        return null;
    },

    /*
     * возвращается правая панель
     */
    getRightPanel: function () {
        var main = this.up('app-main');
        if (main)
            return main.lookupReference('rightpanel');

        return null;
    },

    /*
     * вывод компонента со статусом выполнения задания
     * @param text {string} текст задания
     * @param percent {number} процент выполнения
     */
    showProgressBar: function (text, percent) {
        if (this.rendered) {
            if (!this.progressBar) {
                this.progressBar = this.add({
                    height: 30,
                    xtype: 'core-progressbar',
                    bgcolor: '#454545',
                    color: '#269eff',
                    textColor: 'white',
                    dock: 'top',
                    margin: '0 0 5px 0'
                });
            }
            if (text)
                this.progressBar.setText(text);
            if (percent)
                this.progressBar.setProgress(percent);

            if (!text && !percent) {
                this.progressBar.setProgress(0);
            }
        }
    },

    /*
     * возвращается компонент для вывода списка пользователей
     */
    getUserListComponent: function(){
        var view = Ext.getCurrentApp().getInitView();
        if (view) {
            var list = view.lookupReference('app-user-list');
            if (list) {
                return list;
            }
        }

        return null;
    },

    /*
     * возвращается компонент для вывода фильтр
     */
    getFilterComponent: function () {
        var view = Ext.getCurrentApp().getInitView();
        if (view) {
            var filter = view.lookupReference('app-task-workpanel-filter');
            if (filter) {
                return filter;
            }
        }

        return null;
    },

    /*
     * устанавливаем значения по умолчанию для view
     * @param options {any} опции для обновления
     */
    //setDefaultSettingView: function (options) {
    //    if (!this.getRouteOptions()) {
    //        // данные параметры устанавливаются только тогда, когда нужно прокинуть фильтр с другого раздела

    //        var main = this.up('app-main');
    //        if (main) {
    //            var list = main.down('app-user-list');
    //            if (list) {
    //                //list.setCardView(options.cardView); // !!! тут заменяем карточку пользователя
    //                // теперь нужно найти данные по фильтру
    //                var filter = main.down('app-main-filter');

    //                if (filter) {
    //                    var values = filter.getLastValues();
    //                    if (values) { // устанавливаем условие фильтра, если они были до этого
    //                        main.applyFilter(filter, values);
    //                    }
    //                }
    //            }
    //        }
    //    }
    //},

    /*
     * передача дополнительных параметров через роутинг
     * @param options {any} дополнительные параметры
     */
    onRouteOptions: function (options) {
        this.routeOptions = options;
    },

    destroy: function () {
        Ext.getCurrentApp().un('security', this.onSecurity, this);
        if (this.progressBar)
            this.progressBar.destroy();
        this.progressBar = null;
        this.callParent(arguments);
    }
});