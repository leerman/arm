﻿/*
 * модуль дианостики приложения
 */
Ext.define('ARM.Diagnostic', {
    extend: 'Ext.Container',
    xtype: 'app-diagnostic',

    requires: [
        'Core.ProgressBar'
    ],

    items: [
        {
            xtype: 'core-progressbar',
            bgcolor: '#454545',
            color: '#269eff',
            itemId: 'bar',
            height: 2
        }
    ],
    //максимально допустимый рассинхрон времени (в минутах)
    MAX_SQL_WEB_TIME_DIFF: 10,

    /*
     * начала процесса
     */
    start: function () {
        var me = this;
        this.show();
        _.series(this.tasks, function () {

            // нужно проверить сообщения об ошибках
            if (me.messages.length > 0) {

                var list = [];
                Ext.each(me.messages, function(i){
                    list.push('<li>'+i+'</li>');
                });

                Ext.Msg.show({
                    title: 'Автоматическая диагностика',
                    message: 'При диагностики приложения выявлены следующие проблемы:<ul>' + list + '</li>',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
            }
            me.messages = [];
            me.successCount = 0;
            me.setProgress(0);
            me.hide();
        });
    },

    /*
     * переопределен
     */
    onRender: function () {
        this.callParent(arguments);
        if (Ext.getConf('mode') == 'production') {
            var me = this;
            this.tasks = [
                function (callback) {
                    me.imageUrl_Test(me.tasks.length, callback);
                },
                function (callback) {
                    me.timediff_WebSql_Test(callback);
                }
            ];
            this.start();
        }
    },

    destroy: function () {
        this.successCount = 0;
        this.tasks = [];
        this.callParent(arguments);
    },

    privates: {
        
        // хранение количество выполненых тестов
        successCount: 0,
        // задания
        tasks: [],
        // массив сообщений
        messages: [],

        /*
         * устанавливаем строку со статусом
         * @param value {any} процент выполнения
         */
        setProgress: function (value) {
            var bar = this.getComponent('bar');
            if (bar)
                bar.setProgress(value);
        },

        /*
         * обработчик выполнения задания
         * @param allCount {number} кол-во заданий
         */
        onSuccessed: function (allCount) {
            this.successCount++;

            this.setProgress((this.successCount * 100) / allCount);
        },

        /*
         * проверка синхронизацию времени на WEB и SQL сервере
         * @param callback {()=>void} функция обратного адреса
         */
        timediff_WebSql_Test: function (callback) {
            var me = this;
            PN.Util.MobileService.Domain.Service.SqlServer.GetDate(function(sqlString) {
                PN.Util.It.Module.Extjs.ExtjsDirectRpc.GetDateTime(function (webString) {
                    var sqlTime = new Date(sqlString);
                    var webTime = new Date(webString);
                    var deltaTime = Math.abs(sqlTime - webTime);
                    // если разница больше MAX_SQL_WEB_TIME_DIFF минут то рассинхрон
                    if (Math.floor(deltaTime / (1000 * 60)) > me.MAX_SQL_WEB_TIME_DIFF) {
                        me.messages.push('Время WEB сервера и SQL сервера не синхронизировано: </br>' +
                            'SQL: ' + sqlTime.toLocaleString() + '</br>WEB: ' + webTime.toLocaleString());
                    }
                    if (typeof callback == 'function')
                        callback();
                });
            });
        },

        /*
         * тест проверки адреса в глобальных настройках
         * @param allCount {numder} общее кол-во заданий
         * @param callback {()=>void} функция обратного адреса
         */
        imageUrl_Test: function (allCount, callback) {
            var me = this;

            var dataProvider = Ext.getCurrentApp().getDataProvider();
            dataProvider.getGlobalParams(function (config) {
                if (config) {
                    var address = Ext.getConf('REMOTING_ADDRESS');
                    var url = config.get('C_Url');

                    var addresses = [address];

                    Ext.each(Ext.getConf('validRemotingAddress'), function (i) {
                        addresses.push(i);
                    });

                    var validUrl = false;
                    for (var i = 0; i < addresses.length; i++) {
                        var address = addresses[i];
                        if (url.indexOf(address) >= 0) {
                            validUrl = true;
                            break;
                        }
                    }

                    if (validUrl == false) {
                        me.messages.push('Возможно, адрес для просмотра изображений в БД (C_Url) указан неверно. Текущая ссылка ' + url);
                    }
                }
                me.onSuccessed(allCount);
                if (typeof callback == 'function')
                    callback();
            });
        }
    }
});