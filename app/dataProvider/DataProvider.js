﻿/*
 * базовый класс для провайдера данных
 */
Ext.define('ARM.dataProvider.DataProvider', {
    extend: 'Ext.Component',

    MAX_COUNT: 10000,
    POINT_CORRECT: 0.000100, // корректировка положения

    getCurrentDate: function () {
        return new Date();
    },

    /*
     * текущий день
     * @param date {date} дата
     */
    getToday: function (date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    },

    /*
     * конец дня
     * @param date {date} дата
     */
    getTodayEnd: function (date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 0);
    },

    /**
     * возвращаются уникальные записи по полю  
     * @param items {any[]} список записей
     * @param distinctField {string} 
     */
    getDistincRecord: function (items, distinctField) {
        var results = [];
        Ext.each(items, function (item) {
            if (results.filter(function (i) {
                return i.get(distinctField) == item.get(distinctField);
            }).length == 0)
                results.push(item);
        });

        return results;
    }
});