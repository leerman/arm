﻿/*
 * провайдер тестовых данных для приложения
 */
Ext.define('ARM.dataProvider.DefaultDataProvider', {
    extend: 'ARM.dataProvider.DataProvider',


    getConfigs: function (callback) {
        var store = Ext.create('ARM.store.Configs');
        store.load({
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items[0]);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращаются глобальные настройки
     * @param callback {(any)=>void} функция обратного вызова
     */
    getGlobalParams: function (callback) {
        var store = Ext.create('ARM.store.GlobalParams');
        store.load({
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items[0]);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращается информация о пользователе
     * @param link {string} идентификатор пользователя
     * @param callback {(any)=>void} функция обратного вызова
     */
    getUserInfo: function (link, callback) {
        var store = Ext.create('ARM.store.User');
        store.load({
            params: {
                filter: [
                    {
                        property: 'LINK',
                        value: link
                    }
                ]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items[0]);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /**
     * доступен ли текущий идентификатор
     * @param uuid {string} идентификатор устройства
     * @param callback {()=>void} функция обратного вызова 
     */
    uuidExists: function (uuid, callback) {
        if (uuid) {
            var store = Ext.create('ARM.store.User');
            store.load({
                params: {
                    filter: [
                        {
                            property: 'uuid',
                            value: uuid
                        }
                    ]
                },
                callback: function (items) {
                    if (typeof callback === 'function') {
                        if (items && Array.isArray(items))
                            callback(items.length != 0, items[0]);
                        else {
                            callback(true);
                        }
                    }
                }
            });
        } else {
            if (typeof callback === 'function') {
                callback(false);
            }
        }
    },

    /*
     * возвращаются даты с гео-данными по указанному пользователю
     * @param userId {string} идентификатор пользователя
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getTaskDate: function (userId, callback) {
        var store = Ext.create('ARM.store.GetDocDetailDays');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                params: [userId]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращаются даты с гео-данными по указанному пользователю
     * @param userId {string} идентификатор пользователя
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getGeoDate: function (userId, callback) {
        var store = Ext.create('ARM.store.GetGeoLocationDays');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                params: [userId]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * вовзвращается статистика по пользователю
     * @param userId {string} идентификатор пользователя
     * @param date {Date} дата
     * @param callback {()=>void} функция обратного вызова
     */
    getStat: function (userId, date, callback) {
        var store = Ext.create('ARM.store.GetStatistics');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                params: [userId, date]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * статистика по маршрутам 
     * @param userId {string} идентификатор пользователя
     * @param date {Date} дата
     * @param is_person {boolean} является физ. лицом
     * @param is_done {boolean} выполнено
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getRoutesStat: function (userId, date, is_person, is_done, callback) {
        var store = Ext.create('ARM.store.GetDocStatistics');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                params: [userId, date || new Date(), is_person, is_done]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items)) {
                        var results = [];
                        items.forEach(function (i) {
                            results.push({
                                id: i.get('LINK'),
                                number: i.get('C_Number'),
                                name: i.get('C_Name'),
                                today: i.get('N_Done_Today'),
                                done: i.get('N_Done'), // выполнено
                                old: i.get('N_NotDone'), // осталось
                                b_done: i.get('B_Done')
                            });
                        });
                        callback(results);
                    }
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращаются показания
     * @param detailId {string} строка документа
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getMeters: function (detailId, callback) {
        var store = Ext.create('ARM.store.Meter');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                filter: [
                    { property: 'F_Doc_Details', value: detailId, operator: '=' }
                ]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращаются пломбы
     * @param detailId {string} строка документа
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getConnSeals: function (detailId, callback) {
        var store = Ext.create('ARM.store.ConnSeals');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                filter: [
                    { property: 'F_Doc_Details', value: detailId, operator: '=' }
                ]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращаются выходные документы строки документа
     * @param detailId {string} идентификатор строки документа
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getDocOuts: function (detailId, callback) {
        var store = Ext.create('ARM.store.DocOut');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                select: store.getSelectFields(),
                filter: [
                    { property: 'F_Doc_Details', value: detailId, operator: '=' }
                ]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращется список файлов по исходящему документу
     * @param docoutId {string} исходящий документ
     * @param callback {(any[])=>void} функция обратного вызова
     */
    getFilesByDocout: function (docoutId, callback) {
        var store = Ext.create('ARM.store.File');
        store.load({
            limit: this.MAX_COUNT,
            params: {
                filter: [
                    { property: 'F_Docs_Out', value: docoutId, operator: '=' }
                ]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращается документ с информацией
     * @param documentId {string} идентификатор документа
     * @param callback {()=>void} функция обратного вызова 
     */
    getDocument: function (documentId, callback) {
        var store = Ext.create('ARM.store.Document');
        store.load({
            params: {
                filter: [
                    { property: 'LINK', value: documentId }
                ]
            },
            callback: function (items) {
                if (typeof callback === 'function') {
                    if (items && Array.isArray(items))
                        callback(items[0]);
                    else {
                        callback(null);
                    }
                }
            }
        });
    },

    /*
     * возвращается статистика по пользователю
     * @param filterValues {any} условия фильтрации
     * @param callback {()=>void} функция обратного вызова
     */
    getUserStatistic: function (filterValues, callback) {
        this.getStat(filterValues.User, new Date(), function (statdata) {
            var result = {
                date: new Date(),
                person: {
                    done_today: 0, // завершено сегодня
                    done_total: 0, // всего завершено
                    old: 0, // осталось
                    all: 0 // общее кол-во
                },
                // ЮЛ
                noperson: {
                    done_today: 0, // завершено сегодня
                    done_total: 0, // всего завершено
                    old: 0, // осталось
                    all: 0 // общее кол-во
                }
            };

            statdata.forEach(function (i) {
                result.person.done_today += i.get('N_Done_Today_PE');
                result.noperson.done_today += i.get('N_Done_Today_EE');

                result.person.done_total += i.get('N_Done_PE');
                result.noperson.done_total += i.get('N_Done_EE');

                result.person.old += i.get('N_NotDone_PE');
                result.noperson.old += i.get('N_NotDone_EE');

                result.person.all += i.get('N_Total_PE');
                result.noperson.all += i.get('N_Total_EE');
            });

            if (typeof callback == 'function') {
                callback(result);
            }
        });
    },

    /**
     * возвращается список заданий привязанных к пользователю
     * @param uuid {string} идентификатор устройства
     * @param callback {()=>void} функция обратного вызова
     */
    getUserTasks: function (uuid, date, callback) {
        var store = Ext.create('ARM.store.UserTasks');
        store.load({
            params: {
                limit: this.MAX_COUNT,
                filter: [
                    { property: 'DocDate', value: date, operator: '<=' },
                    { property: 'DocDateEnd', value: date, operator: '>=' },
                    { property: 'PhoneIMEI', value: uuid },
                    { property: 'IsTask', value: true } // !!! может быть эта переменная не нужна
                ],
                sort: [{ property: 'DocDate', direction: 'ASC' }]
            },
            callback: function (items) {
                if (typeof callback === 'function')
                    callback(items);
            }
        });
    },

    /**
     * возвращается список заданий привязанных к пользователям
     * @param users {string[]} идентификаторы пользователей
     * @param callback {()=>void} функция обратного вызова
     */
    getUsersTasks: function (users, callback) {
        var store = Ext.create('ARM.store.UserTasks');
        store.load({
            params: {
                params: [users],
                limit: this.MAX_COUNT
            },
            callback: function (items) {
                if (typeof callback === 'function')
                    callback(items);
            }
        });
    },

    /**
     * возвращается маршрут пользователя с указанную дату
     * @param uuid {string} идентификатор устройства
     * @param date {date} дата
     * @param callback {(ARM.model.GeoLocation[])=>void} функция обратного вызова
     * @param maxCount {int} дополнительный параметр для указания количества возвращаемых данных
     */
    getUserRoutes: function (uuid, date, callback, maxCount) {
        var me = this;
        var store = Ext.create('ARM.store.GeoLocation');
        store.load({
            params: {
                limit: maxCount || this.MAX_COUNT,
                filter: [
                    { property: 'uuid', value: uuid },
                    { property: 'S_Date', value: this.getToday(date), operator: '>=' },
                    { property: 'S_Date', value: this.getTodayEnd(date), operator: '<=' }
                ],
                sort: [{ property: 'S_Date', direction: 'ASC' }]
            },
            callback: function (items) {
                if (items && Array.isArray(items)) {
                    var results = [];

                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];

                        var N_Latitude = item.get('N_Latitude');
                        var N_Longitude = item.get('N_Longitude');

                        if (results.filter(function (_item) {
                            return me.isPointChange(N_Latitude, N_Longitude, _item);
                        }).length == 0) {
                            results.push(item);
                        }
                    }

                    if (callback)
                        callback(results);
                } else {
                    if (callback)
                        callback([]);
                }
            }
        });
    },

    /**
     * возвращаются маршруты пользователя за сегодня
     * @param list {string[]} идентификаторы устройств
     * @param callback {(ARM.model.GeoLocation[])=>void} функция обратного вызова
     */
    getUsersRoutes: function (list, callback) {
        var me = this;
        var store = Ext.create('ARM.store.GetGeoLocationsToday');
        store.load({
            params: {
                limit: 0,
                params: [list]
            },
            callback: function (items) {
                if (items && Array.isArray(items)) {
                    var tmpItems = {};
                    items.forEach(function (i) {
                        var uuid = i.get('uuid');
                        if (uuid) {
                            if (!tmpItems[uuid])
                                tmpItems[uuid] = [];
                            tmpItems[uuid].push(i);
                        }
                    });

                    var results = [];

                    for (var n in tmpItems) {
                        var parts = [];
                        var items = tmpItems[n];
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];

                            var N_Latitude = item.get('N_Latitude');
                            var N_Longitude = item.get('N_Longitude');

                            if (parts.filter(function (_item) {
                                return me.isPointChange(N_Latitude, N_Longitude, _item);
                            }).length == 0) {
                                parts.push(item);
                            }
                        }

                        parts.forEach(function (j) {
                            results.push(j);

                            var uuid = j.get('uuid');
                            // добавляем индексирование
                            if (!results[uuid]) {
                                results[uuid] = [];
                            }
                            results[uuid].push(results[results.length - 1]);
                        });
                    }

                    if (callback)
                        callback(results);
                } else {
                    if (callback)
                        callback([]);
                }
            }
        });
    },

    /**
     * Вернуть список областей контроля
     * @param callback {(ARM.model.Region[])=>void} функция обратного вызова
     */
    getRegions: function (callback) {
        var store = Ext.create('ARM.store.Regions');
        store.load({
            params: {
                limit: this.MAX_COUNT
            },
            callback: function (items) {
                if (typeof callback === 'function')
                    callback(items);
            }
        });
    },

    /**
     * Добавить область контроля
     * @param {any} region - область контроля
     * @param callback {()=>void} функция обратного вызова
     */
    addRegion: function (region, callback) {
        var store = Ext.create('ARM.store.Regions');
        store.add(region);
        store.sync({
            callback: function (result) {
                if (typeof callback == 'function') {
                    callback()
                }
            }
        });
    },

    /**
     * Удалить область контроля
     * @param {number} link - идентификатор региона
     * @param callback {()=>void} функция обратного вызова
     */
    removeRegion: function (link, callback) {
        var store = Ext.create('ARM.store.Regions');

        store.load({
            params: {
                limit: this.MAX_COUNT
            },
            callback: function (result) {
                var record = store.getById(link);
                store.remove(record);
                store.sync({
                    callback: function () {
                        if (typeof callback == 'function')
                            callback();
                    }
                });
            }
        });
    },

    /**
     * Обновить регион
     * @param {number} link - идентификатор региона
     * @param {any} region - данные региона
     * @param callback {()=>void} функция обратного вызова
     */
    updateRegion: function (link, region, callback) {
        var store = Ext.create('ARM.store.Regions');

        store.load({
            params: {
                limit: this.MAX_COUNT
            },
            callback: function (result) {
                var record = store.getById(link);
                for (var r in region) {
                    record.set(r, region[r]);
                }
                store.sync({
                    callback: function () {
                        if (typeof callback == 'function')
                            callback();
                    }
                });
            }
        });
    },

    /**
     * Вернуть список пользователей
     * @param callback {()=>void} функция обратного вызова
     */
    getUsers: function (params, callback) {
        var store = Ext.create('ARM.store.User');

        if (!params)
            params = {};

        params.limit = this.MAX_COUNT;

        store.load({
            params: params,
            callback: function (result) {
                if (typeof callback == 'function')
                    callback(result);
            }
        });
    },

    /**
     * Вернуть всех обходчиков
     */
    getWalkers: function (callback) {
        var store = Ext.create('ARM.store.GetWalkersQuery');

        store.load({
            params: {
                limit: this.MAX_COUNT
            },
            callback: function (result) {
                if (typeof callback == 'function')
                    callback(result);
            }
        });
    },

    /**
     * Привязать пользователя к региону
     * @param user {any} обходчик
     * @param regionLink {number} идентификатор региона
     * @param callback {()=>void} функция обратного вызова
     */
    changeRegionUser: function (user, regionLink, callback) {
        var store = Ext.create('ARM.store.Regions');

        store.load({
            params: {
                limit: this.MAX_COUNT
            },
            callback: function (result) {
                var record = store.getById(regionLink);
                var ur = user;
                record.set('F_Managers', user);

                store.sync({
                    callback: function () {
                        if (typeof callback == 'function')
                            callback();
                    }
                });
            }
        });
    },

    /**
     * Вернуть неназначенные задания
     * @param users {string[]} идентификаторы обходчиков
     * @param callback {()=>void} функция обратного вызова
     */
    getUnAssignedTasks: function (users, divisions, callback) {

        /**
         * Проверить заходит ли документ в выделенные область
         */
        var checkDivisions = function (itemMain, itemDiv, itemSub, itemNet) {
            var div = divisions;
            if (div.S_MainDivision >= 0 && div.S_Division == 0 && div.S_SubDivision == 0 && div.S_MainDivision == itemMain ||
            div.S_MainDivision >= 0 && div.S_Division > 0 && div.S_SubDivision == 0 && div.S_MainDivision == itemMain && div.S_Division == itemDiv ||
            div.S_MainDivision >= 0 && div.S_Division > 0 && div.S_SubDivision > 0 && div.S_MainDivision == itemMain && div.S_Division == itemDiv && div.S_SubDivision == itemSub)
                return true;
            return false;
        }

        var store = Ext.create('ARM.store.AppointByDocs');
        store.load({
            params: {
                params: [users],
                limit: this.MAX_COUNT
            },
            callback: function (items, operation, success) {
                if (typeof callback === 'function') {
                    if (success == true) {
                        var tasks = [];
                        items.forEach(function (item) {
                            if (checkDivisions(item.get('S_MainDivision'), item.get('S_Division'), item.get('S_SubDivision'), item.get('S_Networks'))) {
                                var links = item.get('DetailLinks').split(',');
                                for (var i = 0; i < links.length; i++) {
                                    tasks.push(Ext.create('ARM.model.Task', {
                                        LINK: links[i],
                                        Address: item.get('PointAddress'),
                                        N_Latitude: item.get('N_Latitude'),
                                        N_Longitude: item.get('N_Longitude'),
                                        DocName: item.get('Doc'),
                                        StatusName: item.get('DocStatus'),
                                        UserFIO: item.get('UserFIO'),
                                        UserId: item.get('F_Users')
                                    }));
                                }
                            }
                        });
                        callback(tasks);
                    } else {
                        callback([]);
                    }
                }
            }
        });
    },

    /**
     * Создать документ и привязать его к пользователю
     * @param user {string} идентификатор пользователя
     * @param links {string} идентификаторы задач
     * @param callback {()=>void} функция обратного вызова
     */
    createDocument: function (user, links, callback) {
        PN.Domain.DD_Doc_Details.SetUserForDocDetails(
            user,
            links,
            function (records, options, success) {
                if (typeof callback == 'function')
                    callback(options.meta.success, options.meta.msg);
            }
        );
    },

    /*
     * возвращается типы документов
     * @param callback {()=>void} функция обратного вызова 
     */
    getDocumentsType: function (callback) {
        Ext.create('ARM.store.ActType').load({
            params: {
                filter: [
                    {
                        property: "F_Parents",
                        operator: "isnull"
                    }
                ],
                limit: this.MAX_COUNT
            },
            callback: function (items, operation, success) {
                if (typeof callback === 'function') {
                    callback(items);
                }
            }
        });
    },

    privates: {
        /*
         * для проверки что точка сместилать (чтобы не захламлять карту)
         */
        isPointChange: function (N_Latitude, N_Longitude, item) {
            var k = this.POINT_CORRECT;

            return (item.get('N_Latitude') - k <= N_Latitude && N_Latitude <= item.get('N_Latitude') + k) &&
                    (item.get('N_Longitude') - k <= N_Longitude && N_Longitude <= item.get('N_Longitude') + k);
        }
    }
});