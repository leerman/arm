﻿/**
 * компонент для работы с сокет соединениями
 */
Ext.define('ARM.Socket', {
    alternateClassName: 'Socket',

    registry: false,
    setRegistry: function (val) {
        this.registry = val;
    },

    getRegistry: function () {
        return this.registry;
    },

    socket: null,
    setSocket: function (val) {
        this.socket = val;
    },

    getSocket: function () {
        return this.socket;
    },

    WS_URL: '',

    callbacker: {},

    /*
     * используется ли сокет соединение 
     */
    isUseSocket: function () {
        return Ext.getConf('ws_url') != '';
    },

    /*
     * создание подключение к серверу
     */
    start: function () {
        var me = this;
        if (this.isUseSocket() == true) {
            this.WS_URL = Ext.getConf('ws_url');
            this.loadSocketJs(function (success) {
                if (success == false) {
                    // сервер не доступен
                    me.onAppEvent('unavailable');
                }
            });
        }
    },

    /**
     * отправка сообщения на сервер
     * @param eventName {string} имя события
     * @param data {any} объект для передачи
     */
    send: function (eventName, data) {
        var socket = this.getSocket();
        if (socket) {
            socket.json.send({
                result: data,
                eventName: eventName
            });
        }
    },

    /**
     * отправка сообщения на сервер
     * @param eventName {string} имя события
     * @param data {any} объект для передачи
     */
    sendNotif: function (uuid, data, tid) {
        var socket = this.getSocket();
        if (socket) {
            socket.json.send({
                result: data,
                eventName: 'notif',
                uuid: uuid,
                tid: tid
            });
        }
    },

    /**
     * отправка сообщения на сервер
     * @param eventName {string} имя события
     * @param data {any} объект для передачи
     */
    sendChat: function (uuid, data, callback) {
        var socket = this.getSocket();
        var tid = Core.Utilits.GetGUID();
        if (socket) {
            socket.json.send({
                result: data,
                eventName: 'chat',
                uuid: uuid,
                tid: tid
            });
        }

        this.callbacker[tid] = { callback: callback, options: data };
        return tid;
    },

    /**
     * отправка уведомления клиенту
     * @param uuid {string} идентификатор устройства
     * @param data {any} объект для передачи
     * @param callback {()=>void} функция обратного вызова
     */
    sendSignal: function (uuid, data, callback) {
        var socket = this.getSocket();
        var tid = Core.Utilits.GetGUID();
        if (socket) {
            socket.json.send({
                result: data,
                eventName: 'signal',
                uuid: uuid,
                tid: tid
            });
        }

        if (callback)
            this.callbacker[tid] = { callback: callback, options: data };

        return tid;
    },

    /**
     *обработчик получения данных от сервера
     * @param obj {any} объект от сервера 
     */
    onEvent: function (obj) {
        if (obj) {
            return obj;
        }
        return null;
    },

    /*
     * подключение открыто
     */
    onConnect: function () {
        this.onAppEvent('snConnect', null);
    },

    /*
     * обработчик регистрации пользователя
     */
    onRegistry: function () {
        var me = this;
        getAppInfo(function (info) {
            me.send('registry', {
                uuid: me.getUUID(),
                info: info
            });
        });
    },

    /*
     * обработчик получения сообщения (чат)
     */
    onChat: function (eventResult) {
        var tid = eventResult.result.tid;
        this.onAppEvent('snChat', eventResult.result.result, tid);
    },

    /*
     * обработчик получения сигнала
     */
    onSignal: function (eventResult) {
        var tid = eventResult.result.tid;
        this.onAppEvent('snSignal', eventResult.result.result, tid);
        // уведомляем сервер, что сообщение было успешно получено

        var uuid = '';
        try{
            uuid = eventResult.result.result.server;
        } catch (exc) {

        }
        // без этого будет постоянно слать сообщения
        this.sendNotif(uuid, 'SUCCESS', tid);
    },

    /*
     * обработчик уведомления о подключении нового пользователя
     */
    onStatistic: function (eventResult) {
        this.onAppEvent('snStatistic', eventResult.result.result);
    },

    /**
     * принято сообщение
     */
    onNotif: function (eventResult) {
        var tid = eventResult.tid;
        var callback = this.callbacker[tid];
        if (callback) {
            callback.callback(eventResult.result, callback.options);
            if (eventResult.result == 'SUCCESS') {
                delete this.callbacker[tid];
            }
        }
        this.onAppEvent('snNotif', eventResult);
    },

    /*
     * отправка уведомления пользователю
     * @param uuid {string} идентификатор устройства
     * @param type {string} тип уведомления 
     * @param message {string} текст сообщения
     * @param data {any} дополнительные данные
     * @param callback {()=>void} функция обраного вызова
     */
    notifUser: function (uuid, type, message, data, callback) {
        var app = Ext.getCurrentApp();
        if (this.isUseSocket() == true) {
            var currentUser = app.getUser();
            var tid = Core.Utilits.GetGUID();
            this.sendSignal(uuid, {
                id: tid,
                type: type,
                data: data,
                message: message,
                date: Date.now(),
                isShow: false,
                server: currentUser.getId()
            }, callback || function () { });
        }
    },

    privates: {
        getVirtualDirPath: function () {
            return Ext.getConf('virtualDirPath');
        },

        onAppEvent: function (eventName, data, tid) {
            var app = Ext.getCurrentApp();
            app.fireEvent(eventName, this, data, tid);
        },

        getUUID: function () {
            return Ext.getCurrentApp().getUser().getId();
        },

        onSuccess: function () {
            var me = this;
            var socket = io.connect(this.WS_URL, {
                path: this.getVirtualDirPath() + '/socket.io', 
                transports: Ext.getConf('transports') || ['polling']
            });
            this.setSocket(socket);

            socket.on('connect', function () {
                me.connection(socket);
            });

            // При отключении клиента - уведомляем остальных
            socket.on('disconnect', function (msg) {
                console.info('сокет закрыт');
                me.onAppEvent('snDisconnect', null);
            });

            socket.on('message', function (obj) {
                var eventResult = me.onEvent(obj);
                switch (eventResult.eventName) {
                    case 'registreted':
                        me.setRegistry(obj.result == 'SUCCESS');
                        console.info('Пользователь зарегистрирован успешно');
                        me.onAppEvent('snRegistreted', null);
                        break;

                    case 'notif':
                        me.onNotif(eventResult.result);
                        break;

                    case 'chat':
                        me.onChat(eventResult);
                        break;

                    case 'signal':
                        me.onSignal(eventResult);
                        break;

                    case 'statistic':
                        me.onStatistic(eventResult);
                        break;
                }
            });
        },

        /*
         * загрузка библиотеки сокета 
         */
        loadSocketJs: function (callback) {
            var me = this;

            Ext.Loader.loadScript({
                url: this.WS_URL + this.getVirtualDirPath() + '/socket.io/socket.io.js',
                onLoad: function () {
                    console.info('библиотека для обработки websocket загружена');
                    me.onSuccess();

                    if (typeof callback == 'function')
                        callback(true);
                },
                onError: function (err) {
                    delete Ext.Boot.scripts[err.url];

                    if (typeof callback == 'function')
                        callback(false);
                }
            });
        },

        connection: function (socket) {
            var me = this;
            console.info('сокет открыт');
            me.onConnect();

            me.onRegistry();
        }
    }
});