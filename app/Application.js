﻿/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('ARM.Application', {
    extend: 'Ext.app.Application',

    name: 'ARM',

    defaultToken: 'home',

    /*
     * имя страницы которая выводится
     */
    pageName: '',

    /*
     * подстраница
     */
    subPageName: '',

    /*
     * дополнительные параметры роутинга
     */
    routeOptions: null,

    routes: {
        'home': 'onHome',
        'home/:type/:uuid': 'onHomeFull',
        'page/:itemId': 'onPage',
        'history': 'onHistory',
        'documents': 'onDocuments',
        'settings': 'onSettings',
        'admin': 'onAdmin',
        'admin/:type/:uuid': 'onAdminItem',
        'page/task/:userId/:docLink': 'onDocument',
        'stat': 'onStat',
        'report': 'onReport',
        'view/:id': 'onViewPage',
        'access': 'onAccess',
        'service': 'onService'
    },

    stores: [
        'ARM.store.Ping',
        'ARM.store.UserInfo'
    ],

    chiter: null,
    setChiter: function(val) {
        this.chiter = val;
    },
    getChiter: function() {
        return this.chiter;
    },

    /*
     * обект для создания сокет соединения
     */
    socket: null,
    /*
     * провайдер данных
     */
    dataProvider: null,

    initView: null, // главное представление

    user: null, // данные о текущем пользователе

    authProvider: null,
    configuration: null,


    configs: null,

    getConfigs: function() {
        return this.configs;
    },

    setConfigs: function(value) {
        this.configs = value;
    },

    /*
     * глобальные настройки
     */
    globalParams: null,

    getGlobalParams: function() {
        return this.globalParams;
    },

    setGlobalParams: function(value) {
        this.globalParams = value;
    },

    /*
     * чат
     */
    chat: null,
    setChat: function(value) {
        this.chat = value;
    },
    getChat: function() {
        return this.chat;
    },

    /*
     * безопасность
     */
    security: null,
    getSecurity: function() {
        return this.security;
    },
    setSecurity: function(value) {
        this.security = value;
    },

    listeners: {
        // обработчик того что пользователь авторизовался
        authorize: 'onAuthorize'
    },

    launch: function() {

        Ext.create('Core.DoubleClickByMask').initHackDoubleClickByMask();

        window['appVersion'] = Ext.manifest.version;

        var me = this;

        this.onReady(function(name) {
            window['appTheme'] = me.getConfiguration().get('reference');
            me.destroySplashScreen();
            // использовать приложение только если есть поддержка indexedDb
            var indexedDbValid = me.indexedDbValid();
            if (indexedDbValid == true) {
                if (me.getAuthProvider().isAuthorize() == true) {
                    var el = Ext.create({
                        xtype: name,
                        pageName: me.getPageName(),
                        subPageName: me.getSubPageName()
                    });
                    me.setInitView(el);

                    var placeholder = el.down('app-placeholder');
                    if (placeholder)
                        placeholder.onRouteOptions(me.getRouteOptions());

                    me.fireEvent('authorize');
                } else {
                    // здесь не авторизован
                    Ext.create({
                        xtype: name
                    });
                }
            } else {
                Ext.Msg.alert('Ваш браузер не поддерживат стабильную версию IndexedDB. Требуется обновить браузер.');
            }
        });
    },
    /*
     * возвращается текущее сокет соединение
     */
    getSocket: function() {
        return this.socket
    },
    /*
     * устанавливается текущее сокет соединение
     * @param value {any} сокет соединение
     */
    setSocket: function(value) {
        this.socket = value
    },
    /**
     * устанавливает главное представление
     * @param value {any} представление
     */
    setInitView: function(value) {
        this.initView = value;
    },

    /*
     * возвращается главное представление
     */
    getInitView: function() {
        return this.initView;
    },

    /*
     * устанавливается пользователь 
     */
    setUser: function(value) {
        this.user = value;
    },

    /*
     * возвращается пользователь
     */
    getUser: function() {
        return this.user;
    },

    onAppUpdate: function() {
        Ext.Msg.confirm('Обновление приложения', 'Доступна новая версия приложения. Обновить?',
            function(choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    },

    /**
     * провайдер авторизации
     */
    getAuthProvider: function() {
        if (!this.authProvider) {
            this.authProvider = Ext.create('Core.AuthProvider', {
                REMOTING_ADDRESS: this.getConfiguration().get('REMOTING_ADDRESS')
            });
        }
        return this.authProvider;
    },

    /**
     * провайдер для чтения настроек
     */
    getConfiguration: function() {
        if (!this.configuration) {
            this.configuration = Ext.create('Core.Configuration', {
                url: 'configs'
            });
        }
        return this.configuration;
    },

    /*
     * возвращается провайдер данных
     */
    getDataProvider: function() {
        if (!this.dataProvider) {
            this.dataProvider = Ext.create('ARM.dataProvider.DefaultDataProvider');
        }

        return this.dataProvider;
    },

    /*
     * преобразовать в обсалютный путь
     * @param path {string} путь
     */
    toAbsolutePath: function(path) {
        return path; // Ext.getConf('') path;
    },

    /*
     * выводит всплывающую подсказку
     * @param title {string} заголовок 
     * @param txt {string} тексть сообщения 
     */
    showTooltip: function(title, txt) {
        var t = new Ext.ToolTip({
            title: title,
            html: txt,
            minWidth: 200,
            closable: true
        });
        t.showAt([10, 10]);
        setInterval(function() {
            t.destroy();
        }, 5000);
    },

    //#region privates
    privates: {
        /*
         * устанавливаем имя страницы
         * @param name {string} имя страницы
         */
        setPageName: function(name) {
            this.pageName = name;
        },
        /*
         * возвращается текущее имя страницы
         */
        getPageName: function() {
            return this.pageName;
        },
        /*
         * устанавливаем имя подстраницы
         * @param name {string} имя подстраницы
         */
        setSubPageName: function(name) {
            this.subPageName = name;
        },
        /*
         * возвращается текущее имя подстраницы
         */
        getSubPageName: function() {
            return this.subPageName;
        },
        /**
         * устанавливаем информацию о пользователе
         */
        setUserInfo: function(callback) {
            var me = this;
            if (this.getAuthProvider().isAuthorize() == true) {
                Ext.create('ARM.store.UserInfo').load({
                    callback: function(items) {
                        if (items.length > 0) {
                            var item = items[0];

                            var user = me.getUser();
                            if (user != null)
                                user.destroy();

                            me.setUser(Ext.create('Core.User', {
                                data: item.getData()
                            }));
                            var user = me.getUser();
                            me.getInitView().lookupReference('app-mainmenu').setUserInfo(user.getUserName(), user.getRoles());
                            if (typeof callback == 'function')
                                callback();
                        }
                    }
                })
            }
        },
        /**
         * проверка на поддержку indexeddb
         */
        indexedDbValid: function() {
            window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
            if (!window.indexedDB)
                return false;
            else
                return true;
        },
        /**
         * приложение готово
         * @param callback {()=>void} функция обратного вызова
         */
        onReady: function(callback) {
            var me = this;
            this.loadSystemData(function() {
                console.info('загрузка системных конфигов');
                // обязательно вызывать после чтения конфигов
                var authorization = me.getAuthProvider();
                if (authorization.isAuthorize() == true)
                    me.setAuthrizeHeader();
                me.onLoadMetaData(function() {
                    var name = '';
                    if (authorization.isAuthorize() == true) {
                        name = 'app-main';
                        me.afterAuthLoadData(function() {
                            callback(name);
                        });
                    } else {
                        name = 'app-login';
                        callback(name);
                    }
                });
            });
        },
        /*
         * загрузка системных данных для работы приложения
         */
        loadSystemData: function(callback) {
            var me = this;
            _.series([
                // загрузка конфигов
                function(callback) {
                    me.getConfiguration().read(callback);
                },
                function(callback) {
                    me.setChiter(Ext.create('Core.Chiter', {}));
                    callback();
                }
            ], callback);
        },

        loadSecurity: function(data) {
            var me = this;
            var security = Ext.create('Core.security.Reader');
            me.setSecurity(security);
            security.setSecurity(data);
        },
        /*
         * требуется загружать мета-описание
         */
        isLoadMeta: function() {
            var authorization = this.getAuthProvider();
            var ns = this.getConfiguration().get('REMOTE_NAMESPACE');
            return authorization.isAuthorize() == true && (authorization.isAuthorize() == true && !window[ns]);
        },

        /*
         * добавляем в заголовки дополнительный атрибут для авторизации
         */
        setAuthrizeHeader: function() {
            var authorization = this.getAuthProvider();
            authorization.setAuthrizeHeader(authorization.getToken());
        },

        /**
        Загрузка мета -данных
        @param callback {()=>void} функция обратого вызова
        */
        onLoadMetaData: function(callback) {
            var me = this;
            if (this.isLoadMeta()) {
                Core.Meta.loadMetaData(this.getConfiguration(), function(status) {
                    switch (status) {
                        case 401:
                            me.onLogin();
                            break;

                        case 200:
                            console.info('метаданные загружены');
                            return callback();
                            break;
                    }
                });
            } else {
                callback();
            }
        },

        /*
         * проброс токена в запросы
         */
        overrideBootFetch: function() {
            var authorization = this.getAuthProvider();
            var interceptStatusCode = Ext.create('Core.InterceptStatusCode', {
            });

            if (authorization.isAuthorize() == true) {
                // пробрасываем токен в url запросы
                interceptStatusCode.overrideBootFetch(authorization.getToken());
                interceptStatusCode.overrideBootEntry(authorization.getToken());
            } else {
                interceptStatusCode.overrideBootFetch();
            }
        },

        /*
         * инициализация перехватчика статуса ответа
         */
        initInterceptStatusCode: function() {
            // может нужно избавиться от этого метода
            var me = this;
            this.overrideBootFetch();
            Ext.getBody().on('requiredauth', function(result, viewName) {
                me.onLogin();
            }, this);

            // перехват 500 ошибки
            Ext.getBody().on('servererror', function(result) {}, this);
        },

        /*
         * обработка после авторизации пользователя
         * здесь создаются обработчики ошибок и логирование, т.к. нужна авторизация
         */
        afterAuthLoadData: function(callback) {
            var me = this;
            _.series([
                function(callback) {
                    me.onLoadMetaData(callback);
                },
                function(callback) {
                    var dataProvider = me.getDataProvider();
                    dataProvider.getConfigs(function(config) {
                        me.setConfigs(config);
                        callback();
                    });
                },
                function(callback) {
                    var dataProvider = me.getDataProvider();
                    dataProvider.getGlobalParams(function(config) {
                        me.setGlobalParams(config);
                        callback();
                    });
                },
                function(callback) {
                    me.initInterceptStatusCode();
                    callback();
                }
            ], callback);
        },

        /*
         * удаление splashscreen
         */
        destroySplashScreen: function() {
            Ext.get('loading').destroy();
        },

        /**
         * обработчик того что пользователь авторизовался
         */
        onAuthorize: function() {
            // авторизация пользователя выполнена, идет подготовка данных
            // здесь может быть переавторизация
            var me = this;
            Ext.create('Core.DynamicMask', {
                title: 'Ожидание...',
                sync: true,
                functions: [
                    {
                        msg: 'Добавление информации о пользователе',
                        action: function(callback) {
                            me.setUserInfo(callback);
                        },
                    }, {
                        msg: 'Обновление списока пунктов главного меню',
                        action: function(callback) {
                            var menu = me.getInitView().lookupReference('app-mainmenu');
                            if (menu.menuItems.length == 0) // здесь может быть переавторизация
                                menu.loadMenuItems();

                            callback();
                        }
                    }, {
                        msg: 'Проверка доступа к пунктам меню',
                        action: function(callback) {
                            var menu = me.getInitView().lookupReference('app-mainmenu');
                            var data = menu.getSecurityItems();
                            if (Array.isArray(data) && data.length > 0) {
                                me.loadSecurity(data);
                            }
                            callback();
                        }
                    }
                ],
                callback: function() {
                    // создаем подключение к сокету
                    var socket = Ext.create('ARM.Socket');
                    me.setSocket(socket);
                    socket.start();
                    var win = Ext.create('ARM.view.main.Chat', {});
                    me.setChat(win);

                    me.fireEvent('authorized', me);
                    me.fireEvent('security', me);
                }
            });
        },

        /**
         * вывод формы для авторизации
         */
        onLogin: function() {
            Ext.create({
                xtype: 'app-login'
            });
        },

        /*
         * доступность
         */
        onAccess: function() {
            var result = this.onPage('access');
            return result;
        },

        /*
         * отчеты
         */
        onReport: function() {
            var result = this.onPage('report');
            return result;
        },

        /*
         * статистика
         */
        onStat: function() {
            var result = this.onPage('stat');
            return result;
        },

        /*
         * перейти к документу
         * @param userId {string} идентификатор пользователя
         * @param docLink {string} идентификатор документа
         */
        onDocument: function(userId, docLink) {
            var result = this.onPage('task');
            // устанавливаем дополнительные параметры при роутинге
            this.setRouteOptions({
                userId: userId,
                docLink: docLink
            });

            if (result && result.onRouteOptions) // прокидываем параметры в представление
                result.onRouteOptions(this.getRouteOptions());
        },

        /*
         * настройки
         */
        onAdmin: function() {
            var result = this.onPage('admin');
            return result;
        },

        /*
         * настройки
         */
        onSettings: function() {
            var result = this.onPage('settings');
            return result;
        },

        /*
         * обработчик перехода на страницу История маршрутов
         */
        onHistory: function() {
            var result = this.onPage('history');
            return result;
        },

        /*
         * обработчик перехода на страницу Документы
         */
        onDocuments: function() {
            var result = this.onPage('documents');
            return result;
        },

        onHome: function() {
            var result = this.onPage('home');

            return result;
        },

        /*
         * обработчик перехода на страницу Служебное
         */
        onService: function() {
            var result = this.onPage('service');

            return result;
        },

        /*
         * обработчик для отображения конкретного пользователя на карте
         * @param type {string} тип отображения (центровки)
         * @param uuid {string} идентификатор устройства 
         */
        onHomeFull: function(type, uuid) {
            var result = this.onHome();
            // устанавливаем дополнительные параметры при роутинге
            this.setRouteOptions({
                type: type,
                uuid: uuid
            });

            if (result && result.onRouteOptions) // прокидываем параметры в представление
                result.onRouteOptions(this.getRouteOptions());
        },

        /*
         * страница администрирования
         * @param type {string} тип страницы
         * @param id {string} идентификатор
         */
        onAdminItem: function(type, id) {
            var result = this.onPage(type);
            // устанавливаем дополнительные параметры при роутинге
            this.setRouteOptions({
                type: type,
                id: id
            });

            if (result && result.onRouteOptions) // прокидываем параметры в представление
                result.onRouteOptions(this.getRouteOptions());
        },

        /*
         * для вывода шаблонных страниц
         * @param id {string} имя страницы
         */
        onViewPage: function(id) {
            return this._onPage('view', id);
        },

        onPage: function(itemId) {
            return this._onPage(itemId);
        },

        _onPage: function(itemId, subType) {
            // чтобы сбросить данные о роутинге
            if (this.getSecurity() && this.getSecurity().isAccess() == false) {
                return location.href = '#access';
            }

            var route = null;
            if (itemId == 'home') {
                route = {
                    type: itemId
                };
            }

            this.setRouteOptions(route);

            this.setPageName(itemId);
            this.setSubPageName(subType);
            var initView = this.getInitView();
            if (initView) {
                var rightPanel = initView.lookupReference('rightpanel');
                if (rightPanel) {
                    rightPanel.hidePanel();
                }

                var menu = initView.lookupReference('app-mainmenu');
                if (menu) {
                    menu.changeActiveMark(itemId);
                }

                var placeHolder = initView.lookupReference('placeHolder');
                placeHolder.removeAll(true);
                if (subType) {
                    return placeHolder.add({
                        xtype: 'app-' + itemId,
                        subType: subType
                    });
                } else {
                    return placeHolder.add({
                        xtype: 'app-' + itemId
                    });
                }
            }
        },

        /*
         * установить параметры роутинга
         * @param options {any} параметры
         */
        setRouteOptions: function(options) {
            this.routeOptions = options;
        },

        /*
         * возвращаются дополнительные параметры при роутинге
         */
        getRouteOptions: function() {
            return this.routeOptions;
        }
    }
//#endregion
});
