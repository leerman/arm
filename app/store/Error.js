﻿/*
 * 
 */
Ext.define('ARM.store.Error', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Error',

    sorters: [
        { property: 'S_Date', direction: 'DESC' }
    ],

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Error_Log.Query',
            create: 'PN.Domain.CS_Error_Log.Add',
            update: 'PN.Domain.CS_Error_Log.Update',
            destroy: 'PN.Domain.CS_Error_Log.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});