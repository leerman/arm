﻿/*
 * документ, машрут
 */
Ext.define('ARM.store.Document', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Document',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Docs.Query',
            create: 'PN.Domain.DD_Docs.Add',
            update: 'PN.Domain.DD_Docs.Update',
            destroy: 'PN.Domain.DD_Docs.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});