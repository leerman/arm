﻿/*
 * филиал
 */
Ext.define('ARM.store.MainDivision', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.MainDivision',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.SD_MainDivisions.Query',
            create: 'PN.Domain.SD_MainDivisions.Add',
            update: 'PN.Domain.SD_MainDivisions.Update',
            destroy: 'PN.Domain.SD_MainDivisions.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});