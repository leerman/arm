﻿Ext.define('ARM.store.DocsSetReceived', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Doc',
    autoLoad: false,
    proxy: {
        type: 'itdirect',
        directFn: 'PN.Domain.DD_Docs.SetReceived',
        reader: {
            type: 'json',
            rootProperty: 'records',
            successProperty: 'success',
        }
    }
});