﻿/*
 * пользователь
 */
Ext.define('ARM.store.Subscrs', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Subscr',

    autoLoad: false,

    groupField: '',
    sorters: [],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.SD_Subscrs.Query',
            create: 'PN.Domain.SD_Subscrs.Add',
            update: 'PN.Domain.SD_Subscrs.Update',
            destroy: 'PN.Domain.SD_Subscrs.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        },
        writer: {
            //writeAllFields : true
            dateFormat: "Y-m-d H:i:sO"
        }
    }
});