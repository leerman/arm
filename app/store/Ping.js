﻿Ext.define('ARM.store.Ping', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Ping',
    autoLoad: false,
    proxy: {
        type: 'itdirect',
        directFn: 'PN.Util.It.Module.Extjs.ExtjsDirectRpc.GetDateTime',
        reader: {
            type: 'json',
            rootProperty: 'records'
        }
    }
});