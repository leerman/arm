﻿/*
 * геопозиции
 */
Ext.define('ARM.store.GeoLocation', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GeoLocation',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CD_Geolocation.Query'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});