﻿/*
 * исходящий документ
 */
Ext.define('ARM.store.DocOut', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.DocOut',

    mixins: [
        'Core.data.Selectable'
    ],

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Docs_Out.Query',
            create: 'PN.Domain.DD_Docs_Out.Add',
            update: 'PN.Domain.DD_Docs_Out.Update',
            destroy: 'PN.Domain.DD_Docs_Out.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});