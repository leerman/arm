﻿Ext.define('ARM.store.Version', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Version',

    requires: [
        'ARM.view.version.Reader'
    ],

    proxy: {
        type: 'ajax',
        url: 'update.md',
        reader: {
            type: 'md_reader'
        }
    }
});
