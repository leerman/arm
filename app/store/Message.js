﻿Ext.define('ARM.store.Message', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Message',

    proxy: {
        type: 'localstorage',
        id: 'chat-messages'
    },
    load: function (options) {
        //if (this.isLoadBlocked()) {
        //    return;
        //}
        var me = this,
            operation;
        me.setLoadOptions(options);
        if (me.getRemoteSort() && options.sorters) {
            me.fireEvent('beforesort', me, options.sorters);
        }
        operation = Ext.apply({
            internalScope: me,
            internalCallback: me.onProxyLoad,
            scope: me
        }, options);
        operation.sorters = me.sorters;

        me.lastOptions = operation;
        operation = me.createOperation('read', operation);
        if (me.fireEvent('beforeload', me, operation) !== false) {
            me.onBeforeLoad(operation);
            me.loading = true;
            me.clearLoadTask();
            operation.execute();
        }
        return me;
    },
    getTotalCount: function () {
        var j = 0;
        for (var i in this.proxy.cache)
            j++;
        return j;
    }
})