﻿/*
 * Участок
 */
Ext.define('ARM.store.Brigade', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Brigade',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Brigade.Query',
            create: 'PN.Domain.CS_Brigade.Add',
            update: 'PN.Domain.CS_Brigade.Update',
            destroy: 'PN.Domain.CS_Brigade.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});