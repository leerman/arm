﻿//PN.Domain.DD_Doc_Details.GetCommonStatisticsQuery
/*
 * статистика (общая)
 */
Ext.define('ARM.store.GetCommonStatisticsQuery', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GetCommonStatisticsQuery',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Doc_Details.GetCommonStatisticsQuery'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});