﻿/*
 * статистика по документам
 */
Ext.define('ARM.store.GetDocStatistics', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GetDocStatistics',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Users.GetDocStatistics'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});