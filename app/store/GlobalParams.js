﻿
Ext.define('ARM.store.GlobalParams', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GlobalParams',

    groupField: '',
    sorters: [],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Global_Params.Query',
            create: 'PN.Domain.CS_Global_Params.Add',
            update: 'PN.Domain.CS_Global_Params.Update',
            destroy: 'PN.Domain.CS_Global_Params.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        },
        writer: {
            //writeAllFields : true
            dateFormat: "Y-m-d H:i:sO"
        }
    }
});
