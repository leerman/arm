﻿


Ext.define('ARM.store.AppointByDocs', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Appoint',

    mixins: [
        'Core.data.Selectable'
    ],

    autoLoad: false,
    proxy: {
        type: 'itdirect',
        directFn: 'PN.Domain.DD_Doc_Details.GetActiveDocDetailsAddresses',
        reader: {
            type: 'json',
            rootProperty: 'records',
            successProperty: 'success',
        }
    }
});