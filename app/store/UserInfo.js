﻿/*
 * информация о текущем авторизованном пользователе
 */
Ext.define('ARM.store.UserInfo', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.UserInfo',
    autoLoad: false,
    proxy: {
        type: 'itdirect',
        directFn: 'Util.MobileService.Custom.MobileServiceSecurityRpc.GetCurrentUserInfo',
        reader: {
            type: 'json'
        }
    }
});