﻿
/*
* Store Тип документа
*/
Ext.define('ARM.store.ActType',
{
    /*BEGIN_GEN*/
    extend: 'Ext.data.Store',
    model: 'ARM.model.ActType',

    groupField: '',
    sorters: [],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DS_Types.Query'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        },
        writer: {
            //writeAllFields : true
            dateFormat: "Y-m-d H:i:sO"
        }
    }
    /*END_GEN*/
});

