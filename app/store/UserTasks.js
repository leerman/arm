﻿/*
 * задание по пользователю
 */
Ext.define('ARM.store.UserTasks', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Task',
    autoLoad: false,
    proxy: {
        type: 'itdirect',
        directFn: 'PN.Domain.DD_Doc_Details.QueryInfo',
        reader: {
            type: 'json',
            successProperty: 'success',
            rootProperty: 'records'
        }
    }
});