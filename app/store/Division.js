﻿/*
 * филиал
 */
Ext.define('ARM.store.Division', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Division',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.SD_Divisions.Query',
            create: 'PN.Domain.SD_Divisions.Add',
            update: 'PN.Domain.SD_Divisions.Update',
            destroy: 'PN.Domain.SD_Divisions.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});