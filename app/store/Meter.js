﻿/*
 * потребление
 */
Ext.define('ARM.store.Meter', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Meter',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.ED_Meter_Readings.Query',
            create: 'PN.Domain.ED_Meter_Readings.Add',
            update: 'PN.Domain.ED_Meter_Readings.Update',
            destroy: 'PN.Domain.ED_Meter_Readings.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});