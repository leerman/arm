﻿/*
 * статистика по обходам
 */
Ext.define('ARM.store.StatCard', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.StatCard',

    autoLoad: false,

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});