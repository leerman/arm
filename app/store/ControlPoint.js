﻿Ext.define('ARM.store.ControlPoint', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Task',
    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        directFn: 'PN.Domain.DD_Doc_Details.QueryInfo',
        reader: {
            type: 'json',
            rootProperty: 'records'
        }
    }
});