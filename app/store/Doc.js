﻿
Ext.define('ARM.store.Doc', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Doc',

    sorters: [
        { property: 'S_Create_Date', direction: 'DESC' }
    ],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,
    mixins: [
        'Core.data.Selectable'
    ],

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Docs.Query',
            create: 'PN.Domain.DD_Docs.Add',
            update: 'PN.Domain.DD_Docs.Update',
            destroy: 'PN.Domain.DD_Docs.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        },
        writer: {
            //writeAllFields : true
            dateFormat: "Y-m-d H:i:sO"
        }
    }
});
