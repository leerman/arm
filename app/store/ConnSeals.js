﻿/*
 * Пломбы
 */
Ext.define('ARM.store.ConnSeals', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.ConnSeals',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.ED_Conn_Seals.Query',
            create: 'PN.Domain.ED_Conn_Seals.Add',
            update: 'PN.Domain.ED_Conn_Seals.Update',
            destroy: 'PN.Domain.ED_Conn_Seals.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});