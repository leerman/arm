﻿/*
 * пользователь
 */
Ext.define('ARM.store.User', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.User',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            // тут просто заменили CS_Users на CV_Users
            read: 'PN.Domain.CV_Users.Query'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});