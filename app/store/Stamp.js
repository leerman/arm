﻿/*
* Пломба
*/
Ext.define('ARM.store.Stamp', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Stamp',

    autoLoad: false,

    mixins: [
        'Core.data.Selectable'
    ],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.ED_Seals.Query'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records'
        }
    }
});