﻿
Ext.define('ARM.store.Configs', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Configs',

    groupField: '',
    sorters: [],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Configs.Query'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});