﻿/*
 * геопозиции пользователей
 */
Ext.define('ARM.store.GetGeoLocationsToday', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GeoLocation',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Users.GetGeoLocationsToday'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});