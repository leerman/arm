﻿/*
 * филиал
 */
Ext.define('ARM.store.RES', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.RES',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.ED_Networks.Query',
            create: 'PN.Domain.ED_Networks.Add',
            update: 'PN.Domain.ED_Networks.Update',
            destroy: 'PN.Domain.ED_Networks.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});