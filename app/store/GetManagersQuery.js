﻿/*
 * пользователь (только обходчики)
 */
Ext.define('ARM.store.GetManagersQuery', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.User',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            // тут просто заменили CS_Users на CV_Users
            read: 'PN.Domain.CV_Users.GetManagersQuery'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});