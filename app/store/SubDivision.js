﻿/*
 * участок
 */
Ext.define('ARM.store.SubDivision', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.SubDivision',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.SD_Subdivisions.Query',
            create: 'PN.Domain.SD_Subdivisions.Add',
            update: 'PN.Domain.SD_Subdivisions.Update',
            destroy: 'PN.Domain.SD_Subdivisions.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});