﻿/*
 * строка документа (задание, прибор)
 */
Ext.define('ARM.store.Detail', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Detail',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Doc_Details.Query',
            create: 'PN.Domain.DD_Doc_Details.Add',
            update: 'PN.Domain.DD_Doc_Details.Update',
            destroy: 'PN.Domain.DD_Doc_Details.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});