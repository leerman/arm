﻿/*
 * список дат с геоданными
 */
Ext.define('ARM.store.GetGeoLocationDays', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GetGeoLocationDays',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Users.GetGeoLocationDays'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});