﻿/*
 * области, куда привязаны обходчики
 */
Ext.define('ARM.store.Regions', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Region',
    autoLoad: false,
    proxy: {
        type: 'itdirect',
        reader: {
            type: 'json',
            successProperty: 'success',
            rootProperty: 'records'
        },
        api: {
            read: 'PN.Domain.CS_Brigade.Query',
            create: 'PN.Domain.CS_Brigade.Add',
            update: 'PN.Domain.CS_Brigade.Update',
            destroy: 'PN.Domain.CS_Brigade.Delete'
        }
    }
});