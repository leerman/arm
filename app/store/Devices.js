/*
* Store 
*/
Ext.define('ARM.store.Devices', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Devices',

    groupField: '',
    sorters: [],
    mixins: [
        'Core.data.Selectable'
    ],

    proxy: {
        type: 'itdirect',
        api: {
            read:	    'PN.Domain.ED_Devices.Query',
            create:	    'PN.Domain.ED_Devices.Add',
	        update:     'PN.Domain.ED_Devices.Update',
            destroy:    'PN.Domain.ED_Devices.Delete'
        },
	    reader: {
                successProperty: 'success',
	            rootProperty: 'records',
            } ,
	    writer : {
            dateFormat: "Y-m-d H:i:sO"
	    }
    }
});

