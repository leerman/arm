﻿/*
 * пользователь
 */
Ext.define('ARM.store.EditUser', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.User',

    autoLoad: false,

    mixins: [
        'Core.data.Selectable'
    ],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            // тут просто заменили CS_Users на CV_Users
            read: 'PN.Domain.CS_Users.Query',
            create: 'PN.Domain.CS_Users.Add',
            update: 'PN.Domain.CS_Users.Update',
            destroy: 'PN.Domain.CS_Users.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});