﻿/*
 * статистика
 */
Ext.define('ARM.store.GetStatistics', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.GetStatistics',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.CS_Users.GetStatistics'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});