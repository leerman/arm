﻿/*
* Бланки
*/
Ext.define('ARM.store.Blank', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.Blank',

    autoLoad: false,

    mixins: [
        'Core.data.Selectable'
    ],

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.ED_Blanks.Query'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records'
        }
    }
});