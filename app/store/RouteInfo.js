﻿/*
 * Маршруты
 */
Ext.define('ARM.store.RouteInfo', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.RouteInfo',

    autoLoad: false,

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});