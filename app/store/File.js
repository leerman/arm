﻿/*
 * файлы
 */
Ext.define('ARM.store.File', {
    extend: 'Ext.data.Store',
    model: 'ARM.model.File',

    autoLoad: false,

    remoteFilter: true,
    remoteSort: true,
    remoteGroup: true,

    proxy: {
        type: 'itdirect',
        api: {
            read: 'PN.Domain.DD_Files.Query',
            create: 'PN.Domain.DD_Files.Add',
            update: 'PN.Domain.DD_Files.Update',
            destroy: 'PN.Domain.DD_Files.Delete'
        },
        reader: {
            successProperty: 'success',
            rootProperty: 'records',
        }
    }
});