﻿var Harness = Siesta.Harness.Browser.ExtJS;

Harness.configure({
    title: 'Awesome Test Suite',
    preload: [
        '../build/testing/ARM/resources/ARM-all.css',
        '../ext/build/ext-all-debug.js',
        'preload.js'
    ]
});

Harness.start(
    {
        group: 'Core',
        items: [
            'test/core/configuration.t.js',
            'test/core/authprovider.t.js',
            'test/core/meta.t.js'
        ]
    },
    'test/test.t.js'
);