﻿Ext.Loader.setPath('ARM', '../app');
Ext.Loader.setPath('Core', '../app/core');

function DirectOnReady(callback, configPath) {
    new It.Core.Configuration.AppSettings(function () {

        Ext.IServ.setDirectProvider(new It.Core.Configuration.AppSettings(), function () {
            var appSettings = new It.Core.Configuration.AppSettings();
            Ext.Loader.setPath(appSettings.Get('REMOTE_NAMESPACE'), Ext.String.format(appSettings.Get('REMOTE_DATA_URL'), appSettings.Get('REMOTING_ADDRESS')));
            if (callback)
                callback();
        });

    }, configPath || It.Core.Configuration.ReaderType.JSONCONFIG);
}

function setIntervalByLimit(funcionReturnBooleanType, callback, iterationLimit, timeInterval) {
    timeInterval = timeInterval || 500;
    iterationLimit = iterationLimit || 15;

    var iteration = 0;
    var interval = setInterval(function () {
        if (iteration == iterationLimit) {
            clearInterval(interval);

            if (callback)
                callback({
                    success: false,
                    msg: 'Не удалось дождать завершения из-за таймаута'
                })
        } else {
            iteration++;
            if (funcionReturnBooleanType() === true) {
                clearInterval(interval);
                if (callback)
                    callback({
                        success: true
                    });
            }
        }
    }, timeInterval);
}

/*
 * Cоздание массива из объекта
 */
function createArrayByObject(obj) {
    var types = [];
    for (var key in obj) {
        if (isObject(obj[key]))
            var child = createArrayByObject(obj[key]);
        else if (isArray(obj[key]) && obj[key].length)
            var child = createArrayByObject(obj[key][0]);

        child
            ? types.push({ propertyName: key, type: typeof obj[key], child: child || null })
            : types.push({ propertyName: key, type: typeof obj[key] })
    }
    return types;
}

/*
 * Является ли передаваемый объект - объектом
 */
function isObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
};

/*
 * Является ли передаваемый объект - массивом
 */
function isArray(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
};

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


/*
 * Сравнение объектов по структуре.
 * Проверяются наличия свойств и их типы 
 */
function CompareObjectByStructure(obj1, obj2) {

    var types1 = createArrayByObject(obj1);
    var types2 = createArrayByObject(obj2);

    if (types1.length == types2.length) {

        for (var i = 0; i < types1.length; i++) {
            var item = types1[i];
            if (types2.filter(function (j) { return JSON.stringify(j) == JSON.stringify(item) }).length == 0) {
                return 'Не удалось найти сопоставление для свойства ' + item.propertyName + '(' + item.type + ') в объекта 2';
            }
        }

        return true;
    } else {
        return 'Количество свойств в объекте 1 (' + types1.length + ')' + ' не равно кол. свойств объекта 2 (' + types2.length + ')';
    }
}

/**
* настройки для тестирования
*/
function testSettings() {
    this.getConfigUrl = function () {
        return 'http://localhost:1841/configs';
    }
    this.REMOTING_ADDRESS = 'demo.it-serv.ru/mobileservicedev'
    return this;
}