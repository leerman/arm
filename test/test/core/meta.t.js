﻿StartTest(function (t) {
    var componentName = 'Core.Meta';
    t.diag(componentName);

    var meta;
    var provider;
    var configuration;

    t.chain(function (next) {
        Ext.require(componentName, function () {
            meta = Core.Meta;
            Ext.require('Core.AuthProvider', function () {
                provider = Ext.create('Core.AuthProvider', {
                    REMOTING_ADDRESS: new testSettings().REMOTING_ADDRESS
                });
                configuration = Ext.create('Core.Configuration', {
                    url: new testSettings().getConfigUrl()
                });
                configuration.read(next);
            });
        });
    },
    function (next) {
        provider.singIn('admin', 'admin0', true, function (result) {
            t.ok(result.success, 'Пользователь авторизован');
            if (result.success == true) {
                Core.Meta.loadMetaData(configuration, function (status) {
                    t.ok(status == 200, 'мета данные получены');
                    next();
                });
            }
        });
    },
    function () {
        provider.singOut();
        t.done();
    });
});