﻿StartTest(function (t) {
    var componentName = 'Core.AuthProvider';
    t.diag(componentName);

    var instanse;

    t.chain(function (next) {
        Ext.require(componentName, function () {
            instanse = Ext.create(componentName, {
                REMOTING_ADDRESS: new testSettings().REMOTING_ADDRESS
            });
            next();
        });
    },
    function (next) {
        t.ok(!instanse.isAuthorize(), 'пользователь не авторизован');
        instanse.singIn('admin', 'admin1', true, function (result) {
            t.ok(!result.success, result.msg);
            instanse.singIn('admin', 'admin0', true, function (result) {
                t.ok(result.success, 'пользователь авторизован');
                next();
            });
        });
    },
    function () {
        instanse.singOut();
        t.ok(!instanse.isAuthorize(), 'авторизация сброшена');
        t.done();
    });
});