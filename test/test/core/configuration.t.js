﻿StartTest(function (t) {
    var componentName = 'Core.Configuration';
    t.diag(componentName);

    var instanse;

    t.chain(function (next) {
        Ext.require(componentName, function () {
            instanse = Ext.create(componentName, {
                url: new testSettings().getConfigUrl()
            });
            instanse.read(next);
        });
    },
        function () {
            var config = instanse.getData()
            t.ok(config, 'настройки прочитаны');
            t.expect(config.mode).toBe('developer');
            debugger;
            t.expect(instanse.get('port')).toBe(config.port);
            
            instanse.set('port', 80);
            t.expect(instanse.get('port')).toBe(80);
            t.done();
        });
});